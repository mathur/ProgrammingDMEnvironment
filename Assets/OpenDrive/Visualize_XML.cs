﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using UnityEngine;

public class Visualize_XML : MonoBehaviour
{
    //create top-level OpenDRIVE object
    OpenDrive openDrive;

    //initialize some other variables
    List<Road> roadList;
    List<Junction> junctionList;
    List<Controller> controllerList;
    int junctionNum;
    int connectionNum;

    //set default road lane width
    float defaultRoadWidth = 3.75f;

    //create list of .flt files for tree options
    List<string> treeList = new List<string>() { "VegTreeFir01v13m.flt", "VegTreeFir01v7m.flt", "VegTree03a.flt", "VegTree02v9m.flt" };

    enum INTERSECTION_LANE { STRAIGHT = 0, LEFT = 1, RIGHT = 2};

    public void SetupOpenDRIVE()
    {
        //add the header to the OpenDRIVE object
        Header header = new Header();
        header.revMajor = 1;
        header.revMinor = 1;
        header.name = "";
        header.version = 1;
        header.date = System.DateTime.Now;
        header.north = 0;
        header.south = 0;
        header.east = 0;
        header.west = 0;
        header.originLat = 0;
        header.originLong = 0;
        header.originHdg = 0;

        openDrive.header = header;
    }

    public void Visualize(string fileName, RoadElement parentRoadElement)
    {
        //initialize necessary variables
        openDrive = new OpenDrive();
        roadList = new List<Road>();
        junctionList = new List<Junction>();
        controllerList = new List<Controller>();
        junctionNum = 1;

        SetupOpenDRIVE();

        if (parentRoadElement._descendents.Count == 0)
        {
            if (parentRoadElement is RoadElement)
            {
                //here I will assume that any Abstract Road Elements will simply not be visualized
                if (parentRoadElement is RoadSegment)
                {
                    MakeRoadSegment((RoadSegment)parentRoadElement);
                }
                else if (parentRoadElement is CrossIntersection)
                {
                    MakeCrossIntersection((CrossIntersection)parentRoadElement);
                    junctionNum++;

                }
                else if (parentRoadElement is TIntersection)
                {
                    MakeTIntersection((TIntersection)parentRoadElement);
                    junctionNum++;

                }

            }
            else
            {
                //throw new System.InvalidOperationException("Error in implementation");
            }
        }
        else
        {
            foreach (object item in parentRoadElement._descendents)
            {
                //Type type = item.GetType();
                if (item is RoadElement)
                {
                    //here I will assume that any Abstract Road Elements will simply not be visualized
                    if (item is RoadSegment)
                    {
                        MakeRoadSegment((RoadSegment)item);
                    }
                    else if (item is CrossIntersection)
                    {
                        MakeCrossIntersection((CrossIntersection)item);
                        junctionNum++;

                    }
                    else if (item is TIntersection)
                    {
                        MakeTIntersection((TIntersection)item);
                        junctionNum++;

                    }

                }
                else
                {
                    //throw new System.InvalidOperationException("Error in implementation");
                }
            }
        }
        writeXML(fileName);
    }

    
    public void Visualize(string fileName, RoadSegment road)
    {
        //initialize necessary variables
        openDrive = new OpenDrive();
        roadList = new List<Road>();
        junctionList = new List<Junction>();
        junctionNum = 1;

        SetupOpenDRIVE();

        MakeRoadSegment(road);

        writeXML(fileName);
    }
    
    //TODO: add default values here (or in the class constructor..)
    public void MakeRoadSegment(RoadSegment road)
    {
        int availableID = road._idNum + 1;
        int numLanes = road._numLanes;

        Road r = new Road(road._id, road._lengthOfRoad, road._idNum.ToString(), "-1");
        if(road._id == road._idNum.ToString())
        {
            r.name = "";
        }
        r.link = createLink(road);

        Type type = new Type(0, "town");

        //Add check here for new road._pos and orientation
        //TODO: also add check for lane connections
        road = updateRoadCoordinatesAndOrientation(road);

        List<Geometry> planView = new List<Geometry>();
        Geometry g = new Geometry(0, road._pos._x, road._pos._y, GetHeading(road), road._lengthOfRoad);
        Line l = new Line();
        g.line = l;

        planView.Add(g);

        List<LaneSection> lanes = new List<LaneSection>();
        LaneSection ls = new LaneSection(0);

        List<Lane> cll = new List<Lane>();
        List<Lane> rll = new List<Lane>();
        List<Lane> lll = new List<Lane>();
        for (int i = 0; i <= numLanes; i++)
        {
            if (i == 0)
            {
                Lane l1 = new Lane(0, "driving", 0);
                l1.link = new Link();
                RoadMark rm = new RoadMark(0, "solid solid", "standard", "yellow", 0.13f);
                l1.roadMark = rm;
                cll.Add(l1);
            }
            else if (i % 2 == 0)
            {
                Lane l2 = new Lane(-(i / 2), "driving", 0);
                l2.link = createSingleLaneLink(road, -(i / 2));
                Width w = new Width(0, defaultRoadWidth, 0, 0, 0);
                l2.width = w;
                RoadMark rm2 = new RoadMark(0, "broken", "standard", "standard", 0.13f);
       
                if (i == numLanes)
                {
                    rm2.type = "solid";
                }
                l2.roadMark = rm2;
                rll.Add(l2);
            }
            else
            {
                Lane l3 = new Lane((i + 1) / 2, "driving", 0);
                l3.link = createSingleLaneLink(road, (i + 1) / 2);
                Width w = new Width(0, defaultRoadWidth, 0, 0, 0);
                l3.width = w;
                RoadMark rm3 = new RoadMark(0, "broken", "standard", "standard", 0.13f);

                if (i+1 == numLanes)
                {
                    rm3.type = "solid";
                }
                l3.roadMark = rm3;
                lll.Add(l3);
            }
        }
        

        ls.center = cll;
        ls.right = rll;
        ls.left = lll;
        lanes.Add(ls);

        r.type = type;
        r.planView = planView;
        r.elevationProfile = new ElevationProfile();
        r.lateralProfile = new LateralProfile();
        r.lanes = lanes;

        //add vegetation
        List<OBJECT> objectList = new List<OBJECT>();
        System.Random random = new System.Random();
        if (road._zone1 == Zoning.TREES)
        {
            int n = 0;
            while(n < road._lengthOfRoad)
            {
                int randIndex = random.Next(0, treeList.Count - 1);
                OBJECT tree = new OBJECT("tree", treeList[randIndex], availableID.ToString(), n, defaultRoadWidth * (numLanes / 2) + 4);
                availableID++;
                objectList.Add(tree);

                n = n + random.Next(2, 7);
            }
        }

        if(road._zone2 == Zoning.TREES)
        {
            int n = 0;
            while (n < road._lengthOfRoad)
            {
                int randIndex = random.Next(0, treeList.Count - 1);
                OBJECT tree = new OBJECT("tree", treeList[randIndex], availableID.ToString(), n, -(defaultRoadWidth * (numLanes / 2) + 4));
                availableID++;
                objectList.Add(tree);

                n = n + random.Next(2, 7);
            }
        }
        r.objects = objectList;

        roadList.Add(r);

    }

    public void MakeCrossIntersection(CrossIntersection intersection)
    {
        int availableID = intersection._idNum + 25;
        int numLanes = intersection._numLanes;
        float length = (defaultRoadWidth * numLanes) + 18f;// + 12.5f;

        //Add check here for new road._pos and orientation
        //TODO: also add check for lane connections
        intersection = updateRoadCoordinatesAndOrientation(intersection);

        Road r1 = new Road("1" + intersection._id, length, (intersection._idNum + 1).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r1.name = "";
        }
        r1.link = createLink(intersection, 1, (int)INTERSECTION_LANE.STRAIGHT);
        r1.type = new Type(0, "town");
        List<Geometry> planView = new List<Geometry>();
        Geometry g = new Geometry(0, intersection._pos._x + (length / 2) * (float)Math.Cos(GetHeading(intersection,1) + Math.PI), intersection._pos._y + (length / 2) * (float)Math.Sin(GetHeading(intersection, 1) + Math.PI), GetHeading(intersection,1), length);
        Line line = new Line();
        g.line = line;
        planView.Add(g);
        r1.planView = planView;
        Elevation elevation = new Elevation(0f, 0f, 0f, 0f, 0f);
        r1.elevationProfile = new ElevationProfile(elevation);
        r1.lateralProfile = new LateralProfile();

        Road r2 = new Road("2" + intersection._id, length, (intersection._idNum + 2).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r2.name = "";
        }
        r2.link = createLink(intersection, 2, (int)INTERSECTION_LANE.STRAIGHT);
        r2.type = new Type(0, "town");
        planView = new List<Geometry>();
        g = new Geometry(0, intersection._pos._x + (length / 2) * (float)Math.Cos(GetHeading(intersection, 2) + Math.PI), intersection._pos._y + (length / 2) * (float)Math.Sin(GetHeading(intersection, 2) + Math.PI), GetHeading(intersection, 2), length); g.line = line;
        planView.Add(g);
        r2.planView = planView;
        r2.elevationProfile = new ElevationProfile(elevation);
        r2.lateralProfile = new LateralProfile();

        Road r3 = new Road("3" + intersection._id, length, (intersection._idNum + 3).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r3.name = "";
        }
        r3.link = createLink(intersection, 3, (int)INTERSECTION_LANE.STRAIGHT);
        r3.type = new Type(0, "town");
        planView = new List<Geometry>();
        g = new Geometry(0, intersection._pos._x + (length / 2) * (float)Math.Cos(GetHeading(intersection, 3) + Math.PI), intersection._pos._y + (length / 2) * (float)Math.Sin(GetHeading(intersection, 3) + Math.PI), GetHeading(intersection, 3), length); g.line = line;
        planView.Add(g);
        r3.planView = planView;
        r3.elevationProfile = new ElevationProfile(elevation);
        r3.lateralProfile = new LateralProfile();

        Road r4 = new Road("4" + intersection._id, length, (intersection._idNum + 4).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r4.name = "";
        }
        r4.link = createLink(intersection, 4, (int)INTERSECTION_LANE.STRAIGHT);
        r4.type = new Type(0, "town");
        planView = new List<Geometry>();
        g = new Geometry(0, intersection._pos._x + (length / 2) * (float)Math.Cos(GetHeading(intersection, 4) + Math.PI), intersection._pos._y + (length / 2) * (float)Math.Sin(GetHeading(intersection, 4) + Math.PI), GetHeading(intersection, 4), length); g.line = line;
        g.line = line;
        planView.Add(g);
        r4.planView = planView;
        r4.elevationProfile = new ElevationProfile(elevation);
        r4.lateralProfile = new LateralProfile();

        List<LaneSection> r1_lanes = new List<LaneSection>();
        List<LaneSection> r2_lanes = new List<LaneSection>();
        List<LaneSection> r3_lanes = new List<LaneSection>();
        List<LaneSection> r4_lanes = new List<LaneSection>();

        //add lanes
        List<Lane> cll0 = new List<Lane>();
        List<Lane> ll_r1 = new List<Lane>();
        List<Lane> ll_r2 = new List<Lane>();
        List<Lane> ll_r3 = new List<Lane>();
        List<Lane> ll_r4 = new List<Lane>();
        for (int i = 0; i <= numLanes; i++)
        {
            if (i == 0)
            {
                Lane l1 = new Lane(0, "driving", 0);
                l1.link = new Link();
                    
                    RoadMark rm1 = new RoadMark(0, "none", "standard", "yellow", 0.13f);
                    l1.roadMark = rm1;
                    

                cll0.Add(l1);
            }
            else if (i % 2 == 0)
            {
                Lane l2 = new Lane(-(i / 2), "driving", 0);
                Width w0 = new Width(0, defaultRoadWidth, 0, 0, 0);
                l2.width = w0;
                    
                    RoadMark rm0 = new RoadMark(0, "none", "standard", "standard", 0.13f);
                    l2.roadMark = rm0;
                    
                    

                l2.link = createSingleLaneLink(intersection, -(i / 2), 1, (int)INTERSECTION_LANE.STRAIGHT);
                ll_r1.Add(l2);

                l2.link = createSingleLaneLink(intersection, -(i / 2), 2, (int)INTERSECTION_LANE.STRAIGHT);
                ll_r2.Add(l2);
            }
            else
            {
                Lane l3 = new Lane(-(i + 1) / 2, "driving", 0);
                Width w3 = new Width(0, defaultRoadWidth, 0, 0, 0);
                l3.width = w3;
                    
                    RoadMark rm3 = new RoadMark(0, "none", "standard", "standard", 0.13f);
                    l3.roadMark = rm3;
                    

                l3.link = createSingleLaneLink(intersection, -(i + 1) / 2, 3, (int)INTERSECTION_LANE.STRAIGHT);
                ll_r3.Add(l3);

                l3.link = createSingleLaneLink(intersection, -(i + 1) / 2, 4, (int)INTERSECTION_LANE.STRAIGHT);
                ll_r4.Add(l3);
            }

        }

        Lane border1 = new Lane(-(numLanes / 2) - 1, "border", 0);
        border1.link = new Link();
        Width w = new Width(0, 1.5f, 0, 0, 0);
        border1.width = w;
        RoadMark rm = new RoadMark(0, "none", "standard", "standard", 0.13f);
        border1.roadMark = rm;

        Lane border2 = new Lane(-(numLanes / 2) - 2, "none", 0);
        border2.link = new Link();
        w = new Width(0, 6f, 0, 0, 0);
        border2.width = w;
        rm = new RoadMark(0, "none", "standard", "standard", 0.13f);
        border2.roadMark = rm;

        ll_r1.Add(border1);
        ll_r1.Add(border2);
        ll_r2.Add(border1);
        ll_r2.Add(border2);
        ll_r3.Add(border1);
        ll_r3.Add(border2);
        ll_r4.Add(border1);
        ll_r4.Add(border2);

        LaneSection ls0 = new LaneSection(0);
        ls0.center = cll0;
        ls0.right = ll_r1;
        r1_lanes.Add(ls0);

        ls0 = new LaneSection(0);
        ls0.center = cll0;
        ls0.right = ll_r2;
        r2_lanes.Add(ls0);

        ls0 = new LaneSection(0);
        ls0.center = cll0;
        ls0.right = ll_r3;
        r3_lanes.Add(ls0);

        ls0.center = cll0;
        ls0.right = ll_r4;
        r4_lanes.Add(ls0);

        r1.lanes = r1_lanes;
        r2.lanes = r2_lanes;
        r3.lanes = r3_lanes;
        r4.lanes = r4_lanes;

        List<Signal> signallist1 = new List<Signal>();
        List<Signal> signallist2 = new List<Signal>();
        List<Signal> signallist3 = new List<Signal>();
        List<Signal> signallist4 = new List<Signal>();
        //add traffic lights
        if (intersection._trafficSignal)
        {
            //r1 and r3 will have grouped controllers
            List<Control> controllist = new List<Control>();
            //r1
            //add signal
            Signal signal = new Signal(2f, -defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000001");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            //add stopbar
            Signal stopbar = new Signal(0f, 0f, availableID.ToString(), "Haltelinie", "294", "no");
            availableID++;

            signallist1.Add(signal);
            signallist1.Add(stopbar);
            


            //r3
            //add signal
            signal = new Signal(2f, -defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000001");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            //add halteline
            stopbar = new Signal(0f, 0f, availableID.ToString(), "Haltelinie", "294", "no");
            availableID++;

            signallist3.Add(signal);
            signallist3.Add(stopbar);


            Controller controller1 = new Controller(junctionNum.ToString(), "ctrl" + junctionNum.ToString(), controllist);
            controllerList.Add(controller1);
            junctionNum++;

            //r2 and r4 will have grouped controllers
            controllist = new List<Control>();
            //r2
            //add signal
            signal = new Signal(2f, -defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000001");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            //add halteline
            stopbar = new Signal(0f, 0f, availableID.ToString(), "Haltelinie", "294", "no");
            availableID++;

            signallist2.Add(signal);
            signallist2.Add(stopbar);
            
            //r4
            signal = new Signal(2f, -defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000001");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            //add halteline
            stopbar = new Signal(0f, 0f, availableID.ToString(), "Haltelinie", "294", "no");
            availableID++;

            signallist4.Add(signal);
            signallist4.Add(stopbar);
            


            Controller controller2 = new Controller(junctionNum.ToString(), "ctrl" + junctionNum.ToString(), controllist);
            controllerList.Add(controller2);
            junctionNum++;


        }

        //add pedestriancrossing
        if (intersection._pedestrianCrossing)
        {
            //r1 and r3 will have grouped controllers
            List<Control> controllist = new List<Control>();
            //r1
            //add signal
            Signal signal = new Signal(2f, -defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000002");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            Signal signal2 = new Signal(2f, defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000002");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            //add zebra crossing
            Signal zebrax = new Signal(0f, 0f, availableID.ToString(), "zebrastreifen", "1000003", "no", signalvalue:4f);
            availableID++;

            Signal zebrax2 = new Signal(length, 0f, availableID.ToString(), "zebrastreifen", "1000003", "no", signalvalue: 4f);
            availableID++;

            signallist1.Add(signal);
            signallist1.Add(signal2);
            signallist1.Add(zebrax);
            signallist1.Add(zebrax2);



            //r3
            //add signal
            signal = new Signal(2f, -defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000002");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            signal2 = new Signal(2f, defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000002");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            //add zebra crossing
            zebrax = new Signal(0f, 0f, availableID.ToString(), "zebrastreifen", "1000003", "no", signalvalue: 4f);
            availableID++;

            zebrax2 = new Signal(length, 0f, availableID.ToString(), "zebrastreifen", "1000003", "no", signalvalue: 4f);
            availableID++;

            signallist3.Add(signal);
            signallist3.Add(signal2);
            signallist3.Add(zebrax);
            signallist3.Add(zebrax2);


            Controller controller1 = new Controller(junctionNum.ToString(), "ctrl" + junctionNum.ToString(), controllist);
            controllerList.Add(controller1);
            junctionNum++;

            //r2 and r4 will have grouped controllers
            controllist = new List<Control>();
            //r2
            //add signal
            signal = new Signal(2f, -defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000002");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            signal2 = new Signal(2f, defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000002");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            //add zebra crossing
            zebrax = new Signal(0f, 0f, availableID.ToString(), "zebrastreifen", "1000003", "no", signalvalue: 4f);
            availableID++;

            zebrax2 = new Signal(length, 0f, availableID.ToString(), "zebrastreifen", "1000003", "no", signalvalue: 4f);
            availableID++;

            signallist2.Add(signal);
            signallist2.Add(signal2);
            signallist2.Add(zebrax);
            signallist2.Add(zebrax2);

            //r4
            signal = new Signal(2f, -defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000002");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            signal2 = new Signal(2f, defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000002");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            //add zebra crossing
            zebrax = new Signal(0f, 0f, availableID.ToString(), "zebrastreifen", "1000003", "no", signalvalue: 4f);
            availableID++;

            zebrax2 = new Signal(length, 0f, availableID.ToString(), "zebrastreifen", "1000003", "no", signalvalue: 4f);
            availableID++;

            signallist4.Add(signal);
            signallist4.Add(signal2);
            signallist4.Add(zebrax);
            signallist4.Add(zebrax2);



            Controller controller2 = new Controller(junctionNum.ToString(), "ctrl" + junctionNum.ToString(), controllist);
            controllerList.Add(controller2);
            junctionNum++;


        }

        r1.signals = signallist1;
        r2.signals = signallist2;
        r3.signals = signallist3;
        r4.signals = signallist4;

        roadList.Add(r1);
        roadList.Add(r2);
        roadList.Add(r3);
        roadList.Add(r4);

        //add left/right turn logic
        //center lane is same for all cases
        List<Lane> cll = new List<Lane>();
        Lane cl1 = new Lane(0, "driving", 0);
        cl1.link = new Link();
        RoadMark crm1 = new RoadMark(0, "none", "standard", "standard", 0.13f);
        cl1.roadMark = crm1;
        cll.Add(cl1);

        //make lane lists
        List<Lane> ll_r1left = new List<Lane>();
        List<Lane> ll_r1right = new List<Lane>();
        List<Lane> ll_r2left = new List<Lane>();
        List<Lane> ll_r2right = new List<Lane>();
        List<Lane> ll_r3left = new List<Lane>();
        List<Lane> ll_r3right = new List<Lane>();
        List<Lane> ll_r4left = new List<Lane>();
        List<Lane> ll_r4right = new List<Lane>();


        //for r1
        float hdg = r1.planView[0].hdg;
        float x = r1.planView[0].x;
        float y = r1.planView[0].y;
        float xright = r1.planView[0].x + defaultRoadWidth * ((numLanes / 2) - 1) * (float)Math.Cos(hdg - Math.PI / 2);
        float yright = r1.planView[0].y + defaultRoadWidth * ((numLanes / 2) - 1) * (float)Math.Sin(hdg - Math.PI / 2);

        //make left turns
        float leftRadius = length / 2; //half of road length
        float leftArcLength = (float)(0.5 * Math.PI * leftRadius * 1.05);
        Arc leftArc = new Arc(1 / leftRadius);
        Geometry leftGeo = new Geometry(0, x, y, hdg, leftArcLength, geoarc: leftArc);

        Road r1left = new Road("1l" + intersection._id, length, (intersection._idNum + 21).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r1left.name = "";
        }
        r1left.link = createLink(intersection, 1, (int)INTERSECTION_LANE.LEFT);
        r1left.type = new Type(0, "town");
        planView = new List<Geometry>{ leftGeo };
        r1left.planView = planView;
        r1left.elevationProfile = new ElevationProfile(elevation);
        r1left.lateralProfile = new LateralProfile();

        Lane leftturnlane = new Lane(-1, "driving", 0);
        leftturnlane.link = createSingleLaneLink(intersection, -1, 1, (int)INTERSECTION_LANE.LEFT);
        Width w2 = new Width(0, defaultRoadWidth, 0, 0, 0);
        leftturnlane.width = w2;
        RoadMark rm2 = new RoadMark(0, "none", "standard", "standard", 0.13f);
        leftturnlane.roadMark = rm2;
        ll_r1left.Add(leftturnlane);

        List<LaneSection> lanes = new List<LaneSection>();
        LaneSection ls = new LaneSection(0);
        ls.center = cll;
        ls.right = ll_r1left;
        lanes.Add(ls);
        r1left.lanes = lanes;
        roadList.Add(r1left);

        //make right turns
        float rightRadius = leftRadius - defaultRoadWidth * ((numLanes / 2) - 1); //half of road length
        float rightArcLength = (float)(0.5 * Math.PI * rightRadius * 1.1);
        Arc rightArc = new Arc(-1 / rightRadius);
        Geometry rightGeo = new Geometry(0, xright, yright, hdg, rightArcLength, geoarc: rightArc);

        Road r1right = new Road("1r" + intersection._id, length, (intersection._idNum + 11).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r1right.name = "";
        }
        r1right.link = createLink(intersection, 1, (int)INTERSECTION_LANE.RIGHT);
        r1right.type = new Type(0, "town");
        planView = new List<Geometry> { rightGeo };
        r1right.planView = planView;
        r1right.elevationProfile = new ElevationProfile(elevation);
        r1right.lateralProfile = new LateralProfile();

        Lane rightturnlane = new Lane(-1, "driving", 0);
        rightturnlane.link = createSingleLaneLink(intersection, -(numLanes / 2), 1, (int)INTERSECTION_LANE.RIGHT);
        w2 = new Width(0, defaultRoadWidth, 0, 0, 0);
        rightturnlane.width = w2;
        rm2 = new RoadMark(0, "solid", "standard", "standard", 0.13f);
        rightturnlane.roadMark = rm2;
        ll_r1right.Add(rightturnlane);

        lanes = new List<LaneSection>();
        ls = new LaneSection(0);
        ls.center = cll;
        ls.right = ll_r1right;
        lanes.Add(ls);
        r1right.lanes = lanes;

        //add vegetation
        List<OBJECT> objectList = new List<OBJECT>();
        System.Random random = new System.Random();
        if (intersection._zone1 == Zoning.TREES)
        {
            int n = 0;
            while (n < rightArcLength)
            {
                int randIndex = random.Next(0, treeList.Count - 1);
                OBJECT tree = new OBJECT("tree", treeList[randIndex], availableID.ToString(), n, -(defaultRoadWidth + 4));
                availableID++;
                objectList.Add(tree);

                n = n + random.Next(5, 10);
            }
        }
        r1right.objects = objectList;

        roadList.Add(r1right);


        //for r2
        x = r2.planView[0].x;
        y = r2.planView[0].y;
        hdg = r2.planView[0].hdg;
        xright = r2.planView[0].x + defaultRoadWidth * ((numLanes / 2) - 1) * (float)Math.Cos(hdg + 3 * Math.PI / 2);
        yright = r2.planView[0].y + defaultRoadWidth * ((numLanes / 2) - 1) * (float)Math.Sin(hdg + 3 * Math.PI / 2);

        //make left turns
        leftGeo = new Geometry(0, x, y, hdg, leftArcLength, geoarc: leftArc);

        Road r2left = new Road("2l" + intersection._id, length, (intersection._idNum + 22).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r2left.name = "";
        }
        r2left.link = createLink(intersection, 2, (int)INTERSECTION_LANE.LEFT);
        r2left.type = new Type(0, "town");
        planView = new List<Geometry> { leftGeo };
        r2left.planView = planView;
        r2left.elevationProfile = new ElevationProfile(elevation);
        r2left.lateralProfile = new LateralProfile();

        leftturnlane.link = createSingleLaneLink(intersection, -1, 2, (int)INTERSECTION_LANE.LEFT);
        ll_r2left.Add(leftturnlane);

        lanes = new List<LaneSection>();
        ls = new LaneSection(0);
        ls.center = cll;
        ls.right = ll_r2left;
        lanes.Add(ls);
        r2left.lanes = lanes;
        roadList.Add(r2left);

        //make right turns
        rightGeo = new Geometry(0, xright, yright, hdg, rightArcLength, geoarc: rightArc);

        Road r2right = new Road("2r" + intersection._id, length, (intersection._idNum + 12).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r2right.name = "";
        }
        r2right.link = createLink(intersection, 2, (int)INTERSECTION_LANE.RIGHT);
        r2right.type = new Type(0, "town");
        planView = new List<Geometry> { rightGeo };
        r2right.planView = planView;
        r2right.elevationProfile = new ElevationProfile(elevation);
        r2right.lateralProfile = new LateralProfile();

        rightturnlane.link = createSingleLaneLink(intersection, -(numLanes / 2), 2, (int)INTERSECTION_LANE.RIGHT);
        ll_r2right.Add(rightturnlane);

        lanes = new List<LaneSection>();
        ls = new LaneSection(0);
        ls.center = cll;
        ls.right = ll_r2right;
        lanes.Add(ls);
        r2right.lanes = lanes;

        //add vegetation
        objectList = new List<OBJECT>();
        if (intersection._zone2 == Zoning.TREES)
        {
            int n = 0;
            while (n < rightArcLength)
            {
                int randIndex = random.Next(0, treeList.Count - 1);
                OBJECT tree = new OBJECT("tree", treeList[randIndex], availableID.ToString(), n, -(defaultRoadWidth + 4));
                availableID++;
                objectList.Add(tree);

                n = n + random.Next(5, 10);
            }
        }
        r2right.objects = objectList;

        roadList.Add(r2right);

        //for r3
        x = r3.planView[0].x;
        y = r3.planView[0].y;
        hdg = r3.planView[0].hdg;
        xright = r3.planView[0].x + defaultRoadWidth * ((numLanes / 2) - 1) * (float)Math.Cos(hdg - Math.PI / 2);
        yright = r3.planView[0].y + defaultRoadWidth * ((numLanes / 2) - 1) * (float)Math.Sin(hdg - Math.PI / 2);

        //make left turns
        leftGeo = new Geometry(0, x, y, hdg, leftArcLength, geoarc: leftArc);

        Road r3left = new Road("3l" + intersection._id, length, (intersection._idNum + 23).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r3left.name = "";
        }
        r3left.link = createLink(intersection, 3, (int)INTERSECTION_LANE.LEFT);
        r3left.type = new Type(0, "town");
        planView = new List<Geometry> { leftGeo };
        r3left.planView = planView;
        r3left.elevationProfile = new ElevationProfile(elevation);
        r3left.lateralProfile = new LateralProfile();

        leftturnlane.link = createSingleLaneLink(intersection, -1, 3, (int)INTERSECTION_LANE.LEFT);
        ll_r3left.Add(leftturnlane);

        lanes = new List<LaneSection>();
        ls = new LaneSection(0);
        ls.center = cll;
        ls.right = ll_r3left;
        lanes.Add(ls);
        r3left.lanes = lanes;
        roadList.Add(r3left);

        //make right turns
        rightGeo = new Geometry(0, xright, yright, hdg, rightArcLength, geoarc: rightArc);

        Road r3right = new Road("3r" + intersection._id, length, (intersection._idNum + 13).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r3right.name = "";
        }
        r3right.link = createLink(intersection, 3, (int)INTERSECTION_LANE.RIGHT);
        r3right.type = new Type(0, "town");
        planView = new List<Geometry> { rightGeo };
        r3right.planView = planView;
        r3right.elevationProfile = new ElevationProfile(elevation);
        r3right.lateralProfile = new LateralProfile();

        rightturnlane.link = createSingleLaneLink(intersection, -(numLanes / 2), 3, (int)INTERSECTION_LANE.RIGHT);
        ll_r3right.Add(rightturnlane);

        lanes = new List<LaneSection>();
        ls = new LaneSection(0);
        ls.center = cll;
        ls.right = ll_r3right;
        lanes.Add(ls);
        r3right.lanes = lanes;

        //add vegetation
        objectList = new List<OBJECT>();
        if (intersection._zone3 == Zoning.TREES)
        {
            int n = 0;
            while (n < rightArcLength)
            {
                int randIndex = random.Next(0, treeList.Count - 1);
                OBJECT tree = new OBJECT("tree", treeList[randIndex], availableID.ToString(), n, -(defaultRoadWidth + 4));
                availableID++;
                objectList.Add(tree);

                n = n + random.Next(5, 10);
            }
        }
        r3right.objects = objectList;

        roadList.Add(r3right);

        //for r4
        x = r4.planView[0].x;
        y = r4.planView[0].y;
        hdg = r4.planView[0].hdg;
        xright = r4.planView[0].x + defaultRoadWidth * ((numLanes / 2) - 1) * (float)Math.Cos(hdg - Math.PI / 2);
        yright = r4.planView[0].y + defaultRoadWidth * ((numLanes / 2) - 1) * (float)Math.Sin(hdg - Math.PI / 2);

        //make left turns
        leftGeo = new Geometry(0, x, y, hdg, leftArcLength, geoarc: leftArc);

        Road r4left = new Road("4l" + intersection._id, length, (intersection._idNum + 24).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r4left.name = "";
        }
        r4left.link = createLink(intersection, 4, (int)INTERSECTION_LANE.LEFT);
        r4left.type = new Type(0, "town");
        planView = new List<Geometry> { leftGeo };
        r4left.planView = planView;
        r4left.elevationProfile = new ElevationProfile(elevation);
        r4left.lateralProfile = new LateralProfile();

        leftturnlane.link = createSingleLaneLink(intersection, -1, 4, (int)INTERSECTION_LANE.LEFT);
        ll_r4left.Add(leftturnlane);

        lanes = new List<LaneSection>();
        ls = new LaneSection(0);
        ls.center = cll;
        ls.right = ll_r4left;
        lanes.Add(ls);
        r4left.lanes = lanes;
        roadList.Add(r4left);

        //make right turns
        rightGeo = new Geometry(0, xright, yright, hdg, rightArcLength, geoarc: rightArc);

        Road r4right = new Road("4r" + intersection._id, length, (intersection._idNum + 14).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r4right.name = "";
        }
        r4right.link = createLink(intersection, 4, (int)INTERSECTION_LANE.RIGHT);
        r4right.type = new Type(0, "town");
        planView = new List<Geometry> { rightGeo };
        r4right.planView = planView;
        r4right.elevationProfile = new ElevationProfile(elevation);
        r4right.lateralProfile = new LateralProfile();

        rightturnlane.link = createSingleLaneLink(intersection, -(numLanes / 2), 4, (int)INTERSECTION_LANE.RIGHT);
        ll_r4right.Add(rightturnlane);

        lanes = new List<LaneSection>();
        ls = new LaneSection(0);
        ls.center = cll;
        ls.right = ll_r4right;
        lanes.Add(ls);
        r4right.lanes = lanes;

        //add vegetation
        objectList = new List<OBJECT>();
        if (intersection._zone4 == Zoning.TREES)
        {
            int n = 0;
            while (n < rightArcLength)
            {
                int randIndex = random.Next(0, treeList.Count - 1);
                OBJECT tree = new OBJECT("tree", treeList[randIndex], availableID.ToString(), n, -(defaultRoadWidth + 4));
                availableID++;
                objectList.Add(tree);

                n = n + random.Next(5, 10);
            }
        }
        r4right.objects = objectList;

        roadList.Add(r4right);


        makeJunction(intersection);
    }

    public void MakeTIntersection(TIntersection intersection)
    {
        int availableID = intersection._idNum + 25;
        int numLanes = intersection._numLanes;
        float length = (defaultRoadWidth * numLanes) + 18f;//+12.5f;

        //Add check here for new road._pos and orientation
        //TODO: also add check for lane connections
        intersection = updateRoadCoordinatesAndOrientation(intersection);

        Road r1 = new Road("1" + intersection._id, length, (intersection._idNum + 1).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r1.name = "";
        }
        r1.link = createLink(intersection, 1, (int)INTERSECTION_LANE.STRAIGHT);
        r1.type = new Type(0, "town");
        List<Geometry> planView = new List<Geometry>();
        Geometry g = new Geometry(0, intersection._pos._x + (length / 2) * (float)Math.Cos(GetHeading(intersection, 1) + Math.PI), intersection._pos._y + (length / 2) * (float)Math.Sin(GetHeading(intersection, 1) + Math.PI), GetHeading(intersection, 1), length);
        Line line = new Line();
        g.line = line;
        planView.Add(g);
        r1.planView = planView;
        Elevation elevation = new Elevation(0f, 0f, 0f, 0f, 0f);
        r1.elevationProfile = new ElevationProfile(elevation);
        r1.lateralProfile = new LateralProfile();

        //****this filler road has no practical purpose, just to fill in empty space
        Road filler = new Road("filler", length/2, (intersection._idNum + 2).ToString(), "-1");
        filler.type = new Type(0, "town");
        planView = new List<Geometry>();
        g = new Geometry(0, intersection._pos._x + (length / 2) * (float)Math.Cos(GetHeading(intersection, 2) + Math.PI), intersection._pos._y + (length / 2) * (float)Math.Sin(GetHeading(intersection, 2) + Math.PI), GetHeading(intersection, 2), length); g.line = line;
        planView.Add(g);
        filler.planView = planView;
        filler.elevationProfile = new ElevationProfile(elevation);
        filler.lateralProfile = new LateralProfile();

        Road r3 = new Road("3" + intersection._id, length, (intersection._idNum + 3).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r3.name = "";
        }
        r3.link = createLink(intersection, 3, (int)INTERSECTION_LANE.STRAIGHT);
        r3.type = new Type(0, "town");
        planView = new List<Geometry>();
        g = new Geometry(0, intersection._pos._x + (length / 2) * (float)Math.Cos(GetHeading(intersection, 3) + Math.PI), intersection._pos._y + (length / 2) * (float)Math.Sin(GetHeading(intersection, 3) + Math.PI), GetHeading(intersection, 3), length); g.line = line;
        planView.Add(g);
        r3.planView = planView;
        r3.elevationProfile = new ElevationProfile(elevation);
        r3.lateralProfile = new LateralProfile();

        List<LaneSection> r1_lanes = new List<LaneSection>();
        List<LaneSection> r3_lanes = new List<LaneSection>();
        List<LaneSection> filler_lanes = new List<LaneSection>();

        //add lanes
        List<Lane> cll0 = new List<Lane>();
        List<Lane> ll_r1 = new List<Lane>();
        List<Lane> ll_r3 = new List<Lane>();
        List<Lane> filler_r = new List<Lane>();
        List<Lane> filler_l = new List<Lane>();

        for (int i = 0; i <= numLanes; i++)
        {
            if (i == 0)
            {
                Lane l1 = new Lane(0, "driving", 0);
                l1.link = new Link();

                RoadMark rm1 = new RoadMark(0, "none", "standard", "yellow", 0.13f);
                l1.roadMark = rm1;


                cll0.Add(l1);
            }
            else if (i % 2 == 0)
            {
                Lane l2 = new Lane(-(i / 2), "driving", 0);
                Width w0 = new Width(0, defaultRoadWidth, 0, 0, 0);
                l2.width = w0;

                RoadMark rm0 = new RoadMark(0, "none", "standard", "standard", 0.13f);
                l2.roadMark = rm0;

                filler_r.Add(l2);

                l2.link = createSingleLaneLink(intersection, -(i / 2), 1, (int)INTERSECTION_LANE.STRAIGHT);
                ll_r1.Add(l2);


            }
            else
            {
                Lane l3 = new Lane((i + 1) / 2, "driving", 0);
                Width w3 = new Width(0, defaultRoadWidth, 0, 0, 0);
                l3.width = w3;

                RoadMark rm3 = new RoadMark(0, "none", "standard", "standard", 0.13f);
                l3.roadMark = rm3;

                filler_l.Add(l3);


                l3 = new Lane(-(i + 1) / 2, "driving", 0);
                w3 = new Width(0, defaultRoadWidth, 0, 0, 0);
                l3.width = w3;

                if((numLanes - i) == 1)
                {
                    rm3 = new RoadMark(0, "solid", "standard", "standard", 0.13f);
                }
                else
                {
                    rm3 = new RoadMark(0, "none", "standard", "standard", 0.13f);
                }
                
                l3.roadMark = rm3;

                l3.link = createSingleLaneLink(intersection, -(i + 1) / 2, 3, (int)INTERSECTION_LANE.STRAIGHT);
                ll_r3.Add(l3);

            }

        }

        Lane border1 = new Lane(-(numLanes / 2) - 1, "border", 0);
        border1.link = new Link();
        Width w = new Width(0, 1.5f, 0, 0, 0);
        border1.width = w;
        RoadMark rm = new RoadMark(0, "none", "standard", "standard", 0.13f);
        border1.roadMark = rm;

        Lane border2 = new Lane(-(numLanes / 2) - 2, "none", 0);
        border2.link = new Link();
        w = new Width(0, 6f, 0, 0, 0);
        border2.width = w;
        rm = new RoadMark(0, "none", "standard", "standard", 0.13f);
        border2.roadMark = rm;

        ll_r1.Add(border1);
        ll_r1.Add(border2);
        ll_r3.Add(border1);
        ll_r3.Add(border2);

        LaneSection ls0 = new LaneSection(0);
        ls0.center = cll0;
        ls0.right = ll_r1;
        r1_lanes.Add(ls0);

        ls0 = new LaneSection(0);
        ls0.center = cll0;
        ls0.right = ll_r3;
        r3_lanes.Add(ls0);

        ls0 = new LaneSection(0);
        ls0.center = cll0;
        ls0.right = filler_r;
        ls0.left = filler_l;
        filler_lanes.Add(ls0);

        r1.lanes = r1_lanes;
        r3.lanes = r3_lanes;
        filler.lanes = filler_lanes;

        List<Signal> signallist1 = new List<Signal>();
        List<Signal> signallist2 = new List<Signal>();
        List<Signal> signallist3 = new List<Signal>();
        //add traffic lights
        if (intersection._trafficSignal)
        {
            //r1 and r3 will have grouped controllers
            List<Control> controllist = new List<Control>();
            //r1
            //add signal
            Signal signal = new Signal(2f, -defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000001");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            //add stopbar
            Signal stopbar = new Signal(0f, 0f, availableID.ToString(), "Haltelinie", "294", "no");
            availableID++;


            signallist1.Add(signal);
            signallist1.Add(stopbar);

            //r3
            //add signal
            signal = new Signal(2f, -defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000001");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;


            //add halteline
            stopbar = new Signal(0f, 0f, availableID.ToString(), "Haltelinie", "294", "no");
            availableID++;

            signallist3.Add(signal);
            signallist3.Add(stopbar);


            Controller controller1 = new Controller(junctionNum.ToString(), "ctrl" + junctionNum.ToString(), controllist);
            controllerList.Add(controller1);
            junctionNum++;

            //r2 and r4 will have grouped controllers
            controllist = new List<Control>();
            //r2
            //add signal
            signal = new Signal(2f, -defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000001");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            //add halteline
            stopbar = new Signal(0f, 0f, availableID.ToString(), "Haltelinie", "294", "no");
            availableID++;

            signallist2.Add(signal);
            signallist2.Add(stopbar);


            Controller controller2 = new Controller(junctionNum.ToString(), "ctrl" + junctionNum.ToString(), controllist);
            controllerList.Add(controller2);
            junctionNum++;


        }

        //add pedestriancrossing
        if (intersection._pedestrianCrossing)
        {
            //r1 and r3 will have grouped controllers
            List<Control> controllist = new List<Control>();
            //r1
            //add signal
            Signal signal = new Signal(2f, -defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000002");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            Signal signal2 = new Signal(2f, defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000002");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            //add zebra crossing
            Signal zebrax = new Signal(0f, 0f, availableID.ToString(), "zebrastreifen", "1000003", "no", signalvalue: 4f);
            availableID++;

            Signal zebrax2 = new Signal(length, 0f, availableID.ToString(), "zebrastreifen", "1000003", "no", signalvalue: 4f);
            availableID++;

            signallist1.Add(signal);
            signallist1.Add(signal2);
            signallist1.Add(zebrax);
            signallist1.Add(zebrax2);



            //r3
            //add signal
            signal = new Signal(2f, -defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000002");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            signal2 = new Signal(2f, defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000002");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            //add zebra crossing
            zebrax = new Signal(0f, 0f, availableID.ToString(), "zebrastreifen", "1000003", "no", signalvalue: 4f);
            availableID++;

            zebrax2 = new Signal(length, 0f, availableID.ToString(), "zebrastreifen", "1000003", "no", signalvalue: 4f);
            availableID++;

            signallist3.Add(signal);
            signallist3.Add(signal2);
            signallist3.Add(zebrax);
            signallist3.Add(zebrax2);


            Controller controller1 = new Controller(junctionNum.ToString(), "ctrl" + junctionNum.ToString(), controllist);
            controllerList.Add(controller1);
            junctionNum++;

            //r2 and r4 will have grouped controllers
            controllist = new List<Control>();
            //r2
            //add signal
            signal = new Signal(2f, -defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000002");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            signal2 = new Signal(2f, defaultRoadWidth * (numLanes / 2), availableID.ToString(), "_Sg" + availableID.ToString(), "1000002");
            controllist.Add(new Control(availableID.ToString(), "0"));
            availableID++;

            //add zebra crossing
            zebrax = new Signal(0f, 0f, availableID.ToString(), "zebrastreifen", "1000003", "no", signalvalue: 4f);
            availableID++;

            signallist2.Add(signal);
            signallist2.Add(signal2);
            signallist2.Add(zebrax);


            Controller controller2 = new Controller(junctionNum.ToString(), "ctrl" + junctionNum.ToString(), controllist);
            controllerList.Add(controller2);
            junctionNum++;


        }


        r1.signals = signallist1;
        filler.signals = signallist2;
        r3.signals = signallist3;

        List<OBJECT> objectList = new List<OBJECT>();
        System.Random random = new System.Random();
        if (intersection._zone3 == Zoning.TREES)
        {
            int n = 0;
            while (n < r3.length)
            {
                int randIndex = random.Next(0, treeList.Count - 1);
                OBJECT tree = new OBJECT("tree", treeList[randIndex], availableID.ToString(), n, -(defaultRoadWidth * (numLanes / 2) + 4));
                availableID++;
                objectList.Add(tree);

                n = n + random.Next(2, 7);
            }
        }
        r3.objects = objectList;

        roadList.Add(r1);
        roadList.Add(r3);
        roadList.Add(filler);

        //add left/right turn logic
        //center lane is same for all cases
        List<Lane> cll = new List<Lane>();
        Lane cl1 = new Lane(0, "driving", 0);
        cl1.link = new Link();
        RoadMark crm1 = new RoadMark(0, "none", "standard", "standard", 0.13f);
        cl1.roadMark = crm1;
        cll.Add(cl1);

        //make lane lists
        List<Lane> ll_r1right = new List<Lane>();
        List<Lane> ll_r2left = new List<Lane>();
        List<Lane> ll_r2right = new List<Lane>();
        List<Lane> ll_r3left = new List<Lane>();

        //for r1
        float hdg = r1.planView[0].hdg;
        float x = r1.planView[0].x;
        float y = r1.planView[0].y;
        float xright = r1.planView[0].x + defaultRoadWidth * ((numLanes / 2) - 1) * (float)Math.Cos(hdg - Math.PI / 2);
        float yright = r1.planView[0].y + defaultRoadWidth * ((numLanes / 2) - 1) * (float)Math.Sin(hdg - Math.PI / 2);

        //make left turns
        float leftRadius = length / 2; //half of road length
        float leftArcLength = (float)(0.5 * Math.PI * leftRadius * 1.05);
        Arc leftArc = new Arc(1 / leftRadius);
        Geometry leftGeo = new Geometry(0, x, y, hdg, leftArcLength, geoarc: leftArc);

        //make right turns
        float rightRadius = leftRadius - defaultRoadWidth * ((numLanes / 2) - 1); //half of road length
        float rightArcLength = (float)(0.5 * Math.PI * rightRadius*1.1);
        Arc rightArc = new Arc(-1 / rightRadius);
        Geometry rightGeo = new Geometry(0, xright, yright, hdg, rightArcLength, geoarc: rightArc);

        Road r1right = new Road("1r" + intersection._id, length, (intersection._idNum + 11).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r1right.name = "";
        }
        r1right.link = createLink(intersection, 1, (int)INTERSECTION_LANE.RIGHT);
        r1right.type = new Type(0, "town");
        planView = new List<Geometry> { rightGeo };
        r1right.planView = planView;
        r1right.elevationProfile = new ElevationProfile(elevation);
        r1right.lateralProfile = new LateralProfile();

        Lane rightturnlane = new Lane(-1, "driving", 0);
        rightturnlane.link = createSingleLaneLink(intersection, -(numLanes / 2), 1, (int)INTERSECTION_LANE.RIGHT);
        Width w2 = new Width(0, defaultRoadWidth, 0, 0, 0);
        rightturnlane.width = w2;
        RoadMark rm2 = new RoadMark(0, "solid", "standard", "standard", 0.13f);
        rightturnlane.roadMark = rm2;
        ll_r1right.Add(rightturnlane);

        List<LaneSection> lanes = new List<LaneSection>();
        LaneSection ls = new LaneSection(0);
        ls.center = cll;
        ls.right = ll_r1right;
        lanes.Add(ls);
        r1right.lanes = lanes;

        //add vegetation
        objectList = new List<OBJECT>();
        if (intersection._zone1 == Zoning.TREES)
        {
            int n = 0;
            while (n < rightArcLength)
            {
                int randIndex = random.Next(0, treeList.Count - 1);
                OBJECT tree = new OBJECT("tree", treeList[randIndex], availableID.ToString(), n, -(defaultRoadWidth + 4));
                availableID++;
                objectList.Add(tree);

                n = n + random.Next(5, 10);
            }
        }
        r1right.objects = objectList;

        roadList.Add(r1right);


        //for r2
        x = intersection._pos._x + (length / 2) * (float)Math.Cos(GetHeading(intersection, 2) + Math.PI);
        y = intersection._pos._y + (length / 2) * (float)Math.Sin(GetHeading(intersection, 2) + Math.PI);
        hdg = GetHeading(intersection, 2);
        xright = x + defaultRoadWidth * ((numLanes / 2) - 1) * (float)Math.Cos(hdg + 3 * Math.PI / 2);
        yright = y + defaultRoadWidth * ((numLanes / 2) - 1) * (float)Math.Sin(hdg + 3 * Math.PI / 2);

        //make left turns
        leftGeo = new Geometry(0, x, y, hdg, leftArcLength, geoarc: leftArc);

        Road r2left = new Road("2l" + intersection._id, length, (intersection._idNum + 22).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r2left.name = "";
        }
        r2left.link = createLink(intersection, 2, (int)INTERSECTION_LANE.LEFT);
        r2left.type = new Type(0, "town");
        planView = new List<Geometry> { leftGeo };
        r2left.planView = planView;
        r2left.elevationProfile = new ElevationProfile(elevation);
        r2left.lateralProfile = new LateralProfile();


        Lane leftturnlane = new Lane(-1, "driving", 0);
        leftturnlane.link = createSingleLaneLink(intersection, -1, 2, (int)INTERSECTION_LANE.LEFT);
        w2 = new Width(0, defaultRoadWidth, 0, 0, 0);
        leftturnlane.width = w2;
        rm2 = new RoadMark(0, "none", "standard", "standard", 0.13f);
        leftturnlane.roadMark = rm2;
        ll_r2left.Add(leftturnlane);

        lanes = new List<LaneSection>();
        ls = new LaneSection(0);
        ls.center = cll;
        ls.right = ll_r2left;
        lanes.Add(ls);
        r2left.lanes = lanes;
        roadList.Add(r2left);

        lanes = new List<LaneSection>();
        ls = new LaneSection(0);
        ls.center = cll;
        ls.right = ll_r2left;
        lanes.Add(ls);
        r2left.lanes = lanes;
        roadList.Add(r2left);

        //make right turns
        rightGeo = new Geometry(0, xright, yright, hdg, rightArcLength, geoarc: rightArc);

        Road r2right = new Road("2r" + intersection._id, length, (intersection._idNum + 12).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r2right.name = "";
        }
        r2right.link = createLink(intersection, 2, (int)INTERSECTION_LANE.RIGHT);
        r2right.type = new Type(0, "town");
        planView = new List<Geometry> { rightGeo };
        r2right.planView = planView;
        r2right.elevationProfile = new ElevationProfile(elevation);
        r2right.lateralProfile = new LateralProfile();

        rightturnlane.link = createSingleLaneLink(intersection, -(numLanes / 2), 2, (int)INTERSECTION_LANE.RIGHT);
        ll_r2right.Add(rightturnlane);

        lanes = new List<LaneSection>();
        ls = new LaneSection(0);
        ls.center = cll;
        ls.right = ll_r2right;
        lanes.Add(ls);
        r2right.lanes = lanes;

        //add vegetation
        objectList = new List<OBJECT>();
        if (intersection._zone2 == Zoning.TREES)
        {
            int n = 0;
            while (n < rightArcLength)
            {
                int randIndex = random.Next(0, treeList.Count - 1);
                OBJECT tree = new OBJECT("tree", treeList[randIndex], availableID.ToString(), n, -(defaultRoadWidth + 4));
                availableID++;
                objectList.Add(tree);

                n = n + random.Next(5, 10);
            }
        }
        r2right.objects = objectList;

        roadList.Add(r2right);

        //for r3
        x = r3.planView[0].x;
        y = r3.planView[0].y;
        hdg = r3.planView[0].hdg;
        xright = r3.planView[0].x + defaultRoadWidth * ((numLanes / 2) - 1) * (float)Math.Cos(hdg - Math.PI / 2);
        yright = r3.planView[0].y + defaultRoadWidth * ((numLanes / 2) - 1) * (float)Math.Sin(hdg - Math.PI / 2);

        //make left turns
        leftGeo = new Geometry(0, x, y, hdg, leftArcLength, geoarc: leftArc);

        Road r3left = new Road("3l" + intersection._id, length, (intersection._idNum + 23).ToString(), intersection._id);
        if (intersection._id == intersection._idNum.ToString())
        {
            r3left.name = "";
        }
        r3left.link = createLink(intersection, 3, (int)INTERSECTION_LANE.LEFT);
        r3left.type = new Type(0, "town");
        planView = new List<Geometry> { leftGeo };
        r3left.planView = planView;
        r3left.elevationProfile = new ElevationProfile(elevation);
        r3left.lateralProfile = new LateralProfile();

        leftturnlane.link = createSingleLaneLink(intersection, -1, 3, (int)INTERSECTION_LANE.LEFT);
        ll_r3left.Add(leftturnlane);

        lanes = new List<LaneSection>();
        ls = new LaneSection(0);
        ls.center = cll;
        ls.right = ll_r3left;
        lanes.Add(ls);
        r3left.lanes = lanes;
        roadList.Add(r3left);

        makeJunction(intersection);
    }

    public void makeJunction(CrossIntersection intersection)
    {
        Junction junction = new Junction(intersection._id, junctionNum.ToString());

        foreach (var conn in intersection._connections._physicalConnections)
        {
            if (conn.Value != null)
            {
                if (conn.Value._belongsTo._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    string connPoint = "";
                    int factor = 1;
                    if(conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        connPoint = "start";
                        factor = 1;
                    }
                    else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        connPoint = "end";
                        factor = -1;
                    }
                    else
                    {
                        //nothing
                    }

                    string straightname = "";
                    string leftname = "";
                    string rightname = "";
                    if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        straightname = (intersection._idNum + 3).ToString();
                        leftname = (intersection._idNum + 23).ToString();
                        rightname = (intersection._idNum + 13).ToString();
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        straightname = (intersection._idNum + 2).ToString();
                        leftname = (intersection._idNum + 22).ToString();
                        rightname = (intersection._idNum + 12).ToString();
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                    {
                        straightname = (intersection._idNum + 1).ToString();
                        leftname = (intersection._idNum + 21).ToString();
                        rightname = (intersection._idNum + 11).ToString();
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_4)
                    {
                        straightname = (intersection._idNum + 4).ToString();
                        leftname = (intersection._idNum + 24).ToString();
                        rightname = (intersection._idNum + 14).ToString();
                    }
                    else
                    {
                        //nothing rn
                    }

                    //add straight
                    Connection straightconnection = new Connection(connectionNum.ToString(), conn.Value._belongsTo._idNum.ToString(), straightname, connPoint);
                    connectionNum++;
                    straightconnection.laneLinks = new List<laneLink>();
                    int numLanes = intersection._numLanes / 2;
                    for (int i = 0; i <= numLanes; i++)
                    {
                        if (i == 0)
                        {
                            //do nothing
                        }
                        else
                        {
                            laneLink llink1 = new laneLink(i * factor, -i);
                            straightconnection.laneLinks.Add(llink1);
                        }

                    }


                    //add right turn
                    Connection rightconnection = new Connection(connectionNum.ToString(), conn.Value._belongsTo._idNum.ToString(), rightname, connPoint);
                    connectionNum++;
                    laneLink llink2 = new laneLink(numLanes * factor, -numLanes);
                    rightconnection.laneLinks = new List<laneLink>() { llink2 };

                    //add left turn
                    Connection leftconnection = new Connection(connectionNum.ToString(), conn.Value._belongsTo._idNum.ToString(), leftname, connPoint);
                    connectionNum++;
                    laneLink llink3 = new laneLink(1 * factor, -1);
                    leftconnection.laneLinks = new List<laneLink>() { llink3 };

                    //build junction
                    junction.connections.Add(straightconnection);
                    junction.connections.Add(rightconnection);
                    junction.connections.Add(leftconnection);
                }
                else if (conn.Value._belongsTo._roadElementType == RoadElementType.INTERSECTION)
                {
                    //TODO: implement this
                }
                else
                {
                    //nothing -?
                }
                
            }
            
        }

        Controller ctrl1 = new Controller((junctionNum - 2).ToString(), "0", CONTROLLER_TYPE.JUNCTION);
        Controller ctrl2 = new Controller((junctionNum - 1).ToString(), "0", CONTROLLER_TYPE.JUNCTION);
        junction.controllers = new List<Controller>() { ctrl1, ctrl2 };

        junctionList.Add(junction);
    }

    public void makeJunction(TIntersection intersection)
    {
        Junction junction = new Junction(intersection._id, junctionNum.ToString());

        foreach (var conn in intersection._connections._physicalConnections)
        {
            if (conn.Value != null)
            {
                if (conn.Value._belongsTo._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    string connPoint = "";
                    int factor = 1;
                    if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        connPoint = "start";
                        factor = 1;
                    }
                    else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        connPoint = "end";
                        factor = -1;
                    }
                    else
                    {
                        //nothing
                    }

                    string straightname = "";
                    string leftname = "";
                    string rightname = "";
                    if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        straightname = (intersection._idNum + 3).ToString();
                        leftname = (intersection._idNum + 23).ToString();
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        leftname = (intersection._idNum + 22).ToString();
                        rightname = (intersection._idNum + 12).ToString();
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                    {
                        straightname = (intersection._idNum + 1).ToString();
                        rightname = (intersection._idNum + 11).ToString();
                    }
                    else
                    {
                        //nothing rn
                    }

                    int numLanes = intersection._numLanes / 2;

                    //add straight
                    if (straightname != "")
                    {
                        Connection straightconnection = new Connection(connectionNum.ToString(), conn.Value._belongsTo._idNum.ToString(), straightname, connPoint);
                        connectionNum++;
                        straightconnection.laneLinks = new List<laneLink>();
                        for (int i = 0; i <= numLanes; i++)
                        {
                            if (i == 0)
                            {
                                //do nothing
                            }
                            else
                            {
                                laneLink llink1 = new laneLink(i * factor, -i);
                                straightconnection.laneLinks.Add(llink1);
                            }

                        }

                        junction.connections.Add(straightconnection);
                    }



                    //add right turn
                    if (rightname != "")
                    {
                        Connection rightconnection = new Connection(connectionNum.ToString(), conn.Value._belongsTo._idNum.ToString(), rightname, connPoint);
                        connectionNum++;
                        laneLink llink2 = new laneLink(numLanes * factor, -numLanes);
                        rightconnection.laneLinks = new List<laneLink>() { llink2 };

                        junction.connections.Add(rightconnection);
                    }
                    

                    //add left turn
                    if(leftname != "")
                    {
                        Connection leftconnection = new Connection(connectionNum.ToString(), conn.Value._belongsTo._idNum.ToString(), leftname, connPoint);
                        connectionNum++;
                        laneLink llink3 = new laneLink(1 * factor, -1);
                        leftconnection.laneLinks = new List<laneLink>() { llink3 };

                        junction.connections.Add(leftconnection);
                    } 
                }
                else if (conn.Value._belongsTo._roadElementType == RoadElementType.INTERSECTION)
                {
                    //TODO: implement this
                }
                else
                {
                    //nothing -?
                }

            }

        }

        Controller ctrl1 = new Controller((junctionNum - 2).ToString(), "0", CONTROLLER_TYPE.JUNCTION);
        Controller ctrl2 = new Controller((junctionNum - 1).ToString(), "0", CONTROLLER_TYPE.JUNCTION);
        junction.controllers = new List<Controller>() { ctrl1, ctrl2 };

        junctionList.Add(junction);
    }

    public Link createSingleLaneLink(RoadSegment road, int laneID)
    {
        Predecessor predecessor = new Predecessor();
        Successor successor = new Successor();

        foreach (var conn in road._connections._physicalConnections)
        {
            if (conn.Value != null) // && (conn.Value._belongsTo._children.Contains(road) || conn.Value._belongsTo._descendents.Contains(road)))
            {
                if (conn.Value._belongsTo._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment connRoad = (RoadSegment)conn.Value._belongsTo;
                    if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            predecessor = new Predecessor(laneID * -1);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            predecessor = new Predecessor(laneID);
                        }
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            successor = new Successor(laneID);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            successor = new Successor(laneID * -1);
                        }
                    }
                    else
                    {
                        //nothing - ?
                    }
                }
                else if (conn.Value._belongsTo._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection connRoad = (Intersection)conn.Value._belongsTo;

                    if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor(Math.Abs(laneID) * -1);
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor(Math.Abs(laneID) * -1);
                    }
                    else
                    {
                        //get to this later I guess
                    }
                }
                else
                {
                    //do nothing right now
                }
            }
        }


        Link link = new Link();
        link.predecessor = predecessor;
        link.successor = successor;
        return link;

    }

    public Link createSingleLaneLink(CrossIntersection intersection, int laneID, int roadID, int lane)
    {
        Predecessor predecessor = new Predecessor();
        Successor successor = new Successor();

        if (roadID == 1)
        {
            PhysicalConnection successorPC = null;
            PhysicalConnection predecessorPC = intersection._connections._physicalConnections.ElementAt(2).Key; ;
            if (lane == (int)INTERSECTION_LANE.STRAIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(0).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.LEFT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(3).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.RIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(1).Key;
            }
            else
            {
                //later
            }

            if (intersection._connections._physicalConnections[successorPC] != null)
            {
                RoadElement successorRoad = intersection._connections._physicalConnections[successorPC]._belongsTo;
                PhysicalConnectionPointType successorConn = intersection._connections._physicalConnections[successorPC]._typeOfConnectionPoint;
                if (successorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor(laneID);
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor(-1 * laneID);
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (successorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    successor = new Successor(laneID);

                }
                else
                {
                    //do nothing right now

                }
            }


            if (intersection._connections._physicalConnections[predecessorPC] != null)
            {
                RoadElement predecessorRoad = intersection._connections._physicalConnections[predecessorPC]._belongsTo;
                PhysicalConnectionPointType predecessorConn = intersection._connections._physicalConnections[predecessorPC]._typeOfConnectionPoint;
                if (predecessorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor(-1 * laneID);
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor(laneID);
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (predecessorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    predecessor = new Predecessor(laneID);

                }
                else
                {
                    //do nothing right now

                }
            }

        }
        else if (roadID == 2)
        {
            PhysicalConnection successorPC = null;
            PhysicalConnection predecessorPC = intersection._connections._physicalConnections.ElementAt(1).Key; ;
            if (lane == (int)INTERSECTION_LANE.STRAIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(3).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.LEFT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(2).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.RIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(0).Key;
            }
            else
            {
                //later
            }

            if (intersection._connections._physicalConnections[successorPC] != null)
            {
                RoadElement successorRoad = intersection._connections._physicalConnections[successorPC]._belongsTo;
                PhysicalConnectionPointType successorConn = intersection._connections._physicalConnections[successorPC]._typeOfConnectionPoint;
                if (successorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor(laneID);
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor(-1 * laneID);
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (successorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    successor = new Successor(laneID);
                }
                else
                {
                    //do nothing right now

                }
            }

            if (intersection._connections._physicalConnections[predecessorPC] != null)
            {
                RoadElement predecessorRoad = intersection._connections._physicalConnections[predecessorPC]._belongsTo;
                PhysicalConnectionPointType predecessorConn = intersection._connections._physicalConnections[predecessorPC]._typeOfConnectionPoint;
                if (predecessorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor(-1 * laneID);
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor(laneID);
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (predecessorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    predecessor = new Predecessor(laneID);
                }
                else
                {
                    //do nothing right now

                }
            }
        }
        else if (roadID == 3)
        {
            PhysicalConnection successorPC = null;
            PhysicalConnection predecessorPC = intersection._connections._physicalConnections.ElementAt(0).Key; ;
            if (lane == (int)INTERSECTION_LANE.STRAIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(2).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.LEFT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(2).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.RIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(3).Key;
            }
            else
            {
                //later
            }

            if (intersection._connections._physicalConnections[successorPC] != null)
            {
                RoadElement successorRoad = intersection._connections._physicalConnections[successorPC]._belongsTo;
                PhysicalConnectionPointType successorConn = intersection._connections._physicalConnections[successorPC]._typeOfConnectionPoint;
                if (successorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor(laneID);
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor(-1 * laneID);
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (successorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    successor = new Successor(laneID);
                }
                else
                {
                    //do nothing right now

                }
            }

            if (intersection._connections._physicalConnections[predecessorPC] != null)
            {
                RoadElement predecessorRoad = intersection._connections._physicalConnections[predecessorPC]._belongsTo;
                PhysicalConnectionPointType predecessorConn = intersection._connections._physicalConnections[predecessorPC]._typeOfConnectionPoint;
                if (predecessorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor(-1 * laneID);
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor(laneID);
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (predecessorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    predecessor = new Predecessor(laneID);
                }
                else
                {
                    //do nothing right now

                }
            }

        }
        else if (roadID == 4)
        {
            PhysicalConnection successorPC = null;
            PhysicalConnection predecessorPC = intersection._connections._physicalConnections.ElementAt(3).Key; ;
            if (lane == (int)INTERSECTION_LANE.STRAIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(1).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.LEFT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(0).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.RIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(2).Key;
            }
            else
            {
                //later
            }

            if (intersection._connections._physicalConnections[successorPC] != null)
            {
                RoadElement successorRoad = intersection._connections._physicalConnections[successorPC]._belongsTo;
                PhysicalConnectionPointType successorConn = intersection._connections._physicalConnections[successorPC]._typeOfConnectionPoint;
                if (successorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor(laneID);
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor(-1 * laneID);
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (successorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    successor = new Successor(laneID);

                }
                else
                {
                    //do nothing right now

                }
            }

            if (intersection._connections._physicalConnections[predecessorPC] != null)
            {
                RoadElement predecessorRoad = intersection._connections._physicalConnections[predecessorPC]._belongsTo;
                PhysicalConnectionPointType predecessorConn = intersection._connections._physicalConnections[predecessorPC]._typeOfConnectionPoint;
                if (predecessorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor(-1 * laneID);
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor(laneID);
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (predecessorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    predecessor = new Predecessor(laneID);
                }
                else
                {
                    //do nothing right now

                }
            }

        }
        else
        {
            //nothing
        }


        Link link = new Link();
        link.predecessor = predecessor;
        link.successor = successor;
        return link;

    }

    public Link createSingleLaneLink(TIntersection intersection, int laneID, int roadID, int lane)
    {
        Predecessor predecessor = new Predecessor();
        Successor successor = new Successor();

        if (roadID == 1)
        {
            PhysicalConnection successorPC = null;
            PhysicalConnection predecessorPC = intersection._connections._physicalConnections.ElementAt(2).Key; ;
            if (lane == (int)INTERSECTION_LANE.STRAIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(0).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.RIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(1).Key;
            }
            else
            {
                //later
            }

            if (intersection._connections._physicalConnections[successorPC] != null)
            {
                RoadElement successorRoad = intersection._connections._physicalConnections[successorPC]._belongsTo;
                PhysicalConnectionPointType successorConn = intersection._connections._physicalConnections[successorPC]._typeOfConnectionPoint;
                if (successorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor(laneID);
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor(-1 * laneID);
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (successorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    successor = new Successor(laneID);

                }
                else
                {
                    //do nothing right now

                }
            }


            if (intersection._connections._physicalConnections[predecessorPC] != null)
            {
                RoadElement predecessorRoad = intersection._connections._physicalConnections[predecessorPC]._belongsTo;
                PhysicalConnectionPointType predecessorConn = intersection._connections._physicalConnections[predecessorPC]._typeOfConnectionPoint;
                if (predecessorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor(-1 * laneID);
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor(laneID);
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (predecessorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    predecessor = new Predecessor(laneID);

                }
                else
                {
                    //do nothing right now

                }
            }

        }
        else if (roadID == 2)
        {
            PhysicalConnection successorPC = null;
            PhysicalConnection predecessorPC = intersection._connections._physicalConnections.ElementAt(1).Key; ;
            if (lane == (int)INTERSECTION_LANE.LEFT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(2).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.RIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(0).Key;
            }
            else
            {
                //later
            }

            if (intersection._connections._physicalConnections[successorPC] != null)
            {
                RoadElement successorRoad = intersection._connections._physicalConnections[successorPC]._belongsTo;
                PhysicalConnectionPointType successorConn = intersection._connections._physicalConnections[successorPC]._typeOfConnectionPoint;
                if (successorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor(laneID);
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor(-1 * laneID);
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (successorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    successor = new Successor(laneID);
                }
                else
                {
                    //do nothing right now

                }
            }

            if (intersection._connections._physicalConnections[predecessorPC] != null)
            {
                RoadElement predecessorRoad = intersection._connections._physicalConnections[predecessorPC]._belongsTo;
                PhysicalConnectionPointType predecessorConn = intersection._connections._physicalConnections[predecessorPC]._typeOfConnectionPoint;
                if (predecessorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor(-1 * laneID);
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor(laneID);
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (predecessorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    predecessor = new Predecessor(laneID);
                }
                else
                {
                    //do nothing right now

                }
            }
        }
        else if (roadID == 3)
        {
            PhysicalConnection successorPC = null;
            PhysicalConnection predecessorPC = intersection._connections._physicalConnections.ElementAt(0).Key; ;
            if (lane == (int)INTERSECTION_LANE.STRAIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(2).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.LEFT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(2).Key;
            }
            else
            {
                //later
            }

            if (intersection._connections._physicalConnections[successorPC] != null)
            {
                RoadElement successorRoad = intersection._connections._physicalConnections[successorPC]._belongsTo;
                PhysicalConnectionPointType successorConn = intersection._connections._physicalConnections[successorPC]._typeOfConnectionPoint;
                if (successorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor(laneID);
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor(-1 * laneID);
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (successorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    successor = new Successor(laneID);
                }
                else
                {
                    //do nothing right now

                }
            }

            if (intersection._connections._physicalConnections[predecessorPC] != null)
            {
                RoadElement predecessorRoad = intersection._connections._physicalConnections[predecessorPC]._belongsTo;
                PhysicalConnectionPointType predecessorConn = intersection._connections._physicalConnections[predecessorPC]._typeOfConnectionPoint;
                if (predecessorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor(-1 * laneID);
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor(laneID);
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (predecessorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    predecessor = new Predecessor(laneID);
                }
                else
                {
                    //do nothing right now

                }
            }

        }
        else
        {
            //nothing
        }


        Link link = new Link();
        link.predecessor = predecessor;
        link.successor = successor;
        return link;

    }

    public Link createLink(RoadSegment road)
    {
        Predecessor predecessor = new Predecessor();
        Successor successor = new Successor();

        foreach (var conn in road._connections._physicalConnections)
        {
            

            if (conn.Value != null) // && (conn.Value._belongsTo._children.Contains(road) || conn.Value._belongsTo._descendents.Contains(road)))
            {
                //move the piece
                if (conn.Value._belongsTo._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment connRoad = (RoadSegment)conn.Value._belongsTo;

                    if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            predecessor = new Predecessor("road", connRoad._id, "start");
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            predecessor = new Predecessor("road", connRoad._id, "end");
                        }

                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            successor = new Successor("road", connRoad._id, "start");
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            successor = new Successor("road", connRoad._id, "end");
                        }
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (conn.Value._belongsTo._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection connRoad = (Intersection)conn.Value._belongsTo;

                    if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor("junction", connRoad._id);
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor("junction", connRoad._id);
                    }
                    else
                    {
                        //get to this later I guess
                    }


                }
                else
                {
                    //do nothing right now

                }
            }

        }


        Link link = new Link();
        link.predecessor = predecessor;
        link.successor = successor;
        return link;

    }

    public Link createLink(CrossIntersection intersection, int roadID, int lane)
    {
        Predecessor predecessor = new Predecessor();
        Successor successor = new Successor();

        if(roadID == 1)
        {
            PhysicalConnection successorPC = null;
            PhysicalConnection predecessorPC = intersection._connections._physicalConnections.ElementAt(2).Key; ;
            if(lane == (int)INTERSECTION_LANE.STRAIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(0).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.LEFT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(3).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.RIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(1).Key;
            }
            else
            {
                //later
            }
            
            if (intersection._connections._physicalConnections[successorPC] != null)
            {
                RoadElement successorRoad = intersection._connections._physicalConnections[successorPC]._belongsTo;
                PhysicalConnectionPointType successorConn = intersection._connections._physicalConnections[successorPC]._typeOfConnectionPoint;
                if (successorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor("road", sRoad._id, "start");
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor("road", sRoad._id, "end");
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (successorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection sRoad = (Intersection)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor("junction", sRoad._id);
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor("junction", sRoad._id);
                    }
                    else
                    {
                        //get to this later I guess
                    }


                }
                else
                {
                    //do nothing right now

                }
            }

            
            if (intersection._connections._physicalConnections[predecessorPC] != null)
            {
                RoadElement predecessorRoad = intersection._connections._physicalConnections[predecessorPC]._belongsTo;
                PhysicalConnectionPointType predecessorConn = intersection._connections._physicalConnections[predecessorPC]._typeOfConnectionPoint;
                if (predecessorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor("road", sRoad._id, "start");
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor("road", sRoad._id, "end");
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (predecessorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection sRoad = (Intersection)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor("junction", sRoad._id);
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor("junction", sRoad._id);
                    }
                    else
                    {
                        //get to this later I guess
                    }


                }
                else
                {
                    //do nothing right now

                }
            }

        }
        else if (roadID == 2)
        {
            PhysicalConnection successorPC = null;
            PhysicalConnection predecessorPC = intersection._connections._physicalConnections.ElementAt(1).Key; ;
            if (lane == (int)INTERSECTION_LANE.STRAIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(3).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.LEFT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(2).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.RIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(0).Key;
            }
            else
            {
                //later
            }

            if (intersection._connections._physicalConnections[successorPC] != null)
            {
                RoadElement successorRoad = intersection._connections._physicalConnections[successorPC]._belongsTo;
                PhysicalConnectionPointType successorConn = intersection._connections._physicalConnections[successorPC]._typeOfConnectionPoint;
                if (successorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor("road", sRoad._id, "start");
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor("road", sRoad._id, "end");
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (successorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection sRoad = (Intersection)successorRoad;
                    successor = new Successor("junction", sRoad._id);

                }
                else
                {
                    //do nothing right now

                }
            }

            if (intersection._connections._physicalConnections[predecessorPC] != null)
            {
                RoadElement predecessorRoad = intersection._connections._physicalConnections[predecessorPC]._belongsTo;
                PhysicalConnectionPointType predecessorConn = intersection._connections._physicalConnections[predecessorPC]._typeOfConnectionPoint;
                if (predecessorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor("road", sRoad._id, "start");
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor("road", sRoad._id, "end");
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (predecessorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection sRoad = (Intersection)predecessorRoad;
                    predecessor = new Predecessor("junction", sRoad._id);


                }
                else
                {
                    //do nothing right now

                }
            }
        }
        else if (roadID == 3)
        {
            PhysicalConnection successorPC = null;
            PhysicalConnection predecessorPC = intersection._connections._physicalConnections.ElementAt(0).Key; ;
            if (lane == (int)INTERSECTION_LANE.STRAIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(2).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.LEFT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(2).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.RIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(3).Key;
            }
            else
            {
                //later
            }

            if (intersection._connections._physicalConnections[successorPC] != null)
            {
                RoadElement successorRoad = intersection._connections._physicalConnections[successorPC]._belongsTo;
                PhysicalConnectionPointType successorConn = intersection._connections._physicalConnections[successorPC]._typeOfConnectionPoint;
                if (successorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor("road", sRoad._id, "start");
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor("road", sRoad._id, "end");
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (successorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection sRoad = (Intersection)successorRoad;
                    successor = new Successor("junction", sRoad._id);

                }
                else
                {
                    //do nothing right now

                }
            }

            if (intersection._connections._physicalConnections[predecessorPC] != null)
            {
                RoadElement predecessorRoad = intersection._connections._physicalConnections[predecessorPC]._belongsTo;
                PhysicalConnectionPointType predecessorConn = intersection._connections._physicalConnections[predecessorPC]._typeOfConnectionPoint;
                if (predecessorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor("road", sRoad._id, "start");
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor("road", sRoad._id, "end");
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (predecessorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection sRoad = (Intersection)predecessorRoad;
                    predecessor = new Predecessor("junction", sRoad._id);


                }
                else
                {
                    //do nothing right now

                }
            }

        }
        else if (roadID == 4)
        {
            PhysicalConnection successorPC = null;
            PhysicalConnection predecessorPC = intersection._connections._physicalConnections.ElementAt(3).Key; ;
            if (lane == (int)INTERSECTION_LANE.STRAIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(1).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.LEFT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(0).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.RIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(2).Key;
            }
            else
            {
                //later
            }

            if (intersection._connections._physicalConnections[successorPC] != null)
            {
                RoadElement successorRoad = intersection._connections._physicalConnections[successorPC]._belongsTo;
                PhysicalConnectionPointType successorConn = intersection._connections._physicalConnections[successorPC]._typeOfConnectionPoint;
                if (successorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor("road", sRoad._id, "start");
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor("road", sRoad._id, "end");
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (successorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection sRoad = (Intersection)successorRoad;
                    successor = new Successor("junction", sRoad._id);

                }
                else
                {
                    //do nothing right now

                }
            }

            if (intersection._connections._physicalConnections[predecessorPC] != null)
            {
                RoadElement predecessorRoad = intersection._connections._physicalConnections[predecessorPC]._belongsTo;
                PhysicalConnectionPointType predecessorConn = intersection._connections._physicalConnections[predecessorPC]._typeOfConnectionPoint;
                if (predecessorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor("road", sRoad._id, "start");
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor("road", sRoad._id, "end");
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (predecessorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection sRoad = (Intersection)predecessorRoad;
                    predecessor = new Predecessor("junction", sRoad._id);


                }
                else
                {
                    //do nothing right now

                }
            }

        }
        else
        {
            //nothing
        }

        Link link = new Link();
        link.predecessor = predecessor;
        link.successor = successor;
        return link;

    }

    public Link createLink(TIntersection intersection, int roadID, int lane)
    {
        Predecessor predecessor = new Predecessor();
        Successor successor = new Successor();

        if (roadID == 1)
        {
            PhysicalConnection successorPC = null;
            PhysicalConnection predecessorPC = intersection._connections._physicalConnections.ElementAt(2).Key; ;
            if (lane == (int)INTERSECTION_LANE.STRAIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(0).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.RIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(1).Key;
            }
            else
            {
                //later
            }

            if (intersection._connections._physicalConnections[successorPC] != null)
            {
                RoadElement successorRoad = intersection._connections._physicalConnections[successorPC]._belongsTo;
                PhysicalConnectionPointType successorConn = intersection._connections._physicalConnections[successorPC]._typeOfConnectionPoint;
                if (successorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor("road", sRoad._id, "start");
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor("road", sRoad._id, "end");
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (successorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection sRoad = (Intersection)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor("junction", sRoad._id);
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor("junction", sRoad._id);
                    }
                    else
                    {
                        //get to this later I guess
                    }


                }
                else
                {
                    //do nothing right now

                }
            }


            if (intersection._connections._physicalConnections[predecessorPC] != null)
            {
                RoadElement predecessorRoad = intersection._connections._physicalConnections[predecessorPC]._belongsTo;
                PhysicalConnectionPointType predecessorConn = intersection._connections._physicalConnections[predecessorPC]._typeOfConnectionPoint;
                if (predecessorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor("road", sRoad._id, "start");
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor("road", sRoad._id, "end");
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (predecessorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection sRoad = (Intersection)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor("junction", sRoad._id);
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor("junction", sRoad._id);
                    }
                    else
                    {
                        //get to this later I guess
                    }


                }
                else
                {
                    //do nothing right now

                }
            }

        }
        else if (roadID == 2)
        {
            PhysicalConnection successorPC = null;
            PhysicalConnection predecessorPC = intersection._connections._physicalConnections.ElementAt(1).Key; ;
            if (lane == (int)INTERSECTION_LANE.LEFT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(2).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.RIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(0).Key;
            }
            else
            {
                //later
            }

            if (intersection._connections._physicalConnections[successorPC] != null)
            {
                RoadElement successorRoad = intersection._connections._physicalConnections[successorPC]._belongsTo;
                PhysicalConnectionPointType successorConn = intersection._connections._physicalConnections[successorPC]._typeOfConnectionPoint;
                if (successorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor("road", sRoad._id, "start");
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor("road", sRoad._id, "end");
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (successorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection sRoad = (Intersection)successorRoad;
                    successor = new Successor("junction", sRoad._id);

                }
                else
                {
                    //do nothing right now

                }
            }

            if (intersection._connections._physicalConnections[predecessorPC] != null)
            {
                RoadElement predecessorRoad = intersection._connections._physicalConnections[predecessorPC]._belongsTo;
                PhysicalConnectionPointType predecessorConn = intersection._connections._physicalConnections[predecessorPC]._typeOfConnectionPoint;
                if (predecessorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor("road", sRoad._id, "start");
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor("road", sRoad._id, "end");
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (predecessorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection sRoad = (Intersection)predecessorRoad;
                    predecessor = new Predecessor("junction", sRoad._id);


                }
                else
                {
                    //do nothing right now

                }
            }
        }
        else if (roadID == 3)
        {
            PhysicalConnection successorPC = null;
            PhysicalConnection predecessorPC = intersection._connections._physicalConnections.ElementAt(0).Key; ;
            if (lane == (int)INTERSECTION_LANE.STRAIGHT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(2).Key;
            }
            else if (lane == (int)INTERSECTION_LANE.LEFT)
            {
                successorPC = intersection._connections._physicalConnections.ElementAt(2).Key;
            }
            else
            {
                //later
            }

            if (intersection._connections._physicalConnections[successorPC] != null)
            {
                RoadElement successorRoad = intersection._connections._physicalConnections[successorPC]._belongsTo;
                PhysicalConnectionPointType successorConn = intersection._connections._physicalConnections[successorPC]._typeOfConnectionPoint;
                if (successorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)successorRoad;

                    if (successorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        successor = new Successor("road", sRoad._id, "start");
                    }
                    else if (successorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        successor = new Successor("road", sRoad._id, "end");
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (successorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection sRoad = (Intersection)successorRoad;
                    successor = new Successor("junction", sRoad._id);

                }
                else
                {
                    //do nothing right now

                }
            }

            if (intersection._connections._physicalConnections[predecessorPC] != null)
            {
                RoadElement predecessorRoad = intersection._connections._physicalConnections[predecessorPC]._belongsTo;
                PhysicalConnectionPointType predecessorConn = intersection._connections._physicalConnections[predecessorPC]._typeOfConnectionPoint;
                if (predecessorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment sRoad = (RoadSegment)predecessorRoad;

                    if (predecessorConn == PhysicalConnectionPointType.CONN_1)
                    {
                        predecessor = new Predecessor("road", sRoad._id, "start");
                    }
                    else if (predecessorConn == PhysicalConnectionPointType.CONN_2)
                    {
                        predecessor = new Predecessor("road", sRoad._id, "end");
                    }
                    else
                    {
                        //nothing - ?
                    }

                }
                else if (predecessorRoad._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection sRoad = (Intersection)predecessorRoad;
                    predecessor = new Predecessor("junction", sRoad._id);


                }
                else
                {
                    //do nothing right now

                }
            }

        }
        else
        {
            //nothing
        }

        Link link = new Link();
        link.predecessor = predecessor;
        link.successor = successor;
        return link;

    }


    public RoadSegment updateRoadCoordinatesAndOrientation(RoadSegment road)
    {
        //first figure out if this road moves
        foreach (var conn in road._connections._physicalConnections)
        {
            //check if the road is a child of its connection
            //if so, the road needs moved to line up with the connecting piece
            //TODO: ensure this will only happen once per piece
            if(conn.Value != null && !road._descendents.Contains(conn.Value._belongsTo))
            {
                //move the piece
                if (conn.Value._belongsTo._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment connRoad = (RoadSegment)conn.Value._belongsTo;

                    if (conn.Value._typeOfConnectionPoint == conn.Key._typeOfConnectionPoint)
                    {
                        //if the connections are the same, need opposite orientation
                        road._orientation = incrementOrientation(connRoad._orientation, 2);

                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            road._pos = connRoad._pos;
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            road._pos = connRoad._pos + (road._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0)) + (connRoad._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0));
                        }

                    }
                    else
                    {
                        //if the connections are different, need same orientation
                        road._orientation = conn.Value._belongsTo._orientation;

                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            road._pos = connRoad._pos + (connRoad._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0));
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            road._pos = connRoad._pos + (road._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad) + Math.PI), (float)Math.Sin(GetHeading(connRoad) + Math.PI), 0));
                        }
                    }

                }
                else if (conn.Value._belongsTo._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection connRoad = (Intersection)conn.Value._belongsTo;
                    float roadlength = (defaultRoadWidth * connRoad._numLanes) + 18f;//+ 12.5f;

                    //adjust position
                    //adjust orientation
                    if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            int connNum = 1;
                            road._pos = connRoad._pos + (roadlength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            road._orientation = incrementOrientation(connRoad._orientation, 1);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            int connNum = 2;
                            road._pos = connRoad._pos + (roadlength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum) + Math.PI), (float)Math.Sin(GetHeading(connRoad, connNum) + Math.PI), 0));

                            road._orientation = incrementOrientation(connRoad._orientation, 2);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                        {
                            int connNum = 3;
                            road._pos = connRoad._pos + (roadlength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            road._orientation = incrementOrientation(connRoad._orientation, 3);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_4)
                        {
                            int connNum = 4;
                            road._pos = connRoad._pos + (roadlength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum) + Math.PI), (float)Math.Sin(GetHeading(connRoad, connNum) + Math.PI), 0));

                            road._orientation = incrementOrientation(connRoad._orientation, 0);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            int connNum = 1;
                            road._pos = connRoad._pos + (road._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (roadlength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            road._orientation = incrementOrientation(connRoad._orientation, 3);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            int connNum = 2;
                            road._pos = connRoad._pos + (road._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum) + Math.PI), (float)Math.Sin(GetHeading(connRoad, connNum) + Math.PI), 0)) + (roadlength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum) + Math.PI), (float)Math.Sin(GetHeading(connRoad, connNum) + Math.PI), 0));

                            road._orientation = incrementOrientation(connRoad._orientation, 0);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                        {
                            int connNum = 3;
                            road._pos = connRoad._pos + (road._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (roadlength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            road._orientation = incrementOrientation(connRoad._orientation, 1);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_4)
                        {
                            int connNum = 4;
                            road._pos = connRoad._pos + (road._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum) + Math.PI), (float)Math.Sin(GetHeading(connRoad, connNum) + Math.PI), 0)) + (roadlength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum) + Math.PI), (float)Math.Sin(GetHeading(connRoad, connNum) + Math.PI), 0));

                            road._orientation = incrementOrientation(connRoad._orientation, 2);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else
                    {
                        //get to this later I guess
                    }


                }
                else
                {
                    //do nothing right now

                }
            }

        }

        //default just return
        return road;

    }

    public CrossIntersection updateRoadCoordinatesAndOrientation(CrossIntersection intersection)
    {
        //first figure out if this road moves
        foreach (var conn in intersection._connections._physicalConnections)
        {
            //check if the road is a child of its connection
            //if so, the road needs moved to line up with the connecting piece
            if (conn.Value != null && !intersection._descendents.Contains(conn.Value._belongsTo))
            {
                float intersectionRoadLength = (defaultRoadWidth * intersection._numLanes) + 18f;//+ 12.5f;

                //move the piece
                if (conn.Value._belongsTo._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment connRoad = (RoadSegment)conn.Value._belongsTo;

                    //adjust position
                    //adjust orientation
                    if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad) + Math.PI), (float)Math.Sin(GetHeading(connRoad) + Math.PI), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 3);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            intersection._pos = connRoad._pos + (connRoad._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0)) + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 1);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad) + Math.PI), (float)Math.Sin(GetHeading(connRoad) + Math.PI), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 2);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            intersection._pos = connRoad._pos + (connRoad._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0)) + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0));

                            intersection._orientation = connRoad._orientation;
                        }
                        else
                        {
                            //later
                        }
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad) + Math.PI), (float)Math.Sin(GetHeading(connRoad) + Math.PI), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 1);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            intersection._pos = connRoad._pos + (connRoad._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0)) + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 3);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_4)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad) + Math.PI), (float)Math.Sin(GetHeading(connRoad) + Math.PI), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 0);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            intersection._pos = connRoad._pos + (connRoad._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0)) + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 2);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else
                    {
                        //get to this later I guess
                    }

                }
                else if (conn.Value._belongsTo._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection connRoad = (Intersection)conn.Value._belongsTo;
                    float connRoadLength = (defaultRoadWidth * connRoad._numLanes) + 18f;// + 12.5f;

                    //adjust position
                    //adjust orientation
                    if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            int connNum = 1;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 2);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            int connNum = 2;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 3);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                        {
                            int connNum = 3;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = connRoad._orientation;
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_4)
                        {
                            int connNum = 4;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 1);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            int connNum = 1;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 1);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            int connNum = 2;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 2);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                        {
                            int connNum = 3;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 3);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_4)
                        {
                            int connNum = 4;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 0);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            int connNum = 1;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 0);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            int connNum = 2;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 1);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                        {
                            int connNum = 3;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 2);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_4)
                        {
                            int connNum = 4;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 3);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_4)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            int connNum = 1;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 3);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            int connNum = 2;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 0);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                        {
                            int connNum = 3;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 1);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_4)
                        {
                            int connNum = 4;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 2);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else
                    {
                        //get to this later I guess
                    }


                }
            }
            else
            {
                //do nothing right now

            }
        }

        //default just return
        return intersection;

    }

    public TIntersection updateRoadCoordinatesAndOrientation(TIntersection intersection)
    {
        //first figure out if this road moves
        foreach (var conn in intersection._connections._physicalConnections)
        {
            //check if the road is a child of its connection
            //if so, the road needs moved to line up with the connecting piece
            if (conn.Value != null && !intersection._descendents.Contains(conn.Value._belongsTo))
            {
                float intersectionRoadLength = (defaultRoadWidth * intersection._numLanes) + 18f;// + 12.5f;

                //move the piece
                if (conn.Value._belongsTo._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
                {
                    RoadSegment connRoad = (RoadSegment)conn.Value._belongsTo;

                    //adjust position
                    //adjust orientation
                    if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad) + Math.PI), (float)Math.Sin(GetHeading(connRoad) + Math.PI), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 3);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            intersection._pos = connRoad._pos + (connRoad._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0)) + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 1);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad) + Math.PI), (float)Math.Sin(GetHeading(connRoad) + Math.PI), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 2);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            intersection._pos = connRoad._pos + (connRoad._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0)) + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0));

                            intersection._orientation = connRoad._orientation;
                        }
                        else
                        {
                            //later
                        }
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad) + Math.PI), (float)Math.Sin(GetHeading(connRoad) + Math.PI), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 1);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            intersection._pos = connRoad._pos + (connRoad._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0)) + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 3);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_4)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad) + Math.PI), (float)Math.Sin(GetHeading(connRoad) + Math.PI), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 0);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            intersection._pos = connRoad._pos + (connRoad._lengthOfRoad) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0)) + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad)), (float)Math.Sin(GetHeading(connRoad)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 2);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else
                    {
                        //get to this later I guess
                    }

                }
                else if (conn.Value._belongsTo._roadElementType == RoadElementType.INTERSECTION)
                {
                    Intersection connRoad = (Intersection)conn.Value._belongsTo;
                    float connRoadLength = (defaultRoadWidth * connRoad._numLanes) + 18f;// + 12.5f;

                    //adjust position
                    //adjust orientation
                    if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            int connNum = 1;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 2);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            int connNum = 2;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 3);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                        {
                            int connNum = 3;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = connRoad._orientation;
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_4)
                        {
                            int connNum = 4;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 1);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            int connNum = 1;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 1);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            int connNum = 2;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 2);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                        {
                            int connNum = 3;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 3);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_4)
                        {
                            int connNum = 4;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 0);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            int connNum = 1;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 0);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            int connNum = 2;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 1);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                        {
                            int connNum = 3;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 2);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_4)
                        {
                            int connNum = 4;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 3);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else if (conn.Key._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_4)
                    {
                        if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_1)
                        {
                            int connNum = 1;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 3);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_2)
                        {
                            int connNum = 2;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 0);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_3)
                        {
                            int connNum = 3;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 1);
                        }
                        else if (conn.Value._typeOfConnectionPoint == PhysicalConnectionPointType.CONN_4)
                        {
                            int connNum = 4;
                            intersection._pos = connRoad._pos + (intersectionRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0)) + (connRoadLength / 2) * (new Vector3((float)Math.Cos(GetHeading(connRoad, connNum)), (float)Math.Sin(GetHeading(connRoad, connNum)), 0));

                            intersection._orientation = incrementOrientation(connRoad._orientation, 2);
                        }
                        else
                        {
                            //later
                        }
                    }
                    else
                    {
                        //get to this later I guess
                    }


                }
            }
            else
            {
                //do nothing right now

            }
        }

        //default just return
        return intersection;

    }

    public Orientation incrementOrientation(Orientation startOrientation, int num)
    {
        Orientation newOrientation = startOrientation;

        for (int i = 0; i < num; i++)
        {
            if(newOrientation == Orientation.VERTICAL)
            {
                newOrientation = Orientation.HORIZONTAL;
            }
            else if (newOrientation == Orientation.HORIZONTAL)
            {
                newOrientation = Orientation.VERTICAL_FLIPPED;
            }
            else if (newOrientation == Orientation.VERTICAL_FLIPPED)
            {
                newOrientation = Orientation.HORIZONTAL_FLIPPED;
            }
            else if (newOrientation == Orientation.HORIZONTAL_FLIPPED)
            {
                newOrientation = Orientation.VERTICAL;
            }
            else
            {
                // ????
            }

        }

        return newOrientation;
    }

    public float GetHeading(RoadSegment road)
    {
        Orientation orientation = road._orientation;
        if (orientation == Orientation.VERTICAL)
        {
            return ((float)Math.PI / 2);
        }
        else if (orientation == Orientation.HORIZONTAL)
        {
            return 0f;
        }
        else if (orientation == Orientation.VERTICAL_FLIPPED)
        {
            return (3 * (float)Math.PI / 2);
        }
        else if (orientation == Orientation.HORIZONTAL_FLIPPED)
        {
            return (float)Math.PI;
        }
        else
        {
            return -999;
        }
        
    }

    public float GetHeading(Intersection intersection, int roadID)
    {
        Orientation orientation = intersection._orientation;

        float heading = 0;
        if (orientation == Orientation.VERTICAL)
        {
            heading = 0;
        }
        else if (orientation == Orientation.HORIZONTAL)
        {
            heading = 3 * (float)Math.PI / 2;
        }
        else if (orientation == Orientation.VERTICAL_FLIPPED)
        {
            heading = (float)Math.PI;
        }
        else if (orientation == Orientation.HORIZONTAL_FLIPPED)
        {
            heading = (float)Math.PI / 2;
        }

        if(roadID == 1)
        {
            heading += 0;
        }
        else if (roadID == 2)
        {
            heading += (float)Math.PI / 2;
        }
        else if (roadID == 3)
        {
            heading += (float)Math.PI;
        }
        else if (roadID == 4)
        {
            heading += (3 * (float)Math.PI / 2);
        }
        else
        {
            return -999;
        }

        if(heading > (2 * (float)Math.PI))
        {
            heading -= (2 * (float)Math.PI);
        }

        return heading;

    }

    public void writeXML(string fileName)
    {
        openDrive.Road = roadList;
        openDrive.Junction = junctionList;
        openDrive.Controller = controllerList;


        XmlSerializer serializer = new XmlSerializer(typeof(OpenDrive));
        TextWriter writer = new StreamWriter(fileName);

        serializer.Serialize(writer, openDrive);
        writer.Close();
    }


}

﻿using System.Collections.Generic;

//This script generates XML for an m by n road grid and saves it as the file "road-grid-example.xodr"

public class RoadGridExample : ProgramBaseClass
{
    // specify test variables here
    VarEnum<int> numLanes = new VarEnum<int>(new List<int>() { 2, 4, 6 });
    VarInterval<int> connLength = new VarInterval<int>(20, 10, 50);
    VarInterval<int> numCols = new VarInterval<int>(5, 1, 7);
    VarInterval<int> numRows = new VarInterval<int>(3, 1, 4);

    // specify the desired road network here
    public override RoadElement RoadNetwork()
    {
        // create road element
        RoadElement roadGrid = new RoadElement(id: "roadGrid");

        // construct the road grid
        roadGrid = roadGrid.buildRoadGrid(numLanes: numLanes, numRows: numRows, numCols: numCols, lengthofconnection: 20);

        // return final road network to visalize
        return roadGrid;

    }

    // specify the desired test here
    public override void Test()
    {
        // create a list of test variables
        List<ProDM> TestVars = new List<ProDM>() { numCols, numRows, numLanes, connLength };

        // create the test
        CreateTestFiles(baseFileName: "road-grid-example", testVars: TestVars, iterations: 5);

    }
}



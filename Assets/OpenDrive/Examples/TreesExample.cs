﻿using System.Collections.Generic;

//This script generates XML for a straight road with trees on either side and saves it as the file "trees-example.xodr"

public class TreesExample : ProgramBaseClass
{
    // Test variables
    // A VarEnum represents a parameter that takes a value from the elements specified in the list
    VarEnum<int> numLanes = new VarEnum<int>(new List<int>() { 2, 4, 6 });
    // A Var Interval chooses a value between the supplied min and max
    VarInterval<int> length = new VarInterval<int>(min: 5, max: 50);

    // Create the desired road network here
    public override RoadElement RoadNetwork()
    {
        // The parameterized straight road
        // Zoning is defined with trees on each side of the road
        RoadSegment road = new RoadSegment(lengthOfRoad: length, numLanes: numLanes, zone1: Zoning.TREES, zone2: Zoning.TREES);

        // The road to be visualized needs to be returned
        return road;
    }

    // Specify the desired test here
    public override void Test()
    {
        // A list of test parameters
        List<ProDM> testVars = new List<ProDM>() { numLanes, length };

        // Create the test file(s) by sampling from test parameters for certain number of iterations >=1
        CreateTestFiles(baseFileName: "trees-example", testVars: testVars, iterations: 10);
    }
}

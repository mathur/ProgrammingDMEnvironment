﻿using System.Collections.Generic;

//This script generates XML for a cross intersection and saves it as the file "intersection-example.xodr"

public class IntersectionExample : ProgramBaseClass
{
    // specify variables here
    VarEnum<int> numLanes = new VarEnum<int>(new List<int>() { 2, 4, 6 });

    //  the desired road network here
    public override RoadElement RoadNetwork()
    {
        // make the intersection
        CrossIntersection int1 = new CrossIntersection(numLanes: numLanes);

        // return final road network to visalize
        return int1;
    }

    // specify the desired test here
    public override void Test()
    {
        // create a list of test variables
        List<ProDM> testVars = new List<ProDM>() { numLanes };

        // create the test
        CreateTestFiles(baseFileName: "intersection-example", testVars: testVars, iterations: 1);
    }
}

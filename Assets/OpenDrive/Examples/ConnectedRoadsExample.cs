﻿using System.Collections.Generic;

//This script generates XML for a T-intersection with three straight roads connected and saves it as the file "connected-roads-example.xodr"

public class ConnectedRoadsExample : ProgramBaseClass
{
    // specify variables here
    VarEnum<int> numLanes = new VarEnum<int>(new List<int>() { 2, 4, 6 });
    VarInterval<int> length = new VarInterval<int>(val: 20, min: 5, max: 50);

    // create the desired road network here
    public override RoadElement RoadNetwork()
    {
        // make the T-intersection
        TIntersection int1 = new TIntersection(numLanes: numLanes);

        // make straight roads to connect to the Intersection
        RoadSegment cRoad1 = new RoadSegment(lengthOfRoad: length, numLanes: numLanes);
        RoadSegment cRoad2 = new RoadSegment(lengthOfRoad: length, numLanes: numLanes);
        RoadSegment cRoad3 = new RoadSegment(lengthOfRoad: length, numLanes: numLanes);

        // add straight roads to a list
        List<RoadElement> cRoadList = new List<RoadElement>
        {
            cRoad1,
            cRoad2,
            cRoad3
        };

        // make an encapsulating object of straight roads
        RoadElement cRoads = new RoadElement(cRoadList);

        // Connect the encapsulated object to T-Interesection
        // first specify the connection mapping
        Dictionary<PhysicalConnection, PhysicalConnection> connMap1 = new Dictionary<PhysicalConnection, PhysicalConnection>
        {
            { cRoad1._connections.AtIndex(1), int1._connections.AtIndex(0) },
            { cRoad2._connections.AtIndex(0), int1._connections.AtIndex(1) },
            { cRoad3._connections.AtIndex(0), int1._connections.AtIndex(2) }
        };
        // then make the connection
        RoadElement finalconnection = cRoads.ConnectTo(r2: int1, connectionMap: connMap1);

        // return final road network to visalize
        return finalconnection;
    }

    // specify the desired test here
    public override void Test()
    {
        // create a list of test variables
        List<ProDM> testVars = new List<ProDM>() { numLanes, length };

        // create the test
        CreateTestFiles(baseFileName: "connected-roads-example", testVars: testVars, iterations: 1);
    }
}
﻿using System.Collections.Generic;

//This script generates XML for a straight road and saves it as the file "straight-road-example.xodr"

public class StraightRoadExample : ProgramBaseClass
{
    // Test variables
    // A VarEnum represents a parameter that takes a value from the elements specified in the list
    VarEnum<int> numLanes = new VarEnum<int>(new List<int>() { 2, 4, 6 });
    // A Var Interval chooses a value between the supplied min and max
    VarInterval<int> length = new VarInterval<int>(min: 5, max: 50);

    // Create the desired road network here
    public override RoadElement RoadNetwork()
    {
        // The parameterized straight road
        RoadSegment road = new RoadSegment(lengthOfRoad: length, numLanes: numLanes);

        // The road to be visualized needs to be returned
        return road;
    }

    // Specify the desired test here
    public override void Test()
    {
        // A list of test parameters
        List<ProDM> testVars = new List<ProDM>() { numLanes, length };

        // Create the test file(s) by sampling from test parameters for certain number of iterations >=1
        CreateTestFiles(baseFileName: "straight-road-example", testVars: testVars, iterations: 10);
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

//This script generates XML for basic road elements and saves it as the file "test.xodr"

public class TestBuild : ProgramBaseClass
{

    public override void Test()
    {

        CreateTestFiles("t-intersection-test", new List<ProDM>(), 1);

    }

    public override RoadElement RoadNetwork()
    {

        TIntersection int1 = new TIntersection(4);

        RoadSegment cRoad1 = new RoadSegment(50, 4);
        RoadSegment cRoad2 = new RoadSegment(50, 4);
        RoadSegment cRoad3 = new RoadSegment(50, 4);


        List<RoadElement> cRoadList = new List<RoadElement>();
        cRoadList.Add(cRoad1);
        cRoadList.Add(cRoad2);
        cRoadList.Add(cRoad3);


        //Make an encapsulating object
        RoadElement cRoads = new RoadElement(cRoadList);

        //Connect the encapsulated object to T-Interesection
        Dictionary<PhysicalConnection, PhysicalConnection> connMap1 = new Dictionary<PhysicalConnection, PhysicalConnection>();
        connMap1.Add(cRoad1._connections.AtIndex(1), int1._connections.AtIndex(0));
        connMap1.Add(cRoad2._connections.AtIndex(0), int1._connections.AtIndex(1));
        connMap1.Add(cRoad3._connections.AtIndex(0), int1._connections.AtIndex(2));

        RoadElement finalconnection = cRoads.ConnectTo(int1, connMap1);


        return finalconnection;


    }


}




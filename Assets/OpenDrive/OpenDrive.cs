﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Schema;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


public class OpenDrive : IXmlSerializable
{
    public Header header;
    public List<Road> Road;
    public List<Controller> Controller;
    public List<Junction> Junction;

    public OpenDrive()
    {

    }

    public XmlSchema GetSchema()
    {
        return null;
    }

    public void WriteXml(XmlWriter writer)
    {
        XmlSerializer xmlSerializer = new XmlSerializer(header.GetType());
        // removes namespace
        var xmlns = new XmlSerializerNamespaces();
        xmlns.Add(string.Empty, string.Empty);
        xmlSerializer.Serialize(writer, header, xmlns);

        foreach (var r in Road)
        {
            
            xmlSerializer = new XmlSerializer(r.GetType());
            // removes namespace
            xmlns = new XmlSerializerNamespaces();
            xmlns.Add(string.Empty, string.Empty);

            xmlSerializer.Serialize(writer, r, xmlns);
            
        }

        foreach (var c in Controller)
        {

            xmlSerializer = new XmlSerializer(c.GetType());
            // removes namespace
            xmlns = new XmlSerializerNamespaces();
            xmlns.Add(string.Empty, string.Empty);

            xmlSerializer.Serialize(writer, c, xmlns);

        }

        foreach (var j in Junction)
        {

            xmlSerializer = new XmlSerializer(j.GetType());
            // removes namespace
            xmlns = new XmlSerializerNamespaces();
            xmlns.Add(string.Empty, string.Empty);

            xmlSerializer.Serialize(writer, j, xmlns);

        }

    }

    public void ReadXml(XmlReader reader)
    {

    }

}

public class Header
{
    [XmlAttribute]
    public int revMajor;
    [XmlAttribute]
    public int revMinor;
    [XmlAttribute]
    public string name;
    [XmlAttribute]
    public float version;
    [XmlAttribute]
    public System.DateTime date;
    [XmlAttribute]
    public float north;
    [XmlAttribute]
    public float south;
    [XmlAttribute]
    public float east;
    [XmlAttribute]
    public float west;
    [XmlAttribute]
    public float originLat;
    [XmlAttribute]
    public float originLong;
    [XmlAttribute]
    public float originHdg;
}

public class Road
{
    [XmlAttribute]
    public string name;
    [XmlAttribute]
    public float length;
    [XmlAttribute]
    public string id;
    [XmlAttribute]
    public string junction;

    public Link link;
    public Type type;
    public List<Geometry> planView;
    public ElevationProfile elevationProfile;
    public LateralProfile lateralProfile;

    public List<LaneSection> lanes;
    public List<Signal> signals;
    public List<OBJECT> objects;

    public Road()
    {
        link = new Link();
        type = new Type();
        planView = new List<Geometry>();
        elevationProfile = new ElevationProfile();
        lateralProfile = new LateralProfile();

        lanes = new List<LaneSection>();
        signals = new List<Signal>();
        objects = new List<OBJECT>();
    }

    public Road(string name1, float length1, string id1, string junction1)
    {
        name = name1;
        length = length1;
        id = id1;
        junction = junction1;

        link = new Link();
        type = new Type();
        planView = new List<Geometry>();
        elevationProfile = new ElevationProfile();
        lateralProfile = new LateralProfile();

        lanes = new List<LaneSection>();
        signals = new List<Signal>();
        objects = new List<OBJECT>();
    }
}

public class Link
{
    public Predecessor predecessor;
    public Successor successor;

    public Link()
    {
        predecessor = new Predecessor();
        successor = new Successor();
    }

    public Link(Predecessor p = null, Successor s = null)
    {
        predecessor = p;
        successor = s;
    }
}

public class Type
{
    [XmlAttribute]
    public float s;
    [XmlAttribute]
    public string type;

    public Type()
    {

    }

    public Type(float s1, string type1)
    {
        s = s1;
        type = type1;
    }

}

public class PlanView
{
    public List<Geometry> geometry;

    public PlanView()
    {
        geometry = new List<Geometry>();
    }
}

public class ElevationProfile
{
    public Elevation elevation;

    public ElevationProfile()
    {
        elevation = new Elevation();
    }

    public ElevationProfile(Elevation elevation1)
    {
        elevation = elevation1;
    }
}

public class LateralProfile
{

    public LateralProfile()
    {

    }
}

public class Predecessor
{
    [XmlAttribute]
    public string elementType;
    [XmlAttribute]
    public string elementId;
    [XmlAttribute]
    public string contactPoint;
    [XmlAttribute]
    public int id;

    public Predecessor()
    {

    }
    
    public Predecessor(string elementType1, string elementId1, string contactPoint1)
    {
        elementType = elementType1;
        elementId = elementId1;
        contactPoint = contactPoint1;
    }

    public Predecessor(string elementType1, string elementId1)
    {
        elementType = elementType1;
        elementId = elementId1;
    }

    public Predecessor(int laneId)
    {
        id = laneId;
    }
}

public class Successor
{
    [XmlAttribute]
    public string elementType;
    [XmlAttribute]
    public string elementId;
    [XmlAttribute]
    public string contactPoint;
    [XmlAttribute]
    public int id;

    public Successor()
    {

    }

    public Successor(string elementType1, string elementId1, string contactPoint1)
    {
        elementType = elementType1;
        elementId = elementId1;
        contactPoint = contactPoint1;
    }

    public Successor(string elementType1, string elementId1)
    {
        elementType = elementType1;
        elementId = elementId1;
    }

    public Successor(int laneId)
    {
        id = laneId;
    }
}

public class Geometry
{
    [XmlAttribute]
    public float s;
    [XmlAttribute]
    public float x;
    [XmlAttribute]
    public float y;
    [XmlAttribute]
    public float hdg;
    [XmlAttribute]
    public float length;

    public Line line;
    public Spiral spiral;
    public Arc arc;

    public Geometry()
    {
        line = new Line();
        spiral = new Spiral();
        arc = new Arc();
    }

    public Geometry(float s1, float x1, float y1, float hdg1, float length1, Line geoline = null, Spiral geospiral = null, Arc geoarc = null)
    {
        s = s1;
        x = x1;
        y = y1;
        hdg = hdg1;
        length = length1;

        if(geoline != null)
        {
            line = geoline;
        }

        if (geospiral != null)
        {
            spiral = geospiral;
        }

        if (geoarc != null)
        {
            arc = geoarc;
        }

    }
}

public class Elevation
{
    [XmlAttribute]
    public float s;
    [XmlAttribute]
    public float a;
    [XmlAttribute]
    public float b;
    [XmlAttribute]
    public float c;
    [XmlAttribute]
    public float d;

    public Elevation()
    {

    }

    public Elevation(float s1, float a1, float b1, float c1, float d1)
    {
        s = s1;
        a = a1;
        b = b1;
        c = c1;
        d = d1;
    }
}

public class Line
{

    public Line()
    {

    }
}

public class Spiral
{
    [XmlAttribute]
    public float curvStart;
    [XmlAttribute]
    public float curvEnd;

    public Spiral()
    {

    }

    public Spiral(float curvStart1, float curvEnd1)
    {
        curvStart = curvStart1;
        curvEnd = curvEnd1;
    }
}

public class Arc
{
    [XmlAttribute]
    public float curvature;

    public Arc()
    {

    }

    public Arc(float curvature1)
    {
        curvature = curvature1;
    }
}

public class LaneSection
{
    [XmlAttribute]
    public int s;

    public List<Lane> right;
    public List<Lane> center;
    public List<Lane> left;

    public LaneSection()
    {
        right = new List<Lane>();
        center = new List<Lane>();
        left = new List<Lane>();
    }

    public LaneSection(int s1)
    {
        s = s1;

        right = new List<Lane>();
        center = new List<Lane>();
        left = new List<Lane>();
    }

}

public class Lane
{
    [XmlAttribute]
    public int id;
    [XmlAttribute]
    public string type;
    [XmlAttribute]
    public int level;

    public Link link;
    public Width width;
    public RoadMark roadMark;
    public Height height;
    public UserData userData;

    public Lane()
    {
        link = new Link();
        width = new Width();
        roadMark = new RoadMark();
        height = new Height();
        userData = new UserData();
    }

    public Lane(int id1, string type1, int level1)
    {
        id = id1;
        type = type1;
        level = level1;

        link = new Link();
        width = new Width();
        roadMark = new RoadMark();
        height = new Height();
        userData = new UserData();
    }
}

public class Width
{
    [XmlAttribute]
    public float sOffset;
    [XmlAttribute]
    public float a;
    [XmlAttribute]
    public float b;
    [XmlAttribute]
    public float c;
    [XmlAttribute]
    public float d;

    public Width()
    {

    }

    public Width(float sOffset1, float a1, float b1, float c1, float d1)
    {
        sOffset = sOffset1;
        a = a1;
        b = b1;
        c = c1;
        d = d1;
    }
}

public class RoadMark
{
    [XmlAttribute]
    public float sOffset;
    [XmlAttribute]
    public string type;
    [XmlAttribute]
    public string weight;
    [XmlAttribute]
    public string color;
    [XmlAttribute]
    public float width;
    [XmlAttribute]
    public string laneChange;

    public RoadMark()
    {

    }

    public RoadMark(float sOffset1, string type1, string weight1, string color1, float width1, string laneChange1 = "none")
    {
        sOffset = sOffset1;
        type = type1;
        weight = weight1;
        color = color1;
        width = width1;
        laneChange = laneChange1;
    }
}

public class Height
{
    [XmlAttribute]
    public float sOffset;
    [XmlAttribute]
    public float inner;
    [XmlAttribute]
    public float outer;

    public Height()
    {

    }

    public Height(float sOffset1, float inner1, float outer1)
    {
        sOffset = sOffset1;
        inner = inner1;
        outer = outer1;
    }
}

public class UserData
{
    [XmlAttribute]
    public string code;

    public Style style;
    public Fillet fillet;

    public UserData()
    {
        style = new Style();
        fillet = new Fillet();
    }

    public UserData(string code1)
    {
        code = code1;

        style = new Style();
        fillet = new Fillet();
    }
}

public class Style
{
    [XmlAttribute]
    public float sOffset;
    [XmlAttribute]
    public string laneStyle;
    [XmlAttribute]
    public string mapping;

    public Style()
    {

    }

    public Style(float sOffset1, string laneStyle1, string mapping1)
    {
        sOffset = sOffset1;
        laneStyle = laneStyle1;
        mapping = mapping1;
    }
}

public class Fillet
{
    [XmlAttribute]
    public string pos;
    [XmlAttribute]
    public string style;
    [XmlAttribute]
    public int id;

    public Fillet()
    {

    }

    public Fillet(string pos1, string style1, int id1)
    {
        pos = pos1;
        style = style1;
        id = id1;
    }
}

public class Signal
{
    [XmlAttribute]
    public float s;
    [XmlAttribute]
    public float t;
    [XmlAttribute]
    public string id;
    [XmlAttribute]
    public string name;
    [XmlAttribute]
    public string dynamic;
    [XmlAttribute]
    public string orientation;
    [XmlAttribute]
    public float zOffset;
    [XmlAttribute]
    public string country;
    [XmlAttribute]
    public string countryRevision;
    [XmlAttribute]
    public string type;
    [XmlAttribute]
    public string subtype;
    [XmlAttribute]
    public float value;
    [XmlAttribute]
    public string unit;
    [XmlAttribute]
    public float height;
    [XmlAttribute]
    public float width;
    [XmlAttribute]
    public string text;
    [XmlAttribute]
    public float hOffset;
    [XmlAttribute]
    public float pitch;
    [XmlAttribute]
    public float roll;

    public Signal()
    {

    }

    public Signal(float signals, float siganlt, string signalid, string signalname, string signaltype, string signaldynamic = "yes", string siganlorientation = "+", float signalzoffset = 0f, string signalcountry = "OpenDRIVE", string signalsubtype = "-1", float signalvalue = -1f)
    {
        s = signals;
        t = siganlt;
        id = signalid;
        name = signalname;
        type = signaltype;
        dynamic = signaldynamic;
        orientation = siganlorientation;
        zOffset = signalzoffset;
        country = signalcountry;
        subtype = signalsubtype;
        value = signalvalue;
    }
}

public class OBJECT
{
    [XmlAttribute]
    public string type;
    [XmlAttribute]
    public string name;
    [XmlAttribute]
    public string id;
    [XmlAttribute]
    public float s;
    [XmlAttribute]
    public float t;
    [XmlAttribute]
    public float zOffset;
    [XmlAttribute]
    public float validLength;
    [XmlAttribute]
    public string orientation;
    [XmlAttribute]
    public float hdg;
    [XmlAttribute]
    public float pitch;
    [XmlAttribute]
    public float roll;

    public OBJECT()
    {

    }

    public OBJECT(string objecttype, string objectname, string objectid, float objects, float objectt, float objecthdg = 0f, float objectzoffset = 0f, float objectvalidlength = 0f, string objectorientation = "none")
    {
        type = objecttype;
        name = objectname;
        id = objectid;
        s = objects;
        t = objectt;
        zOffset = objectzoffset;
        validLength = objectvalidlength;
        orientation = objectorientation;
        hdg = objecthdg;

    }

}

public class Junction : IXmlSerializable
{
    [XmlAttribute]
    public string name;
    [XmlAttribute]
    public string id;

    public List<Connection> connections;
    public List<Controller> controllers;

    public Junction()
    {
        connections = new List<Connection>();
        controllers = new List<Controller>();
    }

    public Junction(string junctionName, string junctionID)
    {
        name = junctionName;
        id = junctionID;

        connections = new List<Connection>();
        controllers = new List<Controller>();
    }

    public XmlSchema GetSchema()
    {
        return null;
    }

    public void WriteXml(XmlWriter writer)
    {
        writer.WriteAttributeString("name", name);
        writer.WriteAttributeString("id", id);

        //string temp = "";
        foreach (var conn in connections)
        {
            writer.WriteStartElement("connection");
            writer.WriteAttributeString("id", conn.id);
            writer.WriteAttributeString("incomingRoad", conn.incomingRoad);
            writer.WriteAttributeString("connectingRoad", conn.connectingRoad);
            writer.WriteAttributeString("contactPoint", conn.contactPoint);

            foreach (var llink in conn.laneLinks)
            {
                XmlSerializer xmlSerializer = new XmlSerializer(llink.GetType());
                // removes namespace
                var xmlns = new XmlSerializerNamespaces();
                xmlns.Add(string.Empty, string.Empty);

                xmlSerializer.Serialize(writer, llink, xmlns);
            }

            writer.WriteEndElement();
        }

        foreach (var ctrl in controllers)
        {
            writer.WriteStartElement("controller");
            writer.WriteAttributeString("id", ctrl.id);
            writer.WriteAttributeString("type", ctrl.type);
            writer.WriteEndElement();
        }

    }

    public void ReadXml(XmlReader reader)
    {
        
    }

}

public class Connection
{
    [XmlAttribute]
    public string id;
    [XmlAttribute]
    public string incomingRoad;
    [XmlAttribute]
    public string connectingRoad;
    [XmlAttribute]
    public string contactPoint;

    public List<laneLink> laneLinks;

    public Connection()
    {
        laneLinks = new List<laneLink>();

    }

    public Connection(string connectionID, string incoming, string connecting, string contact)
    {
        id = connectionID;
        incomingRoad = incoming;
        connectingRoad = connecting;
        contactPoint = contact;

        laneLinks = new List<laneLink>();
    }

}

public class laneLink
{
    [XmlAttribute]
    public int from;
    [XmlAttribute]
    public int to;

    public laneLink()
    {

    }

    public laneLink(int fromNum, int toNum)
    {
        from = fromNum;
        to = toNum;
    }
}

public enum CONTROLLER_TYPE {JUNCTION, ROAD};
public class Controller
{
    [XmlAttribute]
    public string name;
    [XmlAttribute]
    public string id;
    [XmlAttribute]
    public string type;

    [XmlElement]
    public List<Control> controls;

    

    public Controller()
    {
        controls = new List<Control>();
    }

    //since the junction controller and normal controller classes each have two string fields, 
    //we need some way to properly set these properties, so we have this CONTROLLER_TYPE enum
    public Controller(string controllerId, string controllerString, CONTROLLER_TYPE t)
    {
        id = controllerId;

        if(t == CONTROLLER_TYPE.JUNCTION)
        {
            type = controllerString;
        }
        else
        {
            name = controllerString;
        }
        

        controls = new List<Control>();
    }

    public Controller(string controllerId, string controllerName, List<Control> controlList)
    {
        name = controllerName;
        id = controllerId;

        controls = controlList;
    }


}

public class Control
{
    [XmlAttribute]
    public string signalId;
    [XmlAttribute]
    public string type;

    public Control()
    {

    }

    public Control(string controlSignalId, string controlSignalType)
    {
        signalId = controlSignalId;
        type = controlSignalType;
    }
}
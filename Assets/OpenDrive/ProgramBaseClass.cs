﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgramBaseClass : MonoBehaviour
{
    public Visualize_XML PlaceHolderForVisualizer;

    private void Awake()
    {
        if (PlaceHolderForVisualizer == null)
        {
            //If not set in inspector, set PlaceHolderForVisualizer
            PlaceHolderForVisualizer = GetComponent<Visualize_XML>();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Test();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual RoadElement RoadNetwork()
    {
        throw new System.Exception("Cannot call this funciton");
    }

    public virtual void Test()
    {
        throw new System.Exception("Cannot call this funciton");
    }

    // Test several iterations over ranged values
    public void CreateTestFiles(string baseFileName, List<ProDM> testVars, int iterations)
    {

        // for each iteration
        for (var i = 0; i < iterations; i++)
        {
            //set each to a random value
            foreach(var number in testVars)
            {
                if(number._type == ProDMType.INT)
                {
                    VarInterval<int> newNum = (VarInterval<int>)number;
                    newNum.SetRandom();
                    
                }
                else if (number._type == ProDMType.ONE_OF)
                {
                    VarEnum<int> newNum = (VarEnum<int>)number;
                    newNum.SetRandom();

                }
                else
                {

                }
                

                
            }

            RoadElement finalRoadElement = RoadNetwork();

            PlaceHolderForVisualizer.Visualize(baseFileName + "_test_" + i.ToString() + ".xodr", finalRoadElement);

        }

    }
}

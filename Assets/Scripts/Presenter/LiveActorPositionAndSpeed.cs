﻿/*
 * This script is attached to actors to set their live position and speed in their underlying structures.
 * NOTE- Speed may not be accurate. Rigidbody speed is used in collision checks.
 *  */

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class LiveActorPositionAndSpeed : MonoBehaviour {
    //Reference to the structure where data is stored
    public TestActor actorRef;
    //Temporary var used for speed calculation
    private Vector3 prevPosition;



    // Use this for initialization
    void Start ()
    {
		if(actorRef == null)
            throw new System.Exception("Some essential properties of LiveActorCollisionCheck are null");
        //Set previous value
        prevPosition = transform.position;
    }
	
	// Calculations should be independent of frame rate
	void FixedUpdate ()
    {
        //Set respective vars
        actorRef._livePos.Add(transform.position);
        UnityStandardAssets.Vehicles.Car.CarController controller = GetComponent<UnityStandardAssets.Vehicles.Car.CarController>();
        if (controller == null)
        {
            actorRef._liveSpeed.Add((transform.position - prevPosition).magnitude / Time.fixedDeltaTime); // Not the best way to get speed
        }
        else
        {
            //Get speed from the car controller
            actorRef._liveSpeed.Add(controller.CurrentSpeed);

        }


        prevPosition = transform.position;

        //if it is an autonomous car, also add its path to the string builder so that it can be exported to a csv later
        if (actorRef._testActorType == TestActorType.EGO_VEHICLE_AUTONOMOUS || actorRef._testActorType == TestActorType.EGO_VEHICLE_TRAINING)
        {
            //track the position for csv recording
            Visualizer.sb.AppendLine(transform.position.ToString()); 
        }
    }
}

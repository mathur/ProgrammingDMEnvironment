﻿//Change camera whenever a user button is pressed or the method for changing cameras is called
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCamera : MonoBehaviour {

    public Camera topDownCamera;
    public Camera interactiveCamera;

	// Use this for initialization
	void Start ()
    {
	    if(topDownCamera == null || interactiveCamera == null)
        {
            Debug.LogError("Camera references have not been set");
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
            Change();
            
	}

    public void Change()
    {
        topDownCamera.enabled = !topDownCamera.enabled;
        interactiveCamera.enabled = !interactiveCamera.enabled;
    }

}

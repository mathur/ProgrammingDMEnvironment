﻿/*
 * This script uses data passed in the form of EnvironmentSettings
 * and makes relevant changes to the environment.
 * These are global settings.
 *  */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeEnvironmentSettings : MonoBehaviour
{
    //Some useful object references
    public GameObject directionalLight;

    public void Start()
    {
        //Set fog color to white    
        RenderSettings.fogColor = new Color(0.89f,0.89f,0.89f);
    }
    public void UpdateTo(EnvironmentSettings settings)
    {
        //Light
        directionalLight.transform.rotation = Quaternion.Euler(settings._light._direction);
        Light lightComponent = directionalLight.GetComponent<Light>();
        lightComponent.color = settings._light._color;
        lightComponent.intensity = settings._light._directionalIntensity;
        RenderSettings.ambientIntensity = settings._light._ambientIntensity;

        //Fog
        RenderSettings.fog = settings._fog._density > 0f ? true : false;
        RenderSettings.fogDensity = settings._fog._density;
    }
}

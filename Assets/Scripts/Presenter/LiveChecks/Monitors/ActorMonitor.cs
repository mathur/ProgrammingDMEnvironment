﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorMonitor : MonoBehaviour
{
    // Reference to the ego vehicle
    public TestActor actorRef;

    public GameObject ScriptPlaceholder; // A reference to this is required and set on attachment to g.o.

    public OnlineEventType monitorType;

    public virtual void Monitor()
    {
        throw new System.Exception("Cannot call this funciton");
    }
}

﻿/*  TODO: Not yet implemented
 *  This monitor, unlike the standard collision monitor, gives high energies 
 *  to tests where there was 'almost' a collision, so that these are selected during simulated annealing
 *  */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Reactive extensions
using UniRx;
using UniRx.Triggers;

public class QualitativeCollisionMonitor : ActorMonitor
{
    private Vector3 minDifference;
    private float minDist;
    private float energy = 0f;
    // The actor of interest (for almost collisions)
    public GameObject actorOfInterest;
    public string actorIdOfInterest="pedestrian";

    private Vector3 colVelocity; // In case of collision, the collision veloctiy
    
    // Start is called before the first frame update
    void Start()
    {
        if (actorRef == null)
        {
            throw new System.Exception("Some essential properties of CollisionMonitor are null");
        }

        monitorType = OnlineEventType.ALMOST_COLLISION;

        //Reactive collision check
        this.OnCollisionEnterAsObservable()
            .Subscribe(col =>
            {
                if (!col.gameObject.CompareTag("Terrain")) //A relevant collision
                {
                    minDist = 0f;
                    minDifference = new Vector3(0f, 0f, 0f);
                    energy = 0f;
                    colVelocity = col.relativeVelocity;
                }
            });
    }

    private void Update()
    {
        if (actorOfInterest == null) // GameObject check done here to ensure the actor of interest has been instantiated
        {
            // Find the actor of interest
            actorOfInterest = GameObject.Find(actorIdOfInterest);
            //Debug.Log("Actor of interest found");
            // Set initial value of minDist
            minDifference = transform.position - actorOfInterest.transform.position;
            minDist = minDifference.magnitude;
        }

        Vector3 difference = transform.position - actorOfInterest.transform.position;
        if (difference.magnitude < minDist)
        {
            minDifference = difference;
            minDist = difference.magnitude;
        }
    }

    public void EndOfTest()
    {
        OnlineEvent col;
        if (minDist != 0f)
        {
            energy = (1 / minDist) * 20f;
            col = new AlmostCollisionEvent(owner: actorRef, suspect: actorIdOfInterest, minDifference: minDifference, minDifferenceMagnitude: minDist, energy: energy);
            ScriptPlaceholder.GetComponent<EnvironmentProgramBaseClass>().currentTestIteration.onlineEvents.Add(col);
        }
        else
        {
            col = new CollisionEvent(owner: actorRef, suspect: actorIdOfInterest, collisionVelocity: colVelocity, energy: energy);
            ScriptPlaceholder.GetComponent<EnvironmentProgramBaseClass>().currentTestIteration.onlineEvents.Add(col);
            ScriptPlaceholder.GetComponent<EnvironmentProgramBaseClass>().currentTestIteration.collision.Add((CollisionEvent)col);
        }
        //Debug.Log("Qualitative monitor info: Actor of interest=" + actorIdOfInterest + " energy=" + energy.ToString() + "time:" + System.DateTime.Now.ToLongTimeString());
        //Visualizer.messageText.text = "Qualitative monitor info: Actor of interest=" + actorIdOfInterest + " energy=" + energy.ToString() + "time:" + System.DateTime.Now.ToLongTimeString();

        // Reset the actorOfInterest for future iterations
        actorOfInterest = null;
    }

}

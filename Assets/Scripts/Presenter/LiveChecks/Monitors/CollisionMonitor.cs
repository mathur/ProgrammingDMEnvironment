﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Reactive extensions
using UniRx;
using UniRx.Triggers;

public class CollisionMonitor : ActorMonitor
{

    // Start is called before the first frame update
    void Start()
    {
        if (actorRef == null)
        {
            throw new System.Exception("Some essential properties of CollisionMonitor are null");
        }

        monitorType = OnlineEventType.COLLISION;

        //Reactive collision check
        this.OnCollisionEnterAsObservable()
            .Subscribe(col =>
            {
                if (!col.gameObject.CompareTag("Terrain")) //A relevant collision
                {
                    float energy = Mathf.Clamp01(Mathf.InverseLerp(0, 20, col.relativeVelocity.magnitude));
                    //Create a new collision event
                    CollisionEvent collision = new CollisionEvent(owner: actorRef, suspect: col.gameObject.name, collisionVelocity: col.relativeVelocity, energy:energy);
                    //Add the event to the actor's list of events
                    actorRef._events.Add(collision);

                    //Send a message to the ScriptPlaceholder. There may be scripts who would like to hear about this
                    ScriptPlaceholder.BroadcastMessage("CollisionDetected", collision);
                    Visualizer.messageText.text = "Collision info: owner:" + actorRef + ", suspect: " + col.gameObject.name +
                        ", collisionVelocity:" + col.relativeVelocity + "time:" + System.DateTime.Now.ToLongTimeString();
                }
                else
                {
                    //Debug.Log("Hit the road!!");
                }
            });
    }

}

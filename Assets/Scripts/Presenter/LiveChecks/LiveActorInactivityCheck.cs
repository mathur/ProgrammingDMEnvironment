﻿/*
 * This script is attached as a component to relevant Actors, typically an ego car.
 * It detects inactivity of the ego car (non-movement).
 * 
 *  */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Reactive extensions
using UniRx;
using UniRx.Triggers;

public class LiveActorInactivityCheck : LiveActorCheck
{
    public int discretization = 100; // Size of the window over which averaging is done
    public float distanceThreshold = 0.35f; //Threshold
    List<float> deltaDistance;
    
    Vector3 prevPos;

    private void Start()
    {
        prevPos = transform.position;
        deltaDistance = new List<float>();

        // Reactive inactivity check
        //TODO: Improve this? 
        this.UpdateAsObservable()
            .Subscribe(_ =>
            {
                Vector3 currPos = transform.position;
                float distance = (currPos - prevPos).magnitude;
                if (deltaDistance.Count == discretization)
                {
                    deltaDistance.RemoveAt(discretization - 1);
                    deltaDistance.Add(distance);

                    //Check average
                    float sum = 0f;
                    foreach (var elem in deltaDistance)
                    {
                        sum += elem;
                    }
                    float average = sum / deltaDistance.Count;
                    //If the average is lower than minimum threshold for movement
                    if (average < distanceThreshold)
                    {
                        //Send appropriate object / message
                        InactivityEvent inactivity = new InactivityEvent(actorRef);
                        ScriptPlaceholder.BroadcastMessage("InactivityDetected", inactivity);
                        Visualizer.messageText.text = "Inactivity info: owner:" + actorRef + ", average distance:" + average
                            + ", time:" + System.DateTime.Now.ToLongTimeString();

                        //Re-init the list (wait for discretization steps again)
                        deltaDistance = new List<float>();
                    }
                }
                //Build the list if not reached the discretization level
                else
                {
                    deltaDistance.Add(distance);
                }
            });
    }
    /*
    void Update ()
    {
        Vector3 currPos = transform.position;
        float distance = (currPos - prevPos).magnitude;
        if(deltaDistance.Count == discretization)
        {
            deltaDistance.RemoveAt(discretization - 1);
            deltaDistance.Add(distance);

            //Check average
            float sum = 0f;
            foreach (var elem in deltaDistance)
            {
                sum += elem;
            }
            float average = sum / deltaDistance.Count;
            //If the average is lower than minimum threshold for movement
            if(average < distanceThreshold)
            {
                //Send appropriate object / message
                InactivityEvent inactivity = new InactivityEvent(actorRef);
                ScriptPlaceholder.BroadcastMessage("InactivityDetected", inactivity);
                Visualizer.messageText.text = "Inactivity info: owner:" + actorRef + ", average distance:" + average 
                    + ", time:" + System.DateTime.Now.ToLongTimeString();

                //Re-init the list (wait for discretization steps again)
                deltaDistance = new List<float>();
            }
        }
        //Build the list if not reached the discretization level
        else
        {
            deltaDistance.Add(distance);
        }
	}
    */
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PedestrianCrossingDelay : ActorBehavior
{
    // Start is called before the first frame update
    void Start()
    {
        if (actorRef == null)
        {
            throw new System.Exception("Some essential properties of LaneChangeBehavior are null");
        }

        behaviorID = "PedestrianCrossingDelay";

        Behavior();

    }

    public override void Behavior()
    {
        Pedestrian pedestrianRef = (Pedestrian)actorRef;
        PedestrianObject go = referenceGO.GetComponent<PedestrianObject>();

        go.m_startMovingDelayMin = pedestrianRef._timeDelay;
        go.m_startMovingDelayMin = pedestrianRef._timeDelay;

        go.GetComponent<PedestrianObject>().isLocked = false;
    }

    
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class PedestrianCrossingBehavior02 : ActorBehavior
{
    // Start is called before the first frame update
    void Start()
    {
        if (actorRef == null)
        {
            throw new System.Exception("Some essential properties of LaneChangeBehavior are null");
        }

        behaviorID = "PedestrianCrossingBehavior02";

        Behavior();

    }

    public override void Behavior()
    {
        Pedestrian pedestrianRef = (Pedestrian)actorRef;
        PedestrianObject go = referenceGO.GetComponent<PedestrianObject>();
        if (pedestrianRef._actorOfInterest != null)
        {
            //Reactive behavior- when to start crossing
            pedestrianRef._actorOfInterest._livePos.ObserveEveryValueChanged(x => x.Count)
                .Where(x => x > 0)
                .RepeatUntilDestroy(this)
                .Subscribe(x =>
                {
                    float pos = (transform.position - pedestrianRef._actorOfInterest._livePos[x - 1]).magnitude;
                    if (pos < pedestrianRef._euclideanDistance && go.GetComponent<PedestrianObject>().isLocked)
                    {
                        go.GetComponent<PedestrianObject>().isLocked = false;
                        //Send a message to the ScriptPlaceholder. There may be scripts who would like to hear about this
                        if (uncalled)
                        {
                            ScriptPlaceholder.BroadcastMessage("BehaviorDetected", behaviorID);
                            uncalled = false;
                        }

                    }
                    else
                        go.GetComponent<PedestrianObject>().isLocked = true;


                });
        }
        else
            go.GetComponent<PedestrianObject>().isLocked = false;
    }



}

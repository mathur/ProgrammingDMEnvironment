﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ActorBehavior : MonoBehaviour
{

    // Reference to the ego vehicle
    public TestActor actorRef;
    public GameObject referenceGO;
    public string behaviorID; //used to track for behavioral coverage fuzzing
    public bool uncalled = true;

    public GameObject ScriptPlaceholder; // A reference to this is required and set on attachment to g.o.

    // Start is called before the first frame update
    void Start()
    {
           
    }

    public virtual void Behavior()
    {
        throw new System.Exception("Cannot call this funciton");
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Reactive extensions
using UniRx;
using UniRx.Triggers;

public class LaneChangeBehavior : ActorBehavior
{
    // Start is called before the first frame update
    void Start()
    {
        if (actorRef == null)
        {
            throw new System.Exception("Some essential properties of LaneChangeBehavior are null");
        }


    }

    private void Update()
    {
        //Debug.Log(actorRef._livePos[actorRef._livePos.Count - 1]);

        if((actorRef._livePos[actorRef._livePos.Count - 1].z - new Vector3(6,0.5f,120).z) < .1)
        {
            //Debug.Log("CHANGING");
            EnvironmentModel.testActorList[actorRef].GetComponent<TrafficSystemVehicle>().m_randomLaneChange = 1;

        }
        else
        {
            //Debug.Log("!!NOT CHANGING!!");
            EnvironmentModel.testActorList[actorRef].GetComponent<TrafficSystemVehicle>().m_randomLaneChange = 0;
        }

    }

    public override void Behavior()
    {
        //Vector3 targetPos = new Vector3(0, 0, 0);

        //if(car._livePos[car._livePos.Count - 1] == targetPos)
        //{
        //    //this function probably goes somewhere else, but I just stuck it in the Visualize_XML script for now
        //    visualize.ChangeLane((AIVehicle)car);
        //}
    }

}

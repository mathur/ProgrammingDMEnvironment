﻿/*
 * This script is attached as a component to relevant Actors, typically an ego car.
 * It detects collision of the ego car with other actors or environment objects.
 * 
 *  */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Reactive extensions
using UniRx; 
using UniRx.Triggers;

public class LiveActorCollisionCheck : LiveActorCheck
{
    // Use this for initialization
    void Start ()
    {
	    if(actorRef == null)
        {
            throw new System.Exception("Some essential properties of LiveActorCollisionCheck are null");
        }

        //Reactive collision check
        this.OnCollisionEnterAsObservable()
            .Subscribe(col => 
            {
                if (!col.gameObject.CompareTag("Terrain")) //A relevant collision
                {
                    //Create a new collision event
                    CollisionEvent collision = new CollisionEvent(owner: actorRef, suspect: col.gameObject.name, collisionVelocity: col.relativeVelocity);
                    //Add the event to the actor's list of events
                    actorRef._events.Add(collision);

                    //Send a message to the ScriptPlaceholder. There may be scripts who would like to hear about this
                    ScriptPlaceholder.BroadcastMessage("CollisionDetected", collision);
                    Visualizer.messageText.text = "Collision info: owner:" + actorRef + ", suspect: " + col.gameObject.name +
                        ", collisionVelocity:" + col.relativeVelocity + "time:" + System.DateTime.Now.ToLongTimeString();
                }
                else
                {
                    Debug.Log("Hit the road!!");
                }
            });
	}
    /*
    //Detect relevant collision
    void OnCollisionEnter(Collision col)
    {
        if (!col.gameObject.CompareTag("Terrain")) //A relevant collision
        {
            //Create a new collision event
            CollisionEvent collision = new CollisionEvent(owner: actorRef, suspect: col.gameObject.name, collisionVelocity: col.relativeVelocity);
            //Add the event to the actor's list of events
            actorRef._events.Add(collision);
            //Debug statement
            //Debug.Log("Collision info: " + JsonUtility.ToJson(actorRef._events[actorRef._events.Count - 1]));
            //Send a message to the ScriptPlaceholder. There may be scripts who would like to hear about this
            ScriptPlaceholder.BroadcastMessage("CollisionDetected", collision);
            Visualizer.messageText.text = "Collision info: owner:" + actorRef + ", suspect: " + col.gameObject.name + 
                ", collisionVelocity:" + col.relativeVelocity + "time:" + System.DateTime.Now.ToLongTimeString();
        }
        else
        {
            Debug.Log("Hit the road!!");
        }
    }
    */

}

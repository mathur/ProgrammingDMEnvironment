﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System;
using System.IO;
using System.Windows;
using Microsoft.Win32;
using System.Text;
using UnityEditor;
using UnityEngine.SceneManagement;

public class Visualizer_withDepthMap : MonoBehaviour
{
    public static StringBuilder sb = new StringBuilder();

    //make sure the correct environment script is loaded prior to running the program!
    #region SETUP/INITIALIZATION
    #region variables
    //dictionary that stores all road element prefabs and zoning prefabs
    public Dictionary<string, GameObject> roadPrefabDictionary = new Dictionary<string, GameObject>();

    //stores the counts for each type of zoning prefab so that they can be randomly spawned
    public int treesPrefabCount;
    public int residentialPrefabCount;

    //stores the prefabs for the ai cars, pedestrians, and ego car
    public TrafficSystemVehicle carPrefab;
    public PedestrianObject pedestrianPrefab;
    public GameObject egoCarPrefab;
    public GameObject autonomousEgoCarPrefab;
    public GameObject trainingEgoCarPrefab;

    //references the environment program so that the MakeView and EnvironmentSettings functions can be properly referenced
    public EnvironmentProgramBaseClass PlaceHolderForEnvironmentProgram;

    //UI variables
    public GameObject newCanvas;    //stores the canvas so that sliders and text can be added to it
    public GameObject canvasPrefab; //prefab for canvas
    public static Text messageText; //stores the message text UI element so that it can be updated
    public GameObject sliderPrefab; //prefab for slider
    public GameObject textPrefab;   //prefab for text
    public Button buttonPrefab;     //prefab for a button

    //Variables that are used elsewhere
    public TestActor egoCarRef; // Reference to the relevant actor that is under test

    //define the relative physical connection points
    //NORTH is z, EAST is x, SOUTH is -z, WEST is -x
    public enum RelativePhysicalConnectionLocation { NONE, NORTH, EAST, SOUTH, WEST };

    //define the zone numbers to specify zoning
    //The zoning describes the zone to the left of the connection point
    //Ex: zone1 describes the area to the left of CONN_1
    public enum ZoneNumber { NONE, ONE, TWO, THREE, FOUR }

    #endregion

    #region awake function
    private void Awake() //this setup needs to happen in the Awake() because the EnvironmentProgram relies on it during its Start()
    {
        //Load environment script
        if (PlaceHolderForEnvironmentProgram == null)
        {
            //If not set in inspector, set PlaceHolderForEnvironmentProgram
            PlaceHolderForEnvironmentProgram = GetComponent<EnvironmentProgramBaseClass>();
        }

    }

    #endregion

    #endregion

    #region ROAD ELEMENT VISUALIZATION

    //creates the abstract road element as a physical game object
    public void visualizeRoadElement(RoadElement roadPiece)
    {
        //create the parent object for the road segment
        GameObject go = new GameObject(roadPiece._id);

        //add parenting
        go.transform.parent = TrafficSystem.Instance.transform;


        //Set Environment model vars 
        EnvironmentModel.roadElements.Add(roadPiece._id, roadPiece);
        EnvironmentModel.roadElementGOMapping.Add(roadPiece._id, go);

        //apply the position and rotation
        go.transform.position = roadPiece._pos;
        go.transform.localRotation = Quaternion.Euler(0f, (float)roadPiece._orientation, 0f);

        //adjust parenting if necessary
        if (roadPiece._children.Count > 0)
        {
            foreach (var child in roadPiece._children)
            {
                changeParenting(roadPiece, child);
            }
        }
    }

    //creates the visual representation of the intersection
    public void visualizeRoadElement(Intersection intersection)
    {
        //grab the intersection properties
        string nameOfRoad = intersection._id;
        int numLanes = intersection._numLanes;
        float zval = intersection._pos._z;
        float xval = intersection._pos._x;
        Orientation orientation = intersection._orientation;

        //create the parent object for the road segment
        GameObject go = new GameObject(nameOfRoad);

        //create the traffic system piece
        TrafficSystemPiece intersectionPiece;
        string lookupKey = intersection._roadElementType.ToString() + intersection._intersectionType.ToString() + numLanes;
        intersectionPiece = spawnRoadPiece(roadPrefabDictionary[lookupKey], new Vector3(0f, 0f, 0f), roadPrefabDictionary[lookupKey].transform.rotation, roadPrefabDictionary[lookupKey].name);
        intersectionPiece.transform.position = new Vector3(xval, 0f, zval);
        intersectionPiece.transform.parent = go.transform;
        //intersectionPiece.transform.parent = TrafficSystem.Instance.transform;

        //set orientation and position of entire segment
        go.transform.position = new Vector3(xval, 0f, zval);
        go.transform.localRotation = Quaternion.Euler(0f, (float)orientation, 0f);

        //set parenting so that the parent go is in the traffic system
        //if this doesn't happen, the traffic system will not work properly
        go.transform.parent = TrafficSystem.Instance.transform;

        //Set Environment model vars 
        EnvironmentModel.roadElements.Add(intersection._id, intersection);
        EnvironmentModel.roadElementGOMapping.Add(intersection._id, go);

        //implement the zoning
        visualizeRoadZone(intersection);
    }

    //creates the visual representation of the contiguous road
    public void visualizeRoadElement(RoadSegment contiguousRoad)
    {
        //grab the contiguous road properties
        string nameOfRoad = contiguousRoad._id;
        int numLanes = contiguousRoad._numLanes;
        int roadLength = contiguousRoad._lengthOfRoad;
        float zval = contiguousRoad._pos._z;
        float xval = contiguousRoad._pos._x;
        Orientation orientation = contiguousRoad._orientation;

        //create the parent object for the road segment
        GameObject go = new GameObject(nameOfRoad);

        //make the first piece
        //this is necessary because you can't line up the rest of the pieces correctly 
        //if there is no initial anchor piece that has been set
        List<TrafficSystemPiece> roadPieces = new List<TrafficSystemPiece>();
        string lookupKey = contiguousRoad._roadElementType.ToString() + contiguousRoad._contiguousRoadType.ToString() + numLanes;
        TrafficSystemPiece roadPieceCurrent = spawnRoadPiece(roadPrefabDictionary[lookupKey], new Vector3(0f, 0f, 0f), Quaternion.identity, "0 - " + roadPrefabDictionary[lookupKey].name);
        roadPieces.Add(roadPieceCurrent);
        TrafficSystemPiece roadPiecePrevious = roadPieceCurrent;


        //build the rest of the road based on the first piece
        for (int i = 1; i < roadLength; i++)
        {
            //create road piece
            roadPieceCurrent = spawnRoadPiece(roadPrefabDictionary[lookupKey], new Vector3(0f, 0f, 0f), Quaternion.identity, i + " - " + roadPrefabDictionary[lookupKey].name);
            roadPieces.Add(roadPieceCurrent);
            //format road piece to match the previous
            roadPieceCurrent.transform.position = getContiguousRoadPieceRelativePosition(roadPieceCurrent, roadPiecePrevious, PhysicalConnectionPointType.CONN_1);
            connectContiguousRoadPieces(roadPiecePrevious, roadPieceCurrent, numLanes);
            roadPiecePrevious = roadPieceCurrent;

        }

        //Set parenting for each element in the list
        foreach (var elem in roadPieces)
        {
            elem.transform.parent = go.transform;
        }

        //set orientation and position of entire segment
        go.transform.position = new Vector3(xval, 0f, zval);
        go.transform.localRotation = Quaternion.Euler(0f, (float)orientation, 0f);

        go.transform.parent = TrafficSystem.Instance.transform;

        //Set Environment model vars 
        EnvironmentModel.roadElements.Add(contiguousRoad._id, contiguousRoad);
        EnvironmentModel.roadElementGOMapping.Add(contiguousRoad._id, go);

        //implement the zoning
        visualizeRoadZone(contiguousRoad);

    }

    //this function is used to visualize the contiguous road by formatting the contiguous road segments such that they are next to each other.
    //the orientation of the contiguous road does not matter because the contiguous road is assembled, then any rotation is applied at the end.
    Vector3 getContiguousRoadPieceRelativePosition(TrafficSystemPiece a_currentPiece, TrafficSystemPiece a_previousPiece, PhysicalConnectionPointType a_roadAttachmentPointIndex = PhysicalConnectionPointType.CONN_1)
    {
        float m_roadPieceSize = 8;

        //set the 
        a_currentPiece.transform.position = a_previousPiece.transform.position;

        Vector3 pos = a_previousPiece.transform.position;

        if (a_currentPiece.m_renderer && a_previousPiece.m_renderer)
        {
            a_currentPiece.transform.position = a_previousPiece.m_renderer.transform.position;
            pos = a_previousPiece.m_renderer.transform.position;
        }

        float roadPieceSize = m_roadPieceSize;

        //calculate the offset based on the size of the traffic system piece
        if (a_previousPiece.m_renderer && a_currentPiece.m_renderer)
        {
            float anchorSize = a_previousPiece.GetRenderBounds().extents.z;
            if (TrafficSystem.Instance.m_swapAnchorDimensions)
                anchorSize = a_previousPiece.GetRenderBounds().extents.x;

            float currentSize = a_currentPiece.GetRenderBounds().extents.z;
            if (TrafficSystem.Instance.m_swapEditDimensions)
                currentSize = a_currentPiece.GetRenderBounds().extents.x;

            roadPieceSize = anchorSize + currentSize;
        }

        pos.z = a_previousPiece.m_renderer.transform.position.z + roadPieceSize;

        return pos;

    }

    //This function was borrowed from the TrafficSystemEditor
    //It spawns a traffic system piece and makes it a child of the traffic system
    //TODO: change this to GameObjects and grab TrafficSystemPiece right before instantiate
    TrafficSystemPiece spawnRoadPiece(GameObject roadPiecePrefab, Vector3 position, Quaternion rotation, string name)
    {
        //actually create the piece
        TrafficSystemPiece roadPiece = Instantiate(roadPiecePrefab.GetComponent<TrafficSystemPiece>(), position, rotation) as TrafficSystemPiece;

        //custom name for piece
        roadPiece.name = name;

        return roadPiece;
    }
    #endregion

    #region DESTROY FUNCTION
    //this function deletes the environment model elements that were visualized so that the view can be refreshed
    public void deleteView()
    {
        //delete the visualization of the road network
        foreach (GameObject temp in EnvironmentModel.roadElementGOMapping.Values)
        {
            Destroy(temp.gameObject);
        }
        EnvironmentModel.roadElementGOMapping.Clear();

        //clear the roadElements so that re-making the network does not cause duplication errors
        EnvironmentModel.roadElements.Clear();


        foreach (GameObject pedestrian in EnvironmentModel.testActorList.Values)
        {
            Destroy(pedestrian.gameObject);
        }
        EnvironmentModel.testActorList.Clear();


    }
    #endregion

    #region ROAD ELEMENT CONNECTION

    //this function takes the connection map and then manipulates the road elements to be connected
    //TODO: Have a better solution for the parenting and look into the callingRE/useKeyAsCallingRE thing. I think there's a less confusing way to do it
    //currently, the callingRE is the one calling the connectTo function. callingRE.connecctTo(midParent) and the top parent is the new abstract road element that will parent the connected road elements
    public void connectRoadElements(Dictionary<PhysicalConnection, PhysicalConnection> connMap, RoadElement callingRE, RoadElement topParent, RoadElement midParent, RoadElement child, bool useKeyAsCallingRE = false)
    {
        //set the position of the abstract parent element so that it is not arbitrarily placed
        topParent._pos = midParent._pos;
        EnvironmentModel.roadElementGOMapping[topParent._id].transform.position = EnvironmentModel.roadElementGOMapping[midParent._id].transform.position;
        //midParent._pos = Vector3.zero;
        //EnvironmentModel.roadElementGOMapping[midParent._id].transform.position = midParent._pos;

        //make the connections according to the dictionary entries
        foreach (KeyValuePair<PhysicalConnection, PhysicalConnection> entry in connMap)
        {
            //TODO: eventually this should be just one function, still need to work out how to do that smoothly.
            if (useKeyAsCallingRE)
            {
                connectRoadElement(entry.Key._belongsTo, entry.Key._typeOfConnectionPoint, entry.Value._belongsTo, entry.Value._typeOfConnectionPoint, entry.Key._belongsTo);
            }
            else
            {
                connectRoadElement(entry.Key._belongsTo, entry.Key._typeOfConnectionPoint, entry.Value._belongsTo, entry.Value._typeOfConnectionPoint, callingRE);
            }

        }

        //set the parenting
        changeParenting(midParent, child);
        changeParenting(topParent, midParent);

    }

    //this function has two parts: first it positions the road elements being connected. Then it makes the physical connection between the waypoints
    void connectRoadElement(RoadElement connectingRoad, PhysicalConnectionPointType connectingAttachPoint, RoadElement anchorRoad, PhysicalConnectionPointType anchorAttachPoint, RoadElement pieceInMotion)
    {
        //TODO: would it be better to have the ProDM values able to do this direct comparison more intuitively?
        if (connectingRoad._numLanes._valOneOf[connectingRoad._numLanes._index] == anchorRoad._numLanes._valOneOf[anchorRoad._numLanes._index])
        {
            //position them physically
            positionConnectedRoadElements(connectingRoad, connectingAttachPoint, anchorRoad, anchorAttachPoint, pieceInMotion);

            //physically connect them (extend the paths of the waypoints to connect)
            connectConnectedRoadElements(connectingRoad, connectingAttachPoint, anchorRoad, anchorAttachPoint);
        }
        else
        {
            throw new System.Exception("Cannot connect- Roads must have same number of lanes.");
        }

    }

    #region adjust physical location
    //this function changes the physical location of the road elements so that they are visually oriented properly after the connection
    //the piece in motion is the one actually being moved. 
    //For example, if we have RE1 which is a TIntersection and Contiguous road, and we want to connect 
    //the contiguous road to another intersection, the piece in motion is the RE1, which includes TIntersection and the Contiguous Road instead of moving only the Contigous Road
    void positionConnectedRoadElements(RoadElement fromA, PhysicalConnectionPointType attachFrom, RoadElement toB, PhysicalConnectionPointType attachTo, RoadElement pieceInMotion)
    {
        //get the trafficsystempiece at each connection point (being attached to and being attached from)
        TrafficSystemPiece tsp1 = getTrafficSystemPiece(fromA, attachFrom);
        TrafficSystemPiece tsp2 = getTrafficSystemPiece(toB, attachTo);

        //connect a contiguous road to an intersection
        if (fromA._roadElementType == RoadElementType.CONTIGUOUS_ROAD && toB._roadElementType == RoadElementType.INTERSECTION)
        {
            Orientation newOrientation = getContiguousRoadRotation(toB, attachTo, fromA, attachFrom);
            Quaternion rotOffset = (EnvironmentModel.roadElementGOMapping[pieceInMotion._id].transform.rotation) * Quaternion.Inverse(EnvironmentModel.roadElementGOMapping[fromA._id].transform.rotation);
            newOrientation = incrementOrientation(newOrientation, rotOffset.eulerAngles.y);
            setOrientation(pieceInMotion, newOrientation);

            Vector3 posOffset = tsp1.transform.TransformPoint(EnvironmentModel.roadElementGOMapping[fromA._parent._id].transform.position);

            RelativePhysicalConnectionLocation temp = getRelativePhysicalConnectionPoint(fromA._orientation, attachFrom, RoadElementType.CONTIGUOUS_ROAD);
            if (attachFrom == PhysicalConnectionPointType.CONN_1)
            {
                posOffset = -posOffset;
            }
            else if (temp == RelativePhysicalConnectionLocation.EAST)
            {
                posOffset.x = -posOffset.x;

            }

            Vector3 newPosition = getRoadPieceRelativePosition(tsp1, tsp2, toB, attachTo, fromA, attachFrom, posOffset);


            setPosition(newPosition, pieceInMotion);
        }
        //connect a contiguous road to a contiguous road
        else if (fromA._roadElementType == RoadElementType.CONTIGUOUS_ROAD && toB._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
        {
            Orientation newOrientation = getContiguousRoadRotation(toB, attachTo, fromA, attachFrom);
            Quaternion offset = (EnvironmentModel.roadElementGOMapping[pieceInMotion._id].transform.rotation) * Quaternion.Inverse(EnvironmentModel.roadElementGOMapping[fromA._id].transform.rotation);
            newOrientation = incrementOrientation(newOrientation, offset.eulerAngles.y);
            setOrientation(pieceInMotion, newOrientation);

            Vector3 posOffset = tsp1.transform.TransformPoint(EnvironmentModel.roadElementGOMapping[fromA._parent._id].transform.position);

            RelativePhysicalConnectionLocation temp = getRelativePhysicalConnectionPoint(fromA._orientation, attachFrom, RoadElementType.CONTIGUOUS_ROAD);
            if (temp == RelativePhysicalConnectionLocation.SOUTH)
            {

                if (fromA._numLanes == 6)
                {
                    posOffset = tsp1.transform.TransformPoint(EnvironmentModel.roadElementGOMapping[fromA._parent._id].transform.localPosition);
                    posOffset.z = -(Math.Abs(posOffset.z));
                }
                else if (fromA._numLanes == 4)
                {
                    posOffset = tsp1.transform.TransformPoint(EnvironmentModel.roadElementGOMapping[fromA._parent._id].transform.position);
                    posOffset.z = -(Math.Abs(posOffset.z));
                }
                else if (fromA._numLanes == 2)
                {
                    posOffset = tsp1.transform.TransformPoint(EnvironmentModel.roadElementGOMapping[fromA._parent._id].transform.position);
                    posOffset.z = -(Math.Abs(posOffset.z));
                }

                posOffset.x = 0;
            }
            else if (temp == RelativePhysicalConnectionLocation.EAST)
            {
                posOffset = tsp1.transform.TransformPoint(EnvironmentModel.roadElementGOMapping[fromA._parent._id].transform.position);
                posOffset.x = -posOffset.x;
            }

            Vector3 newPosition = getRoadPieceRelativePosition(tsp1, tsp2, toB, attachTo, fromA, attachFrom, posOffset);
            setPosition(newPosition, pieceInMotion);

        }
        //connect an intersection to a contiguous road
        else if (fromA._roadElementType == RoadElementType.INTERSECTION && toB._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
        {
            TrafficSystemPiece intersection = EnvironmentModel.roadElementGOMapping[fromA._id].transform.GetChild(0).GetComponent<TrafficSystemPiece>();

            Orientation newOrientation = getIntersectionRotation(toB, attachTo, (Intersection)fromA, attachFrom);
            Quaternion rotOffset = (EnvironmentModel.roadElementGOMapping[pieceInMotion._id].transform.rotation) * Quaternion.Inverse(EnvironmentModel.roadElementGOMapping[fromA._id].transform.rotation);
            newOrientation = incrementOrientation(newOrientation, rotOffset.eulerAngles.y);
            setOrientation(pieceInMotion, newOrientation);


            Vector3 posOffset = Vector3.zero;

            RelativePhysicalConnectionLocation temp = getRelativePhysicalConnectionPoint(fromA._orientation, attachFrom, RoadElementType.INTERSECTION);
            if (attachTo == PhysicalConnectionPointType.CONN_1)
            {
                posOffset = intersection.transform.TransformPoint(EnvironmentModel.roadElementGOMapping[fromA._parent._id].transform.position);
                posOffset = -posOffset;
            }
            if (temp == RelativePhysicalConnectionLocation.SOUTH)
            {

                if (fromA._numLanes == 6)
                {
                    posOffset = intersection.transform.TransformPoint(EnvironmentModel.roadElementGOMapping[fromA._parent._id].transform.localPosition);
                    posOffset.z = -(Math.Abs(posOffset.z));
                }
                else if (fromA._numLanes == 4)
                {
                    posOffset = intersection.transform.TransformPoint(EnvironmentModel.roadElementGOMapping[fromA._parent._id].transform.position);
                    posOffset.z = -(Math.Abs(posOffset.z));
                }
                else if (fromA._numLanes == 2)
                {
                    posOffset = intersection.transform.TransformPoint(EnvironmentModel.roadElementGOMapping[fromA._parent._id].transform.position);
                    posOffset.z = -(Math.Abs(posOffset.z));
                }

                posOffset.x = 0;
            }
            else if (temp == RelativePhysicalConnectionLocation.EAST)
            {
                posOffset = intersection.transform.TransformPoint(EnvironmentModel.roadElementGOMapping[fromA._parent._id].transform.position);
                posOffset.x = -posOffset.x;
            }
            //else if (temp == RelativePhysicalConnectionLocation.WEST)
            //{
            //    posOffset.x = -posOffset.x;
            //    Debug.Log("here west");
            //}
            //Debug.Log("offset " + posOffset);
            //Debug.Log("Experiment offset " + -intersection.transform.TransformPoint(EnvironmentModel.roadElementGOMapping[fromA._parent._id].transform.position));
            Vector3 newPosition = getRoadPieceRelativePosition(intersection, tsp2, toB, attachTo, fromA, attachFrom, posOffset);
            setPosition(newPosition, pieceInMotion);
        }
        //connect an intersection to an intersection
        else if (fromA._roadElementType == RoadElementType.INTERSECTION && toB._roadElementType == RoadElementType.INTERSECTION)
        {
            //still need to add this code.
            throw new System.Exception("Connection not fully implemented yet");
        }
        else
        {
            throw new System.Exception("Invalid connection");
        }

    }

    #region orientation/rotation
    //this functions figures out how much a contiguous road needs to be rotated to connect to another road element
    Orientation getContiguousRoadRotation(RoadElement anchorRoad, PhysicalConnectionPointType anchorAttachmentPoint, RoadElement connectingRoad, PhysicalConnectionPointType connectingAttachmentPoint)
    {

        //if the parent of the road is not the parent road element, you need to account for the parent rotation as well.
        RelativePhysicalConnectionLocation relativeOrientation;
        if (anchorRoad._parent._parent != null)
        {
            relativeOrientation = getRelativePhysicalConnectionPoint(anchorRoad._orientation, anchorAttachmentPoint, anchorRoad._roadElementType);
            Quaternion rotOffset = (EnvironmentModel.roadElementGOMapping[anchorRoad._parent._parent._id].transform.rotation) * Quaternion.Inverse(EnvironmentModel.roadElementGOMapping[anchorRoad._parent._id].transform.rotation);
            relativeOrientation = decrementRelativeOrientation(relativeOrientation, rotOffset.eulerAngles.y);
        }
        else
        {
            relativeOrientation = getRelativePhysicalConnectionPoint(anchorRoad._orientation, anchorAttachmentPoint, anchorRoad._roadElementType);
        }

        Orientation newOrientation = new Orientation();
        //depending on the connection point  of the connecting road, the orientation will have to change
        //this new orientation will also depend on the relative orientation of the anchor piece.
        //The anchor piece connection point and the connecting piece connection point need to line up for a proper connection
        if (anchorRoad._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
        {
            //fix rotation/orientation based on the relative position of the anchor connection point
            if (relativeOrientation == RelativePhysicalConnectionLocation.EAST || relativeOrientation == RelativePhysicalConnectionLocation.WEST)
            {
                if (connectingAttachmentPoint == anchorAttachmentPoint)
                {
                    switch (anchorRoad._orientation)
                    {
                        case Orientation.HORIZONTAL:
                            newOrientation = Orientation.HORIZONTAL_FLIPPED;
                            break;
                        case Orientation.HORIZONTAL_FLIPPED:
                            newOrientation = Orientation.HORIZONTAL;
                            break;
                    }
                }
                else //attachFrom == PhysicalConnectionPointType.CONN_1
                {
                    newOrientation = anchorRoad._orientation;
                }

            }
            else if (relativeOrientation == RelativePhysicalConnectionLocation.SOUTH || relativeOrientation == RelativePhysicalConnectionLocation.NORTH)
            {
                if (connectingAttachmentPoint == anchorAttachmentPoint)
                {
                    switch (anchorRoad._orientation)
                    {
                        case Orientation.VERTICAL:
                            newOrientation = Orientation.VERTICAL_FLIPPED;
                            break;
                        case Orientation.VERTICAL_FLIPPED:
                            newOrientation = Orientation.VERTICAL;
                            break;
                    }
                }
                else //attachFrom == PhysicalConnectionPointType.CONN_1
                {
                    newOrientation = anchorRoad._orientation;
                }

            }
            else
            {
                //do nothing - ?
            }
        }
        else if (anchorRoad._roadElementType == RoadElementType.INTERSECTION)
        {
            if (relativeOrientation == RelativePhysicalConnectionLocation.EAST)
            {
                //newOrientation = Orientation.HORIZONTAL;
                if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_2)
                {
                    newOrientation = Orientation.HORIZONTAL;
                    //contiguousRoad._orientation = Orientation.HORIZONTAL;
                }
                else //attachFrom == PhysicalConnectionPointType.CONN_1
                {
                    newOrientation = Orientation.HORIZONTAL_FLIPPED;
                    //contiguousRoad._orientation = Orientation.HORIZONTAL_FLIPPED;
                }

            }
            else if (relativeOrientation == RelativePhysicalConnectionLocation.SOUTH)
            {
                //newOrientation = Orientation.VERTICAL_FLIPPED;
                if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_2)
                {
                    newOrientation = Orientation.VERTICAL_FLIPPED;
                    //contiguousRoad._orientation = Orientation.VERTICAL_FLIPPED;
                }
                else //attachFrom == PhysicalConnectionPointType.CONN_1
                {
                    newOrientation = Orientation.VERTICAL;
                    //contiguousRoad._orientation = Orientation.VERTICAL;
                }

            }
            else if (relativeOrientation == RelativePhysicalConnectionLocation.WEST)
            {
                //newOrientation = Orientation.HORIZONTAL_FLIPPED;
                if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_2)
                {
                    newOrientation = Orientation.HORIZONTAL_FLIPPED;
                    //contiguousRoad._orientation = Orientation.HORIZONTAL_FLIPPED;
                }
                else //attachFrom == PhysicalConnectionPointType.CONN_1
                {
                    newOrientation = Orientation.HORIZONTAL;
                    //contiguousRoad._orientation = Orientation.HORIZONTAL;
                }

            }
            else if (relativeOrientation == RelativePhysicalConnectionLocation.NORTH)
            {
                //newOrientation = Orientation.VERTICAL;
                if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_2)
                {
                    newOrientation = Orientation.VERTICAL;
                    //contiguousRoad._orientation = Orientation.VERTICAL;
                }
                else //attachFrom == PhysicalConnectionPointType.CONN_1
                {
                    newOrientation = Orientation.VERTICAL_FLIPPED;
                    //contiguousRoad._orientation = Orientation.VERTICAL_FLIPPED;
                }

            }
            else
            {
                //do nothing - ?
            }
        }

        return newOrientation;
    }

    //this functions figures out how much an intersection needs to be rotated to connect to another road element
    Orientation getIntersectionRotation(RoadElement anchorRoad, PhysicalConnectionPointType anchorAttachmentPoint, Intersection connectingRoad, PhysicalConnectionPointType connectingAttachmentPoint)
    {
        RelativePhysicalConnectionLocation relativeOrientation;
        //if the parent of the road is not the parent road element, you need to account for the parent rotation as well.
        if (anchorRoad._parent._parent != null)
        {
            Quaternion rotOffset = (EnvironmentModel.roadElementGOMapping[anchorRoad._parent._parent._id].transform.rotation) * Quaternion.Inverse(EnvironmentModel.roadElementGOMapping[anchorRoad._parent._id].transform.rotation);
            Orientation orien = decrementOrientation(anchorRoad._orientation, rotOffset.eulerAngles.y);
            relativeOrientation = getRelativePhysicalConnectionPoint(orien, anchorAttachmentPoint, anchorRoad._roadElementType);
        }
        else
        {
            relativeOrientation = getRelativePhysicalConnectionPoint(anchorRoad._orientation, anchorAttachmentPoint, anchorRoad._roadElementType);
        }

        Orientation newOrientation = new Orientation();

        //depending on the connection point  of the connecting road, the orientation will have to change
        //this new orientation will also depend on the relative orientation of the anchor piece.
        //The anchor piece connection point and the connecting piece connection point need to line up for a proper connection
        if (connectingRoad._intersectionType != IntersectionType.NONE)
        {
            //fix rotation/orientation
            if (relativeOrientation == RelativePhysicalConnectionLocation.EAST)
            {
                if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_4 && connectingRoad._intersectionType != IntersectionType.T_INTERSECTION)
                {
                    //orientation = HORIZONTAL_FLIPPED
                    newOrientation = Orientation.HORIZONTAL_FLIPPED;
                }
                else if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_3)
                {
                    //orientation = VERTICAL
                    newOrientation = Orientation.VERTICAL;
                }
                else if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_2)
                {
                    //orientation = HORIZONTAL
                    newOrientation = Orientation.HORIZONTAL;
                }
                else //attachFrom == PhysicalConnectionPointType.CONN_1
                {
                    //orientation = VERTICAL_FLIPPED
                    newOrientation = Orientation.VERTICAL_FLIPPED;
                }

            }
            else if (relativeOrientation == RelativePhysicalConnectionLocation.SOUTH)
            {
                if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_4 && connectingRoad._intersectionType != IntersectionType.T_INTERSECTION)
                {
                    //orientation = VERTICAL
                    newOrientation = Orientation.VERTICAL;
                }
                else if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_3)
                {
                    //orientation = HORIZONTAL
                    newOrientation = Orientation.HORIZONTAL;
                }
                else if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_2)
                {
                    //orientation = VERTICAL_FLIPPED
                    newOrientation = Orientation.VERTICAL_FLIPPED;
                }
                else //attachFrom == PhysicalConnectionPointType.CONN_1
                {
                    //orientation = HORIZONTAL_FLIPPED
                    newOrientation = Orientation.HORIZONTAL_FLIPPED;
                }

            }
            else if (relativeOrientation == RelativePhysicalConnectionLocation.WEST)
            {
                if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_4 && connectingRoad._intersectionType != IntersectionType.T_INTERSECTION)
                {
                    //orientation = HORIZONTAL
                    newOrientation = Orientation.HORIZONTAL;
                }
                else if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_3)
                {
                    //orientation = VERTICAL_FLIPPED
                    newOrientation = Orientation.VERTICAL_FLIPPED;
                }
                else if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_2)
                {
                    //orientation = HORIZONTAL_FLIPPED
                    newOrientation = Orientation.HORIZONTAL_FLIPPED;
                }
                else //attachFrom == PhysicalConnectionPointType.CONN_1
                {
                    //orientation = VERTICAL
                    newOrientation = Orientation.VERTICAL;
                }

            }
            else if (relativeOrientation == RelativePhysicalConnectionLocation.NORTH)
            {
                if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_4 && connectingRoad._intersectionType != IntersectionType.T_INTERSECTION)
                {
                    //orientation = VERTICAL_FLIPPED
                    newOrientation = Orientation.VERTICAL_FLIPPED;
                }
                else if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_3)
                {
                    //orientation = HORIZONTAL_FLIPPED
                    newOrientation = Orientation.HORIZONTAL_FLIPPED;
                }
                else if (connectingAttachmentPoint == PhysicalConnectionPointType.CONN_2)
                {
                    //orientation = VERTICAL
                    newOrientation = Orientation.VERTICAL;
                }
                else //attachFrom == PhysicalConnectionPointType.CONN_1
                {
                    //orientation = HORIZONTAL
                    newOrientation = Orientation.HORIZONTAL;
                }

            }
            else
            {
                //do nothing - ?
            }

        }
        else
        {
            //shrug
        }

        //return the new orientation for the intersection
        return newOrientation;
    }

    //this funciton will rotate counterclockwise the relative orientation based on the offset angle. 
    //An offset angle of 270 yields an increment counter of 3 so if it starts as Vertical, it will go Horizontal, Vertical_flip, Horizontal_flip and return Horizontal_flip
    Orientation incrementOrientation(Orientation startOrientation, float offset)
    {
        int num = (int)offset / 90;

        Orientation newOrientation = startOrientation;

        for (int i = 0; i < num; i++)
        {
            switch (newOrientation)
            {
                case Orientation.VERTICAL:
                    newOrientation = Orientation.HORIZONTAL;
                    break;
                case Orientation.HORIZONTAL:
                    newOrientation = Orientation.VERTICAL_FLIPPED;
                    break;
                case Orientation.VERTICAL_FLIPPED:
                    newOrientation = Orientation.HORIZONTAL_FLIPPED;
                    break;
                case Orientation.HORIZONTAL_FLIPPED:
                    newOrientation = Orientation.VERTICAL;
                    break;
            }

        }

        return newOrientation;

    }

    //this funciton will rotate counterclockwise the relative orientation based on the offset angle. 
    //An offset angle of 270 yields a decrement counter of 3 so if it starts as Vertical, it will go Horizontal_flip, Vertical_flip, Horizontal and return Horizontal
    Orientation decrementOrientation(Orientation startOrientation, float offset)
    {
        int num = (int)offset / 90;

        Orientation newOrientation = startOrientation;

        for (int i = 0; i < num; i++)
        {
            switch (newOrientation)
            {
                case Orientation.VERTICAL:
                    newOrientation = Orientation.HORIZONTAL_FLIPPED;
                    break;
                case Orientation.HORIZONTAL:
                    newOrientation = Orientation.VERTICAL;
                    break;
                case Orientation.VERTICAL_FLIPPED:
                    newOrientation = Orientation.HORIZONTAL;
                    break;
                case Orientation.HORIZONTAL_FLIPPED:
                    newOrientation = Orientation.VERTICAL_FLIPPED;
                    break;
            }

        }

        return newOrientation;

    }

    //this funciton will rotate counterclockwise the relative orientation based on the offset angle. 
    //An offset angle of 270 yields a decrement counter of 3 so if it starts as North, it will go West, South, East and return East
    RelativePhysicalConnectionLocation decrementRelativeOrientation(RelativePhysicalConnectionLocation startOrientation, float offset)
    {
        int num = (int)offset / 90;

        RelativePhysicalConnectionLocation newOrientation = startOrientation;

        for (int i = 0; i < num; i++)
        {
            switch (newOrientation)
            {
                case RelativePhysicalConnectionLocation.NORTH:
                    newOrientation = RelativePhysicalConnectionLocation.WEST;
                    break;
                case RelativePhysicalConnectionLocation.EAST:
                    newOrientation = RelativePhysicalConnectionLocation.NORTH;
                    break;
                case RelativePhysicalConnectionLocation.SOUTH:
                    newOrientation = RelativePhysicalConnectionLocation.EAST;
                    break;
                case RelativePhysicalConnectionLocation.WEST:
                    newOrientation = RelativePhysicalConnectionLocation.SOUTH;
                    break;
            }

        }

        return newOrientation;

    }

    //this function sets an orientation for a road element, including changing its rotation and setting the road element orientation property
    void setOrientation(RoadElement roadElement, Orientation newOrientation)
    {
        //get the parent object for the road segment
        GameObject go = EnvironmentModel.roadElementGOMapping[roadElement._id];
        Quaternion newRotation = new Quaternion();

        switch (newOrientation)
        {
            case Orientation.VERTICAL:
                {
                    //orientation = VERTICAL
                    roadElement._orientation = Orientation.VERTICAL;
                    //rotation = attachTo_rotation
                    newRotation = Quaternion.Euler(0f, 0f, 0f);
                    break;

                }
            case Orientation.HORIZONTAL:
                {
                    //orientation = HORIZONTAL
                    roadElement._orientation = Orientation.HORIZONTAL;
                    //rotation = attachTo_rotation + 90
                    newRotation = Quaternion.Euler(0f, 90f, 0f);
                    break;
                }
            case Orientation.VERTICAL_FLIPPED:
                {
                    //orientation = VERTICAL_FLIPPED
                    roadElement._orientation = Orientation.VERTICAL_FLIPPED;
                    //rotation = attachTo_rotation + 180
                    newRotation = Quaternion.Euler(0f, 180f, 0f);
                    break;
                }
            case Orientation.HORIZONTAL_FLIPPED:
                {
                    //orientation = HORIZONTAL_FLIPPED
                    roadElement._orientation = Orientation.HORIZONTAL_FLIPPED;
                    //rotation = attachTo_rotation + 270
                    newRotation = Quaternion.Euler(0f, 270f, 0f);
                    break;
                }
        }

        go.transform.localRotation = newRotation;
        roadElement._pos = go.transform.position;
    }

    //this function looks at a road element's orientation and the attachment point and figures out how that is orientated in the x,y,z axis
    //the result is a relative physical connection location where EAST is the +x axis and NORTH is the +z axis
    RelativePhysicalConnectionLocation getRelativePhysicalConnectionPoint(Orientation baseOrientation, PhysicalConnectionPointType attachmentPoint, RoadElementType roadElementType)
    {
        RelativePhysicalConnectionLocation relativeOrientation = RelativePhysicalConnectionLocation.EAST;

        switch (roadElementType)
        {
            case RoadElementType.INTERSECTION:
                {
                    //account for piece rotation (make positioning absolute instead of relative)
                    switch (baseOrientation)
                    {
                        case Orientation.VERTICAL:
                            //x=x z=z
                            switch (attachmentPoint)
                            {
                                case PhysicalConnectionPointType.NONE:
                                    {
                                        //eventually do something, probably
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_1:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.EAST;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_2:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.SOUTH;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_3:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.WEST;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_4:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.NORTH;
                                    }
                                    break;
                            }
                            break;
                        case Orientation.HORIZONTAL:
                            //x=-z z=x
                            switch (attachmentPoint)
                            {
                                case PhysicalConnectionPointType.NONE:
                                    {
                                        //eventually do something, probably
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_1:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.SOUTH;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_2:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.WEST;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_3:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.NORTH;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_4:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.EAST;
                                    }
                                    break;
                            }
                            break;
                        case Orientation.VERTICAL_FLIPPED:
                            //x=-x z=-z
                            switch (attachmentPoint)
                            {
                                case PhysicalConnectionPointType.NONE:
                                    {
                                        //eventually do something, probably
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_1:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.WEST;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_2:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.NORTH;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_3:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.EAST;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_4:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.SOUTH;
                                    }
                                    break;
                            }
                            break;
                        case Orientation.HORIZONTAL_FLIPPED:
                            //x=z z=-x
                            switch (attachmentPoint)
                            {
                                case PhysicalConnectionPointType.NONE:
                                    {
                                        //eventually do something, probably
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_1:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.NORTH;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_2:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.EAST;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_3:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.SOUTH;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_4:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.WEST;
                                    }
                                    break;
                            }
                            break;
                    }
                    break;
                }
            case RoadElementType.CONTIGUOUS_ROAD:
                {
                    //account for piece rotation (make positioning absolute instead of relative)
                    switch (baseOrientation)
                    {
                        case Orientation.VERTICAL:
                            //x=x z=z
                            switch (attachmentPoint)
                            {
                                case PhysicalConnectionPointType.NONE:
                                    {
                                        //eventually do something, probably
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_1:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.NORTH;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_2:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.SOUTH;
                                    }
                                    break;
                            }
                            break;
                        case Orientation.HORIZONTAL:
                            //x=-z z=x
                            switch (attachmentPoint)
                            {
                                case PhysicalConnectionPointType.NONE:
                                    {
                                        //eventually do something, probably
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_1:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.EAST;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_2:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.WEST;
                                    }
                                    break;
                            }
                            break;
                        case Orientation.VERTICAL_FLIPPED:
                            //x=-x z=-z
                            switch (attachmentPoint)
                            {
                                case PhysicalConnectionPointType.NONE:
                                    {
                                        //eventually do something, probably
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_1:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.SOUTH;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_2:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.NORTH;
                                    }
                                    break;
                            }
                            break;
                        case Orientation.HORIZONTAL_FLIPPED:
                            //x=z z=-x
                            switch (attachmentPoint)
                            {
                                case PhysicalConnectionPointType.NONE:
                                    {
                                        //eventually do something, probably
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_1:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.WEST;
                                    }
                                    break;
                                case PhysicalConnectionPointType.CONN_2:
                                    {
                                        relativeOrientation = RelativePhysicalConnectionLocation.EAST;
                                    }
                                    break;
                            }
                            break;
                    }
                    break;
                }
            case RoadElementType.NONE:
                {
                    //probbaly do something
                    break;
                }
        }


        return relativeOrientation;
    }
    #endregion

    #region coordinates
    //this function returns the appropriate new coordinates a road element should be moved to to connect to another road element
    //it was borrowed from the TrafficSystemEditor code with some modification to return only the coordinates and not actually move the traffic system piece
    public Vector3 getRoadPieceRelativePosition(TrafficSystemPiece a_currentPiece, TrafficSystemPiece a_attachToPiece, RoadElement a_attachToRoadElement, PhysicalConnectionPointType a_roadAttachmentPointIndex, RoadElement a_attachFromRoadElement, PhysicalConnectionPointType a_roadAttachFromPoint, Vector3 offset)
    {
        //define the default road piece size
        float m_roadPieceSize = 8;

        //return zeros if there is no current piece or attach to piece
        if (!a_currentPiece)
            return new Vector3(0f, 0f, 0f);

        if (!a_attachToPiece)
            return new Vector3(0f, 0f, 0f);

        Vector3 pos = a_attachToPiece.transform.position;

        //if the parent of the road is not the parent road element, you need to account for the parent position as well.
        RelativePhysicalConnectionLocation a_pseudoAttachmentPoint;
        if (a_attachToRoadElement._parent._parent != null)
        {
            a_pseudoAttachmentPoint = getRelativePhysicalConnectionPoint(a_attachToRoadElement._orientation, a_roadAttachmentPointIndex, a_attachToRoadElement._roadElementType);
            Quaternion rotOffset = (EnvironmentModel.roadElementGOMapping[a_attachToRoadElement._parent._parent._id].transform.rotation) * Quaternion.Inverse(EnvironmentModel.roadElementGOMapping[a_attachToRoadElement._parent._id].transform.rotation);
            a_pseudoAttachmentPoint = decrementRelativeOrientation(a_pseudoAttachmentPoint, rotOffset.eulerAngles.y);
        }
        else
        {
            a_pseudoAttachmentPoint = getRelativePhysicalConnectionPoint(a_attachToRoadElement._orientation, a_roadAttachmentPointIndex, a_attachToRoadElement._roadElementType);
        }


        float roadPieceSize = m_roadPieceSize;

        //based on the relative attachment point will determine what axis you need to be manipulating. 
        //For example, to connect to the east, you need to move +x
        switch (a_pseudoAttachmentPoint)
        {
            case RelativePhysicalConnectionLocation.NONE:
                {
                    //eventually do something, probably
                }
                break;
            case RelativePhysicalConnectionLocation.EAST:
                {
                    if (a_attachToPiece.m_renderer && a_currentPiece.m_renderer)
                    {
                        float anchorSize = a_attachToPiece.GetRenderBounds().extents.x;
                        if (TrafficSystem.Instance.m_swapAnchorDimensions)
                            anchorSize = a_attachToPiece.GetRenderBounds().extents.z;

                        float currentSize = a_currentPiece.GetRenderBounds().extents.x;
                        if (TrafficSystem.Instance.m_swapEditDimensions)
                            currentSize = a_currentPiece.GetRenderBounds().extents.z;

                        roadPieceSize = anchorSize + currentSize;


                    }
                    pos.x = pos.x + roadPieceSize;
                    pos = pos + offset;
                }
                break;
            case RelativePhysicalConnectionLocation.SOUTH:
                {
                    if (a_attachToPiece.m_renderer && a_currentPiece.m_renderer)
                    {
                        float anchorSize = a_attachToPiece.GetRenderBounds().extents.z;
                        if (TrafficSystem.Instance.m_swapAnchorDimensions)
                            anchorSize = a_attachToPiece.GetRenderBounds().extents.x;

                        float currentSize = a_currentPiece.GetRenderBounds().extents.z;
                        if (TrafficSystem.Instance.m_swapEditDimensions)
                            currentSize = a_currentPiece.GetRenderBounds().extents.x;

                        roadPieceSize = anchorSize + currentSize;
                    }
                    pos.z = pos.z - roadPieceSize;
                    pos = pos - offset;
                }
                break;
            case RelativePhysicalConnectionLocation.WEST:
                {
                    if (a_attachToPiece.m_renderer && a_currentPiece.m_renderer)
                    {
                        float anchorSize = a_attachToPiece.GetRenderBounds().extents.x;
                        if (TrafficSystem.Instance.m_swapAnchorDimensions)
                            anchorSize = a_attachToPiece.GetRenderBounds().extents.z;

                        float currentSize = a_currentPiece.GetRenderBounds().extents.x;
                        if (TrafficSystem.Instance.m_swapEditDimensions)
                            currentSize = a_currentPiece.GetRenderBounds().extents.z;

                        roadPieceSize = anchorSize + currentSize;
                    }
                    pos.x = pos.x - roadPieceSize;
                    pos = pos - offset;
                }
                break;
            case RelativePhysicalConnectionLocation.NORTH:
                {
                    if (a_attachToPiece.m_renderer && a_currentPiece.m_renderer)
                    {
                        float anchorSize = a_attachToPiece.GetRenderBounds().extents.z;
                        if (TrafficSystem.Instance.m_swapAnchorDimensions)
                            anchorSize = a_attachToPiece.GetRenderBounds().extents.x;

                        float currentSize = a_currentPiece.GetRenderBounds().extents.z;
                        if (TrafficSystem.Instance.m_swapEditDimensions)
                            currentSize = a_currentPiece.GetRenderBounds().extents.x;

                        roadPieceSize = anchorSize + currentSize;
                    }

                    pos.z = pos.z + roadPieceSize;
                    pos = pos + offset;


                }
                break;
        }

        Vector3 posOffset = a_currentPiece.m_posOffset;


        //a_currentPiece.transform.position = pos;

        return pos;
    }

    //this function sets the position of a road element and updates the road element properties for position
    public void setPosition(Vector3 position, RoadElement piece)
    {
        //right now it looks like the position is being set globally

        //set the new physical location
        GameObject pieceGO = EnvironmentModel.roadElementGOMapping[piece._id];
        pieceGO.transform.position = position;

        //change these values in the Road Element
        piece._pos._x = position.x;
        piece._pos._z = position.z;
    }
    #endregion
    #endregion

    #region make physical connection
    //-------NOTE ON CONNECTIONS------------//
    //NOTE: The connections are all very complicated due to a number of factors. 
    //Depending on which ends of which kinds of road elements are being connected, sometimes the directions need switched or the left and right nodes need switched.
    //The functions for making these various connections are able to account for all of these factors,but they are very long and have many switch statements as a result

    //this function makes the physical connection between the road elements to enable proper traffic flow
    public void connectConnectedRoadElements(RoadElement fromA, PhysicalConnectionPointType attachFrom, RoadElement toB, PhysicalConnectionPointType attachTo)
    {
        //based on what kinds of elements you are connecting, you need to configure the connection differently. This function sets all that in motion
        if (fromA._roadElementType == RoadElementType.CONTIGUOUS_ROAD && toB._roadElementType == RoadElementType.INTERSECTION)
        {
            connectContiguousRoadToIntersection((RoadSegment)fromA, attachFrom, (Intersection)toB, attachTo);
        }
        else if (fromA._roadElementType == RoadElementType.CONTIGUOUS_ROAD && toB._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
        {
            //grab the traffic system pieces that need connected
            TrafficSystemPiece tsp1 = getTrafficSystemPiece(fromA, attachFrom);
            TrafficSystemPiece tsp2 = getTrafficSystemPiece(toB, attachTo);

            //figure out some connection details
            bool flipColors = false;
            bool flipDirection = false;
            if (attachFrom == attachTo)
            {
                flipColors = true;
            }
            if (attachFrom == PhysicalConnectionPointType.CONN_2)
            {
                flipDirection = true;
            }

            //make the connection
            connectContiguousRoadPieces(tsp1, tsp2, fromA._numLanes, flipColors, flipDirection);
        }
        else if (fromA._roadElementType == RoadElementType.INTERSECTION && toB._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
        {
            connectContiguousRoadToIntersection((RoadSegment)toB, attachTo, (Intersection)fromA, attachFrom);
        }
        else
        {
            throw new System.Exception("Invalid connection");
        }

    }


    //this function processes what kind of intersection a contiguous road is being connected to and calls the appropriate function to actually make the connection
    public void connectContiguousRoadToIntersection(RoadSegment contiguousRoad, PhysicalConnectionPointType contiguousRoadAttachPoint, Intersection intersection, PhysicalConnectionPointType intersectionAttachPoint)
    {
        //Again, the kind of intersection will determine how exactly the connection should be made.
        switch (intersection._intersectionType)
        {
            case IntersectionType.CROSS_INTERSECTION:
                connectContiguousRoadToCrossIntersection(contiguousRoad, contiguousRoadAttachPoint, (CrossIntersection)intersection, intersectionAttachPoint);
                break;
            case IntersectionType.ROUNDABOUT:
                connectContiguousRoadToRoundabout(contiguousRoad, contiguousRoadAttachPoint, (Roundabout)intersection, intersectionAttachPoint);
                break;
            case IntersectionType.T_INTERSECTION:
                connectContiguousRoadToTIntersection(contiguousRoad, contiguousRoadAttachPoint, (TIntersection)intersection, intersectionAttachPoint);
                break;
        }

    }

    //makes the connection between a contiguous road and cross intersection
    void connectContiguousRoadToCrossIntersection(RoadSegment contiguousRoad, PhysicalConnectionPointType contiguousRoadAttachPoint, CrossIntersection crossIntersection, PhysicalConnectionPointType intersectionAttachPoint)
    {
        //grab the traffic system pieces that need connected
        TrafficSystemPiece contiguousRoadSegment = getTrafficSystemPiece(contiguousRoad, contiguousRoadAttachPoint);
        TrafficSystemPiece intersectionSegment = getTrafficSystemPiece(crossIntersection, intersectionAttachPoint);

        //This section is a hack to connect the road traffic so that it flows properly. 
        //The left and right lanes are representing the blue (left) and pink (right) dots in the prefabs, 
        //and if the blue dots on the road piece don't line up with the blue dots on the intersection,
        //you have to cross the connection.
        bool flipColors = false;
        bool rightFirst = false;
        bool contiguousRoadFirst = false;
        switch (contiguousRoadAttachPoint)
        {
            case PhysicalConnectionPointType.CONN_1:
                {
                    if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        //flip
                        flipColors = true;
                        contiguousRoadFirst = false;
                        rightFirst = true;
                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        //flip
                        flipColors = false;
                        contiguousRoadFirst = true;
                        rightFirst = true;
                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_3)
                    {
                        //don't flip
                        flipColors = false;
                        contiguousRoadFirst = true;
                        rightFirst = true;
                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_4)
                    {
                        //don't flip
                        flipColors = true;
                        contiguousRoadFirst = false;
                        rightFirst = true;
                    }
                    else
                    {
                        //do nothing - ?
                    }
                    break;
                }

            case PhysicalConnectionPointType.CONN_2:
                {
                    if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        //flip
                        flipColors = false;
                        contiguousRoadFirst = false;
                        rightFirst = false;
                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        //flip
                        flipColors = true;
                        contiguousRoadFirst = true;
                        rightFirst = false;
                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_3)
                    {
                        //don't flip
                        flipColors = true;
                        contiguousRoadFirst = true;
                        rightFirst = false;
                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_4)
                    {
                        //don't flip
                        flipColors = false;
                        contiguousRoadFirst = false;
                        rightFirst = false;
                    }
                    else
                    {
                        //do nothing - ?
                    }
                    break;
                }
        }

        //determine the directional flow of traffic. The movement is anchor road --> connecting road
        TrafficSystemPiece anchorRoad;
        TrafficSystemPiece connectingRoad;
        if (contiguousRoadFirst)
        {
            anchorRoad = contiguousRoadSegment;
            connectingRoad = intersectionSegment;
        }
        else
        {
            anchorRoad = intersectionSegment;
            connectingRoad = contiguousRoadSegment;

        }

        //connect the nodes based on the number of lanes
        switch (contiguousRoad._numLanes)
        {
            //0 lane
            case 0:
                //do nothing
                break;

            //1 lane
            case 1:
                //do nothing
                break;

            //2 lane
            case 2:

                //This section is a hack to connect the road traffic so that it flows properly. 
                //The left and right lanes are representing the blue (left) and pink (right) dots in the prefabs, 
                //and if the blue dots on the road piece don't line up with the blue dots on the intersection,
                //you have to cross the connection. This should be fixed in the future by modifying the prefab pieces.
                if (flipColors)
                {
                    if (rightFirst)
                    {
                        anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[0]);
                        connectingRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                    }
                    else
                    {
                        anchorRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                        connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[0]);
                    }
                }
                else
                {
                    anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                    connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                }
                break;

            //4 lane
            case 4:

                //This section is a hack to connect the road traffic so that it flows properly. 
                //The left and right lanes are representing the blue and pink dots in the prefabs, 
                //and if the blue dots on the road piece don't line up with the blue dots on the intersection,
                //you have to cross the connection. This should be fixed in the future by modifying the prefab pieces.
                if (flipColors)
                {
                    if (rightFirst)
                    {
                        anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[0]);
                        anchorRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[1]);

                        connectingRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                        connectingRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[1]);
                    }
                    else
                    {
                        anchorRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                        anchorRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[1]);

                        connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[0]);
                        connectingRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[1]);
                    }
                }
                else
                {
                    anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                    anchorRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[1]);

                    connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                    connectingRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[1]);
                }

                break;

            //6 lane
            case 6:

                //This section is a hack to connect the road traffic so that it flows properly. 
                //The left and right lanes are representing the blue and pink dots in the prefabs, 
                //and if the blue dots on the road piece don't line up with the blue dots on the intersection,
                //you have to cross the connection. This should be fixed in the future by modifying the prefab pieces.
                if (flipColors)
                {
                    if (rightFirst)
                    {
                        anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[0]);
                        anchorRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[1]);
                        anchorRoad.m_primaryRightLaneNodes[2].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[2]);

                        connectingRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                        connectingRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[1]);
                        connectingRoad.m_primaryRightLaneNodes[2].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[3]);
                    }
                    else
                    {
                        anchorRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                        anchorRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[1]);
                        anchorRoad.m_primaryLeftLaneNodes[2].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[2]);

                        connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[0]);
                        connectingRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[1]);
                        connectingRoad.m_primaryLeftLaneNodes[2].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[2]);
                    }
                }
                else
                {
                    anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                    anchorRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[1]);
                    anchorRoad.m_primaryRightLaneNodes[2].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[2]);

                    connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                    connectingRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[1]);
                    connectingRoad.m_primaryLeftLaneNodes[2].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[2]);
                }

                break;
        }
    }

    //makes the connection between a contiguous road and T intersection
    void connectContiguousRoadToTIntersection(RoadSegment contiguousRoad, PhysicalConnectionPointType contiguousRoadAttachPoint, TIntersection tIntersection, PhysicalConnectionPointType intersectionAttachPoint)
    {
        //grab the traffic system pieces that need connected
        TrafficSystemPiece contiguousRoadSegment = getTrafficSystemPiece(contiguousRoad, contiguousRoadAttachPoint);
        TrafficSystemPiece intersectionSegment = getTrafficSystemPiece(tIntersection, intersectionAttachPoint);

        //This section is a hack to connect the road traffic so that it flows properly. 
        //The left and right lanes are representing the blue (left) and pink (right) dots in the prefabs, 
        //and if the blue dots on the road piece don't line up with the blue dots on the intersection,
        //you have to cross the connection.
        bool flipColors = false;
        bool rightFirst = false;
        bool contiguousRoadFirst = false;
        switch (contiguousRoadAttachPoint)
        {
            case PhysicalConnectionPointType.CONN_1:
                {
                    if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_4)
                    {
                        //does not exist.. 
                        intersectionSegment = new TrafficSystemPiece();
                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_1)
                    {

                        //flip
                        if (tIntersection._numLanes == 2)
                        {
                            flipColors = false;
                            rightFirst = true;
                            contiguousRoadFirst = true;
                        }
                        else
                        {
                            flipColors = true;
                            rightFirst = true;
                            contiguousRoadFirst = false;
                        }


                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        //don't flip
                        if (tIntersection._numLanes == 2)
                        {
                            flipColors = false;
                            rightFirst = true;
                            contiguousRoadFirst = true;
                        }
                        else
                        {
                            flipColors = true;
                            rightFirst = true;
                            contiguousRoadFirst = false;
                        }

                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_3)
                    {
                        //don't flip
                        if (tIntersection._numLanes == 2)
                        {
                            flipColors = true;
                            rightFirst = true;
                            contiguousRoadFirst = false;
                        }
                        else
                        {
                            flipColors = false;
                            rightFirst = true;
                            contiguousRoadFirst = true;
                        }

                    }
                    else
                    {
                        //do nothing - ?
                    }

                    break;
                }

            case PhysicalConnectionPointType.CONN_2:
                {
                    if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_4)
                    {
                        //does not exist.. 
                        intersectionSegment = new TrafficSystemPiece();
                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_1)
                    {

                        //flip
                        if (tIntersection._numLanes == 2)
                        {
                            flipColors = true;
                        }
                        else
                        {
                            flipColors = false;
                        }

                        contiguousRoadFirst = false;
                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        //don't flip
                        if (tIntersection._numLanes == 2)
                        {
                            flipColors = true;
                            contiguousRoadFirst = true;
                        }
                        else
                        {
                            flipColors = false;
                            contiguousRoadFirst = false;
                        }

                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_3)
                    {
                        //don't flip
                        if (tIntersection._numLanes == 2)
                        {
                            flipColors = false;
                            contiguousRoadFirst = false;
                        }
                        else
                        {
                            flipColors = true;
                            contiguousRoadFirst = true;
                        }

                    }
                    else
                    {
                        //do nothing - ?
                    }


                    break;
                }
        }

        //determine the directional flow of traffic. The movement is anchor road --> connecting road
        TrafficSystemPiece anchorRoad;
        TrafficSystemPiece connectingRoad;
        if (contiguousRoadFirst)
        {
            anchorRoad = contiguousRoadSegment;
            connectingRoad = intersectionSegment;
        }
        else
        {
            anchorRoad = intersectionSegment;
            connectingRoad = contiguousRoadSegment;

        }


        //connect the nodes based on the number of lanes
        switch (contiguousRoad._numLanes)
        {
            //0 lane
            case 0:
                //do nothing
                break;

            //1 lane
            case 1:
                //do nothing
                break;

            //2 lane
            case 2:

                //This section is a hack to connect the road traffic so that it flows properly. 
                //The left and right lanes are representing the blue (left) and pink (right) dots in the prefabs, 
                //and if the blue dots on the road piece don't line up with the blue dots on the intersection,
                //you have to cross the connection. This should be fixed in the future by modifying the prefab pieces.
                if (flipColors)
                {
                    if (rightFirst)
                    {
                        anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[0]);
                        connectingRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                    }
                    else
                    {
                        anchorRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                        connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[0]);
                    }
                }
                else
                {
                    anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                    connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                }
                break;

            //4 lane
            case 4:

                //This section is a hack to connect the road traffic so that it flows properly. 
                //The left and right lanes are representing the blue and pink dots in the prefabs, 
                //and if the blue dots on the road piece don't line up with the blue dots on the intersection,
                //you have to cross the connection. This should be fixed in the future by modifying the prefab pieces.
                if (flipColors)
                {
                    if (rightFirst)
                    {
                        anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[0]);
                        anchorRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[1]);

                        connectingRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                        connectingRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[1]);
                    }
                    else
                    {
                        anchorRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                        anchorRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[1]);

                        connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[0]);
                        connectingRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[1]);
                    }

                }
                else
                {
                    anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                    anchorRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[1]);

                    connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                    connectingRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[1]);
                }

                break;

            //6 lane
            case 6:

                //This section is a hack to connect the road traffic so that it flows properly. 
                //The left and right lanes are representing the blue and pink dots in the prefabs, 
                //and if the blue dots on the road piece don't line up with the blue dots on the intersection,
                //you have to cross the connection. This should be fixed in the future by modifying the prefab pieces.
                if (flipColors)
                {
                    if (rightFirst)
                    {
                        anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[0]);
                        anchorRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[1]);
                        anchorRoad.m_primaryRightLaneNodes[2].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[2]);

                        connectingRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                        connectingRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[1]);
                        connectingRoad.m_primaryRightLaneNodes[2].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[2]);
                    }
                    else
                    {
                        anchorRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                        anchorRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[1]);
                        anchorRoad.m_primaryLeftLaneNodes[2].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[2]);

                        connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[0]);
                        connectingRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[1]);
                        connectingRoad.m_primaryLeftLaneNodes[2].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[2]);
                    }
                }
                else
                {
                    anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                    anchorRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[1]);
                    anchorRoad.m_primaryRightLaneNodes[2].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[2]);

                    connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                    connectingRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[1]);
                    connectingRoad.m_primaryLeftLaneNodes[2].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[2]);
                }

                break;
        }
    }

    //makes the connection between a contiguous road and roundabout
    void connectContiguousRoadToRoundabout(RoadSegment contiguousRoad, PhysicalConnectionPointType contiguousRoadAttachPoint, Roundabout roundabout, PhysicalConnectionPointType intersectionAttachPoint)
    {
        //grab the traffic system pieces that need connected
        TrafficSystemPiece contiguousRoadSegment = getTrafficSystemPiece(contiguousRoad, contiguousRoadAttachPoint);
        TrafficSystemPiece intersectionSegment = getTrafficSystemPiece(roundabout, intersectionAttachPoint);

        //This section is a hack to connect the road traffic so that it flows properly. 
        //The left and right lanes are representing the blue (left) and pink (right) dots in the prefabs, 
        //and if the blue dots on the road piece don't line up with the blue dots on the intersection,
        //you have to cross the connection.
        bool flipColors = false;
        bool rightFirst = false;
        bool contiguousRoadFirst = false;
        switch (contiguousRoadAttachPoint)
        {
            case PhysicalConnectionPointType.CONN_1:
                {
                    if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        //flip
                        flipColors = true;
                        rightFirst = true;
                        contiguousRoadFirst = false;
                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        //flip
                        flipColors = false;
                        rightFirst = true;
                        contiguousRoadFirst = true;
                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_3)
                    {
                        //don't flip
                        flipColors = true;
                        rightFirst = true;
                        contiguousRoadFirst = false;
                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_4)
                    {
                        //don't flip
                        flipColors = true;
                        rightFirst = true;
                        contiguousRoadFirst = false;
                    }
                    else
                    {
                        //do nothing - ?
                    }
                    break;
                }
            case PhysicalConnectionPointType.CONN_2:
                {
                    if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_1)
                    {
                        //flip
                        flipColors = true;
                        contiguousRoadFirst = false;
                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_2)
                    {
                        //flip
                        flipColors = true;
                        contiguousRoadFirst = true;
                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_3)
                    {
                        //don't flip
                        flipColors = false;
                        contiguousRoadFirst = false;
                    }
                    else if (intersectionAttachPoint == PhysicalConnectionPointType.CONN_4)
                    {
                        //don't flip
                        flipColors = false;
                        contiguousRoadFirst = false;
                    }
                    else
                    {
                        //do nothing - ?
                    }
                    break;
                }
        }


        //determine the directional flow of traffic. The movement is anchor road --> connecting road
        TrafficSystemPiece anchorRoad;
        TrafficSystemPiece connectingRoad;
        if (contiguousRoadFirst)
        {
            anchorRoad = contiguousRoadSegment;
            connectingRoad = intersectionSegment;
        }
        else
        {
            anchorRoad = intersectionSegment;
            connectingRoad = contiguousRoadSegment;

        }


        //connect the nodes based on the number of lanes
        switch (contiguousRoad._numLanes)
        {
            //0 lane
            case 0:
                //do nothing
                break;

            //1 lane
            case 1:
                //do nothing
                break;

            //2 lane
            case 2:

                //This section is a hack to connect the road traffic so that it flows properly. 
                //The left and right lanes are representing the blue (left) and pink (right) dots in the prefabs, 
                //and if the blue dots on the road piece don't line up with the blue dots on the intersection,
                //you have to cross the connection. This should be fixed in the future by modifying the prefab pieces.
                if (flipColors)
                {
                    if (rightFirst)
                    {
                        anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[0]);
                        connectingRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                    }
                    else
                    {
                        anchorRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                        connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[0]);
                    }
                }
                else
                {
                    anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                    connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                }
                break;

            //4 lane
            case 4:

                //This section is a hack to connect the road traffic so that it flows properly. 
                //The left and right lanes are representing the blue and pink dots in the prefabs, 
                //and if the blue dots on the road piece don't line up with the blue dots on the intersection,
                //you have to cross the connection. This should be fixed in the future by modifying the prefab pieces.
                if (flipColors)
                {
                    if (rightFirst)
                    {
                        anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[0]);
                        anchorRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[1]);

                        connectingRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                        connectingRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[1]);
                    }
                    else
                    {
                        anchorRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                        anchorRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[1]);

                        connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[0]);
                        connectingRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[1]);
                    }
                }
                else
                {
                    anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                    anchorRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[1]);

                    connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                    connectingRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[1]);
                }

                break;

            //6 lane
            case 6:

                //This section is a hack to connect the road traffic so that it flows properly. 
                //The left and right lanes are representing the blue and pink dots in the prefabs, 
                //and if the blue dots on the road piece don't line up with the blue dots on the intersection,
                //you have to cross the connection. This should be fixed in the future by modifying the prefab pieces.
                if (flipColors)
                {
                    if (rightFirst)
                    {
                        anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[0]);
                        anchorRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[1]);
                        anchorRoad.m_primaryRightLaneNodes[2].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[2]);

                        connectingRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                        connectingRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[1]);
                        connectingRoad.m_primaryRightLaneNodes[2].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[2]);
                    }
                    else
                    {
                        anchorRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                        anchorRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[1]);
                        anchorRoad.m_primaryLeftLaneNodes[2].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[2]);

                        connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[0]);
                        connectingRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[1]);
                        connectingRoad.m_primaryLeftLaneNodes[2].m_connectedNodes.Add(anchorRoad.m_primaryRightLaneNodes[2]);
                    }
                }
                else
                {
                    anchorRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[0]);
                    anchorRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[1]);
                    anchorRoad.m_primaryRightLaneNodes[2].m_connectedNodes.Add(connectingRoad.m_primaryRightLaneNodes[2]);

                    connectingRoad.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[0]);
                    connectingRoad.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[1]);
                    connectingRoad.m_primaryLeftLaneNodes[2].m_connectedNodes.Add(anchorRoad.m_primaryLeftLaneNodes[2]);
                }

                break;
        }
    }

    //makes logical connection between two straight pieces (FROM a TO b)
    public void connectContiguousRoadPieces(TrafficSystemPiece anchorPiece, TrafficSystemPiece connectingPiece, int numLanes, bool flipColors = false, bool flipDirection = false)
    {
        //not quite sure what this does
        //TODO: figure out what this does
        anchorPiece.PerformCleanUp();

        //will flip the connection direction if necessary
        if (flipDirection)
        {
            TrafficSystemPiece temp = anchorPiece;
            anchorPiece = connectingPiece;
            connectingPiece = temp;
        }

        //connect the nodes appropriately depending on the number of lanes
        switch (numLanes)
        {
            //0 lane
            case 0:
                //do nothing
                break;

            //1 lane
            case 1:
                //add the connection in the appropriate direction for the case of a one lane road
                if (anchorPiece.m_primaryRightLaneNodes.Count > 0)
                {
                    anchorPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingPiece.m_primaryRightLaneNodes[0]);
                }
                else
                {
                    connectingPiece.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[0]);
                }

                break;

            //2 lane
            case 2:
                if (flipColors)
                {
                    //connect the nodes (blue (left) to blue and pink (right) to pink)
                    anchorPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingPiece.m_primaryLeftLaneNodes[0]);
                    connectingPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[0]);
                }
                else
                {
                    //connect the nodes (blue (left) to blue and pink (right) to pink)
                    anchorPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingPiece.m_primaryRightLaneNodes[0]);
                    connectingPiece.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[0]);

                }
                break;

            //4 lane
            case 4:
                if (flipColors)
                {
                    //connect the nodes (blue (left) to blue and pink (right) to pink)
                    anchorPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingPiece.m_primaryLeftLaneNodes[0]);
                    anchorPiece.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingPiece.m_primaryLeftLaneNodes[1]);
                    //anchorPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingPiece.m_primaryLeftLaneNodes[1]);
                    //anchorPiece.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingPiece.m_primaryLeftLaneNodes[0]);

                    connectingPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[0]);
                    connectingPiece.m_primaryRightLaneNodes[1].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[1]);
                    //connectingPiece.m_primaryRightLaneNodes[1].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[0]);
                    //connectingPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[1]);
                }
                else
                {
                    //connect the nodes (blue (left) to blue and pink (right) to pink)
                    anchorPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingPiece.m_primaryRightLaneNodes[0]);
                    anchorPiece.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingPiece.m_primaryRightLaneNodes[1]);
                    //anchorPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingPiece.m_primaryRightLaneNodes[1]);
                    //anchorPiece.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingPiece.m_primaryRightLaneNodes[0]);

                    connectingPiece.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[0]);
                    connectingPiece.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[1]);
                    //connectingPiece.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[0]);
                    //connectingPiece.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[1]);
                }
                break;

            //6 lane
            case 6:
                if (flipColors)
                {
                    //connect the nodes (blue (left) to blue and pink (right) to pink)
                    anchorPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingPiece.m_primaryLeftLaneNodes[0]);
                    anchorPiece.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingPiece.m_primaryLeftLaneNodes[1]);
                    anchorPiece.m_primaryRightLaneNodes[2].m_connectedNodes.Add(connectingPiece.m_primaryLeftLaneNodes[2]);
                    //anchorPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingPiece.m_primaryLeftLaneNodes[1]);
                    //anchorPiece.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingPiece.m_primaryLeftLaneNodes[0]);
                    //anchorPiece.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingPiece.m_primaryLeftLaneNodes[2]);
                    //anchorPiece.m_primaryRightLaneNodes[2].m_connectedNodes.Add(connectingPiece.m_primaryLeftLaneNodes[1]);

                    connectingPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[0]);
                    connectingPiece.m_primaryRightLaneNodes[1].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[1]);
                    connectingPiece.m_primaryRightLaneNodes[2].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[2]);
                    //connectingPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[1]);
                    //connectingPiece.m_primaryRightLaneNodes[1].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[0]);
                    //connectingPiece.m_primaryRightLaneNodes[1].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[2]);
                    //connectingPiece.m_primaryRightLaneNodes[2].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[1]);
                }
                else
                {
                    //connect the nodes (blue (left) to blue and pink (right) to pink)
                    anchorPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingPiece.m_primaryRightLaneNodes[0]);
                    anchorPiece.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingPiece.m_primaryRightLaneNodes[1]);
                    anchorPiece.m_primaryRightLaneNodes[2].m_connectedNodes.Add(connectingPiece.m_primaryRightLaneNodes[2]);
                    //anchorPiece.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingPiece.m_primaryRightLaneNodes[1]);
                    //anchorPiece.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingPiece.m_primaryRightLaneNodes[0]);
                    //anchorPiece.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingPiece.m_primaryRightLaneNodes[2]);
                    //anchorPiece.m_primaryRightLaneNodes[2].m_connectedNodes.Add(connectingPiece.m_primaryRightLaneNodes[1]);

                    connectingPiece.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[0]);
                    connectingPiece.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[1]);
                    connectingPiece.m_primaryLeftLaneNodes[2].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[2]);
                    //connectingPiece.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[1]);
                    //connectingPiece.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[0]);
                    //connectingPiece.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[2]);
                    //connectingPiece.m_primaryLeftLaneNodes[2].m_connectedNodes.Add(anchorPiece.m_primaryLeftLaneNodes[1]);
                }
                break;
        }


    }

    //gets the TrafficSystemPiece based on the connection point
    public TrafficSystemPiece getTrafficSystemPiece(RoadElement roadElement, PhysicalConnectionPointType connLocation)
    {
        TrafficSystemPiece trafficSystemPiece;

        switch (roadElement._roadElementType)
        {
            case RoadElementType.CONTIGUOUS_ROAD:
                {
                    if (connLocation == PhysicalConnectionPointType.CONN_2)
                    {
                        trafficSystemPiece = EnvironmentModel.roadElementGOMapping[roadElement._id].transform.GetChild(0).GetComponent<TrafficSystemPiece>();
                    }
                    else //contiguousRoadAttachPoint == PhysicalConnectionPointType.CONN_1
                    {
                        RoadSegment contiguousRoadElement = (RoadSegment)roadElement;
                        int childIndex = contiguousRoadElement._lengthOfRoad - 1;
                        trafficSystemPiece = EnvironmentModel.roadElementGOMapping[roadElement._id].transform.GetChild(childIndex).GetComponent<TrafficSystemPiece>();
                    }
                    break;
                }
            case RoadElementType.INTERSECTION:
                {
                    Intersection intersection = (Intersection)roadElement;
                    if (intersection._intersectionType == IntersectionType.T_INTERSECTION)
                    {
                        if (connLocation == PhysicalConnectionPointType.CONN_1)
                        {
                            trafficSystemPiece = EnvironmentModel.roadElementGOMapping[intersection._id].transform.GetChild(0).GetChild(0).GetComponent<TrafficSystemPiece>();
                        }
                        else if (connLocation == PhysicalConnectionPointType.CONN_2)
                        {
                            trafficSystemPiece = EnvironmentModel.roadElementGOMapping[intersection._id].transform.GetChild(0).GetChild(1).GetComponent<TrafficSystemPiece>();
                        }
                        else if (connLocation == PhysicalConnectionPointType.CONN_3)
                        {
                            trafficSystemPiece = EnvironmentModel.roadElementGOMapping[intersection._id].transform.GetChild(0).GetChild(2).GetComponent<TrafficSystemPiece>();
                        }
                        else if (connLocation == PhysicalConnectionPointType.CONN_4)
                        {
                            //connection does not exist.. do nothing - ?
                            trafficSystemPiece = new TrafficSystemPiece();
                        }
                        else
                        {
                            //do nothing - ?
                            trafficSystemPiece = new TrafficSystemPiece();
                        }
                    }
                    else if (intersection._intersectionType == IntersectionType.CROSS_INTERSECTION || intersection._intersectionType == IntersectionType.ROUNDABOUT)
                    {
                        if (connLocation == PhysicalConnectionPointType.CONN_1)
                        {
                            trafficSystemPiece = EnvironmentModel.roadElementGOMapping[intersection._id].transform.GetChild(0).GetChild(0).GetComponent<TrafficSystemPiece>();
                        }
                        else if (connLocation == PhysicalConnectionPointType.CONN_2)
                        {
                            trafficSystemPiece = EnvironmentModel.roadElementGOMapping[intersection._id].transform.GetChild(0).GetChild(1).GetComponent<TrafficSystemPiece>();
                        }
                        else if (connLocation == PhysicalConnectionPointType.CONN_3)
                        {
                            trafficSystemPiece = EnvironmentModel.roadElementGOMapping[intersection._id].transform.GetChild(0).GetChild(2).GetComponent<TrafficSystemPiece>();
                        }
                        else if (connLocation == PhysicalConnectionPointType.CONN_4)
                        {
                            trafficSystemPiece = EnvironmentModel.roadElementGOMapping[intersection._id].transform.GetChild(0).GetChild(3).GetComponent<TrafficSystemPiece>();
                        }
                        else
                        {
                            //do nothing - ?
                            trafficSystemPiece = new TrafficSystemPiece();
                        }
                    }
                    else //intersection._intersectionType == IntersectionType.NONE
                    {
                        //do nothing - ?
                        trafficSystemPiece = new TrafficSystemPiece();
                    }

                    break;
                }
            case RoadElementType.NONE:
                {
                    //shrug
                    trafficSystemPiece = new TrafficSystemPiece();
                    break;
                }
            default:
                {
                    trafficSystemPiece = EnvironmentModel.roadElementGOMapping[roadElement._id].transform.GetChild(0).GetComponent<TrafficSystemPiece>();
                    break;
                }
        }

        return trafficSystemPiece;
    }

    //gets the TrafficSystemPiece based on an index position along the road element
    public TrafficSystemPiece getTrafficSystemPiece(RoadElement roadElement, int index)
    {
        TrafficSystemPiece trafficSystemPiece;

        trafficSystemPiece = EnvironmentModel.roadElementGOMapping[roadElement._id].transform.GetChild(index).GetComponent<TrafficSystemPiece>();

        return trafficSystemPiece;
    }
    #endregion

    #region parenting
    //this function will set a game object's parent
    public void changeParenting(RoadElement parent, RoadElement child)
    {
        GameObject parentGO = EnvironmentModel.roadElementGOMapping[parent._id];
        GameObject childGO = EnvironmentModel.roadElementGOMapping[child._id];

        childGO.transform.parent = parentGO.transform;
    }
    #endregion
    #endregion

    #region SLIDER VISUALIZATION
    //because of the way each ProDM or VarEnum data type works (int vs float), it it necessary to have a separate 
    //funciton to visualize each kind of ProDM or VarEnum
    //TODO: make an error if you try to implement a ProDM or VarEnum that we can't visualize.

    //this function creates a slider and corresponding labels for a VarInterval
    public void visualizeSlider(string sliderName, VarInterval<float> rangedNum, bool refreshRoad = true)
    {
        if (rangedNum._changeable)
        {
            //create the slider object
            GameObject sliderGO = Instantiate(sliderPrefab, Vector3.zero, Quaternion.identity);
            sliderGO.name = sliderName;

            //configure slider
            Slider slider = sliderGO.GetComponentInChildren<Slider>();
            GameObject sliderslider = sliderGO.transform.GetChild(1).gameObject;
            slider.name = sliderName;
            slider.minValue = rangedNum._min;
            slider.maxValue = rangedNum._max;
            slider.value = rangedNum._val;
            slider.wholeNumbers = true;

            //configure text
            Text labelText = sliderGO.GetComponentsInChildren<Text>()[0];
            GameObject label = sliderGO.transform.GetChild(0).gameObject;
            labelText.text = sliderName;
            Text valueText = sliderGO.GetComponentsInChildren<Text>()[1];
            valueText.text = rangedNum._val.ToString();

            //set the parenting
            sliderGO.transform.SetParent(newCanvas.transform);

            //this sliderlock is very important.
            //It prevents the visualizer from calling the MakeView function before all ProDM variables are initialized
            //If this does not happen, errors will occur because the EnvironmentProgram will be trying to access variables
            //that were not initialized yet
            bool sliderLock = true;
            //make the slider reactive
            slider.OnValueChangedAsObservable()
                .Subscribe(
                x =>
                {
                    //prevents the slider from triggering any action when it is initializing
                    //after that, whenever the slider is moved, the value updates and the view is refreshed.
                    if (sliderLock)
                        sliderLock = false;
                    else
                    {
                        var newNum = int.Parse(x.ToString());

                        //Change val in original object
                        rangedNum.ChangeValue(newNum);

                        //Set the new value to UI Text
                        valueText.text = newNum.ToString();

                        if (refreshRoad)
                        {
                            deleteView();
                            PlaceHolderForEnvironmentProgram.MakeView();
                        }
                        else
                        {
                            PlaceHolderForEnvironmentProgram.MakeEnvironment();
                        }
                    }

                });
        }
    }

    //this function creates a slider and corresponding labels for a VarInterval
    public void visualizeSlider_dm(string sliderName, VarInterval<float> rangedNum, bool refreshRoad = true)
    {
        if (rangedNum._changeable)
        {
            //create the slider object
            GameObject sliderGO = Instantiate(sliderPrefab, Vector3.zero, Quaternion.identity);
            sliderGO.name = sliderName;

            //configure slider
            Slider slider = sliderGO.GetComponentInChildren<Slider>();
            slider.name = sliderName;
            slider.minValue = rangedNum._min;
            slider.maxValue = rangedNum._max;
            slider.value = rangedNum;
            slider.wholeNumbers = false;

            //configure text
            Text labelText = sliderGO.GetComponent<Text>();
            labelText.text = sliderName;
            Text valueText = sliderGO.GetComponentsInChildren<Text>()[1];
            valueText.text = rangedNum.ToString();

            //set the position and parenting
            sliderGO.transform.SetParent(newCanvas.transform);

            //this sliderlock is very important.
            //It prevents the visualizer from calling the MakeView function before all ProDM variables are initialized
            //If this does not happen, errors will occur because the EnvironmentProgram will be trying to access variables
            //that were not initialized yet
            bool sliderLock = true;
            //make the slider reactive
            slider.OnValueChangedAsObservable()
                .Subscribe(
                x =>
                {
                    //prevents the slider from triggering any action when it is initializing
                    //after that, whenever the slider is moved, the value updates and the view is refreshed.
                    if (sliderLock)
                        sliderLock = false;
                    else
                    {
                        var newNum = float.Parse(x.ToString());

                        //Change val in original object
                        rangedNum.ChangeValue(newNum);

                        //Set the new value to UI Text
                        valueText.text = newNum.ToString();

                        //Refresh view
                        if (refreshRoad)
                        {
                            deleteView();
                            PlaceHolderForEnvironmentProgram.MakeView();
                        }
                        else
                        {
                            PlaceHolderForEnvironmentProgram.MakeEnvironment();
                        }
                    }
                });
        }

    }

    //this function creates a slider and corresponding labels for a ProDMOneOfInt
    public void visualizeSlider(string sliderName, VarEnum<int> rangedNum, bool refreshRoad = true)
    {
        //create the slider object
        GameObject sliderGO = Instantiate(sliderPrefab, Vector3.zero, Quaternion.identity);
        sliderGO.name = sliderName + "_text";

        //configure slider
        Slider slider = sliderGO.GetComponentInChildren<Slider>();
        slider.name = sliderName;
        slider.minValue = 0;
        slider.maxValue = rangedNum._valOneOf.Count - 1;
        slider.value = rangedNum._index;
        slider.wholeNumbers = true;

        //configure text
        Text labelText = sliderGO.GetComponentsInChildren<Text>()[0];
        labelText.text = sliderName;
        Text valueText = sliderGO.GetComponentsInChildren<Text>()[1];
        valueText.text = rangedNum._valOneOf[rangedNum._index].ToString();

        //set the position and parenting
        sliderGO.transform.SetParent(newCanvas.transform);

        //this sliderlock is very important.
        //It prevents the visualizer from calling the MakeView function before all ProDM variables are initialized
        //If this does not happen, errors will occur because the EnvironmentProgram will be trying to access variables
        //that were not initialized yet
        bool sliderLock = true;
        //make the slider reactive
        slider.OnValueChangedAsObservable()
            .Subscribe(
            x =>
            {
                //prevents the slider from triggering any action when it is initializing
                //after that, whenever the slider is moved, the value updates and the view is refreshed.
                if (sliderLock)
                    sliderLock = false;
                else
                {
                    var newNum = int.Parse(x.ToString());

                    //Change val in original object
                    rangedNum.ChangeIndex(newNum);

                    //Set the new value to UI Text
                    valueText.text = rangedNum._valOneOf[newNum].ToString();

                    //Refresh view
                    if (refreshRoad)
                    {
                        deleteView();
                        PlaceHolderForEnvironmentProgram.MakeView();
                    }
                    else
                    {
                        PlaceHolderForEnvironmentProgram.MakeEnvironment();
                    }
                }
            });

    }

    //this function creates a slider and corresponding labels for a ProDMOneOfFloat
    public void visualizeSlider(string sliderName, VarEnum<float> rangedNum, bool refreshRoad = true)
    {

        //create the slider object
        GameObject sliderGO = Instantiate(sliderPrefab, Vector3.zero, Quaternion.identity);
        sliderGO.name = sliderName + "_text";

        //configure slider
        Slider slider = sliderGO.GetComponentInChildren<Slider>();
        slider.name = sliderName;
        slider.minValue = 0;
        slider.maxValue = rangedNum._valOneOf.Count - 1;
        slider.value = rangedNum._index;
        slider.wholeNumbers = false;

        //configure text
        Text labelText = sliderGO.GetComponentsInChildren<Text>()[0];
        labelText.text = sliderName;
        Text valueText = sliderGO.GetComponentsInChildren<Text>()[1];
        valueText.text = rangedNum._valOneOf.ToString();

        //set the position and parenting
        sliderGO.transform.SetParent(newCanvas.transform);

        //this sliderlock is very important.
        //It prevents the visualizer from calling the MakeView function before all ProDM variables are initialized
        //If this does not happen, errors will occur because the EnvironmentProgram will be trying to access variables
        //that were not initialized yet
        bool sliderLock = true;
        //make the slider reactive
        slider.OnValueChangedAsObservable()
            .Subscribe(
            x =>
            {
                //prevents the slider from triggering any action when it is initializing
                //after that, whenever the slider is moved, the value updates and the view is refreshed.
                if (sliderLock)
                    sliderLock = false;
                else
                {
                    var newNum = int.Parse(x.ToString());

                    //Change val in original object
                    rangedNum.ChangeIndex(newNum);

                    //Set the new value to UI Text
                    valueText.text = rangedNum._valOneOf[newNum].ToString();

                    if (refreshRoad)
                    {
                        deleteView();
                        PlaceHolderForEnvironmentProgram.MakeView();
                    }
                    else
                    {
                        PlaceHolderForEnvironmentProgram.MakeEnvironment();
                    }
                }
            });

    }

    public void visualizeSlider(string sliderName, ProDMVector2 vector2)
    {

        //x slider
        visualizeSlider(sliderName + "_x", vector2._x);

        //y slider
        visualizeSlider(sliderName + "_y", vector2._y);
    }

    public void visualizSlider(string sliderName, ProDMVector3 vector3)
    {

        //x slider
        visualizeSlider(sliderName + "_x", vector3._x);

        //y slider
        visualizeSlider(sliderName + "_y", vector3._y);

        //z slider
        visualizeSlider(sliderName + "_z", vector3._z);
    }

    public void visualizeSlider(string sliderName, ProDMColor color)
    {

        //r slider
        visualizeSlider(sliderName + "_r", color._r);

        //g slider
        visualizeSlider(sliderName + "_g", color._g);

        //b slider
        visualizeSlider(sliderName + "_b", color._b);

        //a slider
        visualizeSlider(sliderName + "_a", color._a);
    }
    #endregion

    #region TEST ACTORS - CARS & PEDESTRIANS

    //you can't visualize a test actor without a type
    public void visualizeTestActor(TestActor testActor)
    {
        throw new System.Exception("Improper actor type");
    }

    #region ego car
    //spawn autonomous or normal ego car
    public void visualizeTestActor(AutonomousVehicle egoCar)
    {
        Quaternion rotation = Quaternion.Euler(0f, 0f, 0f);

        //if the _road variable is defined, we will use it determine the position and rotation of the car
        //otherwise right now we just assume that the spawn and goal positions are specified
        //I don't think right now we have any controls set up for the spawn and goal position instantiation
        if (egoCar._road != null)
        {
            //first get the node in the correct lane of the specified road, at the normalized distance along the road element
            TrafficSystemNode node = getTrafficSystemNode(egoCar._road, egoCar._normalizedDistance, egoCar._laneNum);
            //then determine the appropriate rotation based on the rotation of that node
            rotation = getEgoCarOrientation(egoCar, node.transform.rotation, node.transform.position);
            //set the spawn position of the car to the node position
            egoCar._spawnPos = node.transform.position;
        }

        //spawn the proper kind of ego car
        //autonomous vehicle is used with the autonomous driving capabilities
        //training car is used in the training scene to train the autonomous driving
        //otherwise spawn the normal ego car without these special configurations
        Scene m_Scene = SceneManager.GetActiveScene();
        if (egoCar._testActorType == TestActorType.EGO_VEHICLE_AUTONOMOUS)
        {
            if (m_Scene.name.Contains("Autonomous"))
            {
                GameObject car = spawnAutonomousCar(egoCar._spawnPos, autonomousEgoCarPrefab, rotation, egoCar);
                EnvironmentModel.roadElementGOMapping.Add(egoCar._id, car);
            }
            else
            {
                Debug.LogError("This ego car has type TestActorType.EGO_VEHICLE_AUTONOMOUS, but the scene is for training. Please ensure you have the correct ego car type.");
            }


        }
        else if (egoCar._testActorType == TestActorType.EGO_VEHICLE_TRAINING)
        {
            if (m_Scene.name.Contains("Training"))
            {
                GameObject car = spawnAutonomousCar(egoCar._spawnPos, trainingEgoCarPrefab, rotation, egoCar);
                EnvironmentModel.roadElementGOMapping.Add(egoCar._id, car);

                //Set the environment program reference if it is a training car
                car.GetComponent<UnityStandardAssets.Vehicles.Car.CarController>().envProgram = GetComponent<EnvironmentProgramBaseClass>();
            }
            else
            {
                Debug.LogError("This ego car has type TestActorType.EGO_VEHICLE_TRAINING, but the scene is for autonomous. Please ensure you have the correct ego car type.");
            }
        }
        else
        {
            GameObject car = spawnEgoCar(egoCar._spawnPos, egoCarPrefab, rotation, egoCar);
            EnvironmentModel.roadElementGOMapping.Add(egoCar._id, car);
        }

        //Set reference to TestActor
        egoCarRef = egoCar;

    }

    //returns the node in the correct lane of the specified road, at the normalized distance along the road element
    TrafficSystemNode getTrafficSystemNode(RoadSegment road, float normalizedDistance, LANE laneNum)
    {
        //calculate the index of the road segment at the normalized distance
        int index = road.normalizedDistanceRoadSegmentIndex(normalizedDistance);

        //get the traffic system piece that is at this road segment
        TrafficSystemPiece tsp1 = getTrafficSystemPiece(road, index);

        //based on the lane, grab the appropriate node
        TrafficSystemNode node;
        switch (road._numLanes)
        {
            case 1:
                {
                    //add the connection in the appropriate direction for the case of a one lane road
                    if (tsp1.m_primaryLeftLaneNodes.Count > 0)
                    {
                        node = tsp1.m_primaryLeftLaneNodes[0];
                    }
                    else if (tsp1.m_primaryRightLaneNodes.Count > 0)
                    {
                        node = tsp1.m_primaryRightLaneNodes[0];
                    }
                    else
                    {
                        throw new System.Exception("Lane does not exist. Cannot spawn car.");
                    }
                    break;
                }
            case 2:
                {
                    if (laneNum == LANE.PLUS_ONE)
                    {
                        node = tsp1.m_primaryRightLaneNodes[(int)laneNum - 1];
                    }
                    else if (laneNum == LANE.MINUS_ONE)
                    {
                        node = tsp1.m_primaryLeftLaneNodes[System.Math.Abs((int)laneNum) - 1];
                    }
                    else
                    {
                        throw new System.Exception("Lane does not exist. Cannot spawn car.");
                    }
                    break;
                }
            case 4:
                {
                    if (laneNum == LANE.PLUS_ONE || laneNum == LANE.PLUS_TWO)
                    {
                        node = tsp1.m_primaryRightLaneNodes[(int)laneNum - 1];
                    }
                    else if (laneNum == LANE.MINUS_ONE || laneNum == LANE.MINUS_TWO)
                    {
                        node = tsp1.m_primaryLeftLaneNodes[System.Math.Abs((int)laneNum) - 1];
                    }
                    else
                    {
                        throw new System.Exception("Lane does not exist. Cannot spawn car.");
                    }
                    break;
                }
            case 6:
                {
                    if (laneNum == LANE.PLUS_ONE || laneNum == LANE.PLUS_TWO || laneNum == LANE.PLUS_THREE)
                    {
                        node = tsp1.m_primaryRightLaneNodes[(int)laneNum - 1];
                    }
                    else if (laneNum == LANE.MINUS_ONE || laneNum == LANE.MINUS_TWO || laneNum == LANE.MINUS_THREE)
                    {
                        node = tsp1.m_primaryLeftLaneNodes[System.Math.Abs((int)laneNum) - 1];
                    }
                    else
                    {
                        throw new System.Exception("Lane does not exist. Cannot spawn car.");
                    }
                    break;
                }
            default:
                {
                    throw new System.Exception("Lane does not exist. Cannot spawn car.");
                }

        }

        //return the node
        return node;
    }

    //returns the rotation for the ego car based on the rotation of the node where it will spawn
    //this rotation ensures the car spawns facing the proper direction in the lane
    //traffic going in the + direction needs -90 deg offset, whereas traffic going in the - direction needs +90 deg offset
    Quaternion getEgoCarOrientation(AutonomousVehicle egoCar, Quaternion rot, Vector3 pos)
    {
        Quaternion rotation = rot;

        switch (egoCar._road._numLanes)
        {
            case 1:
                {
                    int index = egoCar._road.normalizedDistanceRoadSegmentIndex(egoCar._normalizedDistance);
                    TrafficSystemPiece tsp1 = getTrafficSystemPiece(egoCar._road, index);
                    if (tsp1.m_primaryRightLaneNodes.Count > 0)
                    {
                        rotation = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y + 90, rotation.eulerAngles.z);
                    }
                    else if (tsp1.m_primaryLeftLaneNodes.Count > 0)
                    {
                        rotation = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y - 90, rotation.eulerAngles.z);
                    }
                    else
                    {
                        Debug.LogError("Lane does not exist. Cannot spawn car.");
                    }
                    break;
                }
            case 2:
                {
                    if (egoCar._laneNum == LANE.PLUS_ONE)
                    {
                        rotation = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y + 90, rotation.eulerAngles.z);
                    }
                    else if (egoCar._laneNum == LANE.MINUS_ONE)
                    {
                        rotation = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y - 90, rotation.eulerAngles.z);
                    }
                    else
                    {
                        Debug.LogError("Lane does not exist. Cannot spawn car.");
                    }
                    break;
                }
            case 4:
                {
                    if (egoCar._laneNum == LANE.PLUS_ONE || egoCar._laneNum == LANE.PLUS_TWO)
                    {
                        rotation = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y + 90, rotation.eulerAngles.z);
                    }
                    else if (egoCar._laneNum == LANE.MINUS_ONE || egoCar._laneNum == LANE.MINUS_TWO)
                    {
                        rotation = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y - 90, rotation.eulerAngles.z);
                    }
                    else
                    {
                        Debug.LogError("Lane does not exist. Cannot spawn car.");
                    }
                    break;
                }
            case 6:
                {
                    if (egoCar._laneNum == LANE.PLUS_ONE || egoCar._laneNum == LANE.PLUS_TWO || egoCar._laneNum == LANE.PLUS_THREE)
                    {
                        rotation = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y + 90, rotation.eulerAngles.z);
                    }
                    else if (egoCar._laneNum == LANE.MINUS_ONE || egoCar._laneNum == LANE.MINUS_TWO || egoCar._laneNum == LANE.MINUS_THREE)
                    {
                        rotation = Quaternion.Euler(rotation.eulerAngles.x, rotation.eulerAngles.y - 90, rotation.eulerAngles.z);
                    }
                    else
                    {
                        Debug.LogError("Lane does not exist. Cannot spawn car.");
                    }
                    break;
                }
            default:
                {
                    Debug.LogError("Invalid number of lanes.");
                    break;
                }

        }

        return rotation;

    }

    //returns the ego car that was spawned
    GameObject spawnEgoCar(Vector3 pos, GameObject a_vehiclePrefab, Quaternion rotation, AutonomousVehicle egoCar)
    {
        //instantiate the car and set ID
        GameObject vehicle = Instantiate(egoCarPrefab, pos, rotation) as GameObject;
        vehicle.name = egoCar._id;

        //add each online event if it is enabled
        foreach (KeyValuePair<OnlineEventType, bool> entry in egoCar._onlineEventsEnabled)
        {
            //TODO: Add abrupt braking
            //if the collision tracking is enabled, add it
            if (entry.Key == OnlineEventType.COLLISION && entry.Value)
            {
                //Set reference to the ego car actor structure
                LiveActorCollisionCheck checker = vehicle.AddComponent<LiveActorCollisionCheck>();
                checker.actorRef = egoCar;
            }


        }

        //configure the live actor position and speed references
        LiveActorPositionAndSpeed positionAndSpeed = vehicle.AddComponent<LiveActorPositionAndSpeed>();
        positionAndSpeed.actorRef = egoCar;

        //return the vehicle
        return vehicle;
    }

    GameObject spawnAutonomousCar(Vector3 pos, GameObject a_vehiclePrefab, Quaternion rotation, AutonomousVehicle egoCar)
    {
        //instantiate the car
        GameObject vehicle = Instantiate(a_vehiclePrefab, pos, rotation);
        vehicle.name = egoCar._id;

        //configure some settings for the autonomous driving
        UISystem uiSystem = FindObjectOfType<UISystem>();
        uiSystem.carController = vehicle.GetComponent<UnityStandardAssets.Vehicles.Car.CarController>();

        //configure some settings for the autonomous driving
        //these differ if it is the autonomous or training car
        if (egoCar._testActorType == TestActorType.EGO_VEHICLE_AUTONOMOUS)
        {
            CommandServer commandServer = FindObjectOfType<CommandServer>();
            commandServer.CarRemoteControl = vehicle.GetComponent<UnityStandardAssets.Vehicles.Car.CarRemoteControl>();
            Camera view = vehicle.GetComponentsInChildren<Camera>()[1];
            commandServer.FrontFacingCamera = view;

            //Add depth map to the main camera
            Camera mainCamera = vehicle.GetComponentsInChildren<Camera>()[0];
            mainCamera.farClipPlane = 100;
            mainCamera.gameObject.AddComponent<PostProcessDepthGreyscale>();
            Material greyscaleMaterial = Resources.Load("DepthGreyscale", typeof(Material)) as Material;
            mainCamera.gameObject.GetComponent<PostProcessDepthGreyscale>().mat = greyscaleMaterial;
        }
        else
        {
            //for the training car, only this Camera works, not the FreeLookCameraRig
            //So we update this reference
            Camera view = vehicle.GetComponentsInChildren<Camera>()[0];
            view.enabled = true;

            FindObjectOfType<ChangeCamera>().interactiveCamera = view;
        }

        //add online events if they are enabled
        foreach (KeyValuePair<OnlineEventType, bool> entry in egoCar._onlineEventsEnabled)
        {
            //TODO: Add online events that are to be listened for
            if (entry.Key == OnlineEventType.COLLISION && entry.Value)
            {
                //Set reference to the ego car actor structure
                LiveActorCollisionCheck checker = vehicle.AddComponent<LiveActorCollisionCheck>();
                checker.actorRef = egoCar;
                checker.ScriptPlaceholder = gameObject;
            }
            if (entry.Key == OnlineEventType.INACTIVITY && entry.Value)
            {
                //Set reference to the ego car actor structure
                LiveActorInactivityCheck checker = vehicle.AddComponent<LiveActorInactivityCheck>();
                checker.actorRef = egoCar;
                checker.ScriptPlaceholder = gameObject;
            }

        }

        //configure references for live position and speed
        LiveActorPositionAndSpeed positionAndSpeed = vehicle.AddComponent<LiveActorPositionAndSpeed>();
        positionAndSpeed.actorRef = egoCar;

        //return the vehicle
        return vehicle;
    }
    #endregion

    #region ai cars
    public void visualizeTestActor(AIVehicle vehicle)
    {
        //if the _road variable is defined, we will use it determine the position and rotation of the car
        if (vehicle._road != null)
        {
            //first get the node in the correct lane of the specified road, at the normalized distance along the road element
            TrafficSystemNode node = getTrafficSystemNode(vehicle._road, vehicle._normalizedDistance, vehicle._laneNum);

            //this ensures the carPrefab spawns
            TrafficSystem.Instance.m_randomVehicleSpawnChancePerNode = 1;
            TrafficSystem.Instance.m_globalSpeedLimit = 5;

            //set the position. This is from the trafficsystemeditor
            Vector3 pos = node.transform.position;
            pos -= carPrefab.m_offsetPosVal;

            //instantiate the car. This is from the trafficsystemeditor
            TrafficSystemVehicle car = Instantiate(carPrefab, pos, node.transform.rotation) as TrafficSystemVehicle;
            car.name = vehicle._id;
            car.m_nextNode = node;
            car.m_velocityMax = UnityEngine.Random.Range(TrafficSystem.Instance.m_randVehicleVelocityMin, TrafficSystem.Instance.m_randVehicleVelocityMax);
            car.GetComponent<ChangeMaterialColour>().ChangeColor(vehicle._bodyColor);

            EnvironmentModel.testActorList.Add(vehicle, car.gameObject);

            //set the path of the car by specifying its next node
            TrafficSystemNode nextNode = node.GetNextNode(car, false);
            if (nextNode)
                car.transform.forward = nextNode.transform.position - car.transform.position;

            //configure the live position and speed
            LiveActorPositionAndSpeed positionAndSpeed = car.m_meshObject.AddComponent<LiveActorPositionAndSpeed>();
            positionAndSpeed.actorRef = vehicle;

            if (vehicle._changeColor)
            {
                TestActorFloat floatColor = car.m_meshObject.AddComponent<TestActorFloat>();
                floatColor.actorRef = vehicle;
            }

        }
        else
        {
            //instantiate by finding the closest node to use
            //TODO: this is not done
            Debug.Log("This visualization hasn't been fully implemented yet");
        }

    }
    #endregion

    #region pedestrians
    public void visualizeTestActor(Pedestrian pedestrian)
    {
        //if there is no goal position for the pedestrian, 
        //we assume the spawn and goal positions are based on 
        //the pedestrian crossing the road at a specified point
        if (pedestrian._goalPos == null)
        {
            //first get the position at the normalized distance along the road element where the pedestrian should cross
            Vector3 basePos = pedestrian._road.normalizedDistancePosition(pedestrian._normalizedDistance);

            //then set the spawn and goal positions
            pedestrian._spawnPos = getPedestrianSpawnCoordinate(pedestrian, basePos);
            pedestrian._goalPos = getPedestrianGoalCoordinate(pedestrian, basePos);
        }

        //then instantiate the pedestrian and add it to the GO mapping
        PedestrianObject samurai = visualizePedestrian(pedestrian);
        //Time delay set on the Pedestrian Object script
        samurai.m_startMovingDelayMax = pedestrian._timeDelay;
        samurai.m_startMovingDelayMin = pedestrian._timeDelay;
        samurai.m_maxSpeed = pedestrian._walkingSpeed;
        samurai.m_minSpeed = pedestrian._walkingSpeed;

        //Behavior dependent on other actors
        if (pedestrian._actorOfInterest != null)
        {
            samurai.isLocked = true; //Lock movement
            samurai.actorOfInterest = pedestrian._actorOfInterest;
            samurai.euclideanDistance = pedestrian._euclideanDistance;
        }

        EnvironmentModel.testActorList.Add(pedestrian, samurai.gameObject);
    }

    //returns the coordinates where the pedestrian should spawn
    //This places the pedestrian at one side of the road 
    //so that it can cross to the other side
    Vector3 getPedestrianSpawnCoordinate(Pedestrian pedestrian, Vector3 basePos)
    {
        Vector3 spawnPos = basePos;

        int width = getRoadWidth(pedestrian);

        //if the pedestrian is spawning on the left side of the road,
        //the spawn coordinate will be opposite
        if (pedestrian._roadSide == SIDEWALK.LEFT)
        {
            width = -width;
        }

        //based on the orientation of the road, move the pedestrian half a road width to one side
        switch (pedestrian._road._orientation)
        {
            case Orientation.VERTICAL:
                {
                    spawnPos.x = spawnPos.x + (width / 2);
                    break;
                }
            case Orientation.VERTICAL_FLIPPED:
                {
                    spawnPos.x = spawnPos.x - (width / 2);
                    break;
                }
            case Orientation.HORIZONTAL:
                {
                    spawnPos.z = spawnPos.z - (width / 2);
                    break;
                }
            case Orientation.HORIZONTAL_FLIPPED:
                {
                    spawnPos.z = spawnPos.z + (width / 2);
                    break;
                }
            default:
                {

                    break;
                }
        }

        return spawnPos;
    }

    //returns the width of a road segment
    int getRoadWidth(Pedestrian pedestrian)
    {
        int width;

        switch (pedestrian._road._numLanes)
        {
            case 1:
                {
                    width = 8;
                    break;
                }
            case 2:
                {
                    width = 14;
                    break;
                }
            case 4:
                {
                    width = 22;
                    break;
                }
            case 6:
                {
                    width = 30;
                    break;
                }
            default:
                {
                    throw new System.Exception("Invalid number of lanes.");
                    //break;
                }

        }

        return width;
    }

    //returns the coordinates where the pedestrian should go
    //This is the other side of the road from the spawn point
    Vector3 getPedestrianGoalCoordinate(Pedestrian pedestrian, Vector3 basePos)
    {
        Vector3 goalPos = basePos;

        int width = getRoadWidth(pedestrian);

        //if the pedestrian is spawning on the left side of the road,
        //the goal coordinate will be opposite
        if (pedestrian._roadSide == SIDEWALK.LEFT)
        {
            width = -width;
        }

        //based on the orientation of the road, move the pedestrian half a road width to one side
        //opposite the spawn position
        switch (pedestrian._road._orientation)
        {
            case Orientation.VERTICAL:
                {
                    goalPos.x = goalPos.x - (width / 2);
                    break;
                }
            case Orientation.VERTICAL_FLIPPED:
                {
                    goalPos.x = goalPos.x + (width / 2);
                    break;
                }
            case Orientation.HORIZONTAL:
                {
                    goalPos.z = goalPos.z + (width / 2);
                    break;
                }
            case Orientation.HORIZONTAL_FLIPPED:
                {
                    goalPos.z = goalPos.z - (width / 2);
                    break;
                }
            default:
                {

                    break;
                }
        }

        return goalPos;
    }

    //this funciton creates a pedestrian to go from one point to another
    PedestrianObject visualizePedestrian(Pedestrian pedestrian)
    {
        //NOTE: these pedestrian nodes are taken from the traffic system editor

        //first make a pedestrian node where where pedestrian will spawn
        PedestrianNode startNode = Instantiate(PedestrianSystem.Instance.m_nodePrefab) as PedestrianNode;
        startNode.transform.parent = PedestrianSystem.Instance.transform;
        startNode.transform.position = pedestrian._spawnPos;
        startNode.name = pedestrian._id + "_start";

        //then make the goal node where the pedestrian will walk to
        PedestrianNode endNode = Instantiate(PedestrianSystem.Instance.m_nodePrefab) as PedestrianNode;
        endNode.transform.parent = PedestrianSystem.Instance.transform;
        endNode.transform.position = pedestrian._goalPos;
        endNode.name = pedestrian._id + "_end";

        //add the nodes to each other so that they form a path
        startNode.m_nodes.Add(endNode);
        endNode.m_nodes.Add(startNode);

        //instantiate the pedestrian 
        PedestrianObject samurai = Instantiate(pedestrianPrefab, transform.position, transform.rotation) as PedestrianObject;
        //set the lookAtNode to false so that the samurai's orientation is not overriden later
        samurai.m_lookAtNode = false;
        samurai.name = pedestrian._id;

        //trying this to see if it works
        LiveActorPositionAndSpeed positionAndSpeed = samurai.m_meshObject.AddComponent<LiveActorPositionAndSpeed>();
        positionAndSpeed.actorRef = pedestrian;

        //spawn the pedestrian at the start position
        //it will automaticall go to the goal position
        samurai.Spawn(startNode.transform.position, startNode);

        //calculate the proper orientation for the pedestrian
        //such that it spawns facing the direction of its goal position
        Vector3 desiredDirection = ((Vector3)pedestrian._goalPos - (Vector3)pedestrian._spawnPos).normalized;
        Quaternion desiredRotation = Quaternion.LookRotation(desiredDirection);
        //set the rotation  of the pedestrian now so that it is facing the direction it will move
        //NOTE: this has to be done after the pedestrian.Spawn
        samurai.transform.rotation = desiredRotation;

        //add pedestrian nodes to the GO mapping
        EnvironmentModel.roadElementGOMapping.Add(startNode.name, startNode.gameObject);
        EnvironmentModel.roadElementGOMapping.Add(endNode.name, endNode.gameObject);

        //return the pedestrian
        return samurai;
    }

    #endregion



    #endregion

    #region ZONING - TREES & SUCH

    #region general zoning
    //this function handles zoning for the contiguous road elements
    //it looks at each zone and implements the correct type
    void visualizeRoadZone(RoadSegment roadElement)
    {
        switch (roadElement._zone1)
        {
            case Zoning.TREES:
                {
                    addTrees(roadElement, ZoneNumber.ONE);
                    break;
                }

            case Zoning.RESIDENTIAL_ZONE:
                {
                    addHouses(roadElement, ZoneNumber.ONE);
                    break;
                }
            case Zoning.INDUSTRIAL_ZONE:
                {

                    break;
                }
            case Zoning.FARM_ZONE:
                {

                    break;
                }
            case Zoning.COMMERCIAL_ZONE:
                {

                    break;
                }
            case Zoning.NONE:
                {

                    break;
                }
        }

        switch (roadElement._zone2)
        {
            case Zoning.TREES:
                {
                    addTrees(roadElement, ZoneNumber.TWO);
                    break;
                }

            case Zoning.RESIDENTIAL_ZONE:
                {
                    addHouses(roadElement, ZoneNumber.TWO);
                    break;
                }
            case Zoning.INDUSTRIAL_ZONE:
                {

                    break;
                }
            case Zoning.FARM_ZONE:
                {

                    break;
                }
            case Zoning.COMMERCIAL_ZONE:
                {

                    break;
                }
            case Zoning.NONE:
                {

                    break;
                }
        }
    }

    //this function handles zoning for the intersections
    //it looks at each zone and implements the correct type
    void visualizeRoadZone(Intersection roadElement)
    {
        switch (roadElement._zone1)
        {
            case Zoning.TREES:
                {
                    addTrees(roadElement, ZoneNumber.ONE);
                    break;
                }

            case Zoning.RESIDENTIAL_ZONE:
                {
                    addHouses(roadElement, ZoneNumber.ONE);
                    break;
                }
            case Zoning.INDUSTRIAL_ZONE:
                {

                    break;
                }
            case Zoning.FARM_ZONE:
                {

                    break;
                }
            case Zoning.COMMERCIAL_ZONE:
                {

                    break;
                }
            case Zoning.NONE:
                {

                    break;
                }
        }

        switch (roadElement._zone2)
        {
            case Zoning.TREES:
                {
                    addTrees(roadElement, ZoneNumber.TWO);
                    break;
                }

            case Zoning.RESIDENTIAL_ZONE:
                {
                    addHouses(roadElement, ZoneNumber.TWO);
                    break;
                }
            case Zoning.INDUSTRIAL_ZONE:
                {

                    break;
                }
            case Zoning.FARM_ZONE:
                {

                    break;
                }
            case Zoning.COMMERCIAL_ZONE:
                {

                    break;
                }
            case Zoning.NONE:
                {

                    break;
                }
        }

        switch (roadElement._zone3)
        {
            case Zoning.TREES:
                {
                    addTrees(roadElement, ZoneNumber.THREE);
                    break;
                }

            case Zoning.RESIDENTIAL_ZONE:
                {
                    addHouses(roadElement, ZoneNumber.THREE);
                    break;
                }
            case Zoning.INDUSTRIAL_ZONE:
                {

                    break;
                }
            case Zoning.FARM_ZONE:
                {

                    break;
                }
            case Zoning.COMMERCIAL_ZONE:
                {

                    break;
                }
            case Zoning.NONE:
                {

                    break;
                }
        }

        switch (roadElement._zone4)
        {
            case Zoning.TREES:
                {
                    addTrees(roadElement, ZoneNumber.FOUR);
                    break;
                }

            case Zoning.RESIDENTIAL_ZONE:
                {
                    addHouses(roadElement, ZoneNumber.FOUR);
                    break;
                }
            case Zoning.INDUSTRIAL_ZONE:
                {

                    break;
                }
            case Zoning.FARM_ZONE:
                {

                    break;
                }
            case Zoning.COMMERCIAL_ZONE:
                {

                    break;
                }
            case Zoning.NONE:
                {

                    break;
                }
        }
    }

    #endregion

    #region trees
    //this function adds trees to a contiguous road
    void addTrees(RoadSegment road, ZoneNumber zone)
    {
        //for each road segment in the contiguous road, add a tree
        for (int i = 0; i < road._lengthOfRoad; i++)
        {
            //get the trafficsystempiece
            TrafficSystemPiece tsp = getTrafficSystemPiece(road, i);

            //generate the spawn position based on the tsp position
            Vector3 spawnPos = getSpawnPos(road, tsp.transform.position, zone);

            //spawn the tree and add parenting
            GameObject tree = spawnRandomTree(spawnPos);
            tree.transform.SetParent(EnvironmentModel.roadElementGOMapping[road._id].transform);
        }
    }

    //this functions adds trees to an intersection
    void addTrees(Intersection intersection, ZoneNumber zone)
    {
        //add trees to the intersection
        //this must account for the curvature in a roundabout as well as the long side of the t intersection

        //first get the road width
        int roadWidth = getRoadWidth(intersection);

        //Set up some base variables based on the zone and intersection type
        RelativePhysicalConnectionLocation connLocation = RelativePhysicalConnectionLocation.NONE;
        Vector3 base1Pos = Vector3.zero;
        switch (zone)
        {
            case ZoneNumber.ONE:
                {
                    //get the position based on the traffic system piece location
                    TrafficSystemPiece conn1segment = getTrafficSystemPiece(intersection, PhysicalConnectionPointType.CONN_1);
                    base1Pos = conn1segment.transform.position;

                    //find the relative orientation based on the connection point
                    connLocation = getRelativePhysicalConnectionPoint(intersection._orientation, PhysicalConnectionPointType.CONN_1, RoadElementType.INTERSECTION);

                    break;
                }
            case ZoneNumber.TWO:
                {
                    //get the position based on the traffic system piece location
                    TrafficSystemPiece conn2segment = getTrafficSystemPiece(intersection, PhysicalConnectionPointType.CONN_2);
                    base1Pos = conn2segment.transform.position;

                    //find the relative orientation based on the connection point
                    connLocation = getRelativePhysicalConnectionPoint(intersection._orientation, PhysicalConnectionPointType.CONN_2, RoadElementType.INTERSECTION);

                    break;
                }
            case ZoneNumber.THREE:
                {
                    if (intersection._intersectionType == IntersectionType.T_INTERSECTION)
                    {
                        //it's a T intersection, so we will add trees to the long side
                        //therefore, get the position of the intersection
                        base1Pos = EnvironmentModel.roadElementGOMapping[intersection._id].transform.position;
                    }
                    else
                    {
                        //it's not a tintersection, so we will add trees to the corner
                        //get the position based on the traffic system piece location
                        TrafficSystemPiece conn3segment = getTrafficSystemPiece(intersection, PhysicalConnectionPointType.CONN_3);
                        base1Pos = conn3segment.transform.position;

                        //find the relative orientation based on the connection point
                        connLocation = getRelativePhysicalConnectionPoint(intersection._orientation, PhysicalConnectionPointType.CONN_3, RoadElementType.INTERSECTION);

                    }
                    break;
                }
            case ZoneNumber.FOUR:
                {
                    if (intersection._intersectionType == IntersectionType.T_INTERSECTION)
                    {
                        throw new System.Exception("Zone 4 does not exist for a T Intersection");
                    }

                    //get the position based on the traffic system piece location
                    TrafficSystemPiece conn4segment = getTrafficSystemPiece(intersection, PhysicalConnectionPointType.CONN_4);
                    base1Pos = conn4segment.transform.position;

                    //find the relative orientation based on the connection point
                    connLocation = getRelativePhysicalConnectionPoint(intersection._orientation, PhysicalConnectionPointType.CONN_4, RoadElementType.INTERSECTION);

                    break;
                }
        }

        //Add the trees based on the zone number and intersection type
        //roundabouts will need different tree placement because of the curvature
        //the TIntersection does not have zone4 and needs a different zone3 implementation
        if (zone == ZoneNumber.THREE && intersection._intersectionType == IntersectionType.T_INTERSECTION)
        {
            //add trees to long side
            RelativePhysicalConnectionLocation longEndLocation = getRelativePhysicalConnectionPoint(intersection._orientation, PhysicalConnectionPointType.CONN_4, RoadElementType.INTERSECTION);
            Vector3 pos = base1Pos;

            //refers to the length of the long side (zone3)
            float longSideLength = 0;
            //get the trafficsystempiece for the center of the tintersection
            TrafficSystemPiece tsp = EnvironmentModel.roadElementGOMapping[intersection._id].transform.GetChild(0).GetComponent<TrafficSystemPiece>();

            //add the trees based on the orienation
            switch (longEndLocation)
            {
                case RelativePhysicalConnectionLocation.NORTH:
                    {
                        //+z
                        //get the length of the long side
                        longSideLength = tsp.GetRenderBounds().size.x;

                        //set the position for the center tree
                        pos.z = pos.z + (roadWidth / 2);

                        //spawn the center tree and set parenting
                        GameObject tree = spawnRandomTree(pos);
                        tree.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);

                        //add the rest of the trees along the long length
                        for (int i = 0; i < (longSideLength / 2); i = i + 6)
                        {
                            //set the position for trees to the right and left of the center
                            Vector3 posL = pos + new Vector3(i, 0, 0);
                            Vector3 posR = pos - new Vector3(i, 0, 0);

                            //spawn the trees and set parenting
                            GameObject treeL = spawnRandomTree(posL);
                            treeL.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);

                            GameObject treeR = spawnRandomTree(posR);
                            treeR.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);
                        }
                        break;
                    }
                case RelativePhysicalConnectionLocation.EAST:
                    {
                        //+x
                        //get the length of the long side
                        longSideLength = tsp.GetRenderBounds().size.z;

                        // set the position for the center tree
                        pos.x = pos.x + (roadWidth / 2);

                        //spawn the center tree and set parenting
                        GameObject tree = spawnRandomTree(pos);
                        tree.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);

                        //add the rest of the trees along the long length
                        for (int i = 0; i < (longSideLength / 2); i = i + 6)
                        {
                            //set the position for trees to the right and left of the center
                            Vector3 posL = pos + new Vector3(0, 0, i);
                            Vector3 posR = pos - new Vector3(0, 0, i);

                            //spawn the trees and set parenting
                            GameObject treeL = spawnRandomTree(posL);
                            treeL.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);

                            GameObject treeR = spawnRandomTree(posR);
                            treeR.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);
                        }
                        break;
                    }
                case RelativePhysicalConnectionLocation.SOUTH:
                    {
                        //-z
                        //get the length of the long side
                        longSideLength = tsp.GetRenderBounds().size.x;

                        // set the position for the center tree
                        pos.z = pos.z - (roadWidth / 2);

                        //spawn the center tree and set parenting
                        GameObject tree = spawnRandomTree(pos);
                        tree.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);

                        //add the rest of the trees along the long length
                        for (int i = 0; i < (longSideLength / 2); i = i + 6)
                        {
                            //set the position for trees to the right and left of the center
                            Vector3 posL = pos + new Vector3(i, 0, 0);
                            Vector3 posR = pos - new Vector3(i, 0, 0);

                            //spawn the trees and set parenting
                            GameObject treeL = spawnRandomTree(posL);
                            treeL.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);

                            GameObject treeR = spawnRandomTree(posR);
                            treeR.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);
                        }
                        break;
                    }
                case RelativePhysicalConnectionLocation.WEST:
                    {
                        //-x
                        //get the length of the long side
                        longSideLength = tsp.GetRenderBounds().size.z;

                        // set the position for the center tree
                        pos.x = pos.x - (roadWidth / 2);

                        //spawn the center tree and set parenting
                        GameObject tree = spawnRandomTree(pos);
                        tree.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);

                        //add the rest of the trees along the long length
                        for (int i = 0; i < (longSideLength / 2); i = i + 6)
                        {
                            //set the position for trees to the right and left of the center
                            Vector3 posL = pos + new Vector3(0, 0, i);
                            Vector3 posR = pos - new Vector3(0, 0, i);

                            //spawn the trees and set parenting
                            GameObject treeL = spawnRandomTree(posL);
                            treeL.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);

                            GameObject treeR = spawnRandomTree(posR);
                            treeR.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);
                        }
                        break;
                    }
            }
        }
        else if (zone == ZoneNumber.FOUR && intersection._intersectionType == IntersectionType.T_INTERSECTION)
        {
            //throw and error as this zone does not exist
            Debug.LogError("T intersection has no Zone 4. Cannot implement zoning.");
        }
        else
        {
            //define the tree positions
            Vector3 pos1 = base1Pos;
            Vector3 pos3 = base1Pos;
            Vector3 pos2 = base1Pos;
            Vector3 pos4 = base1Pos;
            Vector3 pos5 = base1Pos; //only for roundabout
            Vector3 pos6 = base1Pos; //only for roundabout

            switch (connLocation)
            {
                case RelativePhysicalConnectionLocation.NORTH:
                    {
                        ////+z
                        //set position for trees 1 and 2. These don't change depending on the type of intersection
                        pos1.x = base1Pos.x + (roadWidth / 2);

                        pos2 = pos1;
                        pos2.z = pos1.z - 7;

                        if (intersection._intersectionType == IntersectionType.ROUNDABOUT)
                        {
                            //set the rest of the positions for the roundabout
                            pos3 = pos2;
                            pos3.x = pos2.x + 2;
                            pos3.z = pos2.z - 8;

                            pos4 = pos3;
                            pos4.x = pos3.x + 5;
                            pos4.z = pos3.z - 4;

                            pos5 = pos4;
                            pos5.x = pos4.x + 6;

                            pos6 = pos5;
                            pos6.x = pos5.x + 6;

                        }
                        else
                        {
                            //set the rest of the positions for a T or Cross Intersection
                            pos3 = pos2;
                            pos3.x = pos2.x + 6;

                            pos4 = pos3;
                            pos4.x = pos3.x + 5;
                        }
                        break;
                    }
                case RelativePhysicalConnectionLocation.EAST:
                    {
                        //+x
                        //set position for trees 1 and 2. These don't change depending on the type of intersection
                        pos1.z = base1Pos.z - (roadWidth / 2);

                        pos2 = pos1;
                        pos2.x = pos1.x - 7;

                        if (intersection._intersectionType == IntersectionType.ROUNDABOUT)
                        {
                            //set the rest of the positions for the roundabout
                            pos3 = pos2;
                            pos3.x = pos2.x - 8;
                            pos3.z = pos2.z - 2;

                            pos4 = pos3;
                            pos4.x = pos3.x - 4;
                            pos4.z = pos3.z - 5;

                            pos5 = pos4;
                            pos5.z = pos4.z - 6;

                            pos6 = pos5;
                            pos6.z = pos5.z - 6;

                        }
                        else
                        {
                            //set the rest of the positions for a T or Cross Intersection
                            pos3 = pos2;
                            pos3.z = pos2.z - 6;

                            pos4 = pos3;
                            pos4.z = pos3.z - 5;

                        }
                        break;
                    }
                case RelativePhysicalConnectionLocation.SOUTH:
                    {
                        ////-z
                        //set position for trees 1 and 2. These don't change depending on the type of intersection
                        pos1.x = base1Pos.x - (roadWidth / 2);

                        pos2 = pos1;
                        pos2.z = pos1.z + 7;

                        if (intersection._intersectionType == IntersectionType.ROUNDABOUT)
                        {
                            //set the rest of the positions for the roundabout
                            pos3 = pos2;
                            pos3.x = pos2.x - 2;
                            pos3.z = pos2.z + 8;

                            pos4 = pos3;
                            pos4.x = pos3.x - 5;
                            pos4.z = pos3.z + 4;

                            pos5 = pos4;
                            pos5.x = pos4.x - 6;

                            pos6 = pos5;
                            pos6.x = pos5.x - 6;
                        }
                        else
                        {
                            //set the rest of the positions for a T or Cross Intersection
                            pos3 = pos2;
                            pos3.x = pos2.x - 6;

                            pos4 = pos3;
                            pos4.x = pos3.x - 5;

                        }
                        break;
                    }
                case RelativePhysicalConnectionLocation.WEST:
                    {
                        ////-x
                        //set position for trees 1 and 2. These don't change depending on the type of intersection
                        pos1.z = base1Pos.z + (roadWidth / 2);

                        pos2 = pos1;
                        pos2.x = pos1.x + 7;


                        if (intersection._intersectionType == IntersectionType.ROUNDABOUT)
                        {
                            //set the rest of the positions for the roundabout
                            pos3 = pos2;
                            pos3.x = pos2.x + 8;
                            pos3.z = pos2.z + 2;

                            pos4 = pos3;
                            pos4.z = pos3.z + 4;
                            pos4.x = pos3.x + 5;

                            pos5 = pos4;
                            pos5.z = pos4.z + 6;

                            pos6 = pos5;
                            pos6.z = pos5.z + 6;


                        }
                        else
                        {
                            //set the rest of the positions for a T or Cross Intersection
                            pos3 = pos2;
                            pos3.z = pos2.z + 6;

                            pos4 = pos3;
                            pos4.z = pos3.z + 5;

                        }
                        break;
                    }
            }

            //spawn the trees and add parenting.
            GameObject tree1 = spawnRandomTree(pos1);
            tree1.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);

            GameObject tree2 = spawnRandomTree(pos2);
            tree2.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);

            GameObject tree3 = spawnRandomTree(pos3);
            tree3.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);

            GameObject tree4 = spawnRandomTree(pos4);
            tree4.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);

            if (intersection._intersectionType == IntersectionType.ROUNDABOUT)
            {
                GameObject tree5 = spawnRandomTree(pos5);
                tree5.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);

                GameObject tree6 = spawnRandomTree(pos6);
                tree6.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);
            }
        }

    }

    //this function spawns and returns a random tree
    GameObject spawnRandomTree(Vector3 pos)
    {
        //generate a random index
        int index = UnityEngine.Random.Range(1, treesPrefabCount);

        //spawn a tree based on the random index
        GameObject tree = Instantiate(roadPrefabDictionary["TREES" + index], pos, Quaternion.identity);

        return tree;
    }

    #endregion

    #region residential_zone
    //this function adds a house to a contiguous road
    //it does not account for if the house will cover part of the road
    //TODO: only add the house if it fits
    void addHouses(RoadSegment road, ZoneNumber zone)
    {
        //grab the traffic system piece in the middle of the contiguous road
        TrafficSystemPiece tsp = getTrafficSystemPiece(road, (int)System.Math.Round(road._lengthOfRoad / 2f));

        //calculate the spawn position for the house
        //the 15 buffer space keeps the house back so that it does not sit directly on the road
        Vector3 spawnPos = getSpawnPos(road, tsp.transform.position, zone, bufferSpace: 15);

        //set the rotation based on which zone
        float rotation = (float)road._orientation;
        if (zone == ZoneNumber.ONE)
        {
            rotation = rotation - 90;
        }
        else if (zone == ZoneNumber.TWO)
        {
            rotation = rotation + 90;
        }

        //spawn a random house and make it a child of the road element
        GameObject house = spawnRandomHouse(spawnPos, rotation);
        house.transform.SetParent(EnvironmentModel.roadElementGOMapping[road._id].transform);
    }

    //this functions adds a house to an intersection
    //it is currently limited to addint house to the long side of a TIntersection (zone3)
    void addHouses(Intersection intersection, ZoneNumber zone)
    {
        //add a house based on the zoning
        switch (zone)
        {
            case ZoneNumber.ONE:
                {
                    throw new System.Exception("Zoning has not been fully implemented yet");
                    break;
                }
            case ZoneNumber.TWO:
                {
                    throw new System.Exception("Zoning has not been fully implemented yet");
                    break;
                }
            case ZoneNumber.THREE:
                {
                    //if it's a TIntersection, add the house to the long end
                    if (intersection._intersectionType == IntersectionType.T_INTERSECTION)
                    {
                        //find where is the long end
                        RelativePhysicalConnectionLocation longEndLocation = getRelativePhysicalConnectionPoint(intersection._orientation, PhysicalConnectionPointType.CONN_4, RoadElementType.INTERSECTION);

                        //get the position of the center of the intersection
                        Vector3 pos = EnvironmentModel.roadElementGOMapping[intersection._id].transform.position;

                        //get the intersection width
                        int roadWidth = getRoadWidth(intersection);

                        //T intersection location + half road width + buffer to move house off of the road
                        switch (longEndLocation)
                        {
                            case RelativePhysicalConnectionLocation.NORTH:
                                {
                                    //+z
                                    pos.z = pos.z + (roadWidth / 2) + 15;
                                    break;
                                }
                            case RelativePhysicalConnectionLocation.EAST:
                                {
                                    //+x
                                    pos.x = pos.x + (roadWidth / 2) + 15;
                                    break;
                                }
                            case RelativePhysicalConnectionLocation.SOUTH:
                                {
                                    //-z
                                    pos.z = pos.z - (roadWidth / 2) - 15;
                                    break;
                                }
                            case RelativePhysicalConnectionLocation.WEST:
                                {
                                    //-x
                                    pos.x = pos.x - (roadWidth / 2) - 15;
                                    break;
                                }
                        }

                        //spawn the house and set parenting
                        GameObject house = spawnRandomHouse(pos, 180f);
                        house.transform.SetParent(EnvironmentModel.roadElementGOMapping[intersection._id].transform);
                    }
                    else
                    {
                        throw new System.Exception("Zoning has not been fully implemented yet");
                    }

                    break;
                }
            case ZoneNumber.FOUR:
                {
                    throw new System.Exception("Zoning has not been fully implemented yet");
                    break;
                }

        }


    }

    //this function spawns and returns a random house
    GameObject spawnRandomHouse(Vector3 pos, float orientation)
    {
        //generate a random index
        int index = UnityEngine.Random.Range(1, residentialPrefabCount);

        //spawn the house based on the random index
        GameObject house = Instantiate(roadPrefabDictionary["RESIDENTIAL" + index], pos, Quaternion.Euler(0f, orientation, 0f));

        return house;
    }

    #endregion

    #region spawn pos & road width
    Vector3 getSpawnPos(RoadElement road, Vector3 spawnPos, ZoneNumber zone, int bufferSpace = 0)
    {
        int width = getRoadWidth(road);

        if (zone == ZoneNumber.ONE)
        {
            switch (road._orientation)
            {
                case Orientation.VERTICAL:
                    {
                        spawnPos.x = spawnPos.x + (width / 2) + bufferSpace;
                        break;
                    }
                case Orientation.VERTICAL_FLIPPED:
                    {
                        spawnPos.x = spawnPos.x - (width / 2) - bufferSpace;
                        break;
                    }
                case Orientation.HORIZONTAL:
                    {
                        spawnPos.z = spawnPos.z - (width / 2) - bufferSpace;
                        break;
                    }
                case Orientation.HORIZONTAL_FLIPPED:
                    {
                        spawnPos.z = spawnPos.z + (width / 2) + bufferSpace;
                        break;
                    }
                default:
                    {

                        break;
                    }
            }
        }
        else if (zone == ZoneNumber.TWO)
        {
            switch (road._orientation)
            {
                case Orientation.VERTICAL:
                    {
                        spawnPos.x = spawnPos.x - (width / 2) - bufferSpace;
                        break;
                    }
                case Orientation.VERTICAL_FLIPPED:
                    {
                        spawnPos.x = spawnPos.x + (width / 2) + bufferSpace;
                        break;
                    }
                case Orientation.HORIZONTAL:
                    {
                        spawnPos.z = spawnPos.z + (width / 2) + bufferSpace;
                        break;
                    }
                case Orientation.HORIZONTAL_FLIPPED:
                    {
                        spawnPos.z = spawnPos.z - (width / 2) - bufferSpace;
                        break;
                    }
                default:
                    {

                        break;
                    }
            }
        }


        return spawnPos;
    }

    int getRoadWidth(RoadElement road)
    {
        int width;

        switch (road._numLanes)
        {
            case 1:
                {
                    width = 10;
                    break;
                }
            case 2:
                {
                    width = 16;
                    break;
                }
            case 4:
                {
                    width = 24;
                    break;
                }
            case 6:
                {
                    width = 32;
                    break;
                }
            default:
                {
                    throw new System.Exception("Invalid number of lanes.");
                    //break;
                }

        }

        return width;
    }

    #endregion

    #endregion

    #region UI CONTROLS

    #region add play/pause button
    public Button createPlayButton()
    {
        bool Pause = true;
        Time.timeScale = 0;

        //create the button object
        Button button = Instantiate(buttonPrefab, Vector3.zero, Quaternion.identity) as Button;
        button.name = "play_button";
        button.GetComponentInChildren<Text>().text = "Play";

        //set up the play/pause functionality
        //to pause, set the time scale to 0
        //to play, set the time scale back to 1
        button.OnClickAsObservable().Subscribe(_ =>
        {
            if (Pause == true)
            {
                Pause = false;
                Time.timeScale = 1;
                button.GetComponentInChildren<Text>().text = "Pause";
            }
            else
            {
                Pause = true;
                Time.timeScale = 0;
                button.GetComponentInChildren<Text>().text = "Play";
            }
        });

        //add the button to the canvas
        RectTransform rectTransform = button.GetComponent<RectTransform>();
        rectTransform.SetParent(newCanvas.transform);

        return button;
    }


    #endregion

    #region record

    public Button createRecordButton()
    {

        //create the button object
        Button button = Instantiate(buttonPrefab, Vector3.zero, Quaternion.identity) as Button;
        button.name = "record_button";
        button.GetComponentInChildren<Text>().text = "Capture Path Data";

        //set up the recording functionality
        button.OnClickAsObservable().Subscribe(_ =>
        {
            var filePath = EditorUtility.SaveFilePanel(
                    "Save data as csv",
                    System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                    "path-capture.csv",
                    "csv");


            File.WriteAllText(filePath, sb.ToString());

        });

        //add the button to the canvas
        RectTransform rectTransform = button.GetComponent<RectTransform>();
        rectTransform.SetParent(newCanvas.transform);

        return button;
    }

    #endregion

    #endregion
}

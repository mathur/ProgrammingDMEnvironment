﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class RefreshAndRecreateRoadSegment : MonoBehaviour {

    public List<TrafficSystemPiece> roadNetwork = new List<TrafficSystemPiece>();
    public List<TrafficSystemPiece> tIntersectionPrefabs;
    public List<TrafficSystemPiece> xIntersectionPrefabs;
    public List<TrafficSystemPiece> roundaboutPrefabs;
    public Dictionary<int, TrafficSystemPiece> roadPrefabDictionary = new Dictionary<int, TrafficSystemPiece>();
    public Dictionary<int, TrafficSystemPiece> tIntersectionPrefabDictionary = new Dictionary<int, TrafficSystemPiece>();
    public Dictionary<int, TrafficSystemPiece> xIntersectionPrefabDictionary = new Dictionary<int, TrafficSystemPiece>();
    public Dictionary<int, TrafficSystemPiece> roundaboutPrefabDictionary = new Dictionary<int, TrafficSystemPiece>();

    public List<TrafficSystemPiece> roadPrefabs;

    public UpdateText PlaceHolderForTextUpdateFunction;
    public ValueController PlaceHolderForValueControllerFunction;
    public BuildRoad PlaceHolderForRoadFunction;
    public IntersectionButton PlaceHolderForIntersectionFunction;

    private GameObject roadParent;
    private TrafficSystemPiece roadPieceCurrent;
    private TrafficSystemPiece roadPiecePrevious;

    public TrafficSystemVehicle car;

    public bool includeIntersection = false;

    private void Awake()
    {
        
        //Set up initial data structures
        roadPrefabDictionary.Add(1, (Resources.Load("Prefabs/1 Lane - Blue Straight") as GameObject).GetComponent<TrafficSystemPiece>());
        roadPrefabDictionary.Add(2, (Resources.Load("Prefabs/2 Lanes - Straight - ( Road V2 )") as GameObject).GetComponent<TrafficSystemPiece>());
        roadPrefabDictionary.Add(4, (Resources.Load("Prefabs/4 Lanes - Straight - ( Road V2 )") as GameObject).GetComponent<TrafficSystemPiece>());
        roadPrefabDictionary.Add(6, (Resources.Load("Prefabs/6 Lanes - Straight - ( Road V2 )") as GameObject).GetComponent<TrafficSystemPiece>());


        //load prefab dictionaries for all intersection types
        tIntersectionPrefabDictionary.Add(2, tIntersectionPrefabs[0]);
        tIntersectionPrefabDictionary.Add(4, tIntersectionPrefabs[1]);
        tIntersectionPrefabDictionary.Add(6, tIntersectionPrefabs[2]);

        xIntersectionPrefabDictionary.Add(2, xIntersectionPrefabs[0]);
        xIntersectionPrefabDictionary.Add(4, xIntersectionPrefabs[1]);
        xIntersectionPrefabDictionary.Add(6, xIntersectionPrefabs[2]);

        roundaboutPrefabDictionary.Add(2, roundaboutPrefabs[0]);
        roundaboutPrefabDictionary.Add(4, roundaboutPrefabs[1]);
        roundaboutPrefabDictionary.Add(6, roundaboutPrefabs[2]);
    }

    void Start()
    {
        //Set up the refresh function to be called when road length or num lanes changes
        //CAUTION: Make sure ReactiveProperties are set up before Subscribe 
        PlaceHolderForValueControllerFunction.numLanes
            .Merge(PlaceHolderForValueControllerFunction.roadLength)
            .Subscribe(_=> Refresh());
    }

    public void Refresh()
    {
        //Called when either the num of lanes slider or the length of road segment slider value is changed
        int numLanes = PlaceHolderForValueControllerFunction.GetNumLanes();
        int roadLength = PlaceHolderForValueControllerFunction.GetRoadLength();

        foreach (TrafficSystemPiece temp in roadNetwork)
        {
            Destroy(temp.gameObject);
        }
        roadNetwork.Clear();

        //rebuild the road if the length is not 0
        if(roadLength > 0)
        {
            //make first piece
            roadPieceCurrent = PlaceHolderForRoadFunction.spawnRoadPiece(roadPrefabDictionary[numLanes], new Vector3(0f, 0f, 0f), Quaternion.Euler(0f, 0f, 0f), "0 - " + roadPrefabDictionary[numLanes].name);
            //add to network
            roadNetwork.Add(roadPieceCurrent);
            roadPiecePrevious = roadPieceCurrent;

            //build rest of road
            //this is a hack to see if you placed an intersection or not.
            if (includeIntersection)
            {
                for (int i = 1; i < roadLength - 1; i++)
                {
                    //create road piece
                    roadPieceCurrent = PlaceHolderForRoadFunction.spawnRoadPiece(roadPrefabDictionary[numLanes], new Vector3(0f, 0f, 0f), Quaternion.identity, i + " - " + roadPrefabDictionary[numLanes].name);
                    //add to network
                    roadNetwork.Add(roadPieceCurrent);
                    //format road piece to match the previous
                    PlaceHolderForRoadFunction.FormatRoadSegment(roadPieceCurrent, roadPiecePrevious, BuildRoad.RoadAttachmentPoint.NORTH);
                    roadPiecePrevious = roadPieceCurrent;
                }
                PlaceHolderForIntersectionFunction.addIntersection();
            }
            else
            {
                for (int i = 1; i < roadLength; i++)
                {
                    //create road piece
                    roadPieceCurrent = PlaceHolderForRoadFunction.spawnRoadPiece(roadPrefabDictionary[numLanes], new Vector3(0f, 0f, 0f), Quaternion.identity, i + " - " + roadPrefabDictionary[numLanes].name);
                    //add to network
                    roadNetwork.Add(roadPieceCurrent);
                    //format road piece to match the previous
                    PlaceHolderForRoadFunction.FormatRoadSegment(roadPieceCurrent, roadPiecePrevious, BuildRoad.RoadAttachmentPoint.NORTH);
                    roadPiecePrevious = roadPieceCurrent;

                }
            }
            
        }
        
                
        //CallUpdateTextOnLabels
        PlaceHolderForTextUpdateFunction.UpdateTextOnLabels();

        
    }



    //return the list of TrafficSystemPieces that comprise the road network
    public List<TrafficSystemPiece> GetRoadNetwork()
    {
        return roadNetwork;
    }

    //allows you to add a piece to the road network.
    public void addToRoadNetwork(TrafficSystemPiece roadPiece)
    {
        roadNetwork.Add(roadPiece);
    }

    public void subtractFromRoadNetwork(int index = -1)
    {
        //if no index is specified, delete the last road network piece. Otherwise delete that specific index
        if (index == -1)
        {
            roadNetwork.RemoveAt(roadNetwork.Count - 1);
        }
        else
        {
            roadNetwork.RemoveAt(index);
        }

    }

}

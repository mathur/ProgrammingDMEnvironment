﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;

public class IntersectionButton : MonoBehaviour {

    public Button buttonAddIntersection;

    public BuildRoad PlaceHolderForRoadFunction;
    public ValueController PlaceHolderForValueControllerFunction;
    public RefreshAndRecreateRoadSegment PlaceHolderForRefreshFunction;

    private string intersectionType;
    private int numLanes;
    private List<TrafficSystemPiece> roadNetwork;
    private TrafficSystemPiece intersection;


    // Use this for initialization
    void Start () {
        //add listener so that the add intersection button works
        buttonAddIntersection.onClick.AddListener(addIntersection);
    }


    //this function spawns the intersection and adds it to the network
    public void addIntersection()
    {
        //get the road specs: number of lanes and intersection type
        numLanes = PlaceHolderForValueControllerFunction.GetNumLanes();
        intersectionType = PlaceHolderForValueControllerFunction.GetIntersectionType();

        //get the actual road network
        roadNetwork = PlaceHolderForRefreshFunction.GetRoadNetwork();

        //the intersection only exists for 2-6 lanes
        if (numLanes > 1)
        {
            //change the flag so the road network knows to include the intersection
            PlaceHolderForRefreshFunction.includeIntersection = true;

            //based on the intersection type, spawn the appropriate prefab for the specified number of lanes
            if (intersectionType == PlaceHolderForRefreshFunction.tIntersectionPrefabs[0].m_roadPieceType.ToString())
            {
                intersection = PlaceHolderForRoadFunction.spawnRoadPiece(PlaceHolderForRefreshFunction.tIntersectionPrefabDictionary[numLanes], new Vector3(0f, 0f, 0f), PlaceHolderForRefreshFunction.tIntersectionPrefabDictionary[numLanes].transform.rotation, PlaceHolderForRefreshFunction.tIntersectionPrefabDictionary[numLanes].name);

            }
            else if (intersectionType == PlaceHolderForRefreshFunction.xIntersectionPrefabs[0].m_roadPieceType.ToString())
            {
                intersection = PlaceHolderForRoadFunction.spawnRoadPiece(PlaceHolderForRefreshFunction.xIntersectionPrefabDictionary[numLanes], new Vector3(0f, 0f, 0f), PlaceHolderForRefreshFunction.xIntersectionPrefabDictionary[numLanes].transform.rotation, PlaceHolderForRefreshFunction.xIntersectionPrefabDictionary[numLanes].name);
            }
            else
            {
                intersection = PlaceHolderForRoadFunction.spawnRoadPiece(PlaceHolderForRefreshFunction.roundaboutPrefabDictionary[numLanes], new Vector3(0f, 0f, 0f), PlaceHolderForRefreshFunction.roundaboutPrefabDictionary[numLanes].transform.rotation, PlaceHolderForRefreshFunction.roundaboutPrefabDictionary[numLanes].name);
            }

            //if there is a road network already, format them to match
            if (roadNetwork.Count > 0)
            {
                PlaceHolderForRoadFunction.FormatRoadSegment(intersection, roadNetwork[roadNetwork.Count - 1], BuildRoad.RoadAttachmentPoint.NORTH, true);
            }

            //add the intersection to the road network
            PlaceHolderForRefreshFunction.addToRoadNetwork(intersection);
        }
        else
        {
            //do nothing.
        }
    }

    //will logically connect the intersection to the rest of the road network
    public void connectIntersection()
    {
        //can only do this if the roadnetwork count is greater than 1
        if (roadNetwork.Count > 1)
        {
            //grab the intersection
            intersection = roadNetwork[roadNetwork.Count - 1];
            //grab the road segment before the intersection in the road network
            TrafficSystemPiece connectingRoad = roadNetwork[roadNetwork.Count - 2];

            //create placeholder for the intersection attachment point (child road segment)
            TrafficSystemPiece intersectionAttachmentPoint;

            //connect the nodes based on the number of lanes of the road
            switch (numLanes)
            {
                //0 lane
                case 0:
                    //do nothing
                    break;

                //1 lane
                case 1:
                    //do nothing
                    break;

                //2 lane
                case 2:
                    

                    //This section is a hack to connect the road traffic so that it flows properly. 
                    //The left and right lanes are representing the blue (left) and pink (right) dots in the prefabs, 
                    //and if the blue dots on the road piece don't line up with the blue dots on the intersection,
                    //you have to cross the connection. This should be fixed in the future by modifying the prefab pieces.
                    if (intersectionType == PlaceHolderForRefreshFunction.tIntersectionPrefabs[0].m_roadPieceType.ToString())
                    {
                        //set the attachment point as the appropriate child of the intersection prefab
                        intersectionAttachmentPoint = intersection.gameObject.transform.GetChild(0).GetComponent<TrafficSystemPiece>();

                        connectingRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(intersectionAttachmentPoint.m_primaryRightLaneNodes[0]);
                        intersectionAttachmentPoint.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[0]);
                    }
                    else
                    {
                        //set the attachment point as the appropriate child of the intersection prefab
                        intersectionAttachmentPoint = intersection.gameObject.transform.GetChild(0).GetComponent<TrafficSystemPiece>();

                        connectingRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(intersectionAttachmentPoint.m_primaryRightLaneNodes[0]);
                        intersectionAttachmentPoint.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[0]);
                    }
                    break;

                //4 lane
                case 4:
                    //set the attachment point as the appropriate child of the intersection prefab
                    intersectionAttachmentPoint = intersection.gameObject.transform.GetChild(0).GetComponent<TrafficSystemPiece>();

                    //This section is a hack to connect the road traffic so that it flows properly. 
                    //The left and right lanes are representing the blue and pink dots in the prefabs, 
                    //and if the blue dots on the road piece don't line up with the blue dots on the intersection,
                    //you have to cross the connection. This should be fixed in the future by modifying the prefab pieces.
                    if (intersectionType == PlaceHolderForRefreshFunction.tIntersectionPrefabs[0].m_roadPieceType.ToString())
                    {
                        connectingRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(intersectionAttachmentPoint.m_primaryLeftLaneNodes[0]);
                        connectingRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(intersectionAttachmentPoint.m_primaryLeftLaneNodes[1]);

                        intersectionAttachmentPoint.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[0]);
                        intersectionAttachmentPoint.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[1]);
                    }
                    else
                    {
                        connectingRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(intersectionAttachmentPoint.m_primaryRightLaneNodes[0]);
                        connectingRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(intersectionAttachmentPoint.m_primaryRightLaneNodes[1]);

                        intersectionAttachmentPoint.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[0]);
                        intersectionAttachmentPoint.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[1]);
                    }
                    
                    break;

                //6 lane
                case 6:
                    //set the attachment point as the appropriate child of the intersection prefab
                    if (intersectionType == PlaceHolderForRefreshFunction.xIntersectionPrefabs[0].m_roadPieceType.ToString())
                    {
                        intersectionAttachmentPoint = intersection.gameObject.transform.GetChild(0).GetComponent<TrafficSystemPiece>();
                    }
                    else
                    {
                        intersectionAttachmentPoint = intersection.gameObject.transform.GetChild(0).GetComponent<TrafficSystemPiece>();
                    }

                    

                    //This section is a hack to connect the road traffic so that it flows properly. 
                    //The left and right lanes are representing the blue and pink dots in the prefabs, 
                    //and if the blue dots on the road piece don't line up with the blue dots on the intersection,
                    //you have to cross the connection. This should be fixed in the future by modifying the prefab pieces.
                    if (intersectionType == PlaceHolderForRefreshFunction.tIntersectionPrefabs[0].m_roadPieceType.ToString())
                    {
                        connectingRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(intersectionAttachmentPoint.m_primaryLeftLaneNodes[0]);
                        connectingRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(intersectionAttachmentPoint.m_primaryLeftLaneNodes[1]);
                        connectingRoad.m_primaryRightLaneNodes[2].m_connectedNodes.Add(intersectionAttachmentPoint.m_primaryLeftLaneNodes[2]);

                        intersectionAttachmentPoint.m_primaryRightLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[0]);
                        intersectionAttachmentPoint.m_primaryRightLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[1]);
                        intersectionAttachmentPoint.m_primaryRightLaneNodes[2].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[2]);
                    }
                    else
                    {
                        connectingRoad.m_primaryRightLaneNodes[0].m_connectedNodes.Add(intersectionAttachmentPoint.m_primaryRightLaneNodes[0]);
                        connectingRoad.m_primaryRightLaneNodes[1].m_connectedNodes.Add(intersectionAttachmentPoint.m_primaryRightLaneNodes[1]);
                        connectingRoad.m_primaryRightLaneNodes[2].m_connectedNodes.Add(intersectionAttachmentPoint.m_primaryRightLaneNodes[2]);

                        intersectionAttachmentPoint.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[0]);
                        intersectionAttachmentPoint.m_primaryLeftLaneNodes[1].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[1]);
                        intersectionAttachmentPoint.m_primaryLeftLaneNodes[2].m_connectedNodes.Add(connectingRoad.m_primaryLeftLaneNodes[2]);
                    }
                    
                    break;
            }
        }
    }



}

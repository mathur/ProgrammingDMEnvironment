﻿/*
 * This is the structure for an aggregated Test Log.
 * This is going to be the output of an automated Test.
 * Version 0.4 has support for AggregatedLogStandard and repetitions of TestIteration
 * Version 0.5 has support for energies specified in the monitor, behavior tracking
 * Version 0.6 has support for showing SA repetitions, rangedVar values at each iteration and their min/max values
 *  */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AggregatedLog
{
    public string testType;
    public string version = "0.6";
}


[System.Serializable]
public class AggregatedLogIterative : AggregatedLog
{
    public string setVar;
    public int numIterations;
    public int SArepetitions; // Number of times simulated annealing is performed per iteration
    public float singleIterationDuration; // In seconds
    public int repetitionsPerIteration; // Repeat a given iteration several times
    public List<MinMax> minMaxValues = new List<MinMax>();
    public List<TestIteration> testIterations;

    public Dictionary<string, OnlineBehavior> onlineBehaviors;

    public AggregatedLogIterative(AutomatedVarSetMethod sVar, int n, float duration, int repetitions = 1, List<ProDM> testVars = null)
    {
        setVar = sVar.ToString();
        testType = "ITERATIVE";
        numIterations = n;
        singleIterationDuration = duration;
        testIterations = new List<TestIteration>(numIterations);
        repetitionsPerIteration = repetitions;

        foreach(VarInterval<float> elem in testVars)
        {
            minMaxValues.Add(new MinMax(elem._min, elem._max));
        }
    }

    public AggregatedLogIterative(AutomatedVarSetMethod sVar, int n, float duration, int saReps, int repetitions = 1, List<ProDM> testVars = null)
    {
        setVar = sVar.ToString();
        testType = "ITERATIVE";
        numIterations = n;
        singleIterationDuration = duration;
        SArepetitions = saReps;
        testIterations = new List<TestIteration>(numIterations);
        repetitionsPerIteration = repetitions;

        foreach (VarInterval<float> elem in testVars)
        {
            minMaxValues.Add(new MinMax(elem._min, elem._max));
        }
    }

    public AggregatedLogIterative(AutomatedVarSetMethod sVar, int n, float duration, int repetitions = 1, List<VarInterval<float>> rangedFloats = null)
    {
        setVar = sVar.ToString();
        testType = "ITERATIVE";
        numIterations = n;
        singleIterationDuration = duration;
        testIterations = new List<TestIteration>(numIterations);
        repetitionsPerIteration = repetitions;

        foreach (VarInterval<float> elem in rangedFloats)
        {
            minMaxValues.Add(new MinMax(elem._min, elem._max));
        }
    }

    public AggregatedLogIterative(List<string> onlineBehaviorList, int n, float duration, int repetitions = 1)
    {
        onlineBehaviors = new Dictionary<string, OnlineBehavior>();
        foreach (var behavior in onlineBehaviorList)
        {
            OnlineBehavior ob = new OnlineBehavior(behavior, 0);
            onlineBehaviors.Add(behavior, ob);
        }

        testType = "ITERATIVE";
        numIterations = n;
        singleIterationDuration = duration;
        testIterations = new List<TestIteration>(numIterations);
        repetitionsPerIteration = repetitions;
    }
}

[System.Serializable]
public class MinMax
{
    public float min;
    public float max;

    public MinMax(float _min, float _max)
    {
        min = _min;
        max = _max;
    }
}

// Log for each test iteration
[System.Serializable]
public class TestIteration
{
    public int iterationNumber;
    public int saRepetitionNumber;
    public string timeStarted;
    public List<float> rangedValues = new List<float>();
    public List<int> testVarsDiscrete = new List<int>();
    public List<OnlineEvent> onlineEvents = new List<OnlineEvent>();
    public List<CollisionEvent> collision = new List<CollisionEvent>();
    public List<InactivityEvent> inactivity = new List<InactivityEvent>();
    public List<float> averageSpeedPerFrame = new List<float>(); //average speed on a frame-by-frame basis
    public List<ActorBehavior> behaviors = new List<ActorBehavior>();

    //Some aggregated info on the run
    public float distanceCovered;
    public float averageSpeed;
    public float displacement;
    public Vector3 displacementVector;
    public int repetitionNum; // If there are several repetitions, the number of the repetition
    public TestIteration(int iterationNum, string time, int repetitionNumber = 1)
    {
        iterationNumber = iterationNum;
        timeStarted = time;
        repetitionNum = repetitionNumber;
        
    }

    public TestIteration(int iterationNum, int saRepetitionNum, string time, int repetitionNumber = 1)
    {
        iterationNumber = iterationNum;
        saRepetitionNumber = saRepetitionNum;
        timeStarted = time;
        repetitionNum = repetitionNumber;

    }
}

// Base class
[System.Serializable]
public class AggregatedLogStandard : AggregatedLog
{
    public string timeStarted;
    public float testDuration; //In seconds
    public List<OnlineEvent> onlineEvents = new List<OnlineEvent>();
    public List<CollisionEvent> collision = new List<CollisionEvent>();
    public List<InactivityEvent> inactivity = new List<InactivityEvent>();
    public List<float> averageSpeedPerFrame = new List<float>(); //average speed on a frame-by-frame basis
    //Some aggregated info on the run
    public float distanceCovered;
    public float averageSpeed;
    public float displacement;
    public Vector3 displacementVector;

    public AggregatedLogStandard (float duration, string time)
    {
        testType = "STANDARD";

        timeStarted = time;
        testDuration = duration;
    }
}

// Temporal tests
[System.Serializable]
public class AggregatedLogTemporal : AggregatedLogStandard
{
    public List<List<VarInterval<float>>> testVals;
    public List<List<float>> targetVals;
    public List<float> secondsToDisturbances;


    public AggregatedLogTemporal(List<List<VarInterval<float>>> valBefore, List<List<float>> valAfter, List<float> timeToDisturbance, 
        float duration, string time) : base(duration, time)
    {
        testType = "TEMPORAL";
        testVals = new List<List<VarInterval<float>>>();

        //Make a snapshot of valBefore
        for (int i = 0; i < valBefore.Count; i++)
        {
            List<VarInterval<float>> temp = new List<VarInterval<float>>();

            foreach (var elem in valBefore[i])
            {
                VarInterval<float> elemClone = new VarInterval<float>(elem);
                temp.Add(elemClone);
            }
            testVals.Add(temp);
        }
        targetVals = valAfter;
        secondsToDisturbances = timeToDisturbance;
        timeStarted = time;
        testDuration = duration;
    }
}

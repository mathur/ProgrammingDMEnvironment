﻿/*
 * Some data structures that help in specifying 
 * settings of the environment, for example fog and lighting.
 * 
 *  */

//Environment settings with characteristics of light
public class LightSettings
{
    public VarInterval<float> _directionalIntensity;
    public ProDMVector3 _direction;
    public ProDMColor _color;

    public VarInterval<float> _ambientIntensity;

    //Default values
    public LightSettings()
    {
        _directionalIntensity = 1f;
        _direction = new ProDMVector3(45f, 45f, 45f);
        _color = new ProDMColor(1f, 0.9568627f, 0.8392157f, 1f); //Natural looking yellow-ish tint
        _ambientIntensity = 1f;
    }

    public LightSettings(VarInterval<float> intensity, ProDMVector3 direction, ProDMColor color, VarInterval<float> ambientIntensity)
    {
        _directionalIntensity = intensity;
        _direction = direction;
        _color = color;
        _ambientIntensity = ambientIntensity;
    }


}

//Environment settings with characteristics of fog
public class FogSettings
{
    //This is normalized- [0, 1]
    public VarInterval<float> _density;

    //Default values
    public FogSettings()
    {
        _density = 0f; //No fog
    }

    public FogSettings(VarInterval<float> density)
    {
        _density = density;
    }

}

//Encapsulation of settings for the environment, e.g. light and fog
public class EnvironmentSettings
{
    public LightSettings _light;
    public FogSettings _fog;

    //Set default values
    public EnvironmentSettings()
    {
        _light = new LightSettings();
        _fog = new FogSettings();
    }

    public EnvironmentSettings(LightSettings light, FogSettings fog)
    {
        _light = light;
        _fog = fog;
    }
}

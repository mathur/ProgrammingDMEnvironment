﻿/*
 * Consists of structures for physical and logical connections between road elements
 *  */

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//We consider 5 possible connection points (physical connections)
public enum PhysicalConnectionPointType
{ NONE, CONN_1, CONN_2, CONN_3, CONN_4 };

//An actual physical connection with connection type and owner reference
public class PhysicalConnection
{
    //The RoadElement this belongs to
    public RoadElement _belongsTo;

    //The kind of physical connection point it is
    public PhysicalConnectionPointType _typeOfConnectionPoint;

    //Constructor to set appropriate values
    public PhysicalConnection(RoadElement belongsTo, PhysicalConnectionPointType typeOfConnectionPoint)
    {
        _belongsTo = belongsTo;
        _typeOfConnectionPoint = typeOfConnectionPoint;
    }
}

//Class structure for connections in a RoadElement
public class Connections
{
    //Dictionary mapping one physical connection to another
    public Dictionary<PhysicalConnection, PhysicalConnection> _physicalConnections;

    public Connections()
    {

    }

    //Construct Connections depending on the type of Road Element
    public Connections(RoadElement roadElement)
    {
        //Set-up all possible connections
        PhysicalConnection conn1 = new PhysicalConnection(roadElement, PhysicalConnectionPointType.CONN_1);
        PhysicalConnection conn2 = new PhysicalConnection(roadElement, PhysicalConnectionPointType.CONN_2);
        PhysicalConnection conn3 = new PhysicalConnection(roadElement, PhysicalConnectionPointType.CONN_3);
        PhysicalConnection conn4 = new PhysicalConnection(roadElement, PhysicalConnectionPointType.CONN_4);


        //Init data members

        _physicalConnections = new Dictionary<PhysicalConnection, PhysicalConnection>();

        //Add elems to _physicalConnections Dictionary depending on road element type
        if (roadElement._roadElementType == RoadElementType.CONTIGUOUS_ROAD)
        {
            _physicalConnections.Add(conn1, null);
            _physicalConnections.Add(conn2, null);
        }
        else if (roadElement._roadElementType == RoadElementType.INTERSECTION)
        {
            //Cast roadElement to Intersection type
            Intersection intersection = (Intersection)roadElement;
            switch (intersection._intersectionType)
            {
                case IntersectionType.T_INTERSECTION:
                    //3 connections
                    _physicalConnections.Add(conn1, null);
                    _physicalConnections.Add(conn2, null);
                    _physicalConnections.Add(conn3, null);
                    break;
                case IntersectionType.CROSS_INTERSECTION:
                    //4 connections
                    _physicalConnections.Add(conn1, null);
                    _physicalConnections.Add(conn2, null);
                    _physicalConnections.Add(conn3, null);
                    _physicalConnections.Add(conn4, null);
                    break;
                case IntersectionType.ROUNDABOUT:
                    //Same as X-Intersection
                    _physicalConnections.Add(conn1, null);
                    _physicalConnections.Add(conn2, null);
                    _physicalConnections.Add(conn3, null);
                    _physicalConnections.Add(conn4, null);
                    break;
                default:
                    throw new Exception("Cannot create Connections for this Intersection.");
            }
        }
        else
            throw new Exception("Cannot create Connections for this RoadElement.");
    }
    
    //Create connections based on some map. Directionality is explicity in the Dictionary structure (Key->Value)
    public bool MakeConnections (RoadElement r1, RoadElement r2, Dictionary<PhysicalConnection, PhysicalConnection> connectionMap)
    {
        //TODO: Some checks

        //Make actual connections
        foreach (var keyValPair in connectionMap)
        {
            //Non generic Road Element to which the Key belongs
            RoadElement owner_r1 = keyValPair.Key._belongsTo;
            
            //Check if Key belongs to the calling RoadElement or to one of its descendents
            if (r1.Encapsulates(owner_r1))
            {
                //Make connection on this object
                owner_r1._connections._physicalConnections[keyValPair.Key] = keyValPair.Value;

                //Check if Value belongs to r2 or to one of its descendents

                //Non generic Road Element to which the Value belongs
                RoadElement owner_r2 = keyValPair.Value._belongsTo;
                if (r2.Encapsulates(owner_r2))
                {
                    //Make connection in r2
                    owner_r2._connections._physicalConnections[keyValPair.Value] = keyValPair.Key;
                }

            }
        }

        //return false; //Failure due to non-ownership

        // Always return true
        // TODO: error in case of non-ownership
        return true;
    }

    //Helper function to get PhysicalConnection at a particular index
    public PhysicalConnection AtIndex(int index)
    {
        return _physicalConnections.ElementAt(index).Key;
    }

    //Helper function to get the next free PhysicalConnection
    public PhysicalConnection NextFree()
    {
        List<PhysicalConnection> freePhysicalConnections = _physicalConnections.Where(x => x.Value == null)
            .Select(i => i.Key).ToList();

        return freePhysicalConnections[0];
    }
}
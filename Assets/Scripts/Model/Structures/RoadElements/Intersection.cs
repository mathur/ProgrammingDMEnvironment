﻿/*
 * Contains descriptions of road elements of type INTERSECTION
 * 
 *  */

//Enumerator for specifying the kind of intersection
using System.Collections.Generic;
using UnityEngine;

public enum IntersectionType {NONE, T_INTERSECTION, CROSS_INTERSECTION, ROUNDABOUT};

public enum IntersectionSide {CONN1_LEFT, CONN1_RIGHT, CONN2_LEFT, CONN2_RIGHT, CONN3_LEFT, CONN3_RIGHT, CONN4_LEFT, CONN4_RIGHT};

public class Intersection : RoadElement 
{
    public IntersectionType _intersectionType;

    //The zoning describes the zone to the left of the connection point
    //Ex: zone1 describes the area to the left of CONN_1
    //zone3 for a TIntersection describes the entire long side
    public Zoning _zone1; //Describes what's there on either side of the road
    public Zoning _zone2;
    public Zoning _zone3;
    public Zoning _zone4; //not applicable for TIntersection

    public bool _pedestrianCrossing;
    public bool _trafficSignal;

    public Intersection (string id = "") : base (id)
    {
        _roadElementType = RoadElementType.INTERSECTION;
        _intersectionType = IntersectionType.NONE;

    }

}

public class TIntersection : Intersection
{
    public TIntersection (VarEnum<int> numLanes, ProDMVector3 pos, string id = "", Zoning zone1 = Zoning.NONE, 
        Zoning zone2 = Zoning.NONE, Zoning zone3 = Zoning.NONE, Orientation orientation = Orientation.VERTICAL, bool trafficSignal = true, bool pedestrianCrossing = true ) : base(id)
    {
        //Set type
        _intersectionType = IntersectionType.T_INTERSECTION;

        //Copy values passed
        _numLanes = numLanes;
        _pos = pos;
        _orientation = orientation;
        _zone1 = zone1;
        _zone2 = zone2;
        _zone3 = zone3;
        _pedestrianCrossing = pedestrianCrossing;
        _trafficSignal = trafficSignal;

        //There are 3 free connection points
        _connections = new Connections(this);

    }

    public TIntersection(VarEnum<int> numLanes, string id = "", Zoning zone1 = Zoning.NONE,
        Zoning zone2 = Zoning.NONE, Zoning zone3 = Zoning.NONE, Orientation orientation = Orientation.VERTICAL, bool trafficSignal = true, bool pedestrianCrossing = true) : base(id)
    {
        //Set type
        _intersectionType = IntersectionType.T_INTERSECTION;

        //Copy values passed
        _numLanes = numLanes;
        _pos = Vector3.zero;
        _orientation = orientation;
        _zone1 = zone1;
        _zone2 = zone2;
        _zone3 = zone3;
        _pedestrianCrossing = pedestrianCrossing;
        _trafficSignal = trafficSignal;

        //There are 3 free connection points
        _connections = new Connections(this);

    }
}

public class CrossIntersection : Intersection
{
    public CrossIntersection(VarEnum<int> numLanes, ProDMVector3 pos, string id = "", Zoning zone1 = Zoning.NONE,
        Zoning zone2 = Zoning.NONE, Zoning zone3 = Zoning.NONE, Zoning zone4 = Zoning.NONE, Orientation orientation = Orientation.VERTICAL, bool trafficSignal = true, bool pedestrianCrossing = true) : base(id)
    {
        //Set type
        _intersectionType = IntersectionType.CROSS_INTERSECTION;

        //Copy values passed
        _numLanes = numLanes;
        _pos = pos;
        _orientation = orientation;
        _zone1 = zone1;
        _zone2 = zone2;
        _zone3 = zone3;
        _zone4 = zone4;
        _pedestrianCrossing = pedestrianCrossing;
        _trafficSignal = trafficSignal;

        //There are 4 free connection points
        _connections = new Connections(this);

    }

        public CrossIntersection(VarEnum<int> numLanes, string id = "", Zoning zone1 = Zoning.NONE,
        Zoning zone2 = Zoning.NONE, Zoning zone3 = Zoning.NONE, Zoning zone4 = Zoning.NONE, Orientation orientation = Orientation.VERTICAL, bool trafficSignal = true, bool pedestrianCrossing = true) : base(id)
    {
        //Set type
        _intersectionType = IntersectionType.CROSS_INTERSECTION;

        //Copy values passed
        _numLanes = numLanes;
        _pos = Vector3.zero;
        _orientation = orientation;
        _zone1 = zone1;
        _zone2 = zone2;
        _zone3 = zone3;
        _zone4 = zone4;
        _pedestrianCrossing = pedestrianCrossing;
        _trafficSignal = trafficSignal;

        //There are 4 free connection points
        _connections = new Connections(this);

    }
}

public class Roundabout : Intersection
{
    public Roundabout(VarEnum<int> numLanes, ProDMVector3 pos, string id = "", Zoning zone1 = Zoning.NONE,
        Zoning zone2 = Zoning.NONE, Zoning zone3 = Zoning.NONE, Zoning zone4 = Zoning.NONE, Orientation orientation = Orientation.VERTICAL) : base(id)
    {
        //Set type
        _intersectionType = IntersectionType.ROUNDABOUT;

        //Copy values passed
        _numLanes = numLanes;
        _pos = pos;
        _orientation = orientation;
        _zone1 = zone1;
        _zone2 = zone2;
        _zone3 = zone3;
        _zone4 = zone4;

        //There are 4 free connection points
        _connections = new Connections(this);

    }

    public Roundabout(VarEnum<int> numLanes, string id = "", Zoning zone1 = Zoning.NONE,
        Zoning zone2 = Zoning.NONE, Zoning zone3 = Zoning.NONE, Zoning zone4 = Zoning.NONE, Orientation orientation = Orientation.VERTICAL) : base(id)
    {
        //Set type
        _intersectionType = IntersectionType.ROUNDABOUT;

        //Copy values passed
        _numLanes = numLanes;
        _pos = Vector3.zero;
        _orientation = orientation;
        _zone1 = zone1;
        _zone2 = zone2;
        _zone3 = zone3;
        _zone4 = zone4;

        //There are 4 free connection points
        _connections = new Connections(this);

    }
}
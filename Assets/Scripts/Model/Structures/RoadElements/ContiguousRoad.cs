﻿/*
 * Structures for road elements of type CONTIGUOUS_ROAD
 *  */
using System.Collections.Generic;
using UnityEngine;

//Enumerator for specifying the kind of contiguous road segment
public enum ContiguousRoadType { STRAIGHT, CURVED, ENTRY, EXIT };

//Used to identify the side of the contiguous road
public enum SIDEWALK { LEFT, RIGHT } //Assumption is a VERTICAL Orientation

//Used to identify the lane of the contiguous road
public enum LANE { PLUS_ONE = 1, PLUS_TWO = 2, PLUS_THREE = 3, MINUS_ONE = -1, MINUS_TWO = -2, MINUS_THREE = -3 } //Assumption is a VERTICAL Orientation

public class RoadSegment : RoadElement
{
    public ContiguousRoadType _contiguousRoadType;
    public VarInterval<int> _lengthOfRoad; //Straight segment length of the road

    //The zoning describes the zone to the left of the connection point
    //Ex: zone1 describes the area to the left of CONN_1
    public Zoning _zone1; //Describes what's there on either side of the road
    public Zoning _zone2;

    //TODO: Set the value of singular piece size for calculation of index of piece using distance
    private float sizeOfSingularPiece = 8f;

    public RoadSegment(string id = "") : base(id)
    {
        _roadElementType = RoadElementType.CONTIGUOUS_ROAD;
        _contiguousRoadType = ContiguousRoadType.STRAIGHT;
        _lengthOfRoad = 1;

    }

    //Exhaustive constructor
    public RoadSegment(VarInterval<int> lengthOfRoad, VarEnum<int> numLanes, ProDMVector3 pos, string id = "", Orientation orientation = Orientation.VERTICAL, 
        Zoning zone1 = Zoning.NONE, Zoning zone2 = Zoning.NONE, ContiguousRoadType contiguousRoadType = ContiguousRoadType.STRAIGHT) : base(id)
    {
        _roadElementType = RoadElementType.CONTIGUOUS_ROAD;
        //TODO: What does an ENTRY or EXIT type of length x mean?
        _lengthOfRoad = lengthOfRoad;
        _numLanes = numLanes;
        _contiguousRoadType = contiguousRoadType;
        _pos = pos;
        _orientation = orientation;
        _zone1 = zone1;
        _zone2 = zone2;

        //Set-up connections
        _connections = new Connections(this);

    }

    //Constructor with only length of road and number of lanes
    public RoadSegment (VarInterval<int> lengthOfRoad, VarEnum<int> numLanes, string id = "", Orientation orientation = Orientation.VERTICAL,
        Zoning zone1 = Zoning.NONE, Zoning zone2 = Zoning.NONE, ContiguousRoadType contiguousRoadType = ContiguousRoadType.STRAIGHT) : base(id)
    {
        _roadElementType = RoadElementType.CONTIGUOUS_ROAD;
        _lengthOfRoad = lengthOfRoad;
        _numLanes = numLanes;
        _pos = Vector3.zero;
        _orientation = orientation;
        _contiguousRoadType = contiguousRoadType;
        _zone1 = zone1;
        _zone2 = zone2;

        //Set-up connections
        _connections = new Connections(this);

    }

    //returns the coordinates of a place on a contiguous road based on the normalized distance
    public Vector3 normalizedDistancePosition (float normalizedVal)
    {
        //calculate the distance from the end of the road based on the normalized value
        float dist = (_lengthOfRoad) * normalizedVal * sizeOfSingularPiece;

        Vector3 position = Vector3.zero;
        
        //based on the orientation, find the actual coordinates.
        //The 4.5 offset is used because the position of the road is located in the middle of the first road segment piece
        //First add or subtract 4.5 based on the orientation to get to the actual end of the road, then apply the calculated distance
        switch(_orientation)
        {
            case Orientation.VERTICAL:
                {
                    position.x = _pos._x;
                    position.z = (_pos._y - 4.5f) + dist;
                    break;
                }
            case Orientation.VERTICAL_FLIPPED:
                {
                    position.x = _pos._x;
                    position.z = (_pos._y + 4.5f) - dist;
                    break;
                }
            case Orientation.HORIZONTAL:
                {
                    position.x = (_pos._x - 4.5f) + dist;
                    position.z = _pos._y;
                    break;
                }
            case Orientation.HORIZONTAL_FLIPPED:
                {
                    position.x = (_pos._x + 4.5f) - dist;
                    position.z = _pos._y;
                    break;
                }
            default:
                {
                    position = new Vector3(_pos._x, 0, _pos._y);
                    break;
                }
        }

        return position;

        //throw new System.Exception("This structure hasn't been fully implemented yet");
    }

    //returns the index for the road segment of a contiguous road that corresponds 
    //with the given normalized distance along that road
    public int normalizedDistanceRoadSegmentIndex(float normalizedDistance)
    {
        int index = (int)System.Math.Round(_lengthOfRoad * normalizedDistance, 0);

        return index;
    }
}
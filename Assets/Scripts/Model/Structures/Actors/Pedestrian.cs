﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Pedestrian
public class Pedestrian : TestActor
{
    //The goal position where the actor tries to reach
    public ProDMVector3 _goalPos;
    //The contiguous road where the actor would spawn
    public RoadSegment _road;
    //The placement along the contiguous road where the actor would spawn
    public float _normalizedDistance;
    //The side of the contiguous road where the actor would spawn
    public SIDEWALK _roadSide;
    //Time delay to when the pedestrian starts moving
    public float _timeDelay = 0f;
    //The intersection where the actor would spawn
    public Intersection _intersection;
    //The side of the intersection where the actor woud spawn
    public IntersectionSide intersectionSide;
    //The walking speed of the pedestrian
    public float _walkingSpeed = 1f;

    //Start walking depending on relative distance to actor 
    public TestActor _actorOfInterest = null;
    public float _euclideanDistance = 0f;

    public Pedestrian(string id, ProDMVector3 spawnPos, ProDMVector3 goalPos, float timeDelay = 0f, float walkingSpeed = 1f) : base(id, spawnPos)
    {
        _testActorType = TestActorType.PEDESTRIAN;
        _goalPos = goalPos;
        _timeDelay = timeDelay;
        _walkingSpeed = walkingSpeed;
    }

    //This assumes that the pedestrian is instantiated at one side of the road and then walks to the other side
    public Pedestrian (string id, RoadSegment road, float normalizedDistance, string behavior,
        SIDEWALK side = SIDEWALK.LEFT, float timeDelay = 0f, float walkingSpeed = 1f) : base (id) 
    {
        _testActorType = TestActorType.PEDESTRIAN;

        //Set spawn position, goal position, etc.
        _road = road;
        _normalizedDistance = normalizedDistance;
        _roadSide = side;
        _timeDelay = timeDelay;
        _walkingSpeed = walkingSpeed;
        _behavior = behavior;
    }

    //Instantiates pedestrians who start crossing when euclidean distance to actor is less than a given value
    public Pedestrian(string id, RoadSegment road, float normalizedDistance, TestActor actorOfInterest, float euclideanDistance, string behavior = null, 
        SIDEWALK side = SIDEWALK.LEFT, float walkingSpeed = 1f) : base(id)
    {
        _testActorType = TestActorType.PEDESTRIAN;

        //Set spawn position, goal position, etc.
        _road = road;
        _normalizedDistance = normalizedDistance;
        _roadSide = side;
        _walkingSpeed = walkingSpeed;
        _actorOfInterest = actorOfInterest;
        _euclideanDistance = euclideanDistance;
        _behavior = behavior;
    }

    public Pedestrian(string id, RoadSegment road, float normalizedDistance, string behavior = null, string monitor = null,
    SIDEWALK side = SIDEWALK.LEFT, float walkingSpeed = 1f) : base(id)
    {
        _testActorType = TestActorType.PEDESTRIAN;

        //Set spawn position, goal position, etc.
        _road = road;
        _normalizedDistance = normalizedDistance;
        _roadSide = side;
        _walkingSpeed = walkingSpeed;
        _behavior = behavior;
        _monitor = monitor;
    }

    //Similar structure as above, but for Intersections
    public Pedestrian(string id, Intersection intersection, TestActor actorOfInterest, float euclideanDistance, string behavior = null,
        IntersectionSide side = IntersectionSide.CONN1_LEFT, float walkingSpeed = 1f) : base(id)
    {
        _testActorType = TestActorType.PEDESTRIAN;

        //Set spawn position, goal position, etc.
         _intersection = intersection;
        intersectionSide = side;
        _walkingSpeed = walkingSpeed;
        _actorOfInterest = actorOfInterest;
        _euclideanDistance = euclideanDistance;
        _behavior = behavior;
    }

    //Similar structure as above, but for Intersections
    public Pedestrian(string id, Intersection intersection, string behavior = null, string monitor = null,
        IntersectionSide side = IntersectionSide.CONN1_LEFT, float walkingSpeed = 1f) : base(id)
    {
        _testActorType = TestActorType.PEDESTRIAN;

        //Set spawn position, goal position, etc.
        _intersection = intersection;
        intersectionSide = side;
        _walkingSpeed = walkingSpeed;
        _behavior = behavior;
        _monitor = monitor;
    }

}
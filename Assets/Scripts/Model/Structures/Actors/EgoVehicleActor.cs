﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Ego Vehicle
public class AutonomousVehicle : TestActor
{
    //The goal position where the actor tries to reach
    public ProDMVector3 _goalPos;
    //The contiguous road where the actor would spawn
    public RoadSegment _road;
    //The placement along the contiguous road where the actor would spawn
    public float _normalizedDistance;
    //The lane where the actor will spawn
    public LANE _laneNum;

    //Dictory of which type of OnlineEvents are enabled
    public Dictionary<OnlineEventType, bool> _onlineEventsEnabled;
    //Updated list of events happening live
    


    //public AutonomousVehicle(string id, ProDMVector3 spawnPos, ProDMVector3 goalPos, Dictionary<OnlineEventType, bool> onlineEventsEnabled = null, TestActorType type= TestActorType.EGO_VEHICLE) 
    //    : base(id, spawnPos)
    //{
    //    _testActorType = type;

    //    //Set goal position of the ego vehicle
    //    _goalPos = goalPos;

    //    if(onlineEventsEnabled != null)
    //    {
    //        //TODO: Check for duplicates
    //        _onlineEventsEnabled = onlineEventsEnabled;
    //    }
    //    else
    //    {
    //        //If the dictionary is null, we add collision checks
    //        _onlineEventsEnabled = new Dictionary<OnlineEventType, bool>();
    //        _onlineEventsEnabled.Add(OnlineEventType.COLLISION, true);
    //        _onlineEventsEnabled.Add(OnlineEventType.INACTIVITY, true);
    //    }

    //    //Init list that stores events live
    //    _events = new List<OnlineEvent>();
    //}

    //public AutonomousVehicle(string id, RoadSegment road, float normalizedDistance, LANE laneNum, TestActorType type = TestActorType.EGO_VEHICLE, Dictionary<OnlineEventType, bool> onlineEventsEnabled = null) : base(id)
    //{
    //    //Get the corresponding Road & Traffic System piece and instantiate vehicle
    //    _testActorType = type;

    //    _road = road;
    //    _normalizedDistance = normalizedDistance;
    //    _laneNum = laneNum;

    //    if (onlineEventsEnabled != null)
    //    {
    //        //TODO: Check for duplicates
    //        _onlineEventsEnabled = onlineEventsEnabled;
    //    }
    //    else
    //    {
    //        //If the dictionary is null, we add collision checks
    //        _onlineEventsEnabled = new Dictionary<OnlineEventType, bool>();
    //        _onlineEventsEnabled.Add(OnlineEventType.COLLISION, true);
    //        _onlineEventsEnabled.Add(OnlineEventType.INACTIVITY, true);
    //    }

    //    //Init list that stores events live
    //    _events = new List<OnlineEvent>();

    //}


    public AutonomousVehicle(string id, ProDMVector3 spawnPos, string behavior = null, string monitor = null, Dictionary<OnlineEventType, bool> onlineEventsEnabled = null, TestActorType type = TestActorType.EGO_VEHICLE)
        : base(id, spawnPos)
    {
        _testActorType = type;

        if (onlineEventsEnabled != null)
        {
            //TODO: Check for duplicates
            _onlineEventsEnabled = onlineEventsEnabled;
        }
        else
        {
            //If the dictionary is null, we add collision checks
            _onlineEventsEnabled = new Dictionary<OnlineEventType, bool>();
            _onlineEventsEnabled.Add(OnlineEventType.COLLISION, true);
            _onlineEventsEnabled.Add(OnlineEventType.INACTIVITY, true);
        }

        _behavior = behavior;
        _monitor = monitor;
    }


    public AutonomousVehicle(string id, RoadSegment road, float normalizedDistance, LANE laneNum, string behavior = null, string monitor = null, TestActorType type = TestActorType.EGO_VEHICLE, Dictionary<OnlineEventType, bool> onlineEventsEnabled = null) : base(id)
    {
        //Get the corresponding Road & Traffic System piece and instantiate vehicle
        _testActorType = type;

        _road = road;
        _normalizedDistance = normalizedDistance;
        _laneNum = laneNum;

        if (onlineEventsEnabled != null)
        {
            //TODO: Check for duplicates
            _onlineEventsEnabled = onlineEventsEnabled;
        }
        else
        {
            //If the dictionary is null, we add collision checks
            _onlineEventsEnabled = new Dictionary<OnlineEventType, bool>();
            _onlineEventsEnabled.Add(OnlineEventType.COLLISION, true);
            _onlineEventsEnabled.Add(OnlineEventType.INACTIVITY, true);
        }

        _behavior = behavior;
        _monitor = monitor;

    }
}

﻿/*
 * Attach this to road elements with a relevant MeshRenderer
 * to set the element up for collision .
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SetUpCollider : MonoBehaviour
{
    //Instances of the components we set-up.
    //We may need to remove these components when they are no longer needed
    public MeshCollider mc;
    public Rigidbody rb;
    public RoadElemCollisionCheck cc;

	// Use this for initialization
	void Awake ()
    {
        //Set-up collision on this element

        if (gameObject.GetComponent<MeshRenderer>() != null)
        {
            //Debug.Log("Setting up collision in " + gameObject.name);
            
            //MeshCollider
            if (gameObject.GetComponent<MeshCollider>() == null)
            {
                //Add a mesh collider if it does not exist
                gameObject.AddComponent<MeshCollider>();
            }
            mc = gameObject.GetComponent<MeshCollider>();
            mc.convex = true;
            mc.isTrigger = true;

            //Rigidbody
            if (gameObject.GetComponent<Rigidbody>() == null)
            {
                rb = gameObject.AddComponent<Rigidbody>();
            }
            rb = gameObject.GetComponent<Rigidbody>();
            rb.isKinematic = false;
            rb.constraints = RigidbodyConstraints.FreezeAll; //Freeze everything as roads aren't expected to move
            rb.useGravity = false;

            //CollisionCheck
            if (gameObject.GetComponent<RoadElemCollisionCheck>() == null)
            {
                // Add Collision logic if one does not exist
                gameObject.AddComponent<RoadElemCollisionCheck>();
            }
            cc = gameObject.GetComponent<RoadElemCollisionCheck>();

        }

        else
            Debug.LogError("Cannot set up colliders in " + gameObject.name);
      
	}

}

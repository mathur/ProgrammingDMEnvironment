﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class SetUpVisualizer_DepthMap : MonoBehaviour
{

    Visualizer_withDepthMap PlaceHolderForVisualizer;

    void Awake()
    {
        if (PlaceHolderForVisualizer == null)
        {
            //If not set in inspector, set PlaceHolderForVisualizer
            PlaceHolderForVisualizer = GetComponent<Visualizer_withDepthMap>();
        }

        #region setup for Road Element Visualization
        //load the road element prefabs

        //if this dictionary ever needs to be reloaded, just clear it by uncommenting this line
        //PlaceHolderForVisualizer.roadPrefabDictionary.Clear();
        if (PlaceHolderForVisualizer.roadPrefabDictionary.Count == 0)
        {
            //road element prefabs
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("CONTIGUOUS_ROADSTRAIGHT1", (Resources.Load("Prefabs/1 Lane - Blue Straight") as GameObject));
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("CONTIGUOUS_ROADSTRAIGHT2", (Resources.Load("Prefabs/2 Lanes - Straight - ( Road V2 )") as GameObject));
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("CONTIGUOUS_ROADSTRAIGHT4", (Resources.Load("Prefabs/4 Lanes - Straight - ( Road V2 )") as GameObject));
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("CONTIGUOUS_ROADSTRAIGHT6", (Resources.Load("Prefabs/6 Lanes - Straight - ( Road V2 )") as GameObject));
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("INTERSECTIONT_INTERSECTION2", (Resources.Load("Prefabs/2 Lanes - T Intersection + Traffic Lights (Connected) - ( Road V2 )") as GameObject));
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("INTERSECTIONT_INTERSECTION4", (Resources.Load("Prefabs/4 Lanes - T Intersection + Traffic Lights (Connected) - ( Road V2 )") as GameObject));
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("INTERSECTIONT_INTERSECTION6", (Resources.Load("Prefabs/6 Lanes - T Intersection + Traffic Lights (Connected) - ( Road V2 )") as GameObject));
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("INTERSECTIONCROSS_INTERSECTION2", (Resources.Load("Prefabs/2 Lanes - Intersection + Traffic Lights (Connected) - ( Road V2 )") as GameObject));
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("INTERSECTIONCROSS_INTERSECTION4", (Resources.Load("Prefabs/4 Lanes - Intersection + Traffic Lights (Connected) - ( Road V2 )") as GameObject));
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("INTERSECTIONCROSS_INTERSECTION6", (Resources.Load("Prefabs/6 Lanes - Intersection + Traffic Lights (Connected) - ( Road V2 )") as GameObject));
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("INTERSECTIONROUNDABOUT2", (Resources.Load("Prefabs/2 Lanes - Roundabout - ( Road V2 )") as GameObject));
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("INTERSECTIONROUNDABOUT4", (Resources.Load("Prefabs/4 Lanes - Roundabout - ( Road V2 )") as GameObject));
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("INTERSECTIONROUNDABOUT6", (Resources.Load("Prefabs/6 Lanes - Roundabout - ( Road V2 )") as GameObject));

            //tree prefabs
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("TREES1", Resources.Load("Prefabs/Poplar_Tree") as GameObject);
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("TREES2", Resources.Load("Prefabs/Fir_Tree") as GameObject);
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("TREES3", Resources.Load("Prefabs/Oak_Tree") as GameObject);

            //residential zoning prefabs
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("RESIDENTIAL1", Resources.Load("Prefabs/House_1") as GameObject);
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("RESIDENTIAL2", Resources.Load("Prefabs/House_2") as GameObject);
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("RESIDENTIAL3", Resources.Load("Prefabs/House_3") as GameObject);
            PlaceHolderForVisualizer.roadPrefabDictionary.Add("RESIDENTIAL4", Resources.Load("Prefabs/House_4") as GameObject);

            //store the count of tree and residential zone prefabs because of this random generation that we are using right now.
            PlaceHolderForVisualizer.treesPrefabCount = PlaceHolderForVisualizer.roadPrefabDictionary.Count(kv => kv.Key.Contains("TREES"));
            PlaceHolderForVisualizer.residentialPrefabCount = PlaceHolderForVisualizer.roadPrefabDictionary.Count(kv => kv.Key.Contains("RESIDENTIAL"));
        }
        #endregion

        #region setup for test actors
        //trafficsystem car that is the "general traffic"
        if (PlaceHolderForVisualizer.carPrefab == null)
        {
            PlaceHolderForVisualizer.carPrefab = Resources.Load("Prefabs/AICar", typeof(TrafficSystemVehicle)) as TrafficSystemVehicle;
        }
        //pedestriansystem pedestrian (the samurai guy)
        if (PlaceHolderForVisualizer.pedestrianPrefab == null)
        {
            PlaceHolderForVisualizer.pedestrianPrefab = Resources.Load("Prefabs/Pedestrian", typeof(PedestrianObject)) as PedestrianObject;
        }
        //ego car with autonomous capabilities
        if (PlaceHolderForVisualizer.autonomousEgoCarPrefab == null)
        {
            PlaceHolderForVisualizer.autonomousEgoCarPrefab = Resources.Load("Prefabs/UdacityCar") as GameObject;
        }
        //ego car for training the autonomous car
        if (PlaceHolderForVisualizer.trainingEgoCarPrefab == null)
        {
            PlaceHolderForVisualizer.trainingEgoCarPrefab = Resources.Load("Prefabs/CarTraining") as GameObject;
        }
        //ego car for remote control only
        if (PlaceHolderForVisualizer.egoCarPrefab == null)
        {
            PlaceHolderForVisualizer.egoCarPrefab = Resources.Load("Prefabs/Car") as GameObject;
        }
        #endregion

        #region setup for Slider Visualization/UI elements
        //load the canvas prefab
        if (PlaceHolderForVisualizer.canvasPrefab == null)
        {
            PlaceHolderForVisualizer.canvasPrefab = Resources.Load("Prefabs/CanvasPrefab") as GameObject;
        }
        //slider
        if (PlaceHolderForVisualizer.sliderPrefab == null)
        {
            PlaceHolderForVisualizer.sliderPrefab = Resources.Load("Prefabs/ProDMSliderPrefab") as GameObject;

        }
        //text
        if (PlaceHolderForVisualizer.textPrefab == null)
        {
            PlaceHolderForVisualizer.textPrefab = Resources.Load("Prefabs/TextPrefab") as GameObject;
        }
        //button
        if (PlaceHolderForVisualizer.buttonPrefab == null)
        {
            PlaceHolderForVisualizer.buttonPrefab = (Resources.Load("Prefabs/ButtonPrefab") as GameObject).GetComponent<Button>();
        }

        //if there is no canvas, add one.
        if (PlaceHolderForVisualizer.newCanvas == null)
        {
            PlaceHolderForVisualizer.newCanvas = Instantiate(PlaceHolderForVisualizer.canvasPrefab);
            PlaceHolderForVisualizer.newCanvas.name = "Canvas";
        }
        //if the message text loses its reference, reset it here
        if (Visualizer.messageText == null)
        {
            Visualizer.messageText = PlaceHolderForVisualizer.newCanvas.GetComponentInChildren<Text>();
        }
        #endregion
    }

}


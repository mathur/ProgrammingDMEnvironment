﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCar : MonoBehaviour
{
    public float speedMultiplier = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.rotation.y > 0)
        {
            this.transform.position = this.transform.position + new Vector3(speedMultiplier * 1f, 0, 0);
        }
        else
        {
            this.transform.position = this.transform.position + new Vector3(speedMultiplier * -1f, 0, 0);
        }
    }
}

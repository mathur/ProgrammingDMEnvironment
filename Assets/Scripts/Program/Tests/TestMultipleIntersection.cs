﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMultipleIntersection : EnvironmentProgramBaseClass
{
    //Define my ProDM vars here
    //These are initialized here, but the actual values are designated in the Start() function
    //so you need to check there to set the values.
    VarInterval<int> numCols;
    VarInterval<int> roadLength;

    //These are the variables to control the environment
    //They need to be defined here every time, but can be set to constant values in the Start()
    VarInterval<float> lightIntensity;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;
    ProDMColor color;

    int numLanes = 2;
    VarEnum<Color> oneOfInvariantColor = new VarEnum<Color>(new List<Color> { Color.red, Color.blue, Color.black });
    //This adds the visualization aspect to the datastructure.
    //In the future, it should just be behind-the-scenes and not necessary in this script

    // Parameterize using two ProDMFloats
    VarInterval<float> walkingSpeed;
    VarInterval<float> euclideanDist;

    //We need this saved so that we can connect 
    AutonomousVehicle autonomouscar;

    Color carColor;

    List<TestActor> actorsToUpdate = new List<TestActor>();
    void Start() //needs to be called in the start because the setup in the Visualizer happens in the Awake()
    {
        //add the play/pause button and a button to record the car path
        //PlaceHolderForVisualizer.createPlayButton();
        //initialize the ProDM and VarEnum variables
        //Do not visualize the ProDM or VarEnum variables if they are fixed values.
        fogDensity = new VarInterval<float>(val: 0, min: 0, max: 1);

        lightIntensity = new VarInterval<float>(val: 1, min: 0, max: 10);

        color = new ProDMColor(1f, 0.9f, 0.8f, 1f); //initialize the color var to a constant value instead of a variable range

        ambientIntensity = new VarInterval<float>(val: 1f, min: 0f, max: 2f);

        //initialize my ProDM and VarEnum vars here
        //THIS CONTROLS THE NUMBER OF INTERSECTIONS AND THE CONNECTING ROAD LENGTH
        numCols = new VarInterval<int>(val: 5, min: 1, max: 5);
        roadLength = new VarInterval<int>(val: 3, min: 1, max: 30);
        //PlaceHolderForVisualizer.Visualize(nameof(numCols), numCols);

        walkingSpeed = new VarInterval<float>(val: 4f, min: 1f, max: 10f);
        euclideanDist = new VarInterval<float>(val: 40f, min: 20f, max: 60f);

        //finish the setup, including the initial visualization of the scene and the environment settings
        Test(timeCutOff: 200f);
    }

    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {
        //we need this to connect the elements in the row
        RoadElement previousCol = new RoadSegment(1, 2, nameof(previousCol));
        PhysicalConnection previousRight = previousCol._connections.NextFree();

        List<object> list = new List<object>();

        for (int col = 0; col < numCols; col++)
            {
                Intersection int0 = new CrossIntersection(numLanes, nameof(int0) + col.ToString(), zone1: Zoning.TREES, zone2: Zoning.TREES, zone3: Zoning.TREES, zone4: Zoning.TREES);
                PlaceHolderForVisualizer.Visualize(int0);
                RoadSegment cRoad0 = new RoadSegment(roadLength, numLanes, nameof(cRoad0) + col.ToString(), zone1: Zoning.RESIDENTIAL_ZONE, zone2: Zoning.RESIDENTIAL_ZONE);
                PlaceHolderForVisualizer.Visualize(cRoad0);
            list.Add(int0);
            list.Add(cRoad0);

                List<RoadElement> cRoadList0 = new List<RoadElement>();
                cRoadList0.Add(cRoad0);

                //Make an encapsulating object
                RoadElement cRoads0 = new RoadElement(cRoadList0, nameof(cRoads0) + col.ToString());
                PlaceHolderForVisualizer.Visualize(cRoads0);
            list.Add(cRoads0);

                //Connect the encapsulated object to Interesection
                Dictionary<PhysicalConnection, PhysicalConnection> connMap0 = new Dictionary<PhysicalConnection, PhysicalConnection>();
                connMap0.Add(cRoad0._connections.AtIndex(1), int0._connections.AtIndex(0));

                RoadElement connectedElement0 = cRoads0.ConnectTo(int0, connMap0, nameof(connectedElement0) + col.ToString());
                PlaceHolderForVisualizer.Visualize(connectedElement0);
                PlaceHolderForVisualizer.connectRoadElements(connMap0, cRoads0, connectedElement0, int0, cRoads0, true);


            if (col != 0)
                {
                    Dictionary<PhysicalConnection, PhysicalConnection> connMap = new Dictionary<PhysicalConnection, PhysicalConnection>();
                    connMap.Add(int0._connections.AtIndex(2), previousRight);

                    RoadElement colElement = connectedElement0.ConnectTo(previousCol, connMap, nameof(colElement) + col.ToString());
                    PlaceHolderForVisualizer.Visualize(colElement);
                    PlaceHolderForVisualizer.connectRoadElements(connMap, connectedElement0, colElement, previousCol, connectedElement0, true);

                    //we need this to connect the elements in the row
                    previousCol = colElement;
                    previousRight = cRoad0._connections.AtIndex(0);
                }
                else
                {
                    //we need this to connect the elements in the row
                    previousCol = connectedElement0;
                    previousRight = cRoad0._connections.AtIndex(0);
                }

            //USE THIS SECTION TO PLACE TEST ACTORS
            //Note: You need to do it here before the reference to the appropriate road segment is lost
            //Second note: The AutonomousVehicle is declared earlier on because if pedestrians are declared on different
            //road segments, they need to reference the AutonomousVehicle in their declaration

            oneOfInvariantColor.SetRandom();
            carColor = oneOfInvariantColor;

            if (col == 0)
            {
                autonomouscar = new AutonomousVehicle(nameof(autonomouscar), cRoad0, 0f, LANE.PLUS_ONE, monitor: "CollisionMonitor", type: TestActorType.EGO_VEHICLE_AUTONOMOUS);
                PlaceHolderForVisualizer.Visualize(autonomouscar);

                //Cars for traffic
                for (float offset = 0.25f; offset < 1f; offset += 0.40f)
                {
                    AIVehicle car1 = new AIVehicle(nameof(car1) + "same1" + offset.ToString(), cRoad0, offset, LANE.PLUS_ONE, carColor);
                    PlaceHolderForVisualizer.Visualize(car1);
                    //Add to the list of string of actor ids to be updated
                    actorsToUpdate.Add(car1);
                }

            }


            if (col == 2)
            {
                /*
                Pedestrian pedestrian1 = new Pedestrian(nameof(pedestrian1), int0, actorOfInterest: autonomouscar, euclideanDistance: euclideanDist, walkingSpeed: walkingSpeed, side: IntersectionSide.CONN3_LEFT);
                PlaceHolderForVisualizer.Visualize(pedestrian1);
                */
                //Cars from the other side
                for (float offset = 0f; offset < 1f; offset += 0.40f)
                {
                    AIVehicle car1 = new AIVehicle(nameof(car1) + "opp1" + offset.ToString(), cRoad0, offset, LANE.MINUS_ONE, carColor);
                    PlaceHolderForVisualizer.Visualize(car1);
                    //Add to the list of string of actor ids to be updated
                    actorsToUpdate.Add(car1);
                }
            }


            if (col == 4)
            {
                /*
                Pedestrian pedestrian1 = new Pedestrian(nameof(pedestrian1), int0, actorOfInterest: autonomouscar, euclideanDistance: euclideanDist, walkingSpeed: walkingSpeed, side: IntersectionSide.CONN3_LEFT);
                PlaceHolderForVisualizer.Visualize(pedestrian1);
                */
                //Cars from the other side
                for (float offset = 0f; offset < 1f; offset += 0.40f)
                {
                    AIVehicle car1 = new AIVehicle(nameof(car1) + "opp2" + offset.ToString(), cRoad0, offset, LANE.MINUS_ONE, carColor);
                    PlaceHolderForVisualizer.Visualize(car1);
                    //Add to the list of string of actor ids to be updated
                    actorsToUpdate.Add(car1);
                }
            }

        }

            
        
        //add test actors


        /*

        Pedestrian pedestrian = new Pedestrian(nameof(pedestrian), cRoad0, 0.2f, actorOfInterest: autonomouscar, euclideanDistance: euclideanDist, walkingSpeed: walkingSpeed);
        PlaceHolderForVisualizer.Visualize(pedestrian);

        Pedestrian pedestrian1 = new Pedestrian(nameof(pedestrian1), int0, actorOfInterest: autonomouscar, euclideanDistance: euclideanDist, walkingSpeed: walkingSpeed, side: IntersectionSide.CONN4_LEFT);
        PlaceHolderForVisualizer.Visualize(pedestrian1);

        //Generate traffic on cRoad0---same direction
        for (float offset = 0.20f; offset < 1f; offset += 0.20f)
        {
            AIVehicle car1 = new AIVehicle(nameof(car1) + "same1" + offset.ToString(), cRoad0, offset, LANE.PLUS_ONE, carColor);
            PlaceHolderForVisualizer.Visualize(car1);
        }

        //Generate traffic on cRoad0---opposite direction
        for (float offset = 0f; offset < 1f; offset += 0.20f)
        {
            AIVehicle car1 = new AIVehicle(nameof(car1) + "opp1" + offset.ToString(), cRoad0, offset, LANE.MINUS_ONE, carColor);
            PlaceHolderForVisualizer.Visualize(car1);
        }

        //Generate traffic on cRoad1---same direction
        for (float offset = 0f; offset < 1f; offset += 0.20f)
        {
            AIVehicle car1 = new AIVehicle(nameof(car1) + "same2" + offset.ToString(), cRoad1, offset, LANE.PLUS_ONE, carColor);
            PlaceHolderForVisualizer.Visualize(car1);
        }

        //Generate traffic on cRoad1---opposite direction
        for (float offset = 0f; offset < 1f; offset += 0.20f)
        {
            AIVehicle car1 = new AIVehicle(nameof(car1) + "opp2" + offset.ToString(), cRoad1, offset, LANE.MINUS_ONE, carColor);
            PlaceHolderForVisualizer.Visualize(car1);
        }
        */
    }

    //configure the environment settings here
    public override void MakeEnvironment()
    {
        //Environment settings
        LightSettings light = new LightSettings(intensity: lightIntensity, direction: Vector3.down, color: color, ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }

    //Updating the view
    public override void UpdateViewVars(bool toDefault = false)
    {
        if (toDefault)
        {
            //Set default colour to each AI car
            oneOfInvariantColor.SetDefault();
            carColor = oneOfInvariantColor;
            foreach (var key in actorsToUpdate)
            {
                EnvironmentModel.testActorList[key].GetComponent<ChangeMaterialColour>().ChangeColor(carColor);
            }
        }
        else
        {
            //Set random colour to each AI car
            foreach (var key in actorsToUpdate)
            {
                oneOfInvariantColor.SetRandom();
                carColor = oneOfInvariantColor;
                EnvironmentModel.testActorList[key].GetComponent<ChangeMaterialColour>().ChangeColor(carColor);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StraighthRoadWithLeadCar : EnvironmentProgramBaseClass {

    //Add the ProDM vars here
    VarInterval<int> cr_length;
    VarEnum<int> cr_numLanes;
    VarInterval<float> lightIntensity;
    ProDMColor color;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;
    //int cr_numLanes = 4;


    void Start() //needs to be called in the start because the setup in the RoadElementVisualization happens in the Awake()
    {
        //PlaceHolderForVisualizer.createPlayButton();
        //PlaceHolderForVisualizer.createRecordButton();

        fogDensity = new VarInterval<float>(val: 0, min: 0, max: 0.3f, desc:"FOG_DENSITY");
        PlaceHolderForVisualizer.Visualize(nameof(fogDensity), fogDensity, false);

        lightIntensity = new VarInterval<float>(val: 1, min: 0, max: 10);
        //PlaceHolderForVisualizer.Visualize(nameof(lightIntensity), lightIntensity);

        color = new ProDMColor(new VarInterval<float>(val: 1f, min: 0f, max: 1f), new VarInterval<float>(val: 0.9f, min: 0f, max: 1f),
            new VarInterval<float>(val: 0.8f, min: 0f, max: 1f), 1f);
        //PlaceHolderForVisualizer.Visualize(nameof(color), color);

        ambientIntensity = new VarInterval<float>(val: 1f, min: 0f, max: 2.5f, desc: "AMBIENT_LIGHT_INTENSITY");
        //PlaceHolderForVisualizer.Visualize(nameof(ambientIntensity), ambientIntensity, false);

        //define the ProDM variables
        cr_length = new VarInterval<int>(val: 100, changeable: true, min: 20, max: 4000);
        //PlaceHolderForVisualizer.Visualize(nameof(cr_length), cr_length);

        List<int> numLanesOptions = new List<int> { 2, 4 };

        cr_numLanes = new VarEnum<int>(numLanesOptions, 1);
        PlaceHolderForVisualizer.Visualize(nameof(cr_numLanes), cr_numLanes);

        // Random testing with n iterations over some ranged variables
        List<VarInterval<float>> TestVars = new List<VarInterval<float>>();
        TestVars.Add(fogDensity);
        //TestVars.Add(ambientIntensity);
        TestIterative(rangedFloats:TestVars, iterations: 10, singleTestTime:15f, repetitionsPerIteration:3);
    }

    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {

        RoadSegment cRoad1 = new RoadSegment(cr_length, cr_numLanes);
        cRoad1._zone1 = Zoning.TREES;
        cRoad1._zone2 = Zoning.TREES;
        PlaceHolderForVisualizer.Visualize(cRoad1);

        //add a car
		//Body color of the car is changed
        AIVehicle car = new AIVehicle(nameof(car), cRoad1, 0.12f, LANE.PLUS_ONE, bodyColor: Color.white);
        PlaceHolderForVisualizer.Visualize(car);

        //add a car
        //AIVehicle car1 = new AIVehicle(nameof(car1), cRoad1, 0.15f, LANE.PLUS_ONE);
        //PlaceHolderForVisualizer.Visualize(car1);

        //AutonomousVehicle autonomouscar_training = new AutonomousVehicle(nameof(autonomouscar_training), cRoad1, 0.1f, LANE.MINUS_ONE, type: TestActorType.EGO_VEHICLE_TRAINING);
        //PlaceHolderForVisualizer.Visualize(autonomouscar_training);

        AutonomousVehicle autonomouscar = new AutonomousVehicle(nameof(autonomouscar), cRoad1, 0.10f, LANE.PLUS_ONE, monitor: "CollisionMonitor", type: TestActorType.EGO_VEHICLE_AUTONOMOUS);
        PlaceHolderForVisualizer.Visualize(autonomouscar);

    }


    public override void MakeEnvironment()
    {
        //Environment settings
        LightSettings light = new LightSettings(intensity: lightIntensity, direction: Vector3.down, color: color, ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HaltonSampling_withSAmaxQualitativeCollision : EnvironmentProgramBaseClass
{
    //Add the ProDM vars here
    VarInterval<int> cr_length;
    VarEnum<int> cr_numLanes;
    VarInterval<float> lightIntensity;
    ProDMColor color;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;
    //int cr_numLanes = 4;

    // Parameterize using two ProDMFloats
    VarInterval<float> walkingSpeed;
    VarInterval<float> euclideanDist;

    void Start() //needs to be called in the start because the setup in the RoadElementVisualization happens in the Awake()
    {
        //This button is useful to have everything set before driving begins
        //PlaceHolderForVisualizer.createPlayButton();
        //PlaceHolderForVisualizer.createRecordButton();

        fogDensity = new VarInterval<float>(val: 0, min: 0, max: 0.3f, desc: "FOG_DENSITY");


        lightIntensity = new VarInterval<float>(val: 1, min: 0, max: 10);
        color = new ProDMColor(new VarInterval<float>(val: 1f, min: 0f, max: 1f), new VarInterval<float>(val: 0.9f, min: 0f, max: 1f),
            new VarInterval<float>(val: 0.8f, min: 0f, max: 1f), 1f);
        //PlaceHolderForVisualizer.Visualize(nameof(color), color);

        ambientIntensity = new VarInterval<float>(val: 1.5f, min: 0f, max: 2.5f, desc: "AMBIENT_LIGHT_INTENSITY");

        //define the ProDM variables
        cr_length = new VarInterval<int>(val: 100, changeable: true, min: 20, max: 4000);

        List<int> numLanesOptions = new List<int> { 2, 4 };

        cr_numLanes = new VarEnum<int>(numLanesOptions, 0);

        walkingSpeed = new VarInterval<float>(val: 4f, min: 2f, max: 10f);
        euclideanDist = new VarInterval<float>(val: 40f, min: 30f, max: 60f);

        List<ProDM> rangedFloats = new List<ProDM>();
        rangedFloats.Add(walkingSpeed);
        rangedFloats.Add(euclideanDist);

        TestIterative(testVars: rangedFloats, repetitions: 3, sampleSize: 70, numTopEnergies: 10, singleTestTime: 15f, actorMonitorName: "QualitativeCollisionMonitor", setVar: AutomatedVarSetMethod.OPTIMIZE_ENERGY_HALTON_MAX);

    }

    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {
        List<object> list = new List<object>();

        RoadSegment cRoad1 = new RoadSegment(cr_length, cr_numLanes, orientation: Orientation.VERTICAL);
        cRoad1._zone1 = Zoning.TREES;
        cRoad1._zone2 = Zoning.TREES;
        list.Add(cRoad1);


        AutonomousVehicle autonomouscar = new AutonomousVehicle(nameof(autonomouscar), cRoad1, 0.11f, LANE.PLUS_ONE, monitor: "QualitativeCollisionMonitor", type: TestActorType.EGO_VEHICLE_AUTONOMOUS);
        list.Add(autonomouscar);

        Pedestrian pedestrian = new Pedestrian(nameof(pedestrian), cRoad1, 0.2f, actorOfInterest: autonomouscar, euclideanDistance: euclideanDist, walkingSpeed: walkingSpeed, behavior: "PedestrianCrossingBehavior01");
        list.Add(pedestrian);

        PlaceHolderForVisualizer.Visualize(list);
    }


    public override void MakeEnvironment()
    {
        LightSettings light = new LightSettings(intensity: lightIntensity, direction: Vector3.down, color: color, ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }
}

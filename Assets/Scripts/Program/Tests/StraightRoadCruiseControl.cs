﻿/*
 * A test for automatic cruise control.
 * Test parameters: light intensity and fog density (contionuous), number of lanes, color of lead car (discrete).
 *  */

using System.Collections.Generic;
using UnityEngine;

public class StraightRoadCruiseControl : EnvironmentProgramBaseClass {

    //Add the ProDM vars here
    VarInterval<int> cr_length;
    VarEnum<int> numLanes;
    VarInterval<float> lightIntensity;
    ProDMColor color;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;
    VarEnum<int> carColorIndex;
	VarInterval<float> aiCarDist;
	VarInterval<float> maxVelocity;
    //int cr_numLanes = 4;

    List<Color> someColors = new List<Color> { Color.black, Color.red, Color.yellow, Color.blue };
    void Start() //needs to be called in the start because the setup in the RoadElementVisualization happens in the Awake()
    {
        //PlaceHolderForVisualizer.createPlayButton();
        //PlaceHolderForVisualizer.createRecordButton();
        fogDensity = new VarInterval<float>(val: 0, min: 0, max: 0.15f, desc: "FOG_DENSITY");
        //PlaceHolderForVisualizer.Visualize(nameof(fogDensity), fogDensity, false);

        lightIntensity = new VarInterval<float>(val: 1, min: 0, max: 10);
        //PlaceHolderForVisualizer.Visualize(nameof(lightIntensity), lightIntensity);

        color = new ProDMColor(new VarInterval<float>(val: 1f, min: 0f, max: 1f), new VarInterval<float>(val: 0.9f, min: 0f, max: 1f),
            new VarInterval<float>(val: 0.8f, min: 0f, max: 1f), 1f);
        //PlaceHolderForVisualizer.Visualize(nameof(color), color);

        ambientIntensity = new VarInterval<float>(val: 1.5f, min: 0f, max: 2.5f, desc: "AMBIENT_LIGHT_INTENSITY");
        //PlaceHolderForVisualizer.Visualize(nameof(ambientIntensity), ambientIntensity, false);

		aiCarDist = new VarInterval<float>(val: 0.11f, min:0.11f, max:0.15f, desc:"AI_CAR_DISTANCE");
		maxVelocity = new VarInterval<float>(val: 3f, min: 3f, max: 8f, desc: "MAX_AI_VELOCITY");

        //The color a lead car can take shall be taken from a finite discrete set
        List<int> indices = new List<int>{ 0, 1, 2, 3};
        carColorIndex = new VarEnum<int>(indices, 0, desc: "ColorIndex");
        
        //define the ProDM variables
        cr_length = new VarInterval<int>(val: 100, changeable: true, min: 20, max: 4000);
        //PlaceHolderForVisualizer.Visualize(nameof(cr_length), cr_length);

        List<int> numLanesOptions = new List<int> { 2, 4 };

        numLanes = new VarEnum<int>(numLanesOptions, 0);
        //PlaceHolderForVisualizer.Visualize(nameof(cr_numLanes), cr_numLanes);


        List<VarInterval<float>> testVars = new List<VarInterval<float>> ();
		testVars.Add(aiCarDist);
		testVars.Add(maxVelocity);
        testVars.Add(fogDensity);

        List<VarEnum<int>> testVarsDiscrete = new List<VarEnum<int>>();
        testVarsDiscrete.Add(numLanes);
        testVarsDiscrete.Add(carColorIndex);
        TestIterative(rangedFloats: testVars, discVars:testVarsDiscrete, iterations: 200, singleTestTime: 15f, setVar: AutomatedVarSetMethod.LOW_DISCREPANCY);
    }

    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {
        // Make the list to visualize
        List<object> toVisualize = new List<object>();

        RoadSegment cRoad1 = new RoadSegment(cr_length, numLanes);
        cRoad1._zone1 = Zoning.TREES;
        cRoad1._zone2 = Zoning.TREES;
        toVisualize.Add(cRoad1);

        //add a car
		AIVehicle car = new AIVehicle(nameof(car), cRoad1, aiCarDist, LANE.PLUS_ONE, maxVelocity: maxVelocity,bodyColor: someColors[carColorIndex]);
        toVisualize.Add(car);

        //Car coming from the other direction
		//body color may be red, blue, yellow, green or no car at all
        //AIVehicle car1 = new AIVehicle(nameof(car1), cRoad1, 0.2f, LANE.MINUS_ONE, bodyColor: Color.green);
        //toVisualize.Add(car1);

        AutonomousVehicle autonomouscar = new AutonomousVehicle(nameof(autonomouscar), cRoad1, 0.10f, LANE.PLUS_ONE, monitor: "CruiseCollisionMonitor", type: TestActorType.EGO_VEHICLE_AUTONOMOUS);
        toVisualize.Add(autonomouscar);
        PlaceHolderForVisualizer.Visualize(toVisualize);
    }


    public override void MakeEnvironment()
    {
        //Environment settings
        LightSettings light = new LightSettings(intensity: lightIntensity, direction: Vector3.down, color: color, ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }
}

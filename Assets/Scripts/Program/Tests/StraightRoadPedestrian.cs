﻿//Use model-029.h5 for test

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StraightRoadPedestrian : EnvironmentProgramBaseClass {

    //Add the ProDM vars here
    VarInterval<int> cr_length;
    VarEnum<int> cr_numLanes;
    VarInterval<float> lightIntensity;
    ProDMColor color;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;
    VarInterval<float> distance;
    VarInterval<float> distance2;
    //int cr_numLanes = 4;

    // Parameterize using two ProDMFloats
    VarInterval<float> walkingSpeed;
    VarInterval<float> euclideanDist;

    void Start() //needs to be called in the start because the setup in the RoadElementVisualization happens in the Awake()
    {
		//This button is useful to have everything set before driving begins
		//PlaceHolderForVisualizer.createPlayButton();
		//PlaceHolderForVisualizer.createRecordButton();

		fogDensity = new VarInterval<float>(val: 0, min: 0, max: 0.3f, desc:"FOG_DENSITY");
		PlaceHolderForVisualizer.Visualize(nameof(fogDensity), fogDensity, false);

		lightIntensity = new VarInterval<float>(val: 1, min: 0, max: 10);
		//PlaceHolderForVisualizer.Visualize(nameof(lightIntensity), lightIntensity);

		color = new ProDMColor(new VarInterval<float>(val: 1f, min: 0f, max: 1f), new VarInterval<float>(val: 0.9f, min: 0f, max: 1f),
			new VarInterval<float>(val: 0.8f, min: 0f, max: 1f), 1f);
		//PlaceHolderForVisualizer.Visualize(nameof(color), color);

		ambientIntensity = new VarInterval<float>(val: 1.5f, min: 0f, max: 2.5f, desc: "AMBIENT_LIGHT_INTENSITY");
        PlaceHolderForVisualizer.Visualize(nameof(ambientIntensity), ambientIntensity, false);

		//define the ProDM variables
		cr_length = new VarInterval<int>(val: 100, changeable: true, min: 20, max: 4000);
		//PlaceHolderForVisualizer.Visualize(nameof(cr_length), cr_length);

        List<int> numLanesOptions = new List<int> { 2, 4 };

		cr_numLanes = new VarEnum<int>(numLanesOptions, 0);

        walkingSpeed = new VarInterval<float>(val: 8f, min: 5f, max: 10f);
        euclideanDist = new VarInterval<float>(val: 20f, min: 5f, max: 40f);
        distance = new VarInterval<float>(min: 0.0f, max: 0.5f);
        distance2 = new VarInterval<float>(min: 0.0f, max: 0.5f);
        List<ProDM> rangedFloats = new List<ProDM>();
        //rangedFloats.Add(walkingSpeed);
        //rangedFloats.Add(euclideanDist);
        rangedFloats.Add(distance);
        rangedFloats.Add(distance2);

        //SimulatedAnnealingTemp sat = new SimulatedAnnealingTemp();
        //List<ProDM> final = sat.simulatedAnnealing(rangedFloats, 100);
        TestIterative( testVars: rangedFloats, repetitions: 10, sampleSize: 10, singleTestTime: 2, behaviors: new List<string>() { "PedestrianCrossingBehavior01" , "PedestrianCrossingBehavior02" }, setVar: AutomatedVarSetMethod.OPTIMIZE_BEHAVIOR);
        //TestIterative(new List<VarInterval<float>>() { walkingSpeed });
    }

    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {
        List<object> list = new List<object>();

		RoadSegment cRoad1 = new RoadSegment(cr_length, cr_numLanes, nameof(cRoad1), Orientation.VERTICAL);
		cRoad1._zone1 = Zoning.TREES;
		cRoad1._zone2 = Zoning.TREES;
		//PlaceHolderForVisualizer.Visualize(cRoad1);
        list.Add(cRoad1);

        AutonomousVehicle autonomouscar = new AutonomousVehicle(nameof(autonomouscar), cRoad1, 0.11f, LANE.PLUS_ONE, type: TestActorType.EGO_VEHICLE_AUTONOMOUS, monitor: "CollisionMonitor");
        //PlaceHolderForVisualizer.Visualize(autonomouscar);
        list.Add(autonomouscar);

        Pedestrian pedestrian1 = new Pedestrian(nameof(pedestrian1), cRoad1, normalizedDistance:distance, actorOfInterest:autonomouscar, euclideanDistance: euclideanDist, walkingSpeed:walkingSpeed, behavior: "PedestrianCrossingBehavior01");
		//PlaceHolderForVisualizer.Visualize(pedestrian1);
        list.Add(pedestrian1);

        Pedestrian pedestrian2 = new Pedestrian(nameof(pedestrian2), cRoad1, distance2, actorOfInterest: autonomouscar, euclideanDistance: euclideanDist, walkingSpeed: walkingSpeed, behavior: "PedestrianCrossingBehavior02");
        //PlaceHolderForVisualizer.Visualize(pedestrian1);
        list.Add(pedestrian2);

        PlaceHolderForVisualizer.Visualize(list);

        //Pedestrian pedestrian2 = new Pedestrian(nameof(pedestrian2), cRoad1, 0.3f, SIDEWALK.LEFT, timeDelay:16f);
        //PlaceHolderForVisualizer.Visualize(pedestrian2);

        //Pedestrian pedestrian3 = new Pedestrian(nameof(pedestrian3), cRoad1, 0.4f, SIDEWALK.LEFT, timeDelay:25f);
        //PlaceHolderForVisualizer.Visualize(pedestrian3);

        //add a car
        //AIVehicle car1 = new AIVehicle(nameof(car1), cRoad1, 0.15f, LANE.PLUS_ONE);
        //PlaceHolderForVisualizer.Visualize(car1);

        //AutonomousVehicle autonomouscar_training = new AutonomousVehicle(nameof(autonomouscar_training), cRoad1, 0.1f, LANE.MINUS_ONE, type: TestActorType.EGO_VEHICLE_TRAINING);
        //PlaceHolderForVisualizer.Visualize(autonomouscar_training);

    }


    public override void MakeEnvironment()
    {
        //Environment settings
        LightSettings light = new LightSettings(intensity: lightIntensity, direction: Vector3.down, color: color, ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }
}

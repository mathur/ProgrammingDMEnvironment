﻿//This file contains the base class for programs
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO; //For file operations
using System.Linq;
//Reactive extensions
using UniRx;
using UnityEngine;

public class EnvironmentProgramBaseClass : MonoBehaviour
{

    public Visualizer PlaceHolderForVisualizer;
    private StreamWriter testLog;

    bool isAutomatedTest = false; //Is the test automated or online/hand-on?

    //Structures to hold test information that is later printed out
    public TestIteration currentTestIteration;
    public AggregatedLogIterative currentTestIterative;
    public AggregatedLogTemporal currentTestTemporal;
    public AggregatedLogStandard currentTest;

    //used for behavioral fuzzing
    List<object> sReference;

    bool verboseMode = false;

    int listSizeCutoff = 2;

    //Ticks used by the program
    public long tick;

    // Hack used by CarController to know if ProDM variables need to
    // be updated. Currently non functional (this needs to false) 
    public bool trainUpdateRequired = false;

    //Set reference to the Visualizer
    private void Awake()
    {
        if (PlaceHolderForVisualizer == null)
        {
            //If not set in inspector, set PlaceHolderForVisualizer
            PlaceHolderForVisualizer = GetComponent<Visualizer>();
        }

        //tick = 0;
    }

    private void Update()
    {
        //tick++;
    }

    public void Train(List<VarInterval<float>> invariants = null)
    {
        //Set invariants in a public static structure
        EnvironmentModel.invariants = invariants;
        MakeView();
        MakeEnvironment();
    }

    //Simple test = simple run on the environment program
    public void Test(float timeCutOff = 1000f, bool verbose = false)
    {
        verboseMode = verbose;
        IEnumerator coroutine = StandardTest(timeCutOff);
        StartCoroutine(coroutine);
    }

    // A standard test, that ends after cutoff seconds are elapsed
    IEnumerator StandardTest(float cutoff)
    {
        isAutomatedTest = true;

        //Create log file
        CreateTestLogFile();

        //Test complete due to cut off
        currentTest = new AggregatedLogStandard(duration: cutoff, time: System.DateTime.Now.ToLongTimeString());

        MakeView();
        MakeEnvironment();

        yield return new WaitForSeconds(cutoff);

        //This marks the end of one iteration of the test
        CalculateAndSetTestStatistics();
        WriteStructureToTextLog();

        PlaceHolderForVisualizer.deleteView();
        Application.Quit();
    }

    // The test structure called for tests that optimize for energy (monitors) or coverage of behaviors
    public void TestIterative(List<ProDM> testVars, List<string> behaviors = null, string actorMonitorName = null, int repetitions = 10, int sampleSize = 2, int numTopEnergies = 10, float singleTestTime = 10f,
    AutomatedVarSetMethod setVar = AutomatedVarSetMethod.OPTIMIZE_ENERGY_HALTON_SAMPLE, bool verbose = false)
    {

        if (sampleSize < 2)
        {
            throw new System.Exception("sample size must be at least 2");
        }

        isAutomatedTest = true;

        //Set other test var to null
        currentTestTemporal = null;
        currentTest = null;

        verboseMode = verbose;

        if (setVar == AutomatedVarSetMethod.OPTIMIZE_ENERGY_HALTON_SAMPLE || setVar == AutomatedVarSetMethod.OPTIMIZE_ENERGY_RANDOM_SAMPLE)
        {
            if (actorMonitorName == null)
            {
                if (behaviors != null)
                {
                    throw new Exception("Error! Behaviors are set, but OPTIMIZE_ENERGY is selected in TestIterative.");
                }
                else
                {
                    throw new Exception("Error! Actor Monitor must be set for OPTIMIZE_ENERGY.");
                }

            }

            IEnumerator coroutine = AutomatedTestEnergyOptimization(testVars, repetitions, sampleSize, singleTestTime, setVar, actorMonitorName);
            StartCoroutine(coroutine);
        }
        else if (setVar == AutomatedVarSetMethod.OPTIMIZE_ENERGY_HALTON_MAX || setVar == AutomatedVarSetMethod.OPTIMIZE_ENERGY_RANDOM_MAX)
        {
            if (actorMonitorName == null)
            {
                if (behaviors != null)
                {
                    throw new Exception("Error! Behaviors are set, but OPTIMIZE_ENERGY is selected in TestIterative.");
                }
                else
                {
                    throw new Exception("Error! Actor Monitor must be set for OPTIMIZE_ENERGY.");
                }

            }
            
            if(numTopEnergies > sampleSize)
            {
                throw new Exception("Error! numTopEnergies needs to be greater than sampleSize");
            }

            IEnumerator coroutine = AutomatedTestEnergyOptimizationMAX(testVars, repetitions, sampleSize, numTopEnergies, singleTestTime, setVar, actorMonitorName);
            StartCoroutine(coroutine);
        }
        else if (setVar == AutomatedVarSetMethod.OPTIMIZE_BEHAVIOR)
        {
            if (behaviors == null)
            {
                if (actorMonitorName != null)
                {
                    throw new Exception("Error! Actor Monitor is set, but OPTIMIZE_BEHAVIOR is selected in TestIterative.");
                }
                else
                {
                    throw new Exception("Error! Behaviors must be set for OPTIMIZE_BEHAVIOR.");
                }


            }

            IEnumerator coroutine = AutomatedTestBehaviorOptimization(testVars, repetitions, sampleSize, singleTestTime, setVar, behaviors);
            StartCoroutine(coroutine);
        }

    }

    public void TestIterative(List<VarInterval<float>> rangedFloats, List<VarEnum<int>> discVars = null ,int iterations = 10, float singleTestTime = 10f,
        AutomatedVarSetMethod setVar = AutomatedVarSetMethod.LOW_DISCREPANCY, int repetitionsPerIteration = 1, bool verbose = false)
    {
        isAutomatedTest = true;

        //Set other test var to null
        currentTestTemporal = null;
        currentTest = null;

        verboseMode = verbose;

        //Make coroutine of the iterated test
        IEnumerator coroutine = AutomatedTest(rangedFloats, discVars, iterations, singleTestTime, setVar, repetitionsPerIteration);
        StartCoroutine(coroutine);

    }

    //Change settings after time (single iteration)
    //List of List of ProDMFloats rangedFloats, List float for time
    public void TestTemporal(List<List<VarInterval<float>>> rangedFloats, List<List<float>> targetVals = null, List<float> times = null, float timeCutOff = 15f,
        bool verbose = false)
    {
        isAutomatedTest = true;

        verboseMode = verbose;

        //Check on the list of time
        if (times == null)
        {
            //Build the list by equally dividing time
            var divs = rangedFloats.Count;
            var interval = timeCutOff / divs;
            times = new List<float>();

            for (var i = 0; i < rangedFloats.Count; i++)
            {
                times.Add(interval);
            }
        }
        else
        {
            //Duration should be more than time of temporal disturbance
            var overallTime = 0f;
            foreach (var time in times)
            {
                //Get total time for disturbances
                overallTime += time;
            }
            if (timeCutOff <= overallTime)
            {
                throw new System.Exception("Duration should be more than time of temporal disturbance.");
            }
        }

        //Create log file
        CreateTestLogFile();

        //Make the environment
        MakeView();
        MakeEnvironment();

        //Set other test vars to null
        currentTestIterative = null;
        currentTestIteration = null;
        currentTest = null;

        IEnumerator coroutine = AutomatedTestTemporal(rangedFloats, targetVals, times, timeCutOff);
        StartCoroutine(coroutine);
    }
    //Hack for training OneOf invariants
    public virtual void UpdateViewVars(bool toDefault)
    {
        throw new System.Exception("Cannot call this funciton");
    }
    // Needs to be described 
    public virtual void MakeView()
    {
        throw new System.Exception("Cannot call this funciton");
    }

    //Needs to be set
    public virtual void MakeEnvironment()
    {
        throw new System.Exception("Cannot call this funciton");
    }


    // Test several iterations over ranged values to optimize for maximum energy
    IEnumerator AutomatedTestBehaviorOptimization(List<ProDM> testVars, int repetitions, int sampleSize, float singleTestTime, AutomatedVarSetMethod setVar, List<string> behaviorIDs)
    {
        sReference = new List<object>();
        //discretize continuous parameters
        List<List<float>> discretizedVals = generateHaltonSeq(testVars, sampleSize);

        //get neighborhoodSize
        //get neighborhoodSize
        List<List<float>> orderedHalton = new List<List<float>>(discretizedVals);
        foreach (List<float> list in orderedHalton)
        {
            list.Sort();
        }

        List<float> haltonNeighborhoodSize = new List<float>();
        foreach (List<float> list in orderedHalton)
        {
            haltonNeighborhoodSize.Add(Math.Abs(list[1] - list[0]));
            
        }

        int totalIterations = repetitions + sampleSize;
        Debug.Log("Total Iterations: " + totalIterations);
        int totalIterationCount = 0;


        //Create log file
        CreateTestLogFile();

        //Create TestLog structure
        currentTestIterative = new AggregatedLogIterative(behaviorIDs, totalIterations, singleTestTime);

        Debug.Log("-----INITIAL TEST OF VALUES-----");
        //first do initial loop through all halton sequence values
        for (int i = 0; i < sampleSize; i++)
        {
            //set initial parameters
            List<object> s = setInitialParameters(testVars, discretizedVals, i);
            sReference = s;
            setValues(s, testVars);

            currentTestIteration = new TestIteration(iterationNum: totalIterationCount, time: System.DateTime.Now.ToLongTimeString());
            Debug.Log("Current Iteration: " + totalIterationCount);
            totalIterationCount++;

            MakeView();
            MakeEnvironment();

            yield return new WaitForSeconds(singleTestTime);
            //This marks the end of one iteration of the test
            CalculateAndSetTestStatistics();

            PlaceHolderForVisualizer.deleteView();

            //Create a snapshot to be saved in custom structure
            List<object> snapshot = new List<object>(s);
            foreach (object item in snapshot)
            {
                float snapshot1 = (float)item;
                currentTestIteration.rangedValues.Add(snapshot1);
            }

            Debug.Log("Current Parameters: ");
            foreach (object item in snapshot)
            {
                Debug.Log(item);

            }

            Debug.Log("Behavior Update:");
            foreach (var item in currentTestIterative.onlineBehaviors)
            {
                Debug.Log(item.Value.behaviorID + " counted: " + item.Value.count);
            }

            //Delete view and add this run to the log structure

            currentTestIterative.testIterations.Add(currentTestIteration);

        }

        Debug.Log("-----STARTING REPETITIONS-----");
        //now for each repetition, fuzz around the least observed behavior
        for (int i = 0; i < repetitions; i++)
        {
            //find least observed behavior
            OnlineBehavior leastObserved = currentTestIterative.onlineBehaviors.OrderBy(x => x.Value.count).First().Value;
            setValues(leastObserved.parameters, testVars);

            List<object> s;
            //if there's no count yet for the least observed, just pick some random starting place, then fuzz around that.
            if (leastObserved.count <= 0)
            {
                s = setInitialParameters(testVars, discretizedVals, UnityEngine.Random.Range(0, discretizedVals.Count - 1));
                setValues(s, testVars);
            }
            s = getNeighbor(testVars, haltonNeighborhoodSize, 1);

            sReference = s;
            setValues(s, testVars);

            currentTestIteration = new TestIteration(iterationNum: totalIterationCount, time: System.DateTime.Now.ToLongTimeString());
            Debug.Log("Current Iteration: " + totalIterationCount);
            totalIterationCount++;

            MakeView();
            MakeEnvironment();

            yield return new WaitForSeconds(singleTestTime);
            //This marks the end of one iteration of the test
            CalculateAndSetTestStatistics();

            PlaceHolderForVisualizer.deleteView();

            //Create a snapshot to be saved in custom structure
            List<object> snapshot = new List<object>(s);
            foreach (object item in snapshot)
            {
                float snapshot1 = (float)item;
                currentTestIteration.rangedValues.Add(snapshot1);
            }

            Debug.Log("Current Parameters: ");
            foreach (object item in snapshot)
            {
                Debug.Log(item);

            }

            Debug.Log("Behavior Update:");
            foreach (var item in currentTestIterative.onlineBehaviors)
            {
                Debug.Log(item.Value.behaviorID + " counted: " + item.Value.count);
            }

            //Delete view and add this run to the log structure

            currentTestIterative.testIterations.Add(currentTestIteration);
        }

        Debug.Log("-----FINAL STATS-----");
        Debug.Log("Final count: ");
        int behaviorsTested = 0;
        foreach (var item in currentTestIterative.onlineBehaviors)
        {
            Debug.Log(item.Value.behaviorID + " counted: " + item.Value.count);
            if (item.Value.count > 0)
            {
                behaviorsTested++;
            }
        }
        float percentCoverage = ((float)behaviorsTested / (float)currentTestIterative.onlineBehaviors.Count) * 100f;
        Debug.Log("Percent coverage: " + percentCoverage + "%");

        WriteStructureToTextLog();
        Application.Quit(); //Quit the application if all iterations are complete

    }

    // Test several iterations over ranged values to optimize for maximum energy
    IEnumerator AutomatedTestEnergyOptimization(List<ProDM> testVars, int repetitions, int sampleSize, float singleTestTime, AutomatedVarSetMethod setVar, string actorMonitorName)
    {
        sReference = new List<object>();
        //discretize continuous parameters

        List<List<float>> discretizedVals = new List<List<float>>();
        if (setVar == AutomatedVarSetMethod.OPTIMIZE_ENERGY_HALTON_SAMPLE)
        {
            discretizedVals = generateHaltonSeq(testVars, sampleSize);
        }
        else if (setVar == AutomatedVarSetMethod.OPTIMIZE_ENERGY_RANDOM_SAMPLE)
        {
            discretizedVals = generateRandomSeq(testVars, sampleSize);
        }


        //get neighborhoodSize
        List<List<float>> orderedHalton = new List<List<float>>();
        for(int i = 0; i < discretizedVals.Count; i++)
        {
            List<float> temp = new List<float>(discretizedVals[i]);
            temp.Sort();
            orderedHalton.Add(temp);
        }
        //foreach (List<float> list in orderedHalton)
        //{
            
        //    list.Sort();
        //}

        List<float> haltonNeighborhoodSize = new List<float>();
        foreach (List<float> list in orderedHalton)
        {
            haltonNeighborhoodSize.Add((Math.Abs(list[1] - list[0])));
        }

        int totalIterations = repetitions * sampleSize;
        Debug.Log("Total Iterations: " + totalIterations);
        int totalIterationCount = 0;


        //Create log file
        CreateTestLogFile();

        //Create TestLog structure
        currentTestIterative = new AggregatedLogIterative(setVar, discretizedVals[0].Count, singleTestTime, saReps:repetitions, testVars: testVars);


        List<object> sBest = setInitialParameters(testVars, discretizedVals, 0);
        float energy_sBest = 0;

        for (int m = 0; m < discretizedVals[0].Count; m++)
        {
            //set initial parameters
            List<object> s = setInitialParameters(testVars, discretizedVals, m);
            sReference = s;
            setValues(s, testVars);

            currentTestIteration = new TestIteration(iterationNum: m, time: System.DateTime.Now.ToLongTimeString(), saRepetitionNum:0);
            Debug.Log("Current Iteration: " + totalIterationCount);
            totalIterationCount++;

            MakeView();
            MakeEnvironment();

            yield return new WaitForSeconds(singleTestTime);
            //This marks the end of one iteration of the test
            CalculateAndSetTestStatistics();


            float energy_s = getEnergy(currentTestIteration, actorMonitorName);
            Debug.Log("energy: " + energy_s);
            if (totalIterationCount == 1)
            {
                energy_sBest = energy_s;
            }

            PlaceHolderForVisualizer.deleteView();

            //Create a snapshot to be saved in custom structure
            List<object> snapshot = new List<object>(s);
            foreach (object item in snapshot)
            {
                float snapshot1 = (float)item;
                currentTestIteration.rangedValues.Add(snapshot1);
            }

            Debug.Log("Current Parameters: ");
            foreach (object item in snapshot)
            {
                Debug.Log(item);

            }

            //Delete view and add this run to the log structure

            currentTestIterative.testIterations.Add(currentTestIteration);


            //initialize temperature
            float T = 0;

            //do the simulated annealing
            for (int i = 0; i < repetitions; i++)
            {
                currentTestIteration = new TestIteration(iterationNum: m, time: System.DateTime.Now.ToLongTimeString(), saRepetitionNum: i + 1);
                Debug.Log("Current Iteration: " + totalIterationCount);
                totalIterationCount++;

                //update temperature
                T = getNewTemperature(i, repetitions);

                //pick random neighbor
                List<object> sNew = getNeighbor(testVars, haltonNeighborhoodSize, (T / (0.1f*repetitions)));
                sReference = sNew;
                setValues(sNew, testVars);

                //get energies
                MakeView();
                MakeEnvironment();

                yield return new WaitForSeconds(singleTestTime);

                //This marks the end of one iteration of the test
                CalculateAndSetTestStatistics();
                float energy_sNew = getEnergy(currentTestIteration, actorMonitorName);
                //float energy_sNew = getEnergy((float)sNew[0]);

                PlaceHolderForVisualizer.deleteView();

                //Create a snapshot to be saved in custom structure
                snapshot = new List<object>(sNew);
                foreach (object item in snapshot)
                {
                    float snapshot1 = (float)item;
                    currentTestIteration.rangedValues.Add(snapshot1);
                }

                Debug.Log("energy: " + energy_sNew);
                Debug.Log("Current Parameters: ");
                foreach (object item in snapshot)
                {
                    Debug.Log(item);

                }

                //Delete view and add this run to the log structure

                currentTestIterative.testIterations.Add(currentTestIteration);


                //decide which one to keep
                if (probability(energy_s, energy_sNew, T) >= UnityEngine.Random.Range(0, 1))
                {
                    Debug.Log("Take sNew");
                    s = sNew;
                    energy_s = energy_sNew;
                    setValues(s, testVars);

                }
                else
                {
                    Debug.Log("Reject sNew");
                }

            }

            if (energy_s > energy_sBest)
            {
                Debug.Log("Replacing sBest");
                sBest = s;
                energy_sBest = energy_s;
                setValues(sBest, testVars);
            }

            Debug.Log(totalIterationCount + " ? " + totalIterations);
            Debug.Log((totalIterationCount) == totalIterations);
            if ((totalIterationCount) == totalIterations)
            {
                Debug.Log("Final energy: " + energy_sBest);
                Debug.Log("Final solution: ");
                foreach (object item in sBest)
                {
                    Debug.Log(item);

                }

                WriteStructureToTextLog();
                Application.Quit(); //Quit the application if all iterations are complete
            }
        }
    }

    // Test several iterations over ranged values to optimize for maximum energy
    IEnumerator AutomatedTestEnergyOptimizationMAX(List<ProDM> testVars, int repetitions, int sampleSize, int numTopEnergies, float singleTestTime, AutomatedVarSetMethod setVar, string actorMonitorName)
    {
        //first generate discretized values to test
        sReference = new List<object>();
        //discretize continuous parameters

        List<List<float>> discretizedVals = new List<List<float>>();
        if (setVar == AutomatedVarSetMethod.OPTIMIZE_ENERGY_HALTON_MAX)
        {
            discretizedVals = generateHaltonSeq(testVars, sampleSize);
        }
        else if (setVar == AutomatedVarSetMethod.OPTIMIZE_ENERGY_RANDOM_MAX)
        {
            discretizedVals = generateRandomSeq(testVars, sampleSize);
        }


        //get neighborhoodSize
        List<List<float>> orderedHalton = new List<List<float>>();
        for (int i = 0; i < discretizedVals.Count; i++)
        {
            List<float> temp = new List<float>(discretizedVals[i]);
            temp.Sort();
            orderedHalton.Add(temp);
        }


        List<float> haltonNeighborhoodSize = new List<float>();
        foreach (List<float> list in orderedHalton)
        {
            haltonNeighborhoodSize.Add((Math.Abs(list[1] - list[0])));
            Debug.Log("NEIGHBORHOOD SIZE " + Math.Abs(list[1] - list[0]));
        }


        //second test all of these discretized values to see which has best energy
        int totalIterations = numTopEnergies;
        Debug.Log("Total Iterations now: " + totalIterations);
        int totalIterationCount = 0;


        Debug.Log("Starting initial test...");
        //Create log file
        CreateTestLogFile();

        //use this dictionary to store iteration energy and Halton values
        Dictionary<List<object>, float> initialRunData = new Dictionary<List<object>, float>();

        //Create TestLog structure
        currentTestIterative = new AggregatedLogIterative(setVar, discretizedVals[0].Count, singleTestTime, saReps: repetitions, testVars: testVars);

        for (int m = 0; m < discretizedVals[0].Count; m++)
        {
            //set initial parameters
            List<object> s = setInitialParameters(testVars, discretizedVals, m);
            sReference = s;
            setValues(s, testVars);

            currentTestIteration = new TestIteration(iterationNum: m, time: System.DateTime.Now.ToLongTimeString(), saRepetitionNum: 0);
            Debug.Log("Current Iteration: " + totalIterationCount);
            totalIterationCount++;

            MakeView();
            MakeEnvironment();

            yield return new WaitForSeconds(singleTestTime);
            //This marks the end of one iteration of the test
            CalculateAndSetTestStatistics();


            float energy_s = getEnergy(currentTestIteration, actorMonitorName);
            initialRunData.Add(s, energy_s);
            Debug.Log("energy: " + energy_s);
            
            PlaceHolderForVisualizer.deleteView();

            //Create a snapshot to be saved in custom structure
            List<object> snapshot = new List<object>(s);
            foreach (object item in snapshot)
            {
                float snapshot1 = (float)item;
                currentTestIteration.rangedValues.Add(snapshot1);
            }

            Debug.Log("Current Parameters: ");
            foreach (object item in snapshot)
            {
                Debug.Log(item);

            }

            //Delete view and add this run to the log structure

            currentTestIterative.testIterations.Add(currentTestIteration);
        }


        ////third take top k energies, and perform simulated annealing on them
        Debug.Log("Exploring top energies...");
        Debug.Log("TOP PERFORMERS");
        Dictionary<List<object>, float> topPerformers = new Dictionary<List<object>, float>();
        foreach(var item in initialRunData.OrderByDescending(x => x.Value).Take(numTopEnergies))
        {
            topPerformers.Add(item.Key, item.Value);
            Debug.Log(item.Value);
            foreach (object thing in item.Key)
            {
                Debug.Log((float)thing);

            }
        }
        Debug.Log("END");

        List<object> sBest = new List<object>(topPerformers.ElementAt(0).Key);
        float energy_sBest = 0;
        for (int m = 0; m < numTopEnergies; m++)
        {
             
            List<object> s = new List<object>(topPerformers.ElementAt(m).Key);
            float energy_s = topPerformers.ElementAt(m).Value;
            setValues(s, testVars);
            Debug.Log("CURRENT TOP PERFORMER:");
            Debug.Log(energy_s);
            foreach (object thing in s)
            {
                Debug.Log((float)thing);

            }
            Debug.Log("END");


            //initialize temperature
            float T = 0;

            //do the simulated annealing
            for (int i = 0; i < repetitions; i++)
            {
                currentTestIteration = new TestIteration(iterationNum: m, time: System.DateTime.Now.ToLongTimeString(), saRepetitionNum: i + 1);
                Debug.Log("Current Iteration: " + totalIterationCount);
                totalIterationCount++;

                //update temperature
                T = getNewTemperature(i, repetitions);

                //pick random neighbor
                List<object> sNew = getNeighbor(testVars, haltonNeighborhoodSize, (T / (0.1f * repetitions)));
                sReference = sNew;
                setValues(sNew, testVars);

                //get energies
                MakeView();
                MakeEnvironment();

                yield return new WaitForSeconds(singleTestTime);

                //This marks the end of one iteration of the test
                CalculateAndSetTestStatistics();
                float energy_sNew = getEnergy(currentTestIteration, actorMonitorName);
                //float energy_sNew = getEnergy((float)sNew[0]);

                PlaceHolderForVisualizer.deleteView();

                //Create a snapshot to be saved in custom structure
                List<object> snapshot = new List<object>(sNew);
                foreach (object item in snapshot)
                {
                    float snapshot1 = (float)item;
                    currentTestIteration.rangedValues.Add(snapshot1);
                }

                Debug.Log("energy: " + energy_sNew);
                Debug.Log("Current Parameters: ");
                foreach (object item in snapshot)
                {
                    Debug.Log(item);

                }

                //Delete view and add this run to the log structure

                currentTestIterative.testIterations.Add(currentTestIteration);


                //decide which one to keep
                if (probability(energy_s, energy_sNew, T) >= UnityEngine.Random.Range(0, 1))
                {
                    Debug.Log("Take sNew");
                    s = sNew;
                    energy_s = energy_sNew;
                    setValues(s, testVars);

                }
                else
                {
                    Debug.Log("Reject sNew");
                }

            }

            if (energy_s > energy_sBest)
            {
                Debug.Log("Replacing sBest");
                sBest = s;
                energy_sBest = energy_s;
                setValues(sBest, testVars);
            }

           
            if (m == totalIterations - 1)
            {
                Debug.Log("Final energy: " + energy_sBest);
                Debug.Log("Final solution: ");
                foreach (object item in sBest)
                {
                    Debug.Log(item);

                }

                
            }

        }

        WriteStructureToTextLog();
        Application.Quit(); //Quit the application if all iterations are complete

    }


    #region Simulated Annealing Support

    float getEnergy(TestIteration currentTestIteration, string actorMonitorName)
    {
        if (currentTestIteration.collision.Count > 0)
        {
            foreach (var onlineEvent in currentTestIteration.onlineEvents)
            {
                ActorMonitor monitor = EnvironmentModel.actorMonitorList[actorMonitorName];
                if (onlineEvent._ownerID == monitor.actorRef._id && onlineEvent._type == monitor.monitorType)
                {
                    return onlineEvent._energy;
                }
            }
            return 0f;
        }
        else
        {
            return 0f;
        }


    }

    List<object> setInitialParameters(List<ProDM> testVars, List<List<float>> discretizedVals, int currentIndex)
    {
        //randomly initialize them
        List<object> valList = new List<object>();

        //use this to access halton sequences for continuous parameters
        int floatIndex = 0;

        //set each to a random value
        foreach (var number in testVars)
        {
            if (number._type == ProDMType.INT)
            {
                VarInterval<int> newNum = (VarInterval<int>)number;
                valList.Add(UnityEngine.Random.Range((dynamic)newNum._min, (dynamic)newNum._max));

            }
            else if (number._type == ProDMType.FLOAT)
            {
                valList.Add(discretizedVals[floatIndex][currentIndex]);

                floatIndex++;
            }
            else if (number._type == ProDMType.ONE_OF)
            {
                VarEnum<int> newNum = (VarEnum<int>)number;
                valList.Add(UnityEngine.Random.Range(0, newNum._size));
            }
            else
            {

            }
        }

        return valList;
    }

    List<object> getNeighbor(List<ProDM> testVars, List<float> haltonNeighborhoodSize, float neighborhoodScale)
    {
        List<object> valList = new List<object>();

        //use this to access halton sequences for continuous parameters
        int floatIndex = 0;

        //set each to a random value
        foreach (var number in testVars)
        {
            if (number._type == ProDMType.INT)
            {
                VarInterval<int> newNum = (VarInterval<int>)number;
                int neighborhoodSize = (int)((newNum._max - newNum._min) * neighborhoodScale);
                int neighborVal = UnityEngine.Random.Range(newNum._val - (neighborhoodSize / 2), newNum._val + (neighborhoodSize / 2));
                if (neighborVal > newNum._max)
                {
                    neighborVal = newNum._max;
                }
                else if (neighborVal < newNum._min)
                {
                    neighborVal = newNum._min;
                }
                valList.Add(neighborVal);
            }
            else if (number._type == ProDMType.FLOAT)
            {
                VarInterval<float> newNum = (VarInterval<float>)number;

                float neighborhoodSize = haltonNeighborhoodSize[floatIndex] * neighborhoodScale;
                float neighborVal = UnityEngine.Random.Range((dynamic)(newNum._val - (neighborhoodSize / 2)), (dynamic)(newNum._val + (neighborhoodSize / 2)));
                if (neighborVal > newNum._max)
                {
                    neighborVal = newNum._max;
                }
                else if (neighborVal < newNum._min)
                {
                    neighborVal = newNum._min;
                }
                valList.Add(neighborVal);

                floatIndex++;
            }
            else if (number._type == ProDMType.ONE_OF)
            {
                VarEnum<int> newNum = (VarEnum<int>)number;
                int neighborhoodSize = (int)(newNum._size * neighborhoodScale);
                int neighborVal = UnityEngine.Random.Range(newNum._index - (neighborhoodSize / 2), newNum._index + (neighborhoodSize / 2));
                if (neighborVal > (newNum._size - 1))
                {
                    neighborVal = newNum._size - 1;
                }
                else if (neighborVal < 0)
                {
                    neighborVal = 0;
                }
                valList.Add(neighborVal);
            }
            else
            {

            }
        }

        return valList;
    }

    List<List<float>> generateRandomSeq(List<ProDM> testVars, int sampleSize)
    {
        List<List<float>> rangedFloatsSeq = new List<List<float>>();

        //set each to a random value
        for (var i = 0; i < testVars.Count; i++)
        {
            if (testVars[i]._type == ProDMType.FLOAT)
            {
                VarInterval<float> newNum = (VarInterval<float>)testVars[i];

                List<float> randSeq = new List<float>();

                for (int it = 0; it < sampleSize; it++)
                    randSeq.Add(UnityEngine.Random.Range(newNum._min, newNum._max));
                //Add to list of test vals
                rangedFloatsSeq.Add(randSeq);
            }
        }

        return rangedFloatsSeq;
    }

    List<List<float>> generateHaltonSeq(List<ProDM> testVars, int sampleSize)
    {
        List<List<float>> rangedFloatsSeq = new List<List<float>>();

        //set each to a random value
        for (var i = 0; i < testVars.Count; i++)
        {
            if (testVars[i]._type == ProDMType.FLOAT)
            {
                VarInterval<float> newNum = (VarInterval<float>)testVars[i];

                //generate halton sequence
                // Call the halton function
                List<float> haltonSeq = new List<float>();

                for (int it = 0; it < (sampleSize); it++)
                    haltonSeq.Add(Halton(it + 1, 10)[i]);
                //Now scale this to the requisite min, max
                haltonSeq = ScaledList(haltonSeq, min: newNum._min, max: newNum._max);
                rangedFloatsSeq.Add(haltonSeq);
            }
        }

        return rangedFloatsSeq;
    }

    List<ProDM> setValues(List<object> valList, List<ProDM> testVars)
    {
        for (int n = 0; n < valList.Count; n++)
        {
            if (testVars[n]._type == ProDMType.INT)
            {
                VarInterval<int> newNum = (VarInterval<int>)testVars[n];
                newNum._val = (int)valList[n];
            }
            else if (testVars[n]._type == ProDMType.FLOAT)
            {
                VarInterval<float> newNum = (VarInterval<float>)testVars[n];
                newNum._val = (float)valList[n];
            }
            else if (testVars[n]._type == ProDMType.ONE_OF)
            {
                VarEnum<int> newNum = (VarEnum<int>)testVars[n];
                newNum._index = (int)valList[n];
            }
            else
            {

            }
        }

        return testVars;
    }

    float getNewTemperature(int k, int iterations)
    {
        return (iterations) / (k + 1);
    }

    double probability(float e1, float e2, float T)
    {
        if (e1 < e2)
        {
            return 1;
        }
        else
        {
            return System.Math.Exp((e2 - e1) / T);
        }

    }

    #endregion

    // Test several iterations over ranged values
    IEnumerator AutomatedTest(List<VarInterval<float>> rangedFloats, List<VarEnum<int>> discVars, int iterations, float singleTestTime, AutomatedVarSetMethod setVar, int repetitions)
    {
        //Create log file
        CreateTestLogFile();

        //Create TestLog structure
        currentTestIterative = new AggregatedLogIterative(setVar, iterations, singleTestTime, repetitions, rangedFloats);

        List<List<float>> rangedFloatsLDOrRand = new List<List<float>>(rangedFloats.Count);

        //Calculate values for all rangedFloats over all iterations
        if (setVar == AutomatedVarSetMethod.LOW_DISCREPANCY)
        {
            for (var i = 0; i < rangedFloats.Count; i++)
            {
                // Call the halton function
                List<float> haltonSeq = new List<float>();

                for (int it = 0; it < iterations; it++)
                    haltonSeq.Add(Halton(it + 1, 10)[i]);
                //Now scale this to the requisite min, max
                haltonSeq = ScaledList(haltonSeq, min: rangedFloats[i]._min, max: rangedFloats[i]._max);
                rangedFloatsLDOrRand.Add(haltonSeq);
            }

        }
        //Random
        else if (setVar == AutomatedVarSetMethod.RANDOM)
        {
            for (var i = 0; i < rangedFloats.Count; i++)
            {
                List<float> randSeq = new List<float>();

                for (int it = 0; it < iterations; it++)
                    randSeq.Add(UnityEngine.Random.Range(rangedFloats[i]._min, rangedFloats[i]._max));
                //Add to list of test vals
                rangedFloatsLDOrRand.Add(randSeq);
            }
        }
        else
            throw new System.Exception("Unknown ranged (ProDM) variable set method");

        // Outer loop for several iterations of the test (over different values of the ranged variables)
        for (var i = 0; i < iterations; i++)
        {
            for (int repetitionCounter = 1; repetitionCounter <= repetitions; repetitionCounter++)
            {
                currentTestIteration = new TestIteration(iterationNum: i + 1, time: System.DateTime.Now.ToLongTimeString(),
                    repetitionNumber: repetitionCounter);

                for (var j = 0; j < rangedFloats.Count; j++)
                {
                    var elemFloat = rangedFloats.ElementAt(j);
                    elemFloat.ChangeValue(rangedFloatsLDOrRand.ElementAt(j).ElementAt(i));

                    //Create a snapshot to be saved in custom structure
                    VarInterval<float> snapshot = new VarInterval<float>(elemFloat);
                    float temp = snapshot;
                    currentTestIteration.rangedValues.Add(temp);
                }
                // Similarly, for discrete params
				if (discVars != null) {
					for (var j = 0; j < discVars.Count; j++) {
						// Set new random value
						discVars [j].SetRandom ();
						VarEnum<int> snapshotDisc = new VarEnum<int> (discVars [j]);
						int temp = snapshotDisc;
						currentTestIteration.testVarsDiscrete.Add (temp);
					}
				}

                MakeView();
                MakeEnvironment();

                yield return new WaitForSeconds(singleTestTime);

                //This marks the end of one iteration of the test
                CalculateAndSetTestStatistics();

                //Delete view and add this run to the log structure
                PlaceHolderForVisualizer.deleteView();
                currentTestIterative.testIterations.Add(currentTestIteration);
            }
            if (i == iterations - 1)
            {
                WriteStructureToTextLog();
                Application.Quit(); //Quit the application if all iterations are complete
            }
        }

    }

    //Set environment variables at some time 
    IEnumerator AutomatedTestTemporal(List<List<VarInterval<float>>> rangedFloats, List<List<float>> targetVals, List<float> times,
        float testDuration)
    {
        // Make an aggregated log for this temporal test
        currentTestTemporal = new AggregatedLogTemporal(valBefore: rangedFloats, valAfter: targetVals, timeToDisturbance: times,
            duration: testDuration, time: System.DateTime.Now.ToLongTimeString());

        var totalTimeSoFar = 0f;
        // For each time in 
        for (int timeCounter = 0; timeCounter < times.Count; timeCounter++)
        {
            var time = times[timeCounter];
            yield return new WaitForSeconds(time);
            totalTimeSoFar += time;
            for (int i = 0; i < rangedFloats[timeCounter].Count; i++)
            {
                VarInterval<float> elem = rangedFloats[timeCounter][i];
                if (targetVals == null)
                {
                    elem.SetRandom();
                }
                else
                {
                    elem = (float)targetVals[timeCounter][i];
                }
            }
            //Set the environment settings
            MakeEnvironment();
        }

        yield return new WaitForSeconds(testDuration - totalTimeSoFar);
        //End of simulation

        //Calculate aggregated info
        CalculateAndSetTestStatistics();

        WriteStructureToTextLog();
        PlaceHolderForVisualizer.deleteView();
        Application.Quit(); //Quit the application if all iterations are complete
    }

    // Test interface for reactive properties
    public void TestReactive(ReactiveProperty<float> property)
    {
        Debug.Log("Not yet implemented");
    }

    private void CreateTestLogFile()
    {
        testLog = new StreamWriter("Test-" + System.DateTime.Now.ToString("yyyy-dd-M--HH-mm") + ".txt");
    }

    private void WriteToTestLog(string stuffToWrite)
    {
        testLog.WriteLine(stuffToWrite);
    }

    void WriteStructureToTextLog()
    {
        if (currentTestIterative != null)
            testLog.WriteLine(JsonUtility.ToJson(currentTestIterative, prettyPrint: true));
        else if (currentTestTemporal != null)
            testLog.WriteLine(JsonUtility.ToJson(currentTestTemporal, prettyPrint: true));
        else if (currentTest != null)
            testLog.WriteLine(JsonUtility.ToJson(currentTest, prettyPrint: true));
    }

    void BehaviorDetected(string behaviorID)
    {
        if (currentTestIterative != null && currentTestIterative.onlineBehaviors != null)
        {
            currentTestIterative.onlineBehaviors[behaviorID].count++;
            currentTestIterative.onlineBehaviors[behaviorID].updateParameters(new List<object>(sReference));
        }
    }

    //Called via broadcast from OnlineCheckers
    void CollisionDetected(CollisionEvent col)
    {
        if (isAutomatedTest)
        {
            if (currentTestIteration != null)
            {
                if (!(currentTestIteration.collision.Count > listSizeCutoff))
                {
                    currentTestIteration.onlineEvents.Add(col);
                    currentTestIteration.collision.Add(col);
                }
            }
            else if (currentTestTemporal != null)
            {
                if (!(currentTestTemporal.collision.Count > listSizeCutoff))
                {
                    currentTestTemporal.onlineEvents.Add(col);
                    currentTestTemporal.collision.Add(col);
                }
            }
            else if (currentTest != null)
            {
                if (!(currentTest.collision.Count > listSizeCutoff))
                {
                    currentTest.onlineEvents.Add(col);
                    currentTest.collision.Add(col);
                }
            }
        }
    }
    void InactivityDetected(InactivityEvent inactive)
    {
        if (isAutomatedTest && verboseMode)
        {
            if (currentTestIteration != null)
            {
                if (!(currentTestIteration.inactivity.Count > listSizeCutoff))
                {
                    currentTestIteration.onlineEvents.Add(inactive);
                    currentTestIteration.inactivity.Add(inactive);
                }
            }
            else if (currentTestTemporal != null)
            {
                if (!(currentTestTemporal.inactivity.Count > listSizeCutoff))
                {
                    currentTestTemporal.onlineEvents.Add(inactive);
                    currentTestTemporal.inactivity.Add(inactive);
                }
            }
            else if (currentTest != null)
            {

                if (!(currentTest.inactivity.Count > listSizeCutoff))
                {
                    currentTest.onlineEvents.Add(inactive);
                    currentTest.inactivity.Add(inactive);
                }
            }
        }
    }
    // Called by the car controller
    public void AddSpeedAtFrame(float speed)
    {
        if (isAutomatedTest && verboseMode)
        {
            if (currentTestIteration != null)
            {
                currentTestIteration.averageSpeedPerFrame.Add(speed);
            }
            else if (currentTestTemporal != null)
            {
                currentTestTemporal.averageSpeedPerFrame.Add(speed);
            }
            else if (currentTest != null)
            {
                currentTest.averageSpeedPerFrame.Add(speed);
            }
        }
    }

    // Calculate statistics for this iteration of automated test
    void CalculateAndSetTestStatistics()
    {
        // Sent to the SUT to calculate any statistics before the end of the test
		PlaceHolderForVisualizer.egoCarGO.BroadcastMessage("EndOfTest", SendMessageOptions.DontRequireReceiver);

        if (currentTestIteration != null)
        {
            //average speed calculation
            currentTestIteration.averageSpeed = PlaceHolderForVisualizer.egoCarRef._liveSpeed.Average();

            //distance travelled calculation

            Vector3 prevPos = PlaceHolderForVisualizer.egoCarRef._livePos[0];
            currentTestIteration.distanceCovered = 0f;
            foreach (var pos in PlaceHolderForVisualizer.egoCarRef._livePos)
            {
                currentTestIteration.distanceCovered += ((prevPos - pos).magnitude);
                prevPos = pos;
            }

            // displacement calculation
            currentTestIteration.displacementVector = PlaceHolderForVisualizer.egoCarRef._livePos[PlaceHolderForVisualizer.egoCarRef._livePos.Count - 1] - PlaceHolderForVisualizer.egoCarRef._livePos[0];
            currentTestIteration.displacement = currentTestIteration.displacementVector.magnitude;
        }
        else if (currentTestTemporal != null)
        {
            currentTestTemporal.averageSpeed = PlaceHolderForVisualizer.egoCarRef._liveSpeed.Average();
            Vector3 prevPos = PlaceHolderForVisualizer.egoCarRef._livePos[0];
            currentTestTemporal.distanceCovered = 0f;
            foreach (var pos in PlaceHolderForVisualizer.egoCarRef._livePos)
            {
                currentTestTemporal.distanceCovered += ((prevPos - pos).magnitude);
                prevPos = pos;
            }

            // displacement calculation
            currentTestTemporal.displacementVector = PlaceHolderForVisualizer.egoCarRef._livePos[PlaceHolderForVisualizer.egoCarRef._livePos.Count - 1] - PlaceHolderForVisualizer.egoCarRef._livePos[0];
            currentTestTemporal.displacement = currentTestTemporal.displacementVector.magnitude;
        }
        else if (currentTest != null)
        {
            currentTest.averageSpeed = PlaceHolderForVisualizer.egoCarRef._liveSpeed.Average();
            Vector3 prevPos = PlaceHolderForVisualizer.egoCarRef._livePos[0];
            currentTest.distanceCovered = 0f;
            foreach (var pos in PlaceHolderForVisualizer.egoCarRef._livePos)
            {
                currentTest.distanceCovered += ((prevPos - pos).magnitude);
                prevPos = pos;
            }

            // displacement calculation
            currentTest.displacementVector = PlaceHolderForVisualizer.egoCarRef._livePos[PlaceHolderForVisualizer.egoCarRef._livePos.Count - 1] - PlaceHolderForVisualizer.egoCarRef._livePos[0];
            currentTest.displacement = currentTest.displacementVector.magnitude;
        }
    }

    void OnApplicationQuit()
    {
        if (testLog != null)
            testLog.Close();
    }


    #region Utility functions
    //---------------------------------------------------------------------------------------------------
    //---------------Utility functions-------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------
    //Shuffling lists
    private static System.Random rng = new System.Random();

    public static List<float> Shuffle(List<float> list)
    {
        var source = list.ToList();
        int n = source.Count;
        var shuffled = new List<float>(n);
        shuffled.AddRange(source);
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            float value = shuffled[k];
            shuffled[k] = shuffled[n];
            shuffled[n] = value;
        }
        return shuffled;
    }


    // List scaling
    //TODO: Laura made pubic for testing, shoudl be changed back
    public List<float> ScaledList(List<float> unscaled, float min, float max)
    {
        // Make a new list
        List<float> scaled = new List<float>();

        foreach (var elem in unscaled)
        {
            // Scale curr elem
            float curr = (elem * (max - min)) + min;
            scaled.Add(curr);
        }

        return scaled;
    }


    // Halton sequence
    static int Prime(int n)
    {
        int prime_max = 1600;

        int[] prime_vector = new int[] {
             2, 3, 5, 7, 11, 13, 17, 19, 23, 29,
             31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
             73, 79, 83, 89, 97, 101, 103, 107, 109, 113,
             127, 131, 137, 139, 149, 151, 157, 163, 167, 173,
             179, 181, 191, 193, 197, 199, 211, 223, 227, 229,
             233, 239, 241, 251, 257, 263, 269, 271, 277, 281,
             283, 293, 307, 311, 313, 317, 331, 337, 347, 349,
             353, 359, 367, 373, 379, 383, 389, 397, 401, 409,
             419, 421, 431, 433, 439, 443, 449, 457, 461, 463,
             467, 479, 487, 491, 499, 503, 509, 521, 523, 541,
             547, 557, 563, 569, 571, 577, 587, 593, 599, 601,
             607, 613, 617, 619, 631, 641, 643, 647, 653, 659,
             661, 673, 677, 683, 691, 701, 709, 719, 727, 733,
             739, 743, 751, 757, 761, 769, 773, 787, 797, 809,
             811, 821, 823, 827, 829, 839, 853, 857, 859, 863,
             877, 881, 883, 887, 907, 911, 919, 929, 937, 941,
             947, 953, 967, 971, 977, 983, 991, 997, 1009, 1013,
             1019, 1021, 1031, 1033, 1039, 1049, 1051, 1061, 1063, 1069,
             1087, 1091, 1093, 1097, 1103, 1109, 1117, 1123, 1129, 1151,
             1153, 1163, 1171, 1181, 1187, 1193, 1201, 1213, 1217, 1223,
             1229, 1231, 1237, 1249, 1259, 1277, 1279, 1283, 1289, 1291,
             1297, 1301, 1303, 1307, 1319, 1321, 1327, 1361, 1367, 1373,
             1381, 1399, 1409, 1423, 1427, 1429, 1433, 1439, 1447, 1451,
             1453, 1459, 1471, 1481, 1483, 1487, 1489, 1493, 1499, 1511,
             1523, 1531, 1543, 1549, 1553, 1559, 1567, 1571, 1579, 1583,
             1597, 1601, 1607, 1609, 1613, 1619, 1621, 1627, 1637, 1657,
             1663, 1667, 1669, 1693, 1697, 1699, 1709, 1721, 1723, 1733,
             1741, 1747, 1753, 1759, 1777, 1783, 1787, 1789, 1801, 1811,
             1823, 1831, 1847, 1861, 1867, 1871, 1873, 1877, 1879, 1889,
             1901, 1907, 1913, 1931, 1933, 1949, 1951, 1973, 1979, 1987,
             1993, 1997, 1999, 2003, 2011, 2017, 2027, 2029, 2039, 2053,
             2063, 2069, 2081, 2083, 2087, 2089, 2099, 2111, 2113, 2129,
             2131, 2137, 2141, 2143, 2153, 2161, 2179, 2203, 2207, 2213,
             2221, 2237, 2239, 2243, 2251, 2267, 2269, 2273, 2281, 2287,
             2293, 2297, 2309, 2311, 2333, 2339, 2341, 2347, 2351, 2357,
             2371, 2377, 2381, 2383, 2389, 2393, 2399, 2411, 2417, 2423,
             2437, 2441, 2447, 2459, 2467, 2473, 2477, 2503, 2521, 2531,
             2539, 2543, 2549, 2551, 2557, 2579, 2591, 2593, 2609, 2617,
             2621, 2633, 2647, 2657, 2659, 2663, 2671, 2677, 2683, 2687,
             2689, 2693, 2699, 2707, 2711, 2713, 2719, 2729, 2731, 2741,
             2749, 2753, 2767, 2777, 2789, 2791, 2797, 2801, 2803, 2819,
             2833, 2837, 2843, 2851, 2857, 2861, 2879, 2887, 2897, 2903,
             2909, 2917, 2927, 2939, 2953, 2957, 2963, 2969, 2971, 2999,
             3001, 3011, 3019, 3023, 3037, 3041, 3049, 3061, 3067, 3079,
             3083, 3089, 3109, 3119, 3121, 3137, 3163, 3167, 3169, 3181,
             3187, 3191, 3203, 3209, 3217, 3221, 3229, 3251, 3253, 3257,
             3259, 3271, 3299, 3301, 3307, 3313, 3319, 3323, 3329, 3331,
             3343, 3347, 3359, 3361, 3371, 3373, 3389, 3391, 3407, 3413,
             3433, 3449, 3457, 3461, 3463, 3467, 3469, 3491, 3499, 3511,
             3517, 3527, 3529, 3533, 3539, 3541, 3547, 3557, 3559, 3571,
             3581, 3583, 3593, 3607, 3613, 3617, 3623, 3631, 3637, 3643,
             3659, 3671, 3673, 3677, 3691, 3697, 3701, 3709, 3719, 3727,
             3733, 3739, 3761, 3767, 3769, 3779, 3793, 3797, 3803, 3821,
             3823, 3833, 3847, 3851, 3853, 3863, 3877, 3881, 3889, 3907,
             3911, 3917, 3919, 3923, 3929, 3931, 3943, 3947, 3967, 3989,
             4001, 4003, 4007, 4013, 4019, 4021, 4027, 4049, 4051, 4057,
             4073, 4079, 4091, 4093, 4099, 4111, 4127, 4129, 4133, 4139,
             4153, 4157, 4159, 4177, 4201, 4211, 4217, 4219, 4229, 4231,
             4241, 4243, 4253, 4259, 4261, 4271, 4273, 4283, 4289, 4297,
             4327, 4337, 4339, 4349, 4357, 4363, 4373, 4391, 4397, 4409,
             4421, 4423, 4441, 4447, 4451, 4457, 4463, 4481, 4483, 4493,
             4507, 4513, 4517, 4519, 4523, 4547, 4549, 4561, 4567, 4583,
             4591, 4597, 4603, 4621, 4637, 4639, 4643, 4649, 4651, 4657,
             4663, 4673, 4679, 4691, 4703, 4721, 4723, 4729, 4733, 4751,
             4759, 4783, 4787, 4789, 4793, 4799, 4801, 4813, 4817, 4831,
             4861, 4871, 4877, 4889, 4903, 4909, 4919, 4931, 4933, 4937,
             4943, 4951, 4957, 4967, 4969, 4973, 4987, 4993, 4999, 5003,
             5009, 5011, 5021, 5023, 5039, 5051, 5059, 5077, 5081, 5087,
             5099, 5101, 5107, 5113, 5119, 5147, 5153, 5167, 5171, 5179,
             5189, 5197, 5209, 5227, 5231, 5233, 5237, 5261, 5273, 5279,
             5281, 5297, 5303, 5309, 5323, 5333, 5347, 5351, 5381, 5387,
             5393, 5399, 5407, 5413, 5417, 5419, 5431, 5437, 5441, 5443,
             5449, 5471, 5477, 5479, 5483, 5501, 5503, 5507, 5519, 5521,
             5527, 5531, 5557, 5563, 5569, 5573, 5581, 5591, 5623, 5639,
             5641, 5647, 5651, 5653, 5657, 5659, 5669, 5683, 5689, 5693,
             5701, 5711, 5717, 5737, 5741, 5743, 5749, 5779, 5783, 5791,
             5801, 5807, 5813, 5821, 5827, 5839, 5843, 5849, 5851, 5857,
             5861, 5867, 5869, 5879, 5881, 5897, 5903, 5923, 5927, 5939,
             5953, 5981, 5987, 6007, 6011, 6029, 6037, 6043, 6047, 6053,
             6067, 6073, 6079, 6089, 6091, 6101, 6113, 6121, 6131, 6133,
             6143, 6151, 6163, 6173, 6197, 6199, 6203, 6211, 6217, 6221,
             6229, 6247, 6257, 6263, 6269, 6271, 6277, 6287, 6299, 6301,
             6311, 6317, 6323, 6329, 6337, 6343, 6353, 6359, 6361, 6367,
             6373, 6379, 6389, 6397, 6421, 6427, 6449, 6451, 6469, 6473,
             6481, 6491, 6521, 6529, 6547, 6551, 6553, 6563, 6569, 6571,
             6577, 6581, 6599, 6607, 6619, 6637, 6653, 6659, 6661, 6673,
             6679, 6689, 6691, 6701, 6703, 6709, 6719, 6733, 6737, 6761,
             6763, 6779, 6781, 6791, 6793, 6803, 6823, 6827, 6829, 6833,
             6841, 6857, 6863, 6869, 6871, 6883, 6899, 6907, 6911, 6917,
             6947, 6949, 6959, 6961, 6967, 6971, 6977, 6983, 6991, 6997,
             7001, 7013, 7019, 7027, 7039, 7043, 7057, 7069, 7079, 7103,
             7109, 7121, 7127, 7129, 7151, 7159, 7177, 7187, 7193, 7207,
             7211, 7213, 7219, 7229, 7237, 7243, 7247, 7253, 7283, 7297,
             7307, 7309, 7321, 7331, 7333, 7349, 7351, 7369, 7393, 7411,
             7417, 7433, 7451, 7457, 7459, 7477, 7481, 7487, 7489, 7499,
             7507, 7517, 7523, 7529, 7537, 7541, 7547, 7549, 7559, 7561,
             7573, 7577, 7583, 7589, 7591, 7603, 7607, 7621, 7639, 7643,
             7649, 7669, 7673, 7681, 7687, 7691, 7699, 7703, 7717, 7723,
             7727, 7741, 7753, 7757, 7759, 7789, 7793, 7817, 7823, 7829,
             7841, 7853, 7867, 7873, 7877, 7879, 7883, 7901, 7907, 7919,
             7927, 7933, 7937, 7949, 7951, 7963, 7993, 8009, 8011, 8017,
             8039, 8053, 8059, 8069, 8081, 8087, 8089, 8093, 8101, 8111,
             8117, 8123, 8147, 8161, 8167, 8171, 8179, 8191, 8209, 8219,
             8221, 8231, 8233, 8237, 8243, 8263, 8269, 8273, 8287, 8291,
             8293, 8297, 8311, 8317, 8329, 8353, 8363, 8369, 8377, 8387,
             8389, 8419, 8423, 8429, 8431, 8443, 8447, 8461, 8467, 8501,
             8513, 8521, 8527, 8537, 8539, 8543, 8563, 8573, 8581, 8597,
             8599, 8609, 8623, 8627, 8629, 8641, 8647, 8663, 8669, 8677,
             8681, 8689, 8693, 8699, 8707, 8713, 8719, 8731, 8737, 8741,
             8747, 8753, 8761, 8779, 8783, 8803, 8807, 8819, 8821, 8831,
             8837, 8839, 8849, 8861, 8863, 8867, 8887, 8893, 8923, 8929,
             8933, 8941, 8951, 8963, 8969, 8971, 8999, 9001, 9007, 9011,
             9013, 9029, 9041, 9043, 9049, 9059, 9067, 9091, 9103, 9109,
             9127, 9133, 9137, 9151, 9157, 9161, 9173, 9181, 9187, 9199,
             9203, 9209, 9221, 9227, 9239, 9241, 9257, 9277, 9281, 9283,
             9293, 9311, 9319, 9323, 9337, 9341, 9343, 9349, 9371, 9377,
             9391, 9397, 9403, 9413, 9419, 9421, 9431, 9433, 9437, 9439,
             9461, 9463, 9467, 9473, 9479, 9491, 9497, 9511, 9521, 9533,
             9539, 9547, 9551, 9587, 9601, 9613, 9619, 9623, 9629, 9631,
             9643, 9649, 9661, 9677, 9679, 9689, 9697, 9719, 9721, 9733,
             9739, 9743, 9749, 9767, 9769, 9781, 9787, 9791, 9803, 9811,
             9817, 9829, 9833, 9839, 9851, 9857, 9859, 9871, 9883, 9887,
             9901, 9907, 9923, 9929, 9931, 9941, 9949, 9967, 9973, 10007,
             10009, 10037, 10039, 10061, 10067, 10069, 10079, 10091, 10093, 10099,
             10103, 10111, 10133, 10139, 10141, 10151, 10159, 10163, 10169, 10177,
             10181, 10193, 10211, 10223, 10243, 10247, 10253, 10259, 10267, 10271,
             10273, 10289, 10301, 10303, 10313, 10321, 10331, 10333, 10337, 10343,
             10357, 10369, 10391, 10399, 10427, 10429, 10433, 10453, 10457, 10459,
             10463, 10477, 10487, 10499, 10501, 10513, 10529, 10531, 10559, 10567,
             10589, 10597, 10601, 10607, 10613, 10627, 10631, 10639, 10651, 10657,
             10663, 10667, 10687, 10691, 10709, 10711, 10723, 10729, 10733, 10739,
             10753, 10771, 10781, 10789, 10799, 10831, 10837, 10847, 10853, 10859,
             10861, 10867, 10883, 10889, 10891, 10903, 10909, 10937, 10939, 10949,
             10957, 10973, 10979, 10987, 10993, 11003, 11027, 11047, 11057, 11059,
             11069, 11071, 11083, 11087, 11093, 11113, 11117, 11119, 11131, 11149,
             11159, 11161, 11171, 11173, 11177, 11197, 11213, 11239, 11243, 11251,
             11257, 11261, 11273, 11279, 11287, 11299, 11311, 11317, 11321, 11329,
             11351, 11353, 11369, 11383, 11393, 11399, 11411, 11423, 11437, 11443,
             11447, 11467, 11471, 11483, 11489, 11491, 11497, 11503, 11519, 11527,
             11549, 11551, 11579, 11587, 11593, 11597, 11617, 11621, 11633, 11657,
             11677, 11681, 11689, 11699, 11701, 11717, 11719, 11731, 11743, 11777,
             11779, 11783, 11789, 11801, 11807, 11813, 11821, 11827, 11831, 11833,
             11839, 11863, 11867, 11887, 11897, 11903, 11909, 11923, 11927, 11933,
             11939, 11941, 11953, 11959, 11969, 11971, 11981, 11987, 12007, 12011,
             12037, 12041, 12043, 12049, 12071, 12073, 12097, 12101, 12107, 12109,
             12113, 12119, 12143, 12149, 12157, 12161, 12163, 12197, 12203, 12211,
             12227, 12239, 12241, 12251, 12253, 12263, 12269, 12277, 12281, 12289,
             12301, 12323, 12329, 12343, 12347, 12373, 12377, 12379, 12391, 12401,
             12409, 12413, 12421, 12433, 12437, 12451, 12457, 12473, 12479, 12487,
             12491, 12497, 12503, 12511, 12517, 12527, 12539, 12541, 12547, 12553,
             12569, 12577, 12583, 12589, 12601, 12611, 12613, 12619, 12637, 12641,
             12647, 12653, 12659, 12671, 12689, 12697, 12703, 12713, 12721, 12739,
             12743, 12757, 12763, 12781, 12791, 12799, 12809, 12821, 12823, 12829,
             12841, 12853, 12889, 12893, 12899, 12907, 12911, 12917, 12919, 12923,
             12941, 12953, 12959, 12967, 12973, 12979, 12983, 13001, 13003, 13007,
             13009, 13033, 13037, 13043, 13049, 13063, 13093, 13099, 13103, 13109,
             13121, 13127, 13147, 13151, 13159, 13163, 13171, 13177, 13183, 13187,
             13217, 13219, 13229, 13241, 13249, 13259, 13267, 13291, 13297, 13309,
             13313, 13327, 13331, 13337, 13339, 13367, 13381, 13397, 13399, 13411,
             13417, 13421, 13441, 13451, 13457, 13463, 13469, 13477, 13487, 13499};

        if (n > prime_max)
        {
            throw new System.ArgumentException("Exceeds max_prime");
        }
        else
        {
            return prime_vector[n];
        }
    }


    static List<float> fill_array(int m, int k)
    {
        List<float> a = new List<float>(m);
        for (int i = 0; i < m; i++)
        {
            a.Add(k);
        }
        return a;
    }

    //TODO: Laura made pubic for testing, shoudl be changed back
    public static List<float> Halton(int i, int m)
    {
        List<float> t = fill_array(m, i);
        List<float> prime_inv = fill_array(m, 0);

        for (int j = 0; j < m; j++)
        {
            prime_inv[j] = 1f / (float)Prime(j);
        }

        List<float> r = fill_array(m, 0);

        while (0 < t.Sum())
        {
            for (int j = 0; j < m; j++)
            {
                float d = (t[j] % Prime(j));
                r[j] = (float)(r[j] + (float)d * prime_inv[j]);
                prime_inv[j] = prime_inv[j] / Prime(j);
                t[j] = (int)(t[j] / Prime(j));
            }
        }
        return r;
    }


    static List<float> Halton_Base(int i, int m, float[] b)
    {
        List<float> t = fill_array(m, i);
        List<float> b_inv = fill_array(m, 0);

        for (int j = 0; j < m; j++)
        {
            b_inv[j] = 1f / (float)b[j];
        }

        List<float> r = fill_array(m, 0);

        while (0 < t.Sum())
        {
            for (int j = 0; j < m; j++)
            {
                float d = (t[j] % b[j]);
                r[j] = (float)(r[j] + (float)d * b_inv[j]);
                b_inv[j] = b_inv[j] / b[j];
                t[j] = (int)(t[j] / b[j]);
            }
        }
        return r;
    }

    #endregion
}


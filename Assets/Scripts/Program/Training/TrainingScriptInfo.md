# Training Scripts

We use NVIDIA's car behavioral cloning framework to train our ego car.
This folder has scripts for getting reproducible results on the trained ego car model.
Training and Validation are both done via a variation of https://github.com/naokishibuya/car-behavioral-cloning

 

﻿/*
 * This training script is for training the ego car to drive around in a loop
 * (i.e., always turn right at an intersection)
 * 
 *  */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Training4 : EnvironmentProgramBaseClass {

    //Add the ProDM vars here
    VarInterval<int> cr_length;
    int cr_numLanes;
    VarInterval<float> lightIntensity;
    ProDMColor color;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;


	void Start() //needs to be called in the start because the setup in the RoadElementVisualization happens in the Awake()
	{
		//PlaceHolderForVisualizer.createPlayButton();
		//PlaceHolderForVisualizer.createRecordButton();

		fogDensity = new VarInterval<float>(val: 0, min: 0, max: 0.3f, desc:"FOG_DENSITY");
		//PlaceHolderForVisualizer.Visualize(nameof(fogDensity), fogDensity, false);

		lightIntensity = new VarInterval<float>(val: 1, min: 0, max: 10);
		//PlaceHolderForVisualizer.Visualize(nameof(lightIntensity), lightIntensity);

		color = new ProDMColor(new VarInterval<float>(val: 1f, min: 0f, max: 1f), new VarInterval<float>(val: 0.9f, min: 0f, max: 1f),
			new VarInterval<float>(val: 0.8f, min: 0f, max: 1f), 1f);
		//PlaceHolderForVisualizer.Visualize(nameof(color), color);

		ambientIntensity = new VarInterval<float>(val: 2f, min: 0f, max: 2.5f, desc: "AMBIENT_LIGHT_INTENSITY");
		//PlaceHolderForVisualizer.Visualize(nameof(ambientIntensity), ambientIntensity, false);

		//define the ProDM variables
		cr_length = new VarInterval<int>(val: 3, changeable: true, min: 3, max: 25);
		//PlaceHolderForVisualizer.Visualize(nameof(cr_length), cr_length);

		List<int> numLanesOptions = new List<int> { 2, 4, 6 };

		cr_numLanes = new VarEnum<int>(numLanesOptions, 1);
		//PlaceHolderForVisualizer.Visualize(nameof(cr_numLanes), cr_numLanes);

		Test ();
	}

	//create the visualization of the road network
	//NOTE: The code to make the road network should go here.
	public override void MakeView()
	{
		Intersection tInt1 = new TIntersection(cr_numLanes);
		tInt1._zone3 = Zoning.TREES;
		tInt1._zone1 = Zoning.TREES;
		tInt1._zone2 = Zoning.TREES;
		PlaceHolderForVisualizer.Visualize(tInt1);
		RoadSegment cRoad1 = new RoadSegment(cr_length, cr_numLanes, nameof(cRoad1), Orientation.HORIZONTAL_FLIPPED);
		cRoad1._zone1 = Zoning.TREES;
		cRoad1._zone2 = Zoning.TREES;
		PlaceHolderForVisualizer.Visualize(cRoad1);

		//Connect the encapsulated object to T-Interesection
		Dictionary<PhysicalConnection, PhysicalConnection> connMap1 = new Dictionary<PhysicalConnection, PhysicalConnection>();
		connMap1.Add(cRoad1._connections.NextFree(), tInt1._connections.NextFree());

		RoadElement connectedElement1 = cRoad1.ConnectTo(tInt1, connMap1);
		//vizualize
		PlaceHolderForVisualizer.Visualize(connectedElement1);
		PlaceHolderForVisualizer.connectRoadElements(connMap1, cRoad1, connectedElement1, tInt1, cRoad1);


		//Make some more Road Elements
		Intersection tInt2 = new TIntersection(cr_numLanes);
		tInt2._zone3 = Zoning.TREES;
		tInt2._zone1 = Zoning.TREES;
		tInt2._zone2 = Zoning.TREES;
		PlaceHolderForVisualizer.Visualize(tInt2);
		RoadSegment cRoad2 = new RoadSegment(cr_length, cr_numLanes);
		cRoad2._zone1 = Zoning.TREES;
		cRoad2._zone2 = Zoning.TREES;
		PlaceHolderForVisualizer.Visualize(cRoad2);

		//Connect the encapsulated object to T-Interesection
		Dictionary<PhysicalConnection, PhysicalConnection> connMap2 = new Dictionary<PhysicalConnection, PhysicalConnection>();
		connMap2.Add(cRoad2._connections.NextFree(), tInt2._connections.NextFree());

		RoadElement connectedElement2 = cRoad2.ConnectTo(tInt2, connMap2);

		//vizualize
		PlaceHolderForVisualizer.Visualize(connectedElement2);
		PlaceHolderForVisualizer.connectRoadElements(connMap2, cRoad2, connectedElement2, tInt2, cRoad2, true);

		//connect the two connectedElements to make half of the loop.
		Dictionary<PhysicalConnection, PhysicalConnection> connMapLoop1 = new Dictionary<PhysicalConnection, PhysicalConnection>();
		connMapLoop1.Add(cRoad1._connections.NextFree(), tInt2._connections.NextFree());
		RoadElement loopSegment1 = connectedElement1.ConnectTo(connectedElement2, connMapLoop1);
		//visualize
		PlaceHolderForVisualizer.Visualize(loopSegment1);

		PlaceHolderForVisualizer.connectRoadElements(connMapLoop1, connectedElement1, loopSegment1, connectedElement2, connectedElement1);


		//Make some more Road Elements
		Intersection tInt3 = new TIntersection(cr_numLanes, zone1:Zoning.TREES);
		tInt3._zone3 = Zoning.TREES;
		tInt3._zone1 = Zoning.TREES;
		tInt3._zone2 = Zoning.TREES;
		PlaceHolderForVisualizer.Visualize(tInt3);
		RoadSegment cRoad3 = new RoadSegment(cr_length, cr_numLanes, nameof(cRoad3), zone1: Zoning.TREES, zone2: Zoning.TREES);
		cRoad3._zone1 = Zoning.TREES;
		cRoad3._zone2 = Zoning.TREES;
		PlaceHolderForVisualizer.Visualize(cRoad3);

		//Connect the encapsulated object to T-Interesection
		Dictionary<PhysicalConnection, PhysicalConnection> connMap3 = new Dictionary<PhysicalConnection, PhysicalConnection>();
		connMap3.Add(cRoad3._connections.NextFree(), tInt3._connections.NextFree());

		RoadElement connectedElement3 = cRoad3.ConnectTo(tInt3, connMap3);
		//vizualize
		PlaceHolderForVisualizer.Visualize(connectedElement3);
		PlaceHolderForVisualizer.connectRoadElements(connMap3, cRoad3, connectedElement3, tInt3, cRoad3, true);


		//Make some more Road Elements
		Intersection tInt4 = new TIntersection(cr_numLanes, zone1:Zoning.TREES);
		tInt4._zone3 = Zoning.TREES;
		tInt4._zone1 = Zoning.TREES;
		tInt4._zone2 = Zoning.TREES;
		PlaceHolderForVisualizer.Visualize(tInt4);
		RoadSegment cRoad4 = new RoadSegment(cr_length, cr_numLanes);
		cRoad4._zone1 = Zoning.TREES;
		cRoad4._zone2 = Zoning.TREES;
		PlaceHolderForVisualizer.Visualize(cRoad4);

		//Connect the encapsulated object to T-Interesection
		Dictionary<PhysicalConnection, PhysicalConnection> connMap4 = new Dictionary<PhysicalConnection, PhysicalConnection>();
		connMap4.Add(cRoad4._connections.NextFree(), tInt4._connections.NextFree());

		RoadElement connectedElement4 = cRoad4.ConnectTo(tInt4, connMap4);
		//vizualize
		PlaceHolderForVisualizer.Visualize(connectedElement4);
		PlaceHolderForVisualizer.connectRoadElements(connMap4, cRoad4, connectedElement4, tInt4, cRoad4, true);

		//connect the two connectedElements to make half of the loop.
		Dictionary<PhysicalConnection, PhysicalConnection> connMapLoop2 = new Dictionary<PhysicalConnection, PhysicalConnection>();
		connMapLoop2.Add(cRoad3._connections.NextFree(), tInt4._connections.NextFree());
		RoadElement loopSegment2 = connectedElement3.ConnectTo(connectedElement4, connMapLoop2);
		//visualize
		PlaceHolderForVisualizer.Visualize(loopSegment2);

		PlaceHolderForVisualizer.connectRoadElements(connMapLoop2, connectedElement3, loopSegment2, connectedElement4, connectedElement3);


		//connect the loop segments to make the loop.
		Dictionary<PhysicalConnection, PhysicalConnection> connMapLoopFinal = new Dictionary<PhysicalConnection, PhysicalConnection>();
		connMapLoopFinal.Add(cRoad2._connections.NextFree(), tInt3._connections.NextFree());
		connMapLoopFinal.Add(tInt1._connections.NextFree(), cRoad4._connections.NextFree());
		RoadElement loopFinal = loopSegment1.ConnectTo(loopSegment2, connMapLoopFinal);
		//visualize
		PlaceHolderForVisualizer.Visualize(loopFinal);

		PlaceHolderForVisualizer.connectRoadElements(connMapLoopFinal, loopSegment1, loopFinal, loopSegment2, loopSegment1);


		AutonomousVehicle autonomouscar_training = new AutonomousVehicle(nameof(autonomouscar_training), cRoad4, 0f, LANE.MINUS_TWO, monitor: "CollisionMonitor"
            , type: TestActorType.EGO_VEHICLE_AUTONOMOUS);
		PlaceHolderForVisualizer.Visualize(autonomouscar_training);

		//AutonomousVehicle autonomouscar = new AutonomousVehicle(nameof(autonomouscar), cRoad4, 0.9f, LANE.PLUS_ONE, type: TestActorType.EGO_VEHICLE_AUTONOMOUS);
		//PlaceHolderForVisualizer.Visualize(autonomouscar);


	}


	public override void MakeEnvironment()
	{
		//Environment settings
		LightSettings light = new LightSettings(intensity: lightIntensity, direction: Vector3.down, color: color, ambientIntensity: ambientIntensity);
		FogSettings fog = new FogSettings(density: fogDensity);

		EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }
}

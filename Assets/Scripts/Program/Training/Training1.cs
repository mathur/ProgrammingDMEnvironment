﻿/*
 * This is the trainig script for an autonomous car with fixed ambient lighting and fog.
 * We use a 2-lane road.
 * Recording is usually started when ego car is close enough to the AI car.
 * The ego car  needs to follow the lead (AI) car.
	*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Training1 : EnvironmentProgramBaseClass {

    //Add the ProDM vars here
    VarInterval<int> cr_length;
    VarEnum<int> cr_numLanes;
    VarInterval<float> lightIntensity;
    ProDMColor color;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;


    void Start() //needs to be called in the start because the setup in the RoadElementVisualization happens in the Awake()
    {
        //This button is useful to have everything set before driving begins
		PlaceHolderForVisualizer.createPlayButton();
        //PlaceHolderForVisualizer.createRecordButton();

        fogDensity = new VarInterval<float>(val: 0, min: 0, max: 0.3f, desc:"FOG_DENSITY");
        PlaceHolderForVisualizer.Visualize(nameof(fogDensity), fogDensity, false);

        lightIntensity = new VarInterval<float>(val: 1, min: 0, max: 10);
        //PlaceHolderForVisualizer.Visualize(nameof(lightIntensity), lightIntensity);

        color = new ProDMColor(new VarInterval<float>(val: 1f, min: 0f, max: 1f), new VarInterval<float>(val: 0.9f, min: 0f, max: 1f),
            new VarInterval<float>(val: 0.8f, min: 0f, max: 1f), 1f);
        //PlaceHolderForVisualizer.Visualize(nameof(color), color);

        ambientIntensity = new VarInterval<float>(val: 1.5f, min: 0f, max: 2.5f, desc: "AMBIENT_LIGHT_INTENSITY");
        PlaceHolderForVisualizer.Visualize(nameof(ambientIntensity), ambientIntensity, false);

        //define the ProDM variables
        cr_length = new VarInterval<int>(val: 100, changeable: true, min: 20, max: 4000);
        PlaceHolderForVisualizer.Visualize(nameof(cr_length), cr_length);

        List<int> numLanesOptions = new List<int> { 2, 4 };

        cr_numLanes = new VarEnum<int>(numLanesOptions, 0);
        PlaceHolderForVisualizer.Visualize(nameof(cr_numLanes), cr_numLanes);


        //No iterations----Nothing fancy
        //Test ();
        List<VarInterval<float>> invariants = new List<VarInterval<float>>();
        invariants.Add(fogDensity);
        Train(invariants);
    }

    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {

        RoadSegment cRoad1 = new RoadSegment(cr_length, cr_numLanes);
        cRoad1._zone1 = Zoning.TREES;
        cRoad1._zone2 = Zoning.TREES;
        PlaceHolderForVisualizer.Visualize(cRoad1);

        //add a car
        AIVehicle car = new AIVehicle(nameof(car), cRoad1, 0.13f, LANE.PLUS_ONE, bodyColor: Color.red);
        PlaceHolderForVisualizer.Visualize(car);

        //add a car
        //AIVehicle car1 = new AIVehicle(nameof(car1), cRoad1, 0.15f, LANE.PLUS_ONE);
        //PlaceHolderForVisualizer.Visualize(car1);

        //AutonomousVehicle autonomouscar_training = new AutonomousVehicle(nameof(autonomouscar_training), cRoad1, 0.1f, LANE.MINUS_ONE, type: TestActorType.EGO_VEHICLE_TRAINING);
        //PlaceHolderForVisualizer.Visualize(autonomouscar_training);

		AutonomousVehicle autonomouscar = new AutonomousVehicle(nameof(autonomouscar), cRoad1, 0.11f, LANE.PLUS_ONE, monitor: "CollisionMonitor", type: TestActorType.EGO_VEHICLE_TRAINING);
        PlaceHolderForVisualizer.Visualize(autonomouscar);


    }


    public override void MakeEnvironment()
    {
        //Environment settings
        LightSettings light = new LightSettings(intensity: lightIntensity, direction: Vector3.down, color: color, ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StraightRoad : EnvironmentProgramBaseClass
{

    //Add the ProDM vars here
    VarInterval<int> length;
    VarEnum<int> numLanes;

    //Environment variables
    VarInterval<float> lightIntensity;
    ProDMColor color;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;

    void Start() //needs to be called in the start because the setup in the RoadElementVisualization happens in the Awake()
    {
        
        fogDensity = new VarInterval<float>(val: 0, min: 0, max: 0.3f, desc: "FOG_DENSITY");
        lightIntensity = new VarInterval<float>(val: 1, min: 0, max: 10);
        color = new ProDMColor(new VarInterval<float>(val: 1f, min: 0f, max: 1f), new VarInterval<float>(val: 0.9f, min: 0f, max: 1f),
            new VarInterval<float>(val: 0.8f, min: 0f, max: 1f), 1f);
        ambientIntensity = new VarInterval<float>(val: 1.5f, min: 0f, max: 2.5f, desc: "AMBIENT_LIGHT_INTENSITY");

        //define the ProDM variables
        length = new VarInterval<int>(val: 100, changeable: true, min: 20, max: 4000);

        List<int> numLanesOptions = new List<int> { 2, 4, 6 };
        numLanes = new VarEnum<int>(numLanesOptions, 0);


        Test();
    }

    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {
        //create list of items to be visualized later
        List<object> list = new List<object>();

        //create a straight road segment
        RoadSegment cRoad1 = new RoadSegment(length, numLanes, nameof(cRoad1), Orientation.VERTICAL, zone1: Zoning.TREES, zone2: Zoning.TREES);
        list.Add(cRoad1);

        //place autonomous car on the road segment, and attach a collision monior
        AutonomousVehicle autonomouscar = new AutonomousVehicle(nameof(autonomouscar), cRoad1, 0.11f, LANE.PLUS_ONE, type: TestActorType.EGO_VEHICLE_AUTONOMOUS, monitor: "CollisionMonitor");
        list.Add(autonomouscar);

        //visualize the scene
        PlaceHolderForVisualizer.Visualize(list);

    }


    public override void MakeEnvironment()
    {
        //Environment settings
        LightSettings light = new LightSettings(intensity: lightIntensity, direction: Vector3.down, color: color, ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }
}

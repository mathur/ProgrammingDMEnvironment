﻿//Use model-029.h5 for test

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviorMonitorTest : EnvironmentProgramBaseClass
{

    //Add the ProDM vars here
    VarInterval<int> cr_length;
    VarEnum<int> cr_numLanes;
    VarInterval<float> lightIntensity;
    ProDMColor color;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;
    //int cr_numLanes = 4;


    void Start() //needs to be called in the start because the setup in the RoadElementVisualization happens in the Awake()
    {
        fogDensity = new VarInterval<float>(val: 0, min: 0, max: 0.3f, desc: "FOG_DENSITY");

        lightIntensity = new VarInterval<float>(val: 1, min: 0, max: 10);

        color = new ProDMColor(new VarInterval<float>(val: 1f, min: 0f, max: 1f), new VarInterval<float>(val: 0.9f, min: 0f, max: 1f),
            new VarInterval<float>(val: 0.8f, min: 0f, max: 1f), 1f);

        ambientIntensity = new VarInterval<float>(val: 1.5f, min: 0f, max: 2.5f, desc: "AMBIENT_LIGHT_INTENSITY");

        //define the ProDM variables
        cr_length = new VarInterval<int>(val: 100, changeable: true, min: 20, max: 4000);

        List<int> numLanesOptions = new List<int> { 2, 4 };

        cr_numLanes = new VarEnum<int>(numLanesOptions, 0);

        MakeView();
        MakeEnvironment();
    }

    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {
        RoadSegment cRoad1 = new RoadSegment(50, 6, nameof(cRoad1), Orientation.VERTICAL);
        PlaceHolderForVisualizer.Visualize(cRoad1);

        AutonomousVehicle Ego = new AutonomousVehicle(nameof(Ego), cRoad1, 0.15f, LANE.PLUS_ONE, type: TestActorType.EGO_VEHICLE_AUTONOMOUS);
        PlaceHolderForVisualizer.Visualize(Ego);

        AIVehicle A1 = new AIVehicle(nameof(A1), cRoad1, 0.2f, LANE.PLUS_ONE, Color.black, behavior: "LaneChangeBehavior", monitor: "CollisionMonitor");
        AIVehicle A2 = new AIVehicle(nameof(A2), cRoad1, 0.3f, LANE.PLUS_ONE, Color.black);
        AIVehicle A3 = new AIVehicle(nameof(A3), cRoad1, 0.4f, LANE.PLUS_ONE, Color.black);
        AIVehicle A4 = new AIVehicle(nameof(A4), cRoad1, 0.5f, LANE.PLUS_ONE, Color.black);
        AIVehicle A5 = new AIVehicle(nameof(A5), cRoad1, 0.6f, LANE.PLUS_ONE, Color.black);
        AIVehicle A6 = new AIVehicle(nameof(A6), cRoad1, 0.7f, LANE.PLUS_ONE, Color.black);
        PlaceHolderForVisualizer.Visualize(new List<object> { A1, A2, A3, A4, A5, A6 });

        Pedestrian pedestrian = new Pedestrian(nameof(pedestrian), cRoad1, 0.2f, actorOfInterest: Ego, euclideanDistance: 10f, walkingSpeed: 5f);
        pedestrian._behavior = "PedestrianCrossingBehavior";
        PlaceHolderForVisualizer.Visualize(pedestrian);

    }


    public override void MakeEnvironment()
    {
        //Environment settings
        LightSettings light = new LightSettings(intensity: lightIntensity, direction: Vector3.down, color: color, ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }
}

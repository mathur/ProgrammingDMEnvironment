﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadSegmentation2 : EnvironmentProgramBaseClass
{
    //Add the ProDM vars here
    VarInterval<int> length;
    VarEnum<int> numLanes;
    VarInterval<float> lightIntensity;
    ProDMColor color;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;
    //int cr_numLanes = 4;

    // Parameterize using two ProDMFloats
    VarInterval<float> walkingSpeed;
    VarInterval<float> euclideanDist;

    void Start() //needs to be called in the start because the setup in the RoadElementVisualization happens in the Awake()
    {
        //This button is useful to have everything set before driving begins
        //PlaceHolderForVisualizer.createPlayButton();
        //PlaceHolderForVisualizer.createRecordButton();

        fogDensity = new VarInterval<float>(val: 0, min: 0, max: 0.3f, desc: "FOG_DENSITY");


        lightIntensity = new VarInterval<float>(val: 1, min: 0, max: 10);
        color = new ProDMColor(new VarInterval<float>(val: 1f, min: 0f, max: 1f), new VarInterval<float>(val: 0.78f, min: 0f, max: 1f),
            new VarInterval<float>(val: 0.13f, min: 0f, max: 1f), 1f);
        //PlaceHolderForVisualizer.Visualize(nameof(color), color);

        ambientIntensity = new VarInterval<float>(val: 1.5f, min: 0f, max: 2.5f, desc: "AMBIENT_LIGHT_INTENSITY");

        //define the ProDM variables
        length = new VarInterval<int>(val: 7, changeable: true, min: 5, max: 4000);

        List<int> numLanesOptions = new List<int> { 2, 4 };

        numLanes = new VarEnum<int>(numLanesOptions, 0);

        walkingSpeed = new VarInterval<float>(val: 4f, min: 1f, max: 10f);
        euclideanDist = new VarInterval<float>(val: 40f, min: 20f, max: 60f);

        List<ProDM> rangedFloats = new List<ProDM>();
        rangedFloats.Add(walkingSpeed);
        rangedFloats.Add(euclideanDist);

        Test();
    }

    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {
        // make the T-intersection
        TIntersection int1 = new TIntersection(numLanes: numLanes, zone1: Zoning.TREES, zone2: Zoning.TREES, zone3:Zoning.RESIDENTIAL_ZONE);
        PlaceHolderForVisualizer.Visualize(int1);

        // make straight roads to connect to the Intersection
        RoadSegment cRoad1 = new RoadSegment(lengthOfRoad: length, numLanes: numLanes, zone1:Zoning.RESIDENTIAL_ZONE, zone2:Zoning.TREES);
        //RoadSegment cRoad2 = new RoadSegment(lengthOfRoad: length, numLanes: numLanes);
        RoadSegment cRoad3 = new RoadSegment(lengthOfRoad: length, numLanes: numLanes, zone1: Zoning.RESIDENTIAL_ZONE, zone2: Zoning.TREES);
        PlaceHolderForVisualizer.Visualize(cRoad1);
        //PlaceHolderForVisualizer.Visualize(cRoad2);
        PlaceHolderForVisualizer.Visualize(cRoad3);

        // add straight roads to a list
        List<RoadElement> cRoadList = new List<RoadElement>
        {
            cRoad1,
            cRoad3
        };

        // make an encapsulating object of straight roads
        RoadElement cRoads = new RoadElement(cRoadList);
        PlaceHolderForVisualizer.Visualize(cRoads);

        // Connect the encapsulated object to T-Interesection
        // first specify the connection mapping
        Dictionary<PhysicalConnection, PhysicalConnection> connMap1 = new Dictionary<PhysicalConnection, PhysicalConnection>
        {
            { cRoad1._connections.AtIndex(0), int1._connections.AtIndex(0) },
            { cRoad3._connections.AtIndex(1), int1._connections.AtIndex(2) }
        };
        // then make the connection
        RoadElement finalconnection = cRoads.ConnectTo(r2: int1, connectionMap: connMap1);
        
        PlaceHolderForVisualizer.Visualize(finalconnection);

        PlaceHolderForVisualizer.connectRoadElements(connMap1, cRoads, finalconnection, int1, cRoads, true);


        AutonomousVehicle autonomouscar = new AutonomousVehicle(nameof(autonomouscar), cRoad1, 0.11f, LANE.PLUS_ONE, monitor: "CollisionMonitor", type: TestActorType.EGO_VEHICLE_TRAINING);
        PlaceHolderForVisualizer.Visualize(autonomouscar);

        AIVehicle car1 = new AIVehicle(nameof(car1), cRoad3, 0.15f, LANE.MINUS_ONE, Color.blue, model: "AICar2");
        PlaceHolderForVisualizer.Visualize(car1);

        AIVehicle car2 = new AIVehicle(nameof(car2), cRoad3, 0.36f, LANE.PLUS_ONE, Color.blue, model: "AICar4");
        PlaceHolderForVisualizer.Visualize(car2);

        AIVehicle car3 = new AIVehicle(nameof(car3), cRoad1, 0.3f, LANE.PLUS_ONE, Color.blue, model: "AICar2");
        PlaceHolderForVisualizer.Visualize(car3);

        AIVehicle car4 = new AIVehicle(nameof(car4), cRoad1, 0.7f, LANE.MINUS_ONE, Color.blue, model: "AICar1");
        PlaceHolderForVisualizer.Visualize(car4);

        AIVehicle car5 = new AIVehicle(nameof(car5), cRoad1, 0.5f, LANE.PLUS_ONE, Color.blue, model: "AICar4");
        PlaceHolderForVisualizer.Visualize(car5);

        AIVehicle car6 = new AIVehicle(nameof(car6), cRoad1, 0.5f, LANE.MINUS_ONE, Color.blue, model: "AICar3");
        PlaceHolderForVisualizer.Visualize(car6);

    }


    public override void MakeEnvironment()
    {
        //Environment settings
        ProDMVector3 lightDir = new ProDMVector3(45f, 45f, 45f);
        LightSettings light = new LightSettings(intensity: lightIntensity, direction: lightDir, color: color, ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadSegmentation4 : EnvironmentProgramBaseClass
{
    ///Define my ProDM vars here
    //These are initialized here, but the actual values are designated in the Start() function
    //so you need to check there to set the values.
    VarInterval<int> numCols;
    VarInterval<int> roadLength;

    //These are the variables to control the environment
    //They need to be defined here every time, but can be set to constant values in the Start()
    VarInterval<float> lightIntensity;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;
    ProDMColor color;

    int numLanes = 4;
    VarEnum<Color> oneOfInvariantColor = new VarEnum<Color>(new List<Color> { Color.red, Color.blue, Color.black });
    //This adds the visualization aspect to the datastructure.
    //In the future, it should just be behind-the-scenes and not necessary in this script

    // Parameterize using two ProDMFloats
    VarInterval<float> walkingSpeed;
    VarInterval<float> euclideanDist;

    //We need this saved so that we can connect 
    AutonomousVehicle autonomouscar;

    Color carColor;
    List<TestActor> actorsToUpdate = new List<TestActor>();

    void Start() //needs to be called in the start because the setup in the RoadElementVisualization happens in the Awake()
    {
        //add the play/pause button and a button to record the car path
        //PlaceHolderForVisualizer.createPlayButton();
        //initialize the ProDM and VarEnum variables
        //Do not visualize the ProDM or VarEnum variables if they are fixed values.
        fogDensity = new VarInterval<float>(val: 0, min: 0, max: 1);

        lightIntensity = new VarInterval<float>(val: 1, min: 0, max: 10);

        color = new ProDMColor(1f, 0.9f, 0.8f, 1f); //initialize the color var to a constant value instead of a variable range

        ambientIntensity = new VarInterval<float>(val: 1f, min: 0f, max: 2f);

        //initialize my ProDM and VarEnum vars here
        //THIS CONTROLS THE NUMBER OF INTERSECTIONS AND THE CONNECTING ROAD LENGTH
        numCols = new VarInterval<int>(val: 5, min: 1, max: 5);
        roadLength = new VarInterval<int>(val: 3, min: 1, max: 30);
        //PlaceHolderForVisualizer.Visualize(nameof(numCols), numCols);

        walkingSpeed = new VarInterval<float>(val: 4f, min: 1f, max: 10f);
        euclideanDist = new VarInterval<float>(val: 40f, min: 20f, max: 60f);

        //finish the setup, including the initial visualization of the scene and the environment settings
        Test(timeCutOff: 200f);
    }

    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {
        //we need this to connect the elements in the row
        RoadElement previousCol = new RoadSegment(1, 2, nameof(previousCol));
        PhysicalConnection previousRight = previousCol._connections.NextFree();

        for (int col = 0; col < numCols; col++)
        {
            Intersection int0 = new CrossIntersection(numLanes, nameof(int0) + col.ToString(), zone1: Zoning.TREES, zone2: Zoning.TREES, zone3: Zoning.TREES, zone4: Zoning.TREES);
            PlaceHolderForVisualizer.Visualize(int0);
            RoadSegment cRoad0 = new RoadSegment(roadLength, numLanes, nameof(cRoad0) + col.ToString(), zone1: Zoning.RESIDENTIAL_ZONE, zone2: Zoning.RESIDENTIAL_ZONE);
            PlaceHolderForVisualizer.Visualize(cRoad0);
            RoadSegment cRoad1 = new RoadSegment(roadLength, numLanes, nameof(cRoad1) + col.ToString(), zone1: Zoning.TREES, zone2: Zoning.TREES);
            PlaceHolderForVisualizer.Visualize(cRoad1);
            RoadSegment cRoad2 = new RoadSegment(roadLength, numLanes, nameof(cRoad2) + col.ToString(), zone1: Zoning.TREES, zone2: Zoning.TREES);
            PlaceHolderForVisualizer.Visualize(cRoad2);

            List<RoadElement> cRoadList0 = new List<RoadElement>();
            cRoadList0.Add(cRoad0);
            cRoadList0.Add(cRoad1);
            cRoadList0.Add(cRoad2);

            //Make an encapsulating object
            RoadElement cRoads0 = new RoadElement(cRoadList0, nameof(cRoads0) + col.ToString());
            PlaceHolderForVisualizer.Visualize(cRoads0);

            //Connect the encapsulated object to Interesection
            Dictionary<PhysicalConnection, PhysicalConnection> connMap0 = new Dictionary<PhysicalConnection, PhysicalConnection>();
            connMap0.Add(cRoad0._connections.AtIndex(1), int0._connections.AtIndex(0));
            connMap0.Add(cRoad1._connections.AtIndex(1), int0._connections.AtIndex(1));
            connMap0.Add(cRoad2._connections.AtIndex(1), int0._connections.AtIndex(3));

            RoadElement connectedElement0 = cRoads0.ConnectTo(int0, connMap0, nameof(connectedElement0) + col.ToString());
            PlaceHolderForVisualizer.Visualize(connectedElement0);
            PlaceHolderForVisualizer.connectRoadElements(connMap0, cRoads0, connectedElement0, int0, cRoads0, true);


            if (col != 0)
            {
                Dictionary<PhysicalConnection, PhysicalConnection> connMap = new Dictionary<PhysicalConnection, PhysicalConnection>();
                connMap.Add(int0._connections.AtIndex(2), previousRight);

                RoadElement colElement = connectedElement0.ConnectTo(previousCol, connMap, nameof(colElement) + col.ToString());
                PlaceHolderForVisualizer.Visualize(colElement);
                PlaceHolderForVisualizer.connectRoadElements(connMap, connectedElement0, colElement, previousCol, connectedElement0, true);

                //we need this to connect the elements in the row
                previousCol = colElement;
                previousRight = cRoad0._connections.AtIndex(0);
            }
            else
            {
                //we need this to connect the elements in the row
                previousCol = connectedElement0;
                previousRight = cRoad0._connections.AtIndex(0);
            }

            //USE THIS SECTION TO PLACE TEST ACTORS
            //Note: You need to do it here before the reference to the appropriate road segment is lost
            //Second note: The AutonomousVehicle is declared earlier on because if pedestrians are declared on different
            //road segments, they need to reference the AutonomousVehicle in their declaration

            oneOfInvariantColor.SetRandom();
            carColor = oneOfInvariantColor;

            if (col == 0)
            {
                autonomouscar = new AutonomousVehicle(nameof(autonomouscar), cRoad0, 0f, LANE.PLUS_TWO, monitor: "CollisionMonitor", type: TestActorType.EGO_VEHICLE_TRAINING);
                PlaceHolderForVisualizer.Visualize(autonomouscar);

                AIVehicle car1 = new AIVehicle(nameof(car1), cRoad0, 0.3f, LANE.PLUS_TWO, Color.blue, model: "AICar2");
                PlaceHolderForVisualizer.Visualize(car1);

                AIVehicle car2 = new AIVehicle(nameof(car2), cRoad0, 0.4f, LANE.PLUS_ONE, Color.blue, model: "AICar4");
                PlaceHolderForVisualizer.Visualize(car2);

                AIVehicle car3 = new AIVehicle(nameof(car3), cRoad1, 0.8f, LANE.PLUS_TWO, Color.blue, model: "AICar2");
                PlaceHolderForVisualizer.Visualize(car3);
                
                AIVehicle car5 = new AIVehicle(nameof(car5), cRoad0, 0.7f, LANE.PLUS_ONE, Color.green, model: "AICar");
                PlaceHolderForVisualizer.Visualize(car5);

                AIVehicle car6 = new AIVehicle(nameof(car6), cRoad0, 0.5f, LANE.MINUS_ONE, Color.blue, model: "AICar3");
                PlaceHolderForVisualizer.Visualize(car6);

                AIVehicle car7 = new AIVehicle(nameof(car7), cRoad1, 0.8f, LANE.PLUS_TWO, Color.blue, model: "AICar2");
                PlaceHolderForVisualizer.Visualize(car7);

                AIVehicle car8 = new AIVehicle(nameof(car8), cRoad0, 0.7f, LANE.PLUS_ONE, Color.blue, model: "AICar1");
                PlaceHolderForVisualizer.Visualize(car8);

                AIVehicle car9 = new AIVehicle(nameof(car9), cRoad0, 0.5f, LANE.PLUS_ONE, Color.blue, model: "AICar4");
                PlaceHolderForVisualizer.Visualize(car9);

                AIVehicle car10 = new AIVehicle(nameof(car10), cRoad0, 0.5f, LANE.MINUS_ONE, Color.blue, model: "AICar3");
                PlaceHolderForVisualizer.Visualize(car10);

            }

            if (col == 1)
            {
                AIVehicle car1 = new AIVehicle(nameof(car1) + col.ToString(), cRoad0, 0.15f, LANE.MINUS_TWO, Color.blue, model: "AICar5");
                PlaceHolderForVisualizer.Visualize(car1);

                AIVehicle car2 = new AIVehicle(nameof(car2) + col.ToString(), cRoad1, 0.36f, LANE.PLUS_ONE, Color.blue, model: "AICar4");
                PlaceHolderForVisualizer.Visualize(car2);

                AIVehicle car3 = new AIVehicle(nameof(car3) + col.ToString(), cRoad1, 0.8f, LANE.PLUS_TWO, Color.blue, model: "AICar2");
                PlaceHolderForVisualizer.Visualize(car3);

                AIVehicle car8 = new AIVehicle(nameof(car8) + col.ToString(), cRoad2, 0.7f, LANE.PLUS_TWO, Color.blue, model: "AICar1");
                PlaceHolderForVisualizer.Visualize(car8);

                AIVehicle car9 = new AIVehicle(nameof(car9) + col.ToString(), cRoad2, 0.5f, LANE.PLUS_ONE, Color.blue, model: "AICar4");
                PlaceHolderForVisualizer.Visualize(car9);

                AIVehicle car10 = new AIVehicle(nameof(car10) + col.ToString(), cRoad2, 0.5f, LANE.MINUS_ONE, Color.blue, model: "AICar3");
                PlaceHolderForVisualizer.Visualize(car10);


            }

        }
    }


    public override void MakeEnvironment()
    {
        //Environment settings
        ProDMVector3 lightDir = new ProDMVector3(45f, 45f, 45f);
        LightSettings light = new LightSettings(intensity: lightIntensity, direction: lightDir, color: color, ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }
}

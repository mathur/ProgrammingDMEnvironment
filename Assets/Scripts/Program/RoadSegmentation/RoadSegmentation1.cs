﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadSegmentation1 : EnvironmentProgramBaseClass
{
    //Add the ProDM vars here
    VarInterval<int> cr_length;
    VarEnum<int> cr_numLanes;
    VarInterval<float> lightIntensity;
    ProDMColor color;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;
    //int cr_numLanes = 4;

    // Parameterize using two ProDMFloats
    VarInterval<float> walkingSpeed;
    VarInterval<float> euclideanDist;

    void Start() //needs to be called in the start because the setup in the RoadElementVisualization happens in the Awake()
    {
        //This button is useful to have everything set before driving begins
        //PlaceHolderForVisualizer.createPlayButton();
        //PlaceHolderForVisualizer.createRecordButton();

        fogDensity = new VarInterval<float>(val: 0, min: 0, max: 0.3f, desc: "FOG_DENSITY");


        lightIntensity = new VarInterval<float>(val: 2, min: 0, max: 10);
        color = new ProDMColor(new VarInterval<float>(val: 1f, min: 0f, max: 1f), new VarInterval<float>(val: 0.9f, min: 0f, max: 1f),
            new VarInterval<float>(val: 0.8f, min: 0f, max: 1f), 1f);
        //PlaceHolderForVisualizer.Visualize(nameof(color), color);

        ambientIntensity = new VarInterval<float>(val: 1.5f, min: 0f, max: 5f, desc: "AMBIENT_LIGHT_INTENSITY");

        //define the ProDM variables
        cr_length = new VarInterval<int>(val: 50, changeable: true, min: 20, max: 4000);

        List<int> numLanesOptions = new List<int> { 2, 4 };

        cr_numLanes = new VarEnum<int>(numLanesOptions, 0);

        walkingSpeed = new VarInterval<float>(val: 4f, min: 1f, max: 10f);
        euclideanDist = new VarInterval<float>(val: 40f, min: 20f, max: 60f);

        List<ProDM> rangedFloats = new List<ProDM>();
        rangedFloats.Add(walkingSpeed);
        rangedFloats.Add(euclideanDist);

        Test();
    }

    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {
        List<object> list = new List<object>();

        RoadSegment cRoad1 = new RoadSegment(cr_length, cr_numLanes, orientation: Orientation.VERTICAL);
        cRoad1._zone1 = Zoning.TREES;
        cRoad1._zone2 = Zoning.TREES;
        list.Add(cRoad1);


        AutonomousVehicle autonomouscar = new AutonomousVehicle(nameof(autonomouscar), cRoad1, 0.14f, LANE.PLUS_ONE, monitor: "CollisionMonitor", type: TestActorType.EGO_VEHICLE_TRAINING);
        list.Add(autonomouscar);

        AIVehicle car1 = new AIVehicle(nameof(car1), cRoad1, 0.15f, LANE.MINUS_ONE, Color.blue, model: "AICar5");
        list.Add(car1);

        AIVehicle car2 = new AIVehicle(nameof(car2), cRoad1, 0.36f, LANE.PLUS_ONE, Color.blue, model: "AICar2");
        list.Add(car2);

        AIVehicle car3 = new AIVehicle(nameof(car3), cRoad1, 0.29f, LANE.PLUS_ONE, Color.blue, model: "AICar1");
        list.Add(car3);

        AIVehicle car4 = new AIVehicle(nameof(car4), cRoad1, 0.21f, LANE.MINUS_ONE, Color.blue, model: "AICar3");
        list.Add(car4);

        AIVehicle car5 = new AIVehicle(nameof(car5), cRoad1, 0.21f, LANE.PLUS_ONE, Color.blue, model: "AICar");
        list.Add(car5);

        AIVehicle car6 = new AIVehicle(nameof(car6), cRoad1, 0.33f, LANE.MINUS_ONE, Color.blue, model: "AICar3");
        list.Add(car6);

        PlaceHolderForVisualizer.Visualize(list);
    }


    public override void MakeEnvironment()
    {
        //Environment settings
        ProDMVector3 lightDir = new ProDMVector3(45f, 45f, 45f);
        LightSettings light = new LightSettings(intensity: lightIntensity, direction: lightDir, color: color, ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }
}

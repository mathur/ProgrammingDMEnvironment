﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulatedAnnealingTemp : EnvironmentProgramBaseClass
{
    public List<ProDM> simulatedAnnealing(List<ProDM> testVars, int iterations)
    {
        //discretize continuous parameters
        List<List<float>> discretizedVals = generateHaltonSeq(testVars, iterations);

        //get neighborhoodSize
        List<float> orderedHalton = new List<float>(discretizedVals[0]);
        orderedHalton.Sort();
        float haltonNeighborhoodSize = Math.Abs(orderedHalton[1] - orderedHalton[0]);
       
        List<object> sBest = setInitialParameters(testVars, discretizedVals, 0);
        float energy_sBest = 0;
        

        for (int m = 0; m < discretizedVals[0].Count; m++)
        {
            //set initial parameters
            List<object> s = setInitialParameters(testVars, discretizedVals, m);
            setValues(s, discretizedVals, testVars);
            float energy_s = getEnergy((float)s[0]);
            
            //initialize temperature
            float T = 0;
            
            //do the simulated annealing
            for (int i = 0; i < iterations; i++)
            {
                //update temperature
                T = getNewTemperature(i, iterations);

                //pick random neighbor
                List<object> sNew = getNeighbor(testVars, haltonNeighborhoodSize, (T / iterations));

                //get energies
                //TODO: this should come from the monitor
                float energy_sNew = getEnergy((float)sNew[0]);

                //decide which one to keep
                if(probability(energy_s, energy_sNew, T) >= UnityEngine.Random.Range(0,1))
                {
                    s = sNew;
                    energy_s = energy_sNew;
                    setValues(s, discretizedVals, testVars);

                }
            }

            energy_sBest = getEnergy((float)sBest[0]);
            if (energy_s > energy_sBest)
            {
                sBest = s;
                energy_sBest = energy_s;
                setValues(sBest, discretizedVals, testVars);
            }
        }

        Debug.Log((float)sBest[0]);
        Debug.Log(getEnergy((float)sBest[0]).ToString());
        Debug.Log(getEnergy(1f));
        //return the best one
        return setValues(sBest, discretizedVals, testVars);
    }

    float getEnergy(float x)
    {
        return (float)Math.Pow(x, 4) - 4*(float)Math.Pow(x,3)- (float)Math.Pow(x, 2)+(10*x);
    }

    List<object> setInitialParameters(List<ProDM> testVars, List<List<float>> discretizedVals, int currentIndex)
    {
        //randomly initialize them
        List<object> valList = new List<object>();

        //use this to access halton sequences for continuous parameters
        int floatIndex = 0;

        //set each to a random value
        foreach (var number in testVars)
        {
            if (number._type == ProDMType.INT)
            {
                VarInterval<int> newNum = (VarInterval<int>)number;
                valList.Add(UnityEngine.Random.Range((dynamic)newNum._min, (dynamic)newNum._max));

            }
            else if (number._type == ProDMType.FLOAT)
            {
                valList.Add(discretizedVals[floatIndex][currentIndex]);

                floatIndex++;
            }
            else if (number._type == ProDMType.ONE_OF)
            {
                VarEnum<int> newNum = (VarEnum<int>)number;
                valList.Add(UnityEngine.Random.Range(0, newNum._size));
            }
            else
            {

            }
        }

        return valList;
    }

    List<object> getNeighbor(List<ProDM> testVars, float haltonNeighborhoodSize, float neighborhoodScale)
    {
        List<object> valList = new List<object>();
        
        //use this to access halton sequences for continuous parameters
        int floatIndex = 0;

        //set each to a random value
        foreach (var number in testVars)
        {
            if (number._type == ProDMType.INT)
            {
                VarInterval<int> newNum = (VarInterval<int>)number;
                int neighborhoodSize = (int)((newNum._max - newNum._min) * neighborhoodScale);
                int neighborVal = UnityEngine.Random.Range(newNum._val - (neighborhoodSize / 2), newNum._val + (neighborhoodSize / 2));
                if(neighborVal > newNum._max)
                {
                    neighborVal = newNum._max;
                }
                else if(neighborVal < newNum._min)
                {
                    neighborVal = newNum._min;
                }
                valList.Add(neighborVal);   
            }
            else if (number._type == ProDMType.FLOAT)
            {
                VarInterval<float> newNum = (VarInterval<float>)number;

                float neighborhoodSize = haltonNeighborhoodSize * neighborhoodScale;
                float neighborVal = UnityEngine.Random.Range((dynamic)(newNum._val - (neighborhoodSize / 2)), (dynamic)(newNum._val + (neighborhoodSize / 2)));
                if (neighborVal > newNum._max)
                {
                    neighborVal = newNum._max;
                }
                else if (neighborVal < newNum._min)
                {
                    neighborVal = newNum._min;
                }
                valList.Add(neighborVal);

                floatIndex++;
            }
            else if (number._type == ProDMType.ONE_OF)
            {
                VarEnum<int> newNum = (VarEnum<int>)number;
                int neighborhoodSize = (int)(newNum._size * neighborhoodScale);
                int neighborVal = UnityEngine.Random.Range(newNum._index - (neighborhoodSize / 2), newNum._index + (neighborhoodSize / 2));
                if (neighborVal > (newNum._size - 1))
                {
                    neighborVal = newNum._size - 1;
                }
                else if (neighborVal < 0)
                {
                    neighborVal = 0;
                }
                valList.Add(neighborVal);
            }
            else
            {

            }
        }

        return valList;
    }

    List<List<float>> generateHaltonSeq(List<ProDM> testVars, int iterations)
    {
        List<List<float>> rangedFloatsSeq = new List<List<float>>();

        //set each to a random value
        for (var i = 0; i < testVars.Count; i++)
        {
            if (testVars[i]._type == ProDMType.FLOAT)
            {
                VarInterval<float> newNum = (VarInterval<float>)testVars[i];

                //generate halton sequence
                // Call the halton function
                List<float> haltonSeq = new List<float>();

                for (int it = 0; it < (iterations * 10); it++) 
                    haltonSeq.Add(Halton(it + 1, 10)[i]);
                //Now scale this to the requisite min, max
                haltonSeq = ScaledList(haltonSeq, min: newNum._min, max: newNum._max);
                rangedFloatsSeq.Add(haltonSeq);
            }
        }

        return rangedFloatsSeq;
    }

    List<ProDM> setValues(List<object> valList, List<List<float>> discretizedVals, List<ProDM> testVars)
    {
        for (int n = 0; n < valList.Count; n++)
        {
            if (testVars[n]._type == ProDMType.INT)
            {
                VarInterval<int> newNum = (VarInterval<int>)testVars[n];
                newNum._val = (int)valList[n];
            }
            else if (testVars[n]._type == ProDMType.FLOAT)
            {
                VarInterval<float> newNum = (VarInterval<float>)testVars[n];
                newNum._val = (float)valList[n];
            }
            else if (testVars[n]._type == ProDMType.ONE_OF)
            {
                VarEnum<int> newNum = (VarEnum<int>)testVars[n];
                newNum._index = (int)valList[n];
            }
            else
            {

            }
        }

        return testVars;
    }

    float getNewTemperature(int k, int iterations)
    {
        return (iterations) / (k + 1);
    }

    double probability(float e1, float e2, float T)
    {
        if (e1 < e2)
        {
            return 1;
        }
        else
        {
            return Math.Exp((e2 - e1) / T);
        }
        
    }
}

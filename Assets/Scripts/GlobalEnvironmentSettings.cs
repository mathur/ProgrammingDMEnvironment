﻿//Implements functions for changing/setting global environment settings

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalEnvironmentSettings : MonoBehaviour
{
    void Start()
    {
        //SetFog();
    }

    //Fog
    public void SetFog(float density = 0.1f)
    {
        //Set exponential fog of given density 
        RenderSettings.fogDensity = density;
        RenderSettings.fogMode = FogMode.Exponential;
        RenderSettings.fog = true;
    }

    public void UnsetFog()
    {
        RenderSettings.fog = false;
    }

    //Lighting
    // Light component is attached to the game object the script is attached to

    public void SetDirectionalLightDirection(Vector3 angle)
    {
        //angle is given in Euler angles

        transform.rotation = Quaternion.Euler(angle);
    }
    public void SetDirectionalLightIntensity(float intensity = 1.0f)
    {
        //intensit=1 is normal, .01 is dark, 2 is very bright
        
        GetComponent<Light>().intensity = intensity;
    }
    public void SetDirectionalLightColor(Color color)
    {
        //Color of the directional light
        
        GetComponent<Light>().color = color;
    }


}

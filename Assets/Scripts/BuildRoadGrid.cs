﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildRoadGrid : MonoBehaviour {

    public Button buttonMakeGrid;

    public BuildRoad PlaceHolderForRoadFunction;
    public ValueController PlaceHolderForValueControllerFunction;
    public RefreshAndRecreateRoadSegment PlaceHolderForRefreshFunction;
    public IntersectionButton PlaceHolderForIntersection;

    private int numLanes;
    private List<TrafficSystemPiece> roadGrid = new List<TrafficSystemPiece>();

    private TrafficSystemPiece roadPiece;
    private string intersectionType;
    private BuildRoad.RoadAttachmentPoint buildDirection = BuildRoad.RoadAttachmentPoint.EAST;

    // Use this for initialization
    void Start()
    {
        //add listener for pressing the button
        buttonMakeGrid.onClick.AddListener(buttonPress);

    }


    //this is a hack just to display a grid. Nothing gets logically connected, only spawned visually.
    void buttonPress()
    {
        //figure out the number of lanes
        numLanes = PlaceHolderForValueControllerFunction.GetNumLanes();

        //destroy the current grid if it exists
        foreach (TrafficSystemPiece temp in roadGrid)
        {
            Destroy(temp.gameObject);
        }
        roadGrid.Clear();

        //the grid only exists for 2-6 lanes
        if (numLanes > 1)
        {
            //check what intersection type
            intersectionType = PlaceHolderForValueControllerFunction.GetIntersectionType();

            //get the row offset to build a grid. 
            //This is a hack because otherwise I don't know how much offset to add to each row of the grid
            TrafficSystemPiece spacer;
            if (intersectionType == PlaceHolderForRefreshFunction.tIntersectionPrefabs[0].m_roadPieceType.ToString())
            {
                spacer = PlaceHolderForRefreshFunction.tIntersectionPrefabDictionary[numLanes];

            }
            else if (intersectionType == PlaceHolderForRefreshFunction.xIntersectionPrefabs[0].m_roadPieceType.ToString())
            {
                spacer = PlaceHolderForRefreshFunction.xIntersectionPrefabDictionary[numLanes];
            }
            else
            {
                spacer = PlaceHolderForRefreshFunction.roundaboutPrefabDictionary[numLanes];
            }

            //build the grid with a nested for loop
            for (int j = 0; j < 5; j++)
            {
                //assign the row offset according to the spacer size
                float zOff = j * (spacer.GetRenderBounds().size.z);

                //make the first piece of the row and add it to the network. The rest of the row is based off this orientation and location.
                roadPiece = PlaceHolderForRoadFunction.spawnRoadPiece(PlaceHolderForRefreshFunction.roadPrefabDictionary[numLanes], new Vector3(0f, 0f, zOff), PlaceHolderForRefreshFunction.roadPrefabDictionary[numLanes].transform.rotation, "0 - " + PlaceHolderForRefreshFunction.roadPrefabDictionary[numLanes].name);
                roadGrid.Add(roadPiece);

                //build the rest of the row
                for (int i = 1; i < 10; i++)
                {
                    //add intersection about every other place--easily changed by adjusting the 2
                    if (IsDivisible(i, 2))
                    {
                        //create the intersection based on what intersection type and how many rows
                        if (intersectionType == PlaceHolderForRefreshFunction.tIntersectionPrefabs[0].m_roadPieceType.ToString())
                        {
                            roadPiece = PlaceHolderForRoadFunction.spawnRoadPiece(PlaceHolderForRefreshFunction.tIntersectionPrefabDictionary[numLanes], new Vector3(0f, 0f, 0f), PlaceHolderForRefreshFunction.tIntersectionPrefabDictionary[numLanes].transform.rotation, i + " - " + PlaceHolderForRefreshFunction.tIntersectionPrefabDictionary[numLanes].name);

                        }
                        else if (intersectionType == PlaceHolderForRefreshFunction.xIntersectionPrefabs[0].m_roadPieceType.ToString())
                        {
                            roadPiece = PlaceHolderForRoadFunction.spawnRoadPiece(PlaceHolderForRefreshFunction.xIntersectionPrefabDictionary[numLanes], new Vector3(0f, 0f, 0f), PlaceHolderForRefreshFunction.xIntersectionPrefabDictionary[numLanes].transform.rotation, i + " - " + PlaceHolderForRefreshFunction.xIntersectionPrefabDictionary[numLanes].name);
                        }
                        else
                        {
                            roadPiece = PlaceHolderForRoadFunction.spawnRoadPiece(PlaceHolderForRefreshFunction.roundaboutPrefabDictionary[numLanes], new Vector3(0f, 0f, 0f), PlaceHolderForRefreshFunction.roundaboutPrefabDictionary[numLanes].transform.rotation, i + " - " + PlaceHolderForRefreshFunction.roundaboutPrefabDictionary[numLanes].name);
                        }

                    }
                    //add straight segment if you don't add an intersection
                    else
                    {
                        roadPiece = PlaceHolderForRoadFunction.spawnRoadPiece(PlaceHolderForRefreshFunction.roadPrefabDictionary[numLanes], new Vector3(0f, 0f, zOff), PlaceHolderForRefreshFunction.roadPrefabDictionary[numLanes].transform.rotation, i + " - " + PlaceHolderForRefreshFunction.roadPrefabDictionary[numLanes].name);

                    }

                    //format the piece to match the first one in the row
                    PlaceHolderForRoadFunction.FormatRoadSegment(roadPiece, roadGrid[roadGrid.Count - 1], buildDirection, false);

                    //add to grid
                    roadGrid.Add(roadPiece);

                }
            }
            
        }
        else
        {
            //do nothing.
        }
    }

    private bool IsDivisible(int dividend, int divisor)
    {
        return dividend % divisor == 0;
    }

    //***DOES NOT WORK***
    //void connect(TrafficSystemPiece anchorPiece, TrafficSystemPiece connectPiece)
    //{

    //    if (roadGrid.Count > 1)
    //    {
    //        TrafficSystemPiece attachmentPoint;

    //        switch (numLanes)
    //        {
    //            //0 lane
    //            case 0:
    //                //do nothing
    //                break;

    //            //1 lane
    //            //do nothing
    //            //break;

    //            //2 lane
    //            case 2:
    //                attachmentPoint = tIntersection.gameObject.transform.GetChild(3).GetComponent<TrafficSystemPiece>();
    //                roadNetwork[roadNetwork.Count - 1].m_primaryRightLaneNodes[0].m_connectedNodes.Add(attachmentPoint.m_primaryRightLaneNodes[0]);
    //                attachmentPoint.m_primaryLeftLaneNodes[0].m_connectedNodes.Add(roadNetwork[roadNetwork.Count - 1].m_primaryLeftLaneNodes[0]);
    //                break;

    //            //4 lane
    //            case 4:
    //                attachmentPoint = tIntersection.gameObject.transform.GetChild(3).GetComponent<TrafficSystemPiece>();
    //                roadNetwork[roadNetwork.Count - 1].m_primaryRightLaneNodes[0].m_connectedNodes.Add(attachmentPoint.m_primaryLeftLaneNodes[0]);
    //                roadNetwork[roadNetwork.Count - 1].m_primaryRightLaneNodes[1].m_connectedNodes.Add(attachmentPoint.m_primaryLeftLaneNodes[1]);

    //                attachmentPoint.m_primaryRightLaneNodes[0].m_connectedNodes.Add(roadNetwork[roadNetwork.Count - 1].m_primaryLeftLaneNodes[0]);
    //                attachmentPoint.m_primaryRightLaneNodes[1].m_connectedNodes.Add(roadNetwork[roadNetwork.Count - 1].m_primaryLeftLaneNodes[1]);
    //                break;

    //            //6 lane
    //            case 6:
    //                attachmentPoint = tIntersection.gameObject.transform.GetChild(4).GetComponent<TrafficSystemPiece>();
    //                roadNetwork[roadNetwork.Count - 1].m_primaryRightLaneNodes[0].m_connectedNodes.Add(attachmentPoint.m_primaryLeftLaneNodes[0]);
    //                roadNetwork[roadNetwork.Count - 1].m_primaryRightLaneNodes[1].m_connectedNodes.Add(attachmentPoint.m_primaryLeftLaneNodes[1]);
    //                roadNetwork[roadNetwork.Count - 1].m_primaryRightLaneNodes[2].m_connectedNodes.Add(attachmentPoint.m_primaryLeftLaneNodes[2]);

    //                attachmentPoint.m_primaryRightLaneNodes[0].m_connectedNodes.Add(roadNetwork[roadNetwork.Count - 1].m_primaryLeftLaneNodes[0]);
    //                attachmentPoint.m_primaryRightLaneNodes[1].m_connectedNodes.Add(roadNetwork[roadNetwork.Count - 1].m_primaryLeftLaneNodes[1]);
    //                attachmentPoint.m_primaryRightLaneNodes[2].m_connectedNodes.Add(roadNetwork[roadNetwork.Count - 1].m_primaryLeftLaneNodes[2]);
    //                break;
    //        }
    //    }
    //}
}


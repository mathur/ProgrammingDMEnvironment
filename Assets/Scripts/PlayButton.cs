﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//This class contains the logic such that when the Play button is pressed, the road segment nodes are connected and traffic is spawned at the pre-specified locations

public class PlayButton : MonoBehaviour {

    //add the places you want the car to spawn here
    //The format is {direction, segment number, lane}
    //directions: 1 for blue lane, 0 for pink lane
    //segment number: cannot pick 0 because it is the end of the road.
    //lane: lane 0 is the curb lane
    private int[,] carSpawnLocation = new int[,] { { 1, 1, 0 }, { 0, 2, 0 }, { 2, 1, 0 }, { 1, 6, 1 }, { 2, 2, 1 }, { 1, 6, 0 }, { 2, 8, 1 } };

    public RefreshAndRecreateRoadSegment PlaceHolderForRefreshFunction;
    public ValueController PlaceHolderForValueControllerFunction;
    public IntersectionButton PlaceHolderForIntersection;
    public TrafficSystemVehicle car;
    public Button buttonPlay;
    private List<TrafficSystemPiece> roadNetwork;
    

	// Use this for initialization
	void Start () {
        //add the handler to make the button click work
        buttonPlay.onClick.AddListener(connectNodes);

    }
	
    //makes the logical connection in the road network when the play button is pressed.
	void connectNodes () {
        //get the road network
        roadNetwork = PlaceHolderForRefreshFunction.GetRoadNetwork();

        //get the length of the road
        int lengthOfStrightRoad = roadNetwork.Count - 1;
        //if there is an intersection, you have to connect it differently, so the length of the straight road is shorter
        if (PlaceHolderForRefreshFunction.includeIntersection)
        {
            lengthOfStrightRoad--;
        }

        //loop through the straight road segments to logically connect them.
        for (int i = 0; i < lengthOfStrightRoad; i++)
        {
            roadNetwork[i].PerformCleanUp();

            //connect road nodes so that traffic flows in opposite direction
            int totalLanes = roadNetwork[i].m_primaryLeftLaneNodes.Count + roadNetwork[i].m_primaryRightLaneNodes.Count;

            switch (totalLanes)
            {
                //0 lane
                case 0:
                    //do nothing
                    break;

                //1 lane
                case 1:
                    //add the connection in the appropriate direction for the case of a one lane road
                    if (roadNetwork[i].m_primaryRightLaneNodes.Count > 0)
                    {
                        roadNetwork[i].m_primaryRightLaneNodes[0].m_connectedNodes.Add(roadNetwork[i + 1].m_primaryRightLaneNodes[0]);
                    }
                    else
                    {
                        roadNetwork[i + 1].m_primaryLeftLaneNodes[0].m_connectedNodes.Add(roadNetwork[i].m_primaryLeftLaneNodes[0]);
                    }
                    
                    break;

                //2 lane
                case 2:
                    //connect the nodes (blue (left) to blue and pink (right) to pink)
                    roadNetwork[i].m_primaryRightLaneNodes[0].m_connectedNodes.Add(roadNetwork[i + 1].m_primaryRightLaneNodes[0]);
                    roadNetwork[i + 1].m_primaryLeftLaneNodes[0].m_connectedNodes.Add(roadNetwork[i].m_primaryLeftLaneNodes[0]);
                    break;

                //4 lane
                case 4:
                    //connect the nodes (blue (left) to blue and pink (right) to pink)
                    roadNetwork[i].m_primaryRightLaneNodes[0].m_connectedNodes.Add(roadNetwork[i + 1].m_primaryRightLaneNodes[0]);
                    roadNetwork[i].m_primaryRightLaneNodes[1].m_connectedNodes.Add(roadNetwork[i + 1].m_primaryRightLaneNodes[1]);
                    //roadNetwork[i].m_primaryRightLaneNodes[0].m_connectedNodes.Add(roadNetwork[i + 1].m_primaryRightLaneNodes[1]);
                    //roadNetwork[i].m_primaryRightLaneNodes[1].m_connectedNodes.Add(roadNetwork[i + 1].m_primaryRightLaneNodes[0]);

                    roadNetwork[i + 1].m_primaryLeftLaneNodes[0].m_connectedNodes.Add(roadNetwork[i].m_primaryLeftLaneNodes[0]);
                    roadNetwork[i + 1].m_primaryLeftLaneNodes[1].m_connectedNodes.Add(roadNetwork[i].m_primaryLeftLaneNodes[1]);
                    //roadNetwork[i + 1].m_primaryLeftLaneNodes[1].m_connectedNodes.Add(roadNetwork[i].m_primaryLeftLaneNodes[0]);
                    //roadNetwork[i + 1].m_primaryLeftLaneNodes[0].m_connectedNodes.Add(roadNetwork[i].m_primaryLeftLaneNodes[1]);
                    break;

                //6 lane
                case 6:
                    //connect the nodes (blue (left) to blue and pink (right) to pink)
                    roadNetwork[i].m_primaryRightLaneNodes[0].m_connectedNodes.Add(roadNetwork[i + 1].m_primaryRightLaneNodes[0]);
                    roadNetwork[i].m_primaryRightLaneNodes[1].m_connectedNodes.Add(roadNetwork[i + 1].m_primaryRightLaneNodes[1]);
                    roadNetwork[i].m_primaryRightLaneNodes[2].m_connectedNodes.Add(roadNetwork[i + 1].m_primaryRightLaneNodes[2]);
                    //roadNetwork[i].m_primaryRightLaneNodes[0].m_connectedNodes.Add(roadNetwork[i + 1].m_primaryRightLaneNodes[1]);
                    //roadNetwork[i].m_primaryRightLaneNodes[1].m_connectedNodes.Add(roadNetwork[i + 1].m_primaryRightLaneNodes[0]);
                    //roadNetwork[i].m_primaryRightLaneNodes[1].m_connectedNodes.Add(roadNetwork[i + 1].m_primaryRightLaneNodes[2]);
                    //roadNetwork[i].m_primaryRightLaneNodes[2].m_connectedNodes.Add(roadNetwork[i + 1].m_primaryRightLaneNodes[1]);

                    roadNetwork[i + 1].m_primaryLeftLaneNodes[0].m_connectedNodes.Add(roadNetwork[i].m_primaryLeftLaneNodes[0]);
                    roadNetwork[i + 1].m_primaryLeftLaneNodes[1].m_connectedNodes.Add(roadNetwork[i].m_primaryLeftLaneNodes[1]);
                    roadNetwork[i + 1].m_primaryLeftLaneNodes[2].m_connectedNodes.Add(roadNetwork[i].m_primaryLeftLaneNodes[2]);
                    //roadNetwork[i + 1].m_primaryLeftLaneNodes[0].m_connectedNodes.Add(roadNetwork[i].m_primaryLeftLaneNodes[1]);
                    //roadNetwork[i + 1].m_primaryLeftLaneNodes[1].m_connectedNodes.Add(roadNetwork[i].m_primaryLeftLaneNodes[0]);
                    //roadNetwork[i + 1].m_primaryLeftLaneNodes[1].m_connectedNodes.Add(roadNetwork[i].m_primaryLeftLaneNodes[2]);
                    //roadNetwork[i + 1].m_primaryLeftLaneNodes[2].m_connectedNodes.Add(roadNetwork[i].m_primaryLeftLaneNodes[1]);
                    break;
            }

        }

        //if there is an intersection, connect it to the network logically
        if (PlaceHolderForRefreshFunction.includeIntersection && PlaceHolderForValueControllerFunction.GetNumLanes() > 1)
        {
            PlaceHolderForIntersection.connectIntersection();
        }

        //Spawn traffic
        TrafficSystem.Instance.m_randomVehicleSpawnChancePerNode = 1;
        addTraffic();
        

    }

    //spawns the traffic at the preset locations
    void addTraffic()
    {
        //loop through each car spawn location
        for (int i = 0; i < carSpawnLocation.GetLength(0); i++)
        {
            int direction = carSpawnLocation[i, 0];
            int piece = carSpawnLocation[i, 1];
            int lane = carSpawnLocation[i, 2];

            //make sure the road segment exists
            if (piece < (roadNetwork.Count))
            {
                //check what direction to go
                if (direction == 1)
                {
                    //make sure the lane actually exists
                    if (lane < (roadNetwork[piece].m_primaryLeftLaneNodes.Count))
                    {
                        //spawn the vehicle
                        SpawnVehicle(roadNetwork[piece].m_primaryLeftLaneNodes[lane], car);
                    }
                }
                else
                {
                    //make sure lane actually exists
                    if (lane < (roadNetwork[piece].m_primaryRightLaneNodes.Count))
                    {
                        //spawn the vehicle
                        SpawnVehicle(roadNetwork[piece].m_primaryRightLaneNodes[lane], car);
                    }
                }
            }
        }
    }


    //this function spawns the vehicles. I'm borrowing it from the TrafficRoadSystemEditor scripts
    void SpawnVehicle(TrafficSystemNode a_location, TrafficSystemVehicle a_vehiclePrefab)
    {
        //a_vehiclePrefab = Resources.Load("Traffic System Vechile - Full Test Car") as TrafficSystemVehicle;
        if (!TrafficSystem.Instance)
            return;

        TrafficSystemNode node = a_location;

        if (a_location)
        {
            Vector3 pos = node.transform.position;
            pos -= a_vehiclePrefab.m_offsetPosVal;

            TrafficSystemVehicle vehicle = Instantiate(a_vehiclePrefab, pos, node.transform.rotation) as TrafficSystemVehicle;
            vehicle.m_nextNode = node;
            vehicle.m_velocityMax = Random.Range(TrafficSystem.Instance.m_randVehicleVelocityMin, TrafficSystem.Instance.m_randVehicleVelocityMax);

            TrafficSystemNode nextNode = node.GetNextNode(vehicle, false);
            if (nextNode)
                vehicle.transform.forward = nextNode.transform.position - vehicle.transform.position;

        }
    }
}

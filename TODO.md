# TODOs

- Invariant checks (number of lanes equal between connectors, no overlap between elements, etc.).
- ~~Fix programming interface to also instantiate autonomous cars (currently, these are only moved at run-time).~~
- ~~Capability of pausing simulation.~~
- (stretch goal) Implement other haphazard driving behaviors such as erratic velocity. 
- Proper specification & implementation of zoning (3D objects seen on sides of the road). Also, possibly have better suited and realistic 3D models.
- ~~Index lanes using +ve and -ve numbers that encode opposite directions. ~~
- Fix gapping issues in between road segments and intersections.
- Cleaner programming interface for setting environment, actors and tests up.
- Test various perception+controller systems for autonomous cars inside our environment.
- (minor) Increase size of colliders on either side of the road.
- (minor) Composite ProDM component sliders need to be aligned horizontally.
- ~~(minor) Fix distracting UI elements and shortcuts in the Autonoumous driving and training scenes.~~
- ~~(minor) Fix camera view selection issues.~~
- (stretch goal) Ensure camera focus on relevant things in bird's-eye view.
- ~~track position of autonomous vehicle and export data to csv to review/compare paths~~

# KNOWN ISSUES

- The box colliders are not functioning properly unless they are disabled then enabled. They will not work, then randomly the car will hit one of these box colliders, even though nothing is actually there.
- The trees and residences spawn randomly, so when the view is refreshed, which tree and which residence that spawns will often change.
- Some road segments have this gap issue where there is a small space between the pieces when the scene is rendered, despite the pre-fab pieces not having this gap.